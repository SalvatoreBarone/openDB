/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "AllArcGenerator.hpp"
#include "MainWindow.hpp"
#include "verifyMacro.hpp"

#include <QtGui>
#include <QFileDialog>
#include <QProgressBar>

#include <unordered_map>

#include "core/Database.hpp"
#include "core/Schema.hpp"
#include "core/Table.hpp"
#include "core/Storage.hpp"
#include "core/BackendConnector.hpp"
#include "core/NonTransactional.hpp"

const std::string AllArcGenerator::schemaName = "public";
const std::string AllArcGenerator::tableName = "tracciati";
const std::string AllArcGenerator::fileNameColumn = "file_name";
const std::string AllArcGenerator::sqlCommandColumn = "sql_code";
const std::string AllArcGenerator::sqlQuery = "select file_name, sql_code from tracciati";
const struct openDB::CsvUpdateManager::Setting AllArcGenerator::exportParameter('|', false, '"', false);

AllArcGeneratorInterface::AllArcGeneratorInterface(MainWindow* window, QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		labelDestDirectory(new QLabel("Destinazione", this)),
		editDestDirectory(new QLineEdit(this)),
		buttonDestDirectory(new QPushButton("Apri", this)),
		buttonCreate(new QPushButton("Crea", this)),
		progressBar(new QProgressBar(this)),
		layoutButton(new QHBoxLayout()),
		mainLayout(new QGridLayout(this)),
		worker_thread(0),
		generator(0)
{
	setLayout(mainLayout);

	mainLayout->addWidget(labelDestDirectory, 0, 0, 1, 1);
	mainLayout->addWidget(editDestDirectory, 0, 1, 1, 1);
	mainLayout->addWidget(buttonDestDirectory, 0, 2, 1, 1);

	layoutButton->addWidget(buttonCreate);
	mainLayout->addLayout(layoutButton, 1, 0, 1, 3, Qt::AlignCenter | Qt::AlignBottom);
	mainLayout->addWidget(progressBar, 2, 0, 1, 3);

	connect(buttonDestDirectory, SIGNAL(clicked()), this, SLOT(slotChooseDestDirectory()));
	connect(buttonCreate, SIGNAL(clicked()), this, SLOT(slotCreate()));
}

AllArcGeneratorInterface::~AllArcGeneratorInterface()
{
	delete labelDestDirectory;
	delete editDestDirectory;
	delete buttonDestDirectory;
	delete buttonCreate;
	delete progressBar;
	delete layoutButton;
	delete mainLayout;
}

bool AllArcGeneratorInterface::verificaCampi() {verifyField(editDestDirectory, "Scegli una directory di destinazione")}

void AllArcGeneratorInterface::slotChooseDestDirectory()
{
	QString fileName = QFileDialog::getExistingDirectory(this, "Directory immagini", QDir::currentPath());
	if ( !fileName.isEmpty() )
		editDestDirectory->setText(fileName + "/");
}

void AllArcGeneratorInterface::slotCreate()
{
	if (verificaCampi())
	{
		if (generator == 0 && worker_thread == 0)
		{
			openDB::BackendConnector* connector = &(parentWindow->backendConnector());
			std::string destDirectory = editDestDirectory->text().toStdString();

			generator = new AllArcGenerator(destDirectory, connector);
			worker_thread = new QThread;
			generator->moveToThread(worker_thread);

			connect (generator, SIGNAL(total(unsigned int)), this, SLOT(slotSetMaximum(unsigned int)));
			connect (generator, SIGNAL(incProgress()), this, SLOT(slotIncProgress()));
			connect (worker_thread, SIGNAL(started()), generator, SLOT(start()));
			connect (generator, SIGNAL(done()), this, SLOT(slotDone()));

			worker_thread->start();
		}
		else
			QMessageBox::warning(this, "Attenzione!", "Il processo di generazione e' ancora in corso! Attendi o crea una nuova sessione.", QMessageBox::Ok);
	}
}

void AllArcGeneratorInterface::slotSetMaximum(unsigned int i)
{
	progressBar->setMaximum(i);
	progressBar->setValue(0);
}

void AllArcGeneratorInterface::slotIncProgress()
{
	progressBar->setValue(progressBar->value() + 1);
}

void AllArcGeneratorInterface::slotDone()
{
	worker_thread->quit();
	worker_thread->wait();
	delete worker_thread;
	delete generator;
	worker_thread = 0;
	generator = 0;
}

AllArcGenerator::AllArcGenerator(const std::string& destDir, openDB::BackendConnector* conn) :
	QObject(),
	destinationDir(destDir),
	backendConn(conn)
{
}

AllArcGenerator::~AllArcGenerator()
{
}

void AllArcGenerator::start()
{
	openDB::NonTransactional tuttiTracciati;
	tuttiTracciati.appendCommand(sqlQuery);
	openDB::BackendConnector::Iterator tracciati_it = backendConn->execTransaction(tuttiTracciati, "tracciati");


	openDB::Table& tableTracciati = tracciati_it.result().resultTable();

	emit total(tableTracciati.storage().records() - 1);

	openDB::Storage::Iterator storage_it = tableTracciati.storage().begin(), storage_end = tableTracciati.storage().end();

	for(; storage_it != storage_end; storage_it++)
	{
		openDB::Record record = *storage_it;
		std::unordered_map<std::string, std::string> values;
		record.current(values);

		openDB::NonTransactional getTracciato;
		getTracciato.appendCommand(values[sqlCommandColumn]);
		openDB::BackendConnector::Iterator tracciato_it = backendConn->execTransaction(getTracciato, "tracciato");
		openDB::Table& result = tracciato_it.result().resultTable();

		openDB::CsvUpdateManager expmanager(result);
		expmanager.csvExport(destinationDir + values[fileNameColumn], exportParameter);

		backendConn->deleteTransaction(tracciato_it);

		emit incProgress();
	}

	backendConn->deleteTransaction(tracciati_it);
	emit done();
}


