/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __ALLARCGENERATOR_H
#define __ALLARCGENERATOR_H

#include <QObject>
#include <QWidget>
#include <QThread>

#include <string>

#include "core/CsvUpdateManager.hpp"

class QLabel;
class QLineEdit;
class QPushButton;
class QProgressBar;
class QHBoxLayout;
class QGridLayout;

class AllArcGenerator;
class MainWindow;

class AllArcGeneratorInterface : public QWidget
{
	Q_OBJECT
public:
	explicit AllArcGeneratorInterface(MainWindow* window, QWidget* parent);

	virtual ~AllArcGeneratorInterface();

private:
	MainWindow* parentWindow;

	QLabel* labelDestDirectory;
	QLineEdit* editDestDirectory;
	QPushButton* buttonDestDirectory;
	QPushButton* buttonCreate;
	QProgressBar* progressBar;
	QHBoxLayout* layoutButton;
	QGridLayout* mainLayout;

	QThread* worker_thread;
	AllArcGenerator* generator;

	bool verificaCampi();

private slots:
	void slotChooseDestDirectory();
	void slotCreate();

	void slotSetMaximum(unsigned int i);
	void slotIncProgress();
	void slotDone();
};


namespace openDB
{
	class BackendConnector;
};

class AllArcGenerator : public QObject
{
	Q_OBJECT
public:
	explicit AllArcGenerator(const std::string& destDir, openDB::BackendConnector* conn);

	virtual ~AllArcGenerator();

signals:
	void total(unsigned int n);

	void incProgress();

	void done();

public slots:
	void start();

private:
	std::string destinationDir;
	openDB::BackendConnector* backendConn;

	static const std::string schemaName;
	static const std::string tableName;
	static const std::string fileNameColumn;
	static const std::string sqlCommandColumn;
	static const std::string sqlQuery; // non usa la tabella del database, in modo da non apportare modifiche
	static const struct openDB::CsvUpdateManager::Setting exportParameter;


};

#endif
