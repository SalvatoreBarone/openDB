/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "AlldecorImgTest.hpp"
#include "MainWindow.hpp"

#include <unordered_map>
#include <algorithm>

#include "core/common.hpp"
#include "core/NonTransactional.hpp"
#include "core/BackendConnector.hpp"
#include "core/Table.hpp"
#include "core/CsvUpdateManager.hpp"

#include <QtGui>
#include <QString>
#include <QProgressDialog>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

const std::string AlldecorImgTest::getCodesSqlQuery = "\
select \
aa.codice as \"CODICE\", \
aa.descrizione as \"DESCRIZIONE\" \
from \
	anagrafica_articoli aa \
	join \
	pubblicazione_articoli pa \
	on aa.codice=pa.codice_art \
where \
	pa.sito='www.alldecor.it' \
	and \
	aa.listino3 is not null \
order by pa.codice_art\
";
const std::string AlldecorImgTest::codesColName = "CODICE";
const std::string AlldecorImgTest::fstImgUrl = "http://www.alldecor.it/apollo/foto_prodotti/small/$CODICE_1.jpg";
const std::string AlldecorImgTest::sndImgUrl = "http://www.alldecor.it/apollo/foto_prodotti/$CODICE_2.jpg";

AlldecorImgTest::AlldecorImgTest(MainWindow* window, QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		netManager(new QNetworkAccessManager),
		textBrowser(new QTextBrowser(this)),
		progressBar(new QProgressBar(this)),
		layout(new QVBoxLayout(this))
{
	setLayout(layout);
	layout->addWidget(textBrowser);
	layout->addWidget(progressBar);
	connect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(updateRecordStatus(QNetworkReply*)));

	//esecuzione della query per ottenere i codici degli articoli pubblicati su alldecor
	openDB::NonTransactional query;
	query.appendCommand(getCodesSqlQuery);
	result_it = parentWindow->backendConnector().execTransaction(query);
	openDB::Table& _resultTable = result_it.result().resultTable();

	progressBar->setMinimum(0);
	// progressBar->setMaximum((2 * _resultTable.storage().records()) - 1);
	progressBar->setMaximum( _resultTable.storage().records() - 1);

	openDB::Storage::Iterator storage_it = _resultTable.storage().begin(), storage_end = _resultTable.storage().end();
	for (; storage_it != storage_end; storage_it++)
	{
		std::unordered_map<std::string, std::string> parMap;
		(*storage_it).current(parMap);

		std::string fst = fstImgUrl;

//		std::string snd = sndImgUrl;
		openDB::prepare(fst, parMap);
		std::transform(fst.begin(), fst.end(), fst.begin(), ::tolower);
//		openDB::prepare(snd, parMap);

		QUrl fstUrl = QString::fromStdString(fst);
//		QUrl sndUrl = QString::fromStdString(snd);

		QNetworkReply* reply1 = netManager->head(QNetworkRequest(fstUrl));
//		QNetworkReply* reply2 = netManager->head(QNetworkRequest(sndUrl));

		testMap.insert(std::pair<QNetworkReply*, openDB::Storage::Iterator> (reply1, storage_it));
//		testMap.insert(std::pair<QNetworkReply*, openDB::Storage::Iterator> (reply2, storage_it));
	}

}

AlldecorImgTest::~AlldecorImgTest()
{
	delete netManager;
	delete textBrowser;
	delete progressBar;
	delete layout;

	auto it = testMap.begin(), end = testMap.end();
	for (; it != end; it++)
		delete it->first;
	parentWindow->backendConnector().deleteTransaction(result_it);
}

void AlldecorImgTest::updateRecordStatus(QNetworkReply* reply)
{
	openDB::Storage::Iterator it = testMap[reply];

	/* Se head non restituisce http_OK (200) allora il link non e' valido.
	 * Se il link e' valido il record corretto viene eliminato dall'elenco.
	 */
	if ( reply->attribute ( QNetworkRequest::HttpStatusCodeAttribute ).toInt() == http_OK)
	{
		textBrowser->append(QString::fromStdString((*it).current(codesColName)) + "\turl: " + reply->url().path() + "\tok");
		result_it.result().resultTable().storage().eraseRecord(it);
	}
	else
		textBrowser->append("<b>" + QString::fromStdString((*it).current(codesColName)) + "\turl: " + reply->url().path() + "\turl error</b>");

	/* Viene predisposta l'eliminazione di reply
	 */
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator>::iterator mit = testMap.find(reply);
	testMap.erase(mit);
	reply->deleteLater();

	progressBar_mutex.lock();
		progressBar->setValue(progressBar->value() + 1);
	progressBar_mutex.unlock();

	if (progressBar->value() == progressBar->maximum())
		eraseGoodUrl();
}

void AlldecorImgTest::eraseGoodUrl()
{
	/* Se un oggetto Storage::Iterator non viene trovato in testMap vieen eliminato dal result
	 */
	openDB::Table& _resultTable = result_it.result().resultTable();

	progressBar->setMinimum(0);
	progressBar->setMaximum((2 * _resultTable.storage().records()) - 1);

	openDB::Storage::Iterator	storage_it = _resultTable.storage().begin(),
					storage_end = _resultTable.storage().end();
	for (; storage_it != storage_end; storage_it++)
		if (!findIterator(storage_it))
			_resultTable.storage().deleteRecord(storage_it);
	/* Il result viene, poi, esportato su file csv
	 */
	QString fileName =  QFileDialog::getSaveFileName (	this,
								"Export in formato csv",
								QDir::currentPath() + "/prodotti_senza_immagine.csv",
								"csv file (*.csv)" );
	if ( !fileName.isEmpty() )
	{
		openDB::CsvUpdateManager::Setting par;
		openDB::CsvUpdateManager manager(_resultTable);
		manager.csvExport(fileName.toStdString(), par);
	}
}

bool AlldecorImgTest::findIterator(openDB::Storage::Iterator& sit)
{
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator>::iterator it = testMap.begin(), end = testMap.end();
	while (it != end)
		if (it->second == sit)
			return true;
		else it ++;
	return false;
}
