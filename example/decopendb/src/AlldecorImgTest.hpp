/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __ALLDECOR_IMG_TEST_H
#define __ALLDECOR_IMG_TEST_H

#include <string>
#include <mutex>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

#include <QWidget>

class MainWindow;
class QNetworkAccessManager;
class QNetworkReply;
class QTextBrowser;
class QProgressBar;
class QVBoxLayout;

class AlldecorImgTest : public QWidget
{
	Q_OBJECT
public:
	explicit AlldecorImgTest(MainWindow* window, QWidget* parent);

	virtual ~AlldecorImgTest();

private slots:
	void updateRecordStatus(QNetworkReply* reply);
private:
	MainWindow* parentWindow;

	QNetworkAccessManager* netManager;
	QTextBrowser* textBrowser;
	QProgressBar* progressBar;
	QVBoxLayout* layout;

	/* - getCodeSqlQuery conserva la query sql per ottenere i codici degli articoli pubblicati su alldecor;
	 * - fstImgUrl conserva l'url parametrico della prima immagine di un articolo pubblicato su alldecor
	 * - sndImgUrl conserva l'url parametrico della seconda immagine di un articolo pubblicato su alldecor;
	 * - result_it conserva l'iteratore al result generato dall'esecuzione della query memorizzata in getCodesSqlQuery
	 */
	openDB::BackendConnector::Iterator result_it;
	static const std::string getCodesSqlQuery;
	static const std::string codesColName;
	static const std::string fstImgUrl;
	static const std::string sndImgUrl;

	/* httpStatusCode definisce i codici identificativi delle risposte ai comandi Head o Get previste dal protocollo HTTP  */
	enum httpStatusCode
	{
		http_OK = 200,
		http_unauthorized = 401,
		http_forbidden = 403,
		http_notFound = 404,
		http_serverError = 500
	};

	/* Il seguente mutex viene utilizzato per l'accesso in mutua esclusione all'aggiornamento della progressBar */
	std::mutex progressBar_mutex;

	/* La mappa testMap conserva le corrispondenze QNetworkReply-Storage::Iterator ossia le corrispondenze tra le risposte di QNetworkManager
	 * relative al test generato a partire dalle informazioni contenute in un record puntato da un'istanza di un oggetto Storage::Iterator.
	 * La mappa viene utilizzata dalla funzione test ogni volta che QNetworkManager completa il prelievo della risorsa associata ad un particolare
	 * url per risalire al record che l'ha generata.
	 */
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator> testMap;

	void eraseGoodUrl();
	bool findIterator(openDB::Storage::Iterator& it);

};

#endif
