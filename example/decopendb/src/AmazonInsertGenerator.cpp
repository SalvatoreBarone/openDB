/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "AmazonInsertGenerator.hpp"
#include "verifyMacro.hpp"

#include <QtGui>
#include <QThread>
#include <QProgressDialog>
#include <QMessageBox>

#include "MainWindow.hpp"


const std::string AmazonInsertGenerator::codeColumnName = "part_number";

AmazonInsertGenerator::AmazonInsertGenerator(	MainWindow* window,
						openDB::Table* _table,
						QWidget* parent) :
	QWidget(parent),
	parentWindow(window),
	table(_table),
	labelFileName(new QLabel("Salva su", this)),
	editFileName(new QLineEdit(this)),
	buttonChooseFile(new QPushButton("Salva", this)),
	buttonGenera(new QPushButton("Genera Template", this)),
	buttonLayout(new QHBoxLayout),
	mainLayout(new QGridLayout(this)),
	generator_thread(0),
	generator(0),
	progDialog(0)
{
	setLayout(mainLayout);
	buttonGenera->setFixedSize(buttonGenera->sizeHint());
	mainLayout->addWidget(labelFileName, 0, 0, 1, 1);
	mainLayout->addWidget(editFileName, 0, 1, 1, 1);
	mainLayout->addWidget(buttonChooseFile, 0, 2, 1, 1);
	buttonLayout->addWidget(buttonGenera);
	mainLayout->addLayout(buttonLayout, 1, 0, 1, 3, Qt::AlignCenter | Qt::AlignBottom);

	connect(buttonChooseFile, SIGNAL(clicked()), this, SLOT(slotChooseFile()));
	connect(buttonGenera, SIGNAL(clicked()), this, SLOT(slotMakeTemplate()));
}

AmazonInsertGenerator::~AmazonInsertGenerator()
{
	delete labelFileName;
	delete editFileName;
	delete buttonChooseFile;
	delete buttonGenera;
	delete buttonLayout;
	delete mainLayout;

	if (generator_thread)
	{
		generator_thread->quit();
		generator_thread->wait();
		delete generator;
		delete generator_thread;
	}

	if (progDialog)
		delete progDialog;
}


void AmazonInsertGenerator::slotChooseFile() {chooseSaveFile(parentWindow, editFileName, "Salva su file", "*.csv")}

void AmazonInsertGenerator::slotMakeTemplate()
{
	if (!editFileName->text().isEmpty())
	{
		if (!generator)
		{
			std::string codesList;
			auto it = table->storage().begin(), begin = table->storage().begin(), end = table->storage().end();
			for (; it != end; it++)
			{
				if (it != begin)
					codesList += ", ";
				codesList += "'" + (*it).current(codeColumnName) + "'";
			}

			generator_thread = new QThread;
			generator = new InsertTemplateGenerator( &parentWindow->backendConnector(),
								codesList,
								editFileName->text().toStdString());

			connect(generator_thread, SIGNAL(started()), generator, SLOT(start()));
			connect(generator, SIGNAL(done()), this, SLOT(slotDone()));
			generator->moveToThread(generator_thread);
			generator_thread->start();

			progDialog = new QProgressDialog("Caricamento", "Annulla", 0, 0, this);
			connect(progDialog, SIGNAL(canceled()), this, SLOT(slotAbort()));
			progDialog->show();
		}
		else
			QMessageBox::warning(	this,
						"Attenzione!",
						"Il processo di generazione e' gia' in esecuzione.",
						QMessageBox::Ok);
	}
	else
		QMessageBox::warning(	this,
					"Attenzione!",
					"Imposta un file di destinazione.",
					QMessageBox::Ok);

}

void AmazonInsertGenerator::slotDone()
{
	generator_thread->quit();
	generator_thread->wait();
	delete generator;
	delete generator_thread;
	generator = 0;
	generator_thread = 0;
	delete progDialog;
	progDialog = 0;
	QMessageBox::information(this,
				"Generazione completata!",
				"Il template e' stato salvato sul file " + editFileName->text(),
				QMessageBox::Ok);
}

void AmazonInsertGenerator::slotAbort()
{
	generator_thread->quit();
	generator_thread->wait();
	delete generator;
	delete generator_thread;
	generator = 0;
	generator_thread = 0;
	delete progDialog;
	progDialog = 0;
	QMessageBox::information(this,
				"Generazione interrotta!",
				"La generazione del template e' stata interrotta dall'utente.",
				QMessageBox::Ok);
}

const std::string InsertTemplateGenerator::templateGeneratorQuery = "\
select \
amz.item_sku, \
(case when length(aa.codice_alt) >= 8 then aa.codice_alt else '' end) as \"external_product_id\", \
(case when length(aa.codice_alt) >= 8 then 'EAN' else '' end) as \"external_product_id_type\", \
amz.item_name, \
amz.brand_name, \
amz.manufacturer, \
amz.product_description as \"product_description\", \
'FurnitureAndDecor'  as \"feed_product_type\", \
'' as \"update_delete\", \
amz.part_number, \
'' as \"missing_keyset_reason\", \
amz.standard_price, \
'EUR' as \"currency\", \
case when gm.giacenza is null then '1000' else gm.giacenza end as \"quantity\", \
'Nuovo' as \"contidion_type\", 'Oggetto nuovo, nella confezione originale' as \"condition_note\", '' as \"item_package_quantity\", '' as \"product_site_launch_date\", '' as \"merchant_release_date\", '' as \"fulfillment_latency\", '' as \"restock_date\", '' as \"max_aggregate_ship_quantity\", '' as \"offering_can_be_gift_messaged\", '' as \"offering_can_be_giftwrapped\", '' as \"is_discontinued_by_manufacturer\", '' as \"sale_price\", '' as \"sale_end_date\", '' as \"sale_from_date\", '' as \"item_display_weight\", '' as \"item_display_weight_unit_of_measure\", '' as \"item_display_volume\", '' as \"item_display_volume_unit_of_measure\", '' as \"item_display_length\", '' as \"item_display_length_unit_of_measure\", '' as \"website_shipping_weight\", '' as \"website_shipping_weight_unit_of_measure\", '' as \"item_length\", '' as \"item_height\", '' as \"item_weight\", '' as \"item_weight_unit_of_measure\", '' as \"item_width\", '' as \"item_dimensions_unit_of_measure\", \
amz.bullet_point1, \
amz.bullet_point2, \
amz.bullet_point3, \
amz.bullet_point4, \
amz.bullet_point5, \
amz.recommended_browse_nodes1, \
amz.recommended_browse_nodes2, \
'' as \"catalog_number\", \
amz.generic_keywords1, \
amz.generic_keywords2, \
amz.generic_keywords3, \
amz.generic_keywords4, \
amz.generic_keywords5, \
amz.main_image_url, \
amz.other_image_url1, \
amz.other_image_url2,  \
amz.other_image_url3, \
'' as \"other_image_url4\", '' as \"other_image_url5\", '' as \"other_image_url6\", '' as \"other_image_url7\", '' as \"other_image_url8\", '' as \"swatch_image_url\", '' as \"fulfillment_center_id\", \
amz.parent_child, \
amz.parent_sku, \
amz.relationship_type, \
amz.variation_theme, \
'' as \"eu_toys_safety_directive_age_warning\", '' as \"eu_toys_safety_directive_warning1\", '' as \"eu_toys_safety_directive_warning2\", '' as \"eu_toys_safety_directive_warning3\", '' as \"eu_toys_safety_directive_warning4\", '' as \"eu_toys_safety_directive_warning5\", '' as \"eu_toys_safety_directive_warning6\", '' as \"eu_toys_safety_directive_warning7\", '' as \"eu_toys_safety_directive_warning8\", '' as \"eu_toys_safety_directive_language1\", '' as \"eu_toys_safety_directive_language2\", '' as \"eu_toys_safety_directive_language3\", '' as \"eu_toys_safety_directive_language4\", '' as \"eu_toys_safety_directive_language5\", '' as \"eu_toys_safety_directive_language6\", '' as \"eu_toys_safety_directive_language7\", '' as \"eu_toys_safety_directive_language8\", '' as \"legal_disclaimer_description\", '' as \"safety_warning\", '' as \"country_string\", '' as \"power_source_type\", '' as \"battery_average_life\", '' as \"battery_average_life_standby\", '' as \"battery_charge_time\", '' as \"lithium_battery_energy_content\", '' as \"lithium_battery_packaging\", '' as \"lithium_battery_voltage\", '' as \"lithium_battery_weight\", '' as \"number_of_lithium_ion_cells\", '' as \"number_of_lithium_metal_cells\", '' as \"seller_warranty_description\", '' as \"number_of_pieces\", '' as \"scent_name\", '' as \"thread_count\", '' as \"number_of_sets\", '' as \"material_type\", '' as \"is_stain_resistant\", '' as \"wattage\", '' as \"are_batteries_included\", '' as \"batteries_required\", '' as \"battery_type1\", '' as \"battery_type2\", '' as \"battery_type3\", '' as \"number_of_batteries1\", '' as \"number_of_batteries2\", '' as \"number_of_batteries3\", \
amz.size_name, \
'' as \"size_map\", \
amz.color_name, \
'' as \"color_map\", '' as \"sunlight_exposure\", '' as \"moisture_needs\", '' as \"usda_hardiness_zone1\", '' as \"usda_hardiness_zone2\", '' as \"usda_hardiness_zone3\", '' as \"usda_hardiness_zone4\", '' as \"usda_hardiness_zone5\", '' as \"usda_hardiness_zone6\", '' as \"usda_hardiness_zone7\", '' as \"usda_hardiness_zone8\", '' as \"usda_hardiness_zone9\", '' as \"usda_hardiness_zone10\", '' as \"usda_hardiness_zone11\", '' as \"sunset_climate_zone1\", '' as \"sunset_climate_zone2\", '' as \"sunset_climate_zone3\", '' as \"sunset_climate_zone4\", '' as \"sunset_climate_zone5\", '' as \"sunset_climate_zone6\", '' as \"sunset_climate_zone7\", '' as \"sunset_climate_zone8\", '' as \"sunset_climate_zone9\", '' as \"sunset_climate_zone10\", '' as \"sunset_climate_zone11\", '' as \"sunset_climate_zone12\", '' as \"sunset_climate_zone13\", '' as \"sunset_climate_zone14\", '' as \"sunset_climate_zone15\", '' as \"sunset_climate_zone16\", '' as \"sunset_climate_zone17\", '' as \"sunset_climate_zone18\", '' as \"sunset_climate_zone19\", '' as \"sunset_climate_zone20\", '' as \"sunset_climate_zone21\", '' as \"sunset_climate_zone22\", '' as \"sunset_climate_zone23\", '' as \"sunset_climate_zone24\", '' as \"expected_plant_spread\", '' as \"expected_plant_spread_unit_of_measure\" \
from 	(anagrafica_articoli aa join articoli_amazon amz on amz.part_number=aa.codice) left join descrizioni_articoli da on aa.codice=da.codice_art left join giacenze_magazzino gm on aa.codice=gm.codice_art where amz.part_number in ($CODESLIST) \
order by amz.part_number; \
";
const std::string InsertTemplateGenerator::templateGenPar_codesList = "CODESLIST";
const std::string InsertTemplateGenerator::fstRowTemplate = "TemplateType=Home\tVersion=2013.0903\tLe prime 3 righe sono destinate a uso esclusivo di Amazon.com. Non modificare, ne' cancellarle prime 3 righe.\t\t\t\t\t\t\t\tOfferta - Informazioni sull'offerta - Attributi obbligatori per rendere il prodotto acquistabile dai clienti sul sito.\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tDimensioni - Dimensioni del prodotto - Attributi che specificano le dimensioni e il peso di un prodotto.\t\t\t\t\t\t\t\t\t\t\t\t\t\tScoperta - Informazioni sulla scoperta del prodotto - Questi attributi influiscono sulla modalita' in cui il cliente può trovare i prodotti sul sito mediante navigazione o ricerca.\t\t\t\t\t\t\t\t\t\t\t\t\t\"Immagine - Informazioni sull'immagine. Si veda la scheda \"\"Istruzioni per l'immagine\"\", per maggiori dettagli.\"\t\t\t\t\t\t\t\t\t\t\"Spedito - FBA - Utilizzare queste colonne se si partecipa al programma \"\"Spedito da Amazon\"\".\"\tVarianti - Informazioni varianti - Popolare questi attributi se il prodotto è disponibile in diverse varianti (Per esempio: colore o wattaggio).\t\t\t\tConformita' - Informazioni di conformita' - Attributi utilizzati per conformita' alle leggi a tutela del consumatore nel Paese o nella regione in cui e' venduto l'articolo.\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tNon raggruppati - Questi attributi di creare elenchi di prodotti ricche per i vostri acquirenti.\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
const std::string InsertTemplateGenerator::sndRowTemplate = "SKU\tCodice prodotto\tTipo codice prodotto\tNome articolo (titolo)\tMarca\tProduttore\tDescrizione prodotto\tTipo di prodotto\tAggiornamento - Eliminazione\tCodice articolo produttore\tParametro registrato\tPrezzo articolo\tValuta\tQuantita'\tTipo condizioni offerta\tNota condizioni\tQuantita' colli\tDisponibile su Amazon a partire da\tData di uscita\tTempi di evasione\tData riassortimento\tQuantita' massima spedizione cumulativa\tDisponibilita' messaggio auguri\tDisponibilita' confezione regalo\tFuori produzione\tPrezzo scontato\tData fine sconto\tData inizio sconto\tVisualizzare peso\tUnita' di misura peso visibile del prodotto\tVisualizzare volume\tUnita' di misura volume visibile del prodotto\tVisualizzare lunghezza\tUnita' di misura lunghezza visibile del prodotto\tPeso spedizione\tUnita' di misura peso di spedizione\tLunghezza articolo\tAltezza articolo\tPeso articolo\tUnita' di misura peso del prodotto\titem_width\tUnita' di misura dimensioni del prodotto\tFunzionalita' principali1\tFunzionalita' principali2\tFunzionalita' principali3\tFunzionalita' principali4\tFunzionalita' principali5\tBrowse node raccomandato1\tBrowse node raccomandato2\tNumero catalogo\tTermini ricerca1\tTermini ricerca2\tTermini ricerca3\tTermini ricerca4\tTermini ricerca5\tURL immagine principale\tURL altre immagini1\tURL altre immagini2\tURL altre immagini3\tURL altre immagini4\tURL altre immagini5\tURL altre immagini6\tURL altre immagini7\tURL altre immagini8\tURL immagine campione\tNumero centro di distribuzione\tParentela\t\"SKU \"\"parent\"\"\"\tTipo di relazione\tTema variante\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla fascia d'eta'\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'1\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'2\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'3\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'4\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'5\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'6\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'7\tDirettiva UE sulla sicurezza dei giocattoli avvertimento non relativo alla fascia d'eta'8\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.1\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.2\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.3\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.4\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.5\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.6\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.7\tDirettiva UE sulla sicurezza dei giocattoli avvertimento relativo alla lingua utilizzata.8\tDescrizione esonero di responsabilita'\tAvvertenze sicurezza\tPaese di origine\tAlimentazione\tVita media batteria\tVita media batteria in standby\tTempo di ricarica batteria\tEnergia batteria al litio\tConfezione batteria al litio\tVoltaggio batteria al litio\tPeso batteria al litio\tNume celle di litio (ioni)\tNumero di celle di litio (metallo)\tDescrizione garanzia venditore\tNumero di pezzi\tFragranza\tNumero di fili\tNumero di set\tTipo materiale\tAnti-macchia\tWattaggio\tBatterie incluse?\tBatterie necessarie?\tTipo di batterie1\tTipo di batterie2\tTipo di batterie3\tNumero di batterie1\tNumero di batterie2\tNumero di batterie3\tDimensioni\tMappa taglie\tColore\tMappa colori\tTecnica colturale\tAnnaffiatura\tTipo di terreno1\tTipo di terreno2\tTipo di terreno3\tTipo di terreno4\tTipo di terreno5\tTipo di terreno6\tTipo di terreno7\tTipo di terreno8\tTipo di terreno9\tTipo di terreno10\tTipo di terreno11\tZona climatica1\tZona climatica2\tZona climatica3\tZona climatica4\tZona climatica5\tZona climatica6\tZona climatica7\tZona climatica8\tZona climatica9\tZona climatica10\tZona climatica11\tZona climatica12\tZona climatica13\tZona climatica14\tZona climatica15\tZona climatica16\tZona climatica17\tZona climatica18\tZona climatica19\tZona climatica20\tZona climatica21\tZona climatica22\tZona climatica23\tZona climatica24\tCrescita prevista\tUnita' di misura per previsioni di crescita della pianta";
const std::string InsertTemplateGenerator::trdRowTemplate = "item_sku\texternal_product_id\texternal_product_id_type\titem_name\tbrand_name\tmanufacturer\tproduct_description\tfeed_product_type\tupdate_delete\tpart_number\tmissing_keyset_reason\tstandard_price\tcurrency\tquantity\tcondition_type\tcondition_note\titem_package_quantity\tproduct_site_launch_date\tmerchant_release_date\tfulfillment_latency\trestock_date\tmax_aggregate_ship_quantity\toffering_can_be_gift_messaged\toffering_can_be_giftwrapped\tis_discontinued_by_manufacturer\tsale_price\tsale_end_date\tsale_from_date\titem_display_weight\titem_display_weight_unit_of_measure\titem_display_volume\titem_display_volume_unit_of_measure\titem_display_length\titem_display_length_unit_of_measure\twebsite_shipping_weight\twebsite_shipping_weight_unit_of_measure\titem_length\titem_height\titem_weight\titem_weight_unit_of_measure\titem_width\titem_dimensions_unit_of_measure\tbullet_point1\tbullet_point2\tbullet_point3\tbullet_point4\tbullet_point5\trecommended_browse_nodes1\trecommended_browse_nodes2\tcatalog_number\tgeneric_keywords1\tgeneric_keywords2\tgeneric_keywords3\tgeneric_keywords4\tgeneric_keywords5\tmain_image_url\tother_image_url1\tother_image_url2\tother_image_url3\tother_image_url4\tother_image_url5\tother_image_url6\tother_image_url7\tother_image_url8\tswatch_image_url\tfulfillment_center_id\tparent_child\tparent_sku\trelationship_type\tvariation_theme\teu_toys_safety_directive_age_warning\teu_toys_safety_directive_warning1\teu_toys_safety_directive_warning2\teu_toys_safety_directive_warning3\teu_toys_safety_directive_warning4\teu_toys_safety_directive_warning5\teu_toys_safety_directive_warning6\teu_toys_safety_directive_warning7\teu_toys_safety_directive_warning8\teu_toys_safety_directive_language1\teu_toys_safety_directive_language2\teu_toys_safety_directive_language3\teu_toys_safety_directive_language4\teu_toys_safety_directive_language5\teu_toys_safety_directive_language6\teu_toys_safety_directive_language7\teu_toys_safety_directive_language8\tlegal_disclaimer_description\tsafety_warning\tcountry_string\tpower_source_type\tbattery_average_life\tbattery_average_life_standby\tbattery_charge_time\tlithium_battery_energy_content\tlithium_battery_packaging\tlithium_battery_voltage\tlithium_battery_weight\tnumber_of_lithium_ion_cells\tnumber_of_lithium_metal_cells\tseller_warranty_description\tnumber_of_pieces\tscent_name\tthread_count\tnumber_of_sets\tmaterial_type\tis_stain_resistant\twattage\tare_batteries_included\tbatteries_required\tbattery_type1\tbattery_type2\tbattery_type3\tnumber_of_batteries1\tnumber_of_batteries2\tnumber_of_batteries3\tsize_name\tsize_map\tcolor_name\tcolor_map\tsunlight_exposure\tmoisture_needs\tusda_hardiness_zone1\tusda_hardiness_zone2\tusda_hardiness_zone3\tusda_hardiness_zone4\tusda_hardiness_zone5\tusda_hardiness_zone6\tusda_hardiness_zone7\tusda_hardiness_zone8\tusda_hardiness_zone9\tusda_hardiness_zone10\tusda_hardiness_zone11\tsunset_climate_zone1\tsunset_climate_zone2\tsunset_climate_zone3\tsunset_climate_zone4\tsunset_climate_zone5\tsunset_climate_zone6\tsunset_climate_zone7\tsunset_climate_zone8\tsunset_climate_zone9\tsunset_climate_zone10\tsunset_climate_zone11\tsunset_climate_zone12\tsunset_climate_zone13\tsunset_climate_zone14\tsunset_climate_zone15\tsunset_climate_zone16\tsunset_climate_zone17\tsunset_climate_zone18\tsunset_climate_zone19\tsunset_climate_zone20\tsunset_climate_zone21\tsunset_climate_zone22\tsunset_climate_zone23\tsunset_climate_zone24\texpected_plant_spread\texpected_plant_spread_unit_of_measure";

InsertTemplateGenerator::InsertTemplateGenerator(	openDB::BackendConnector* connector,
							const std::string& codes,
							const std::string& file_name) :
	QObject(),
	backConnector(connector),
	codesList(codes),
	fileName(file_name)
{
	query = templateGeneratorQuery;
	std::unordered_map<std::string, std::string> parMap = {{templateGenPar_codesList, codesList}};
	openDB::prepare(query, parMap);
	ntxn.appendCommand(query);
}

InsertTemplateGenerator::~InsertTemplateGenerator()
{
	backConnector->deleteTransaction(it);
}

void InsertTemplateGenerator::start()
{
	it = backConnector->execTransaction(ntxn, "Template");
	printTemplate();
	emit done();
}

void InsertTemplateGenerator::printTemplate()
{
	std::fstream stream(fileName.c_str(), std::ios::out);
	stream << fstRowTemplate << std::endl << sndRowTemplate << std::endl << trdRowTemplate << std::endl;
	openDB::Table& template_table = it.result().resultTable();
	openDB::Storage::Iterator sit = template_table.storage().begin(), send = template_table.storage().end();
	for (; sit != send; sit++)
	{
		openDB::Table::Iterator cit = template_table.begin(), cbegin = cit, cend = template_table.end();
		std::unordered_map<std::string, std::string> row;
		(*sit).current(row);
		for (; cit != cend; cit++)
		{
			if (cit != cbegin)
				stream << "\t";
			stream << row.at(cit->name());
		}
		stream << std::endl;
	}
	stream.close();
}
