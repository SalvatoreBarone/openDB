/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __AMAZONINSERTTEMPLATEGENERATOR_H
#define __AMAZONINSERTTEMPLATEGENERATOR_H

#include <string>

#include <QObject>
#include <QWidget>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

namespace openDB
{
	class Table;
}

class MainWindow;
class InsertTemplateGenerator;

class QThread;
class QLabel;
class QLineEdit;
class QPushButton;
class QProgressDialog;
class QHBoxLayout;
class QGridLayout;

class AmazonInsertGenerator : public QWidget
{
	Q_OBJECT
public:
	explicit AmazonInsertGenerator(MainWindow* window, openDB::Table* _table, QWidget* parent);

	virtual ~AmazonInsertGenerator();

private:
	MainWindow* parentWindow;
	openDB::Table* table;

	QLabel* labelFileName;
	QLineEdit* editFileName;
	QPushButton* buttonChooseFile;
	QPushButton* buttonGenera;
	QHBoxLayout* buttonLayout;
	QGridLayout* mainLayout;

	static const std::string codeColumnName;

	QThread* generator_thread;
	InsertTemplateGenerator* generator;
	QProgressDialog* progDialog;

private slots:
	void slotChooseFile();
	void slotMakeTemplate();
	void slotDone();
	void slotAbort();
};



class InsertTemplateGenerator : public QObject
{
	Q_OBJECT
public:
	explicit InsertTemplateGenerator(	openDB::BackendConnector* connector,
						const std::string& codes,
						const std::string& file_name);

	virtual ~InsertTemplateGenerator ();

signals:
	void done();

public slots:
	void start();

private:
	openDB::BackendConnector* backConnector;
	std::string codesList;
	std::string fileName;

	std::string query;
	openDB::NonTransactional ntxn;
	openDB::BackendConnector::Iterator it;

	static const std::string templateGeneratorQuery;
	static const std::string templateGenPar_codesList;
	static const std::string fstRowTemplate;
	static const std::string sndRowTemplate;
	static const std::string trdRowTemplate;

	void printTemplate();
};

#endif
