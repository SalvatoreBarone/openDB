/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "AmazonInsertLoader.hpp"
#include "MainWindow.hpp"

#include <QtGui>
#include <QThread>
#include <QProgressDialog>

AmazonInsertLoader::AmazonInsertLoader(	MainWindow* window,
				openDB::Table* table,
				QWidget* parent) :
	QWidget(parent),
	parentWindow(window),
	amzart_table(table),
	labelUrlImages(new QLabel("Url dir. immagini", this)),
	editUrlImages(new QLineEdit(this)),
	labelFilterNatura(new QLabel("Natura", this)),
	editFilterNatura(new QLineEdit(this)),
	labelFilterCatMerc(new QLabel("Cat. Merceologica", this)),
	editFilterCatMerc(new QLineEdit(this)),
	labelFilterCodes(new QLabel("Codici", this)),
	editFilterCodes(new QLineEdit(this)),
	buttonLoadFromDb(new QPushButton("Carica da Db", this)),
	buttonLayout(new QHBoxLayout),
	mainLayout(new QGridLayout(this)),
	loader_thread(0),
	loader(0),
	progDialog(0)
{
	setLayout(mainLayout);

	mainLayout->addWidget(labelUrlImages, 0, 0, 1, 1);
	mainLayout->addWidget(editUrlImages, 0, 1, 1, 3);
	mainLayout->addWidget(labelFilterNatura, 1, 0, 1, 1);
	mainLayout->addWidget(editFilterNatura, 1, 1, 1, 1);
	mainLayout->addWidget(labelFilterCatMerc, 1, 2, 1, 1);
	mainLayout->addWidget(editFilterCatMerc, 1, 3, 1, 1);

	mainLayout->addWidget(labelFilterCodes, 2, 0, 1, 1);
	mainLayout->addWidget(editFilterCodes, 2, 1, 1, 3);


	buttonLayout->addWidget(buttonLoadFromDb);
	mainLayout->addLayout(buttonLayout, 6, 0, 1, 4, Qt::AlignCenter | Qt::AlignBottom);

	connect(buttonLoadFromDb, SIGNAL(clicked()), this, SLOT(slotLoadFromDb()));
}

AmazonInsertLoader::~AmazonInsertLoader()
{
	delete labelUrlImages;
	delete editUrlImages;
	delete labelFilterNatura;
	delete editFilterNatura;
	delete labelFilterCatMerc;
	delete editFilterCatMerc;
	delete labelFilterCodes;
	delete editFilterCodes;
	delete buttonLoadFromDb;
	delete buttonLayout;
	delete mainLayout;

	if (progDialog)
		delete progDialog;

	if (loader_thread)
	{
		loader_thread->quit();
		loader_thread->wait();
		delete loader_thread;
		delete loader;
	}
}

void AmazonInsertLoader::slotLoadFromDb()
{
	if (!loader)
	{
		std::string preparedCodes = editFilterCodes->text().toStdString();
		if (!preparedCodes.empty())
		{
			openDB::Varchar obj;
			obj.prepare(preparedCodes);
		}

		loader_thread = new QThread;
		loader = new RecordLoader(	&parentWindow->backendConnector(),
						amzart_table,
						editUrlImages->text().toStdString(),
						editFilterNatura->text().toStdString(),
						editFilterCatMerc->text().toStdString(),
						preparedCodes);

		connect (loader_thread, SIGNAL(started()), loader, SLOT(start()));
		connect (loader, SIGNAL(done()), this, SLOT(slotDone()));
		loader->moveToThread(loader_thread);
		loader_thread->start();

		progDialog = new QProgressDialog("Caricamento", "Annulla", 0, 0, this);
		progDialog->show();
		connect(progDialog, SIGNAL(canceled()), this, SLOT(slotAbort()));
	}
	else
		QMessageBox::warning(	this,
					"Attenzione!",
					"Il processo di generazione e' gia' in esecuzione.",
					QMessageBox::Ok);
}

void AmazonInsertLoader::slotDone()
{
	loader_thread->quit();
	loader_thread->wait();
	delete loader_thread;
	delete loader;
	loader_thread = 0;
	loader = 0;
	delete progDialog;
	progDialog = 0;
	QMessageBox::information(this,
				"Caricamento completato!",
				"Caricamento completato. I record sono stati caricati nella tabelle \"articoli_amazon\".",
				QMessageBox::Ok);
}

void AmazonInsertLoader::slotAbort()
{
	loader_thread->quit();
	loader_thread->wait();
	delete loader_thread;
	delete loader;
	loader_thread = 0;
	loader = 0;
	delete progDialog;
	progDialog = 0;
	QMessageBox::information(this,
				"Caricamento interrotto!",
				"Il caricamento e' stato interrotto dall'utente.",
				QMessageBox::Ok);
}


const std::string RecordLoader::loaderQuery = "\
select \
(aa.codice || '_' || aa.codice_alt) as \"item_sku\", \
aa.descrizione as item_name, \
'' as \"brand_name\", \
'' as \"manufacturer\", \
aa.codice as \"part_number\", \
round(aa.listino3 * 0.7 * 1.22, 2) as \"standard_price\", \
'' as \"bullet_point1\", \
'' as \"bullet_point2\", \
'' as \"bullet_point3\", \
'' as \"bullet_point4\", \
'' as \"bullet_point5\", \
ac.amazon_browse_node_1 as \"recommended_browse_nodes1\", \
ac.amazon_browse_node_2 as \"recommended_browse_nodes2\", \
'' as \"generic_keywords1\", \
'' as \"generic_keywords2\", \
'' as \"generic_keywords3\", \
'' as \"generic_keywords4\", \
'' as \"generic_keywords5\", \
('$IMGPATH' || aa.codice || '_1.jpg') as \"main_image_url\", \
('$IMGPATH' || aa.codice || '_2.jpg') as \"other_image_url1\", \
('$IMGPATH' || aa.codice || '_3.jpg') as \"other_image_url2\",  \
'' as \"other_image_url3\", \
(case when aa.flag_cart = true then 'parent' else (case when aa.codice_padre is not null then 'child' else ''end) end) as \"parent_child\", \
(select aatmp.codice || '_' || aatmp.codice_alt from anagrafica_articoli aatmp where aatmp.codice=aa.codice_padre) as \"parent_sku\", \
(case when aa.codice_padre is not null then 'Variation' else '' end) as \"relationship_type\", \
(case when aa.codice_padre is not null or aa.codice in (select codice_art from cartelle_articoli) then 'SizeName or ColorName'  else '' end) as \"variation_theme\", \
(case when aa.codice_padre is not null then (select aatmp.descrizione from anagrafica_articoli aatmp where aatmp.codice = replace(aa.codice, aa.codice_padre, '')) else '' end) as \"size_name\", \
(case when aa.codice_padre is not null then (select aatmp.descrizione from anagrafica_articoli aatmp where aatmp.codice = replace(aa.codice, aa.codice_padre, '')) else '' end) as \"color_name\", \
left(da.descrizione, 2000) as \"product_description\" \
from ((anagrafica_articoli aa join anagrafica_categorie ac on ac.codice=aa.cat_merc) left join descrizioni_articoli da on da.codice_art=aa.codice) \
where aa.codice in (select codice from anagrafica_articoli where $ADDITIONFILTERCODESLIST $ADDITIONALFILTERNATURA $ADDITIONALFILTERCATMERC codice not in (select part_number as codice from articoli_amazon)) \
order by aa.codice; \
";

const std::string RecordLoader::additionalFilterNaturaParName = "ADDITIONALFILTERNATURA";
const std::string RecordLoader::additionalFilterNaturaParValue = "natura='$NATURA' and ";

const std::string RecordLoader::additionalFilterCatMercParName = "ADDITIONALFILTERCATMERC";
const std::string RecordLoader::additionalFilterCatMercParValue = "cat_merc='$CAT_MERC' and ";

const std::string  RecordLoader::additionalFilterCodesParName = "ADDITIONFILTERCODESLIST";
const std::string  RecordLoader::additionalFilterCodesParValue = "aa.codice in ($CODESLIST) and ";

const std::string RecordLoader::loaderPar_natura = "NATURA";
const std::string RecordLoader::loaderPar_catMerc = "CAT_MERC";
const std::string RecordLoader::loaderPar_imgPath = "IMGPATH";
const std::string RecordLoader::loaderPar_codes = "CODESLIST";

const std::string RecordLoader::codelColumnName = "part_number";

RecordLoader::RecordLoader(	openDB::BackendConnector* connector,
				openDB::Table* _table,
				const std::string& imgDir,
				const std::string& nat,
				const std::string& cat,
				const std::string& cod) :
	QObject(),
	backConnector(connector),
	table(_table),
	imgDirectory(imgDir),
	natura(nat),
	categoria(cat),
	codes(cod)
{
	query = loaderQuery;
	std::unordered_map<std::string, std::string> filterMap =
	{
		{additionalFilterCodesParName, (codes.empty() ? "" : additionalFilterCodesParValue)},
		{additionalFilterNaturaParName, (natura.empty() ? "" : additionalFilterNaturaParValue)},
		{additionalFilterCatMercParName, (categoria.empty() ? "" : additionalFilterCatMercParValue)}
	};
	openDB::prepare(query, filterMap);

	std::unordered_map<std::string, std::string> parametersMap =
	{
		{loaderPar_codes, codes},
		{loaderPar_natura, natura},
		{loaderPar_catMerc, categoria},
		{loaderPar_imgPath, imgDirectory}
	};
	openDB::prepare(query, parametersMap);
	ntxn.appendCommand(query);
}

RecordLoader::~RecordLoader()
{
	backConnector->deleteTransaction(it);
}

void RecordLoader::start()
{
	it = backConnector->execTransaction(ntxn);
	table->storage().clear();
	openDB::Table& result = it.result().resultTable();
	openDB::Storage::Iterator it = result.storage().begin(), end = result.storage().end();
	for (; it != end; it++)
	{
		openDB::Record rec = table->storage().newRecord();
		std::unordered_map<std::string, std::string> values;
		(*it).current(values);
		rec.edit(values);
		rec.state(openDB::Record::inserting);
		table->storage().insertRecord(rec);
	}
	emit done();
}
