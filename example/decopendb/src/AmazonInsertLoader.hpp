/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef __AMAZONLOADER_H
#define __AMAZONLOADER_H

#include <QObject>
#include <QWidget>

#include <string>
#include <list>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class QThread;
class QLabel;
class QLineEdit;
class QPushButton;
class QProgressDialog;
class QHBoxLayout;
class QGridLayout;

namespace openDB
{
	class Table;
};

class MainWindow;
class RecordLoader;
class InsertTemplateGenerator;

class AmazonInsertLoader : public QWidget
{
	Q_OBJECT
public:
	explicit AmazonInsertLoader(MainWindow* window, openDB::Table* table, QWidget* parent);

	virtual ~AmazonInsertLoader();

private slots:
	void slotLoadFromDb();
	void slotDone();
	void slotAbort();


private:
	MainWindow* parentWindow;
	openDB::Table* amzart_table;

	QLabel* labelUrlImages;
	QLineEdit* editUrlImages;
	QLabel* labelFilterNatura;
	QLineEdit* editFilterNatura;
	QLabel* labelFilterCatMerc;
	QLineEdit* editFilterCatMerc;
	QLabel* labelFilterCodes;
	QLineEdit* editFilterCodes;
	QPushButton* buttonLoadFromDb;
	QHBoxLayout* buttonLayout;
	QGridLayout* mainLayout;

	QThread* loader_thread;
	RecordLoader* loader;
	QProgressDialog* progDialog;
};

class RecordLoader : public QObject
{
	Q_OBJECT
public:
	explicit RecordLoader(	openDB::BackendConnector* connector,
				openDB::Table* _table,
				const std::string& imgDir,
				const std::string& nat,
				const std::string& cat,
				const std::string& codes);

	virtual ~RecordLoader();

signals:
	void done();

public slots:
	void start();

private:
	openDB::BackendConnector* backConnector;
	openDB::Table* table;
	std::string imgDirectory;
	std::string natura;
	std::string categoria;
	std::string codes;

	std::string query;
	openDB::NonTransactional ntxn;
	openDB::BackendConnector::Iterator it;

	/* loaderQuery contiene la query parametrica per il retrieve delle informazioni relative agli articoli
	 * da inserire su amazon.
	 * La query possiede tre parametri principali che consentono di specificare criteri di selezione per
	 * gli articoli da caricare.
	 * I nomi dei tre parametri sono contenuti nei membri statici
	 * - additionalFilterNaturaParName
	 * - additionalFilterCatMercParName
	 * - additionalFilterCodesParName
	 * a cui vanno fatti corrispondere i membri statici
	 * - additionalFilterNaturaParValue, contenente codice sql per il filtro sulla natura degli atricoli;
	 * - additionalFilterCatMercParValue, contenente codice per il filtro sulla categoria degli articoli;
	 * - additionalFilterCodesParValue, contente codice per il filtro che usa un elenco di codici articoli;
	 * il codice sql memorizzato in ognuno dei tre membri statici precedenti e' parametrico. In particolare
	 * - additionalFilterNaturaParValue ha parametro il cui nome e' memorizzato nel membro loaderPar_natura;
	 * - additionalFilterCatMercParValue ha parametro il cui nome e' memorizzato nel membro loaderPar_catMerc;
	 * - additionalFilterCodesParValue ha parametro il cui nome e' memorizzato nel membro loaderPar_codes;
	 *
	 * Un quatro parametro di loaderQuery, il cui nome e' memorizzato nel membro loaderPar_imgPath, consente
	 * di specificare l'indirizzo della cartella da cui potranno essere prelevate le immagini dei prodotti.
	 */
	static const std::string loaderQuery;

	static const std::string additionalFilterNaturaParName;
	static const std::string additionalFilterNaturaParValue;

	static const std::string additionalFilterCatMercParName;
	static const std::string additionalFilterCatMercParValue;

	static const std::string additionalFilterCodesParName;
	static const std::string additionalFilterCodesParValue;

	static const std::string loaderPar_natura;
	static const std::string loaderPar_catMerc;
	static const std::string loaderPar_codes;
	static const std::string loaderPar_imgPath;

	static const std::string codelColumnName;
};




#endif
