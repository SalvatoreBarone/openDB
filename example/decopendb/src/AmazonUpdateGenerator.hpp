/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __AMAZONUPDATER_H
#define __AMAZONUPDATER_H

#include <string>

#include <QObject>
#include <QWidget>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

namespace openDB
{
	class Table;
}

class MainWindow;
class UpdateTemplateGenerator;

class QThread;
class QLabel;
class QLineEdit;
class QPushButton;
class QCheckBox;
class QProgressDialog;
class QHBoxLayout;
class QGridLayout;

class AmazonUpdateGenerator : public QWidget
{
	Q_OBJECT
public:
	explicit AmazonUpdateGenerator(MainWindow* window, QWidget* parent);

	virtual ~AmazonUpdateGenerator();

private slots:
	void slotChooseFile();
	void slotMakeTemplate();
	void slotDone();
	void slotAbort();

private:
	MainWindow* parentWindow;

	QLabel* labelFileName;
	QLineEdit* editFileName;
	QPushButton* buttonChooseFile;
	QCheckBox* checkBoxImg;
	QPushButton* buttonGenera;
	QHBoxLayout* buttonLayout;
	QGridLayout* mainLayout;

	QThread* generator_thread;
	UpdateTemplateGenerator* generator;
	QProgressDialog* progDialog;
};




class UpdateTemplateGenerator : public QObject
{
	Q_OBJECT
public:
	explicit UpdateTemplateGenerator(	openDB::BackendConnector* connector,
						bool imgUpdate,
						const std::string& file_name);

	virtual ~UpdateTemplateGenerator ();

signals:
	void done();

public slots:
	void start();

private:
	openDB::BackendConnector* backConnector;
	bool imageUpdate;
	std::string fileName;

	std::string query;
	openDB::NonTransactional ntxn;
	openDB::BackendConnector::Iterator it;

	static const std::string templateGeneratorQuery;
	static const std::string templateGeneratorQuery_withoutImg;
	static const std::string fstRowTemplate;
	static const std::string sndRowTemplate;
	static const std::string trdRowTemplate;

	void printTemplate();
};


#endif
