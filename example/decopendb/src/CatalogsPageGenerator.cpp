/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "CatalogsPageGenerator.hpp"
#include "verifyMacro.hpp"

#include "MainWindow.hpp"

#include <unordered_map>
#include <fstream>

#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>

#define __DEBUG

#ifdef __DEBUG
#include <iostream>
#endif

CatalogGeneratorInterface::CatalogGeneratorInterface(	MainWindow* window,
							QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		workerThread(0),
		generator(0),
		labelSite(new QLabel("Sito", this)),
		editSite(new QLineEdit(this)),
		labelStyleTemplateFile(new QLabel("Style template", this)),
		editStyleTemplateFile(new QLineEdit(this)),
		buttonStyleTemplateFile(new QPushButton("Apri", this)),
		labelListTemplateFile(new QLabel("List Template", this)),
		editListTemplateFile(new QLineEdit(this)),
		buttonListTemplateFile(new QPushButton("Apri", this)),
		labelNovitaTemplateFile(new QLabel("Novita' Template", this)),
		editNovitaTemplateFile(new QLineEdit(this)),
		buttonNovitaTemplateFile(new QPushButton("Apri", this)),
		labelLineTemplateFile(new QLabel("Line Template", this)),
		editLineTemplateFile(new QLineEdit(this)),
		buttonLineTemplateFile(new QPushButton("Apri", this)),
		labelOutFile(new QLabel("File Output", this)),
		editOutFile(new QLineEdit(this)),
		buttonOutFile(new QPushButton("Scegli", this)),
		buttonCreate(new QPushButton("Crea", this)),
		progressBar(new QProgressBar(this)),
		layout(new QGridLayout(this))
{

	setLayout(layout);

	resizeWidgets();
	layoutWidgets();
	connectWidgets();
	loadConfig();
}

CatalogGeneratorInterface::~CatalogGeneratorInterface()
{
	if (workerThread)
	{
		workerThread->quit();
		workerThread->wait();
		delete workerThread;
		delete generator;
	}

	delete labelSite;
	delete editSite;
	delete labelStyleTemplateFile;
	delete editStyleTemplateFile;
	delete buttonStyleTemplateFile;
	delete labelListTemplateFile;
	delete editListTemplateFile;
	delete buttonListTemplateFile;
	delete labelNovitaTemplateFile;
	delete editNovitaTemplateFile;
	delete buttonNovitaTemplateFile;
	delete labelLineTemplateFile;
	delete editLineTemplateFile;
	delete buttonLineTemplateFile;
	delete labelOutFile;
	delete editOutFile;
	delete buttonOutFile;
	delete buttonCreate;
	delete progressBar;
	delete layout;
}

void CatalogGeneratorInterface::resizeWidgets()
{
	buttonStyleTemplateFile->setFixedSize(buttonStyleTemplateFile->sizeHint());
	buttonListTemplateFile->setFixedSize(buttonListTemplateFile->sizeHint());
	buttonNovitaTemplateFile->setFixedSize(buttonNovitaTemplateFile->sizeHint());
	buttonLineTemplateFile->setFixedSize(buttonLineTemplateFile->sizeHint());
	buttonOutFile->setFixedSize(buttonOutFile->sizeHint());
	buttonCreate->setFixedSize(buttonCreate->sizeHint());
}

void CatalogGeneratorInterface::layoutWidgets()
{
	layout->addWidget(labelSite, 0, 0, 1, 1);
	layout->addWidget(editSite, 0, 1, 1, 2);
	layout->addWidget(labelStyleTemplateFile, 1, 0, 1, 1);
	layout->addWidget(editStyleTemplateFile, 1, 1, 1, 1);
	layout->addWidget(buttonStyleTemplateFile, 1, 2, 1, 1);
	layout->addWidget(labelListTemplateFile, 2, 0, 1, 1);
	layout->addWidget(editListTemplateFile, 2, 1, 1, 1);
	layout->addWidget(buttonListTemplateFile, 2, 2, 1, 1);
	layout->addWidget(labelNovitaTemplateFile, 3, 0, 1, 1);
	layout->addWidget(editNovitaTemplateFile, 3, 1, 1, 1);
	layout->addWidget(buttonNovitaTemplateFile, 3, 2, 1, 1);
	layout->addWidget(labelLineTemplateFile, 4, 0, 1, 1);
	layout->addWidget(editLineTemplateFile, 4, 1, 1, 1);
	layout->addWidget(buttonLineTemplateFile, 4, 2, 1, 1);
	layout->addWidget(labelOutFile, 5, 0, 1, 1);
	layout->addWidget(editOutFile, 5, 1, 1, 1);
	layout->addWidget(buttonOutFile, 5, 2, 1, 1);
	layout->addWidget(buttonCreate, 6, 0, 1, 4, Qt::AlignCenter);
	layout->addWidget(progressBar, 7, 0, 1, 4, Qt::AlignBottom);

}

void CatalogGeneratorInterface::connectWidgets()
{
	connect(buttonStyleTemplateFile, SIGNAL(clicked()), this, SLOT(slotChooseStyleFile()));
	connect(buttonListTemplateFile, SIGNAL(clicked()), this, SLOT(slotChooseListFile()));
	connect(buttonNovitaTemplateFile, SIGNAL(clicked()), this, SLOT(slotChooseNovitaFile()));
	connect(buttonLineTemplateFile, SIGNAL(clicked()), this, SLOT(slotChooseLineFile()));
	connect(buttonOutFile, SIGNAL(clicked()), this, SLOT(slotChooseOutFile()));
	connect(buttonCreate, SIGNAL(clicked()), this, SLOT(slotCreate()));
}

bool CatalogGeneratorInterface::verifySite() {verifyField(editSite, "Specifica il sito per il quale creare il componente.")}
bool CatalogGeneratorInterface::verifyStyleFile() {verifyFile(editStyleTemplateFile, "Specifica il file template per lo style.")}
bool CatalogGeneratorInterface::verifyListFile() {verifyFile(editListTemplateFile, "Specifica il file template per le categorie.")}
bool CatalogGeneratorInterface::verifyNovitaFile() {verifyFile(editNovitaTemplateFile, "Specifica il file template le novita'.")}
bool CatalogGeneratorInterface::verifyLineFile() {verifyFile(editLineTemplateFile, "Specifica il file template per i cataloghi.")}
bool CatalogGeneratorInterface::verifyOutFile() {verifyField(editOutFile, "Specifica il file di output!")}

bool CatalogGeneratorInterface::verifyFields()
{
	if (
		verifySite() &&
		verifyStyleFile() &&
		verifyListFile() &&
		verifyNovitaFile() &&
		verifyLineFile() &&
		verifyOutFile()
	)
		return true;
	return false;
}

void CatalogGeneratorInterface::slotChooseStyleFile() {chooseOpenFile(parentWindow, editStyleTemplateFile, "Style Template", "*.html")}
void CatalogGeneratorInterface::slotChooseListFile() {chooseOpenFile(parentWindow, editListTemplateFile, "List Template", "*.html")}
void CatalogGeneratorInterface::slotChooseNovitaFile() {chooseOpenFile(parentWindow, editNovitaTemplateFile, "Novita' Template", "*.html")}
void CatalogGeneratorInterface::slotChooseLineFile() {chooseOpenFile(parentWindow, editLineTemplateFile, "Line Template", "*.html")}
void CatalogGeneratorInterface::slotChooseOutFile() {chooseSaveFile(parentWindow, editOutFile, "Output File", "*.html")}

void CatalogGeneratorInterface::slotCreate()
{
	if (verifyFields())
	{
		if (workerThread == 0 && generator == 0)
		{
			saveConfig();

			std::string site = editSite->text().toStdString();
			std::string style = editStyleTemplateFile->text().toStdString();
			std::string template_list = editListTemplateFile->text().toStdString();
			std::string template_novita = editNovitaTemplateFile->text().toStdString();
			std::string template_line = editLineTemplateFile->text().toStdString();
			std::string out_file = editOutFile->text().toStdString();

			workerThread = new QThread;
			generator = new CatalogGenerator (	&(parentWindow->backendConnector()),
								site,
								style,
								template_list,
								template_novita,
								template_line,
								out_file);

			generator->moveToThread(workerThread);
			connect (workerThread, SIGNAL(started()), generator, SLOT(start()));
			connect (generator, SIGNAL(totalRecords(unsigned int)), this, SLOT(slotTotal(unsigned int)));
			connect (generator, SIGNAL(incProgress()), this, SLOT(slotIncProgress()));
			connect (generator, SIGNAL(done()), this, SLOT(slotDone()));

			workerThread->start();
		}
		else
			QMessageBox::warning(	this,
						"Attenzione!",
						"Il processo di e' ancora in corso! Attendi o crea una nuova sessione.",
						QMessageBox::Ok);
	}
}

void CatalogGeneratorInterface::slotTotal(unsigned int n)
{
	progressBar->setValue(0);
	progressBar->setMaximum(n);
}

void CatalogGeneratorInterface::slotIncProgress()
{
	progressBar->setValue(progressBar->value() + 1);
}

void CatalogGeneratorInterface::slotDone()
{
	workerThread->quit();
	workerThread->wait();
	delete workerThread;
	delete generator;
	workerThread = 0;
	generator = 0;
}


const std::string CatalogGenerator::sqlQuery_getNewCatalogs = "\
select distinct un.url as \"PAGEURL\", un.img as \"IMGURL\", un.nome_campionario as \"CATALOGNAME\", un.descr_aggiuntiva as \"CATALOGDESCR\" \
from componenti.campionari_parati un \
where un.sito='$SITEURL' and un.novita=true \
order by un.nome_campionario\
";

const std::string CatalogGenerator::queryParm_getNewCatalogs_site = "SITEURL";

const std::string CatalogGenerator::sqlQuery_getCategory = "\
select codice as \"CATEGORYCODE\", descrizione as \"CATEGORYNAME\" \
from anagrafica_categorie \
where codice_padre='1' and codice <> '1006' \
order by codice\
";

const std::string CatalogGenerator::column_getCategory_CategoryName = "CATEGORYNAME";
const std::string CatalogGenerator::column_getCategory_CategoryCode = "CATEGORYCODE";

const std::string CatalogGenerator::sqlQuery_getCatalogsByCategory = "\
select distinct un.url as \"PAGEURL\", un.img as \"IMGURL\", un.nome_campionario as \"CATALOGNAME\", un.descr_aggiuntiva as \"CATALOGDESCR\" \
from componenti.campionari_parati un \
where un.sito='$SITEURL' and un.cod_natura in (select natura from anagrafica_articoli where cat_merc like '$CATEGORYCODE%') \
order by un.nome_campionario\
";
const std::string CatalogGenerator::queryParm_getCatalogsByCategory_site = "SITEURL";
const std::string CatalogGenerator::queryParm_getCatalogsByCategory_catCode = "CATEGORYCODE";


const std::string CatalogGenerator::lineTemplate_pageUrl = "PAGEURL";
const std::string CatalogGenerator::lineTemplate_imgUrl = "IMGURL";
const std::string CatalogGenerator::lineTemplate_catalogName = "CATALOGNAME";
const std::string CatalogGenerator::lineTemplate_catalogDescr = "CATALOGDESCR";
const std::string CatalogGenerator::listTemplate_categoryName = "CATEGORYNAME";
const std::string CatalogGenerator::listTemplate_catalogsList = "CATALOGSLIST";

CatalogGenerator::CatalogGenerator(	openDB::BackendConnector* conn,
					const std::string& site,
					const std::string& style,
					const std::string& template_list,
					const std::string& template_novita,
					const std::string& template_line,
					const std::string& out_file) :
		QObject(),
		backendConn(conn),
		siteUrl(site),
		fileStyleTemplate(style),
		fileListTemplate(template_list),
		fileNovitaTemplate(template_novita),
		fileLineTemplate(template_line),
		fileOutput(out_file)
{
}

CatalogGenerator::~CatalogGenerator()
{
}

void CatalogGenerator::start()
{
	std::fstream stream(fileOutput.c_str(), std::ios::out);

	//getting templates
	std::string templ_style, templ_list, templ_new, templ_line;
	openDB::readTemplate(templ_style, fileStyleTemplate);
	openDB::readTemplate(templ_list, fileListTemplate);
	openDB::readTemplate(templ_new, fileNovitaTemplate);
	openDB::readTemplate(templ_line, fileLineTemplate);

	stream << templ_style << std::endl;

	//getting category
	openDB::BackendConnector::Iterator category_iterator = getCategory();
	openDB::Table category_table = category_iterator.result().resultTable();

	emit totalRecords(category_table.storage().records() + 1);

	//getting new catalogs
	makeNewCatalogsSection(stream, templ_new, templ_line);

	emit incProgress();

	//for each category
	openDB::Storage::Iterator cat_it = category_table.storage().begin(), cat_end = category_table.storage().end();
	for (; cat_it != cat_end; cat_it ++)
	{
		makeCatalogsByCategory(	stream,
					(*cat_it).current(column_getCategory_CategoryCode),
					(*cat_it).current(column_getCategory_CategoryName),
					templ_list, templ_line);
		emit incProgress();
	}

	backendConn->deleteTransaction(category_iterator);

	stream.close();
	emit done();

}


openDB::BackendConnector::Iterator CatalogGenerator::getCategory()
{
	openDB::NonTransactional getCategoryTxn;
	getCategoryTxn.appendCommand(sqlQuery_getCategory);
	openDB::BackendConnector::Iterator category_iterator = backendConn->execTransaction(getCategoryTxn);
	return category_iterator;
}

openDB::BackendConnector::Iterator CatalogGenerator::getCatalogsByCategory(const std::string& category)
{
	std::string sqlGetCatalogs = sqlQuery_getCatalogsByCategory;
	std::unordered_map<std::string, std::string> getCatalogsPar =
	{
		{queryParm_getCatalogsByCategory_site, siteUrl},
		{queryParm_getCatalogsByCategory_catCode, category}
	};

	openDB::prepare(sqlGetCatalogs, getCatalogsPar);
	openDB::NonTransactional getCatalogsTxn;
	getCatalogsTxn.appendCommand(sqlGetCatalogs);
	openDB::BackendConnector::Iterator catalogs_it = backendConn->execTransaction(getCatalogsTxn);
	return catalogs_it;
}

openDB::BackendConnector::Iterator CatalogGenerator::getNewCatalogs()
{
	std::string sqlGetNewCat = sqlQuery_getNewCatalogs;
	std::unordered_map<std::string, std::string> getNewCatalParamethers = {{queryParm_getNewCatalogs_site, siteUrl}};
	openDB::prepare(sqlGetNewCat, getNewCatalParamethers);
	openDB::NonTransactional getNewCatalogs;
	getNewCatalogs.appendCommand(sqlGetNewCat);
	openDB::BackendConnector::Iterator newcatalogs_iterator = backendConn->execTransaction(getNewCatalogs);
	return newcatalogs_iterator;
}


void CatalogGenerator::makeNewCatalogsSection(std::fstream& stream, const std::string& templ_new, const std::string& templ_line)
{
	openDB::BackendConnector::Iterator newcatalogs_iterator = getNewCatalogs();
	openDB::Table newCatalogs_table = newcatalogs_iterator.result().resultTable();
	openDB::Storage::Iterator newcats_it = newCatalogs_table.storage().begin(), newcats_end = newCatalogs_table.storage().end();
	std::string newCatalogsString;
	for (; newcats_it != newcats_end; newcats_it++)
	{
		std::string tmp = templ_line;
		std::unordered_map<std::string, std::string> par;
		(*newcats_it).current(par);

		openDB::prepare(tmp, par);
		newCatalogsString += tmp;
	}
	std::string listString = templ_new;
	std::unordered_map<std::string, std::string> par = {{listTemplate_catalogsList, newCatalogsString}};
	openDB::prepare(listString, par);
	stream << listString << std::endl;
	backendConn->deleteTransaction(newcatalogs_iterator);
}

void CatalogGenerator::makeCatalogsByCategory(	std::fstream& stream,
						const std::string& categoryCode,
						const std::string& categoryName,
						const std::string& templ_list,
						const std::string& templ_line)
{
	openDB::BackendConnector::Iterator catalogs_it = getCatalogsByCategory(categoryCode);
	openDB::Table& catalogs_table = catalogs_it.result().resultTable();
	openDB::Storage::Iterator cats_it = catalogs_table.storage().begin(), cats_end = catalogs_table.storage().end();
	std::string catalogsString;
	for (; cats_it != cats_end; cats_it++)
	{
		std::string tmp = templ_line;
		std::unordered_map<std::string, std::string> par;
		(*cats_it).current(par);

		openDB::prepare(tmp, par);
		catalogsString += tmp;
	}
	std::string listString = templ_list;
	std::unordered_map<std::string, std::string> par =
	{
		{listTemplate_categoryName, categoryName},
		{listTemplate_catalogsList, catalogsString}
	};
	openDB::prepare(listString, par);
	stream << listString << std::endl;
	backendConn->deleteTransaction(catalogs_it);

}


const std::string CatalogGeneratorInterface::configGroupName = "catalogs";
const std::string CatalogGeneratorInterface::sitoConfigName = "sito";
const std::string CatalogGeneratorInterface::styleConfigName = "style_template";
const std::string CatalogGeneratorInterface::listConfigName = "list_template";
const std::string CatalogGeneratorInterface::novitaConfigName = "novita_template";
const std::string CatalogGeneratorInterface::lineConfigName = "line_template";
const std::string CatalogGeneratorInterface::outConfigName = "output_file";

void CatalogGeneratorInterface::saveConfig()
{
	libconfig::Config cfg;
	try
	{
		// lettura del file di configurazione
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting &root = cfg.getRoot();

		// se nella root del file di configurazione non esiste il gruppo "catalogs" allora viene aggiunto
		if (!root.exists(configGroupName))
			addSetting(cfg);
		else
		{
			// se nella root esiste un setting configGroupName ma non e' un gruppo, viene rimosso quello esistente ed aggiunto
			// il nuovo gruppo configGroupName. Se esiste il gruppo configGroupName nella root, i suoi settaggi vengono aggiornati.
			libconfig::Setting& setting = root[configGroupName];
			if (!setting.isGroup())
			{
				root.remove(configGroupName);
				addSetting(cfg);
			}
			else
				updateSetting(setting);
		}

		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void CatalogGeneratorInterface::loadConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(configGroupName) &&
			root[configGroupName].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[configGroupName]);

	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}

}

void CatalogGeneratorInterface::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting &root = cfg.getRoot();
	libconfig::Setting& setting = root.add(configGroupName, libconfig::Setting::TypeGroup);

	setting.add(sitoConfigName, libconfig::Setting::TypeString) = editSite->text().toStdString();
	setting.add(styleConfigName, libconfig::Setting::TypeString) = editStyleTemplateFile->text().toStdString();
	setting.add(listConfigName, libconfig::Setting::TypeString) = editListTemplateFile->text().toStdString();
	setting.add(novitaConfigName, libconfig::Setting::TypeString) = editNovitaTemplateFile->text().toStdString();
	setting.add(lineConfigName, libconfig::Setting::TypeString) = editLineTemplateFile->text().toStdString();
	setting.add(outConfigName, libconfig::Setting::TypeString) = editOutFile->text().toStdString();
}

void CatalogGeneratorInterface::updateSetting(libconfig::Setting& setting)
{
	setting[sitoConfigName] = editSite->text().toStdString();
	setting[styleConfigName] = editStyleTemplateFile->text().toStdString();
	setting[listConfigName] = editListTemplateFile->text().toStdString();
	setting[novitaConfigName] = editNovitaTemplateFile->text().toStdString();
	setting[lineConfigName] = editLineTemplateFile->text().toStdString();
	setting[outConfigName] = editOutFile->text().toStdString();
}

void CatalogGeneratorInterface::loadSetting(libconfig::Setting& setting)
{
	editSite->setText(QString::fromStdString(setting[sitoConfigName]));
	editStyleTemplateFile->setText(QString::fromStdString(setting[styleConfigName]));
	editListTemplateFile->setText(QString::fromStdString(setting[listConfigName]));
	editNovitaTemplateFile->setText(QString::fromStdString(setting[novitaConfigName]));
	editLineTemplateFile->setText(QString::fromStdString(setting[lineConfigName]));
	editOutFile->setText(QString::fromStdString(setting[outConfigName]));
}
