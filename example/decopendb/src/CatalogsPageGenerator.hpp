/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __CATALOGS_PAGE_GENERATOR_H
#define __CATALOGS_PAGE_GENERATOR_H

#include <QObject>
#include <QWidget>

#include <string>

#include <libconfig.h++>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class QThread;
class CatalogGenerator;

class QLabel;
class QLineEdit;
class QPushButton;
class QProgressBar;
class QHBoxLayout;
class QGridLayout;

class MainWindow;

class CatalogGeneratorInterface : public QWidget
{
	Q_OBJECT
public:
	explicit CatalogGeneratorInterface (MainWindow* window, QWidget* parent);

	virtual ~CatalogGeneratorInterface();

private slots:
	void slotChooseStyleFile();
	void slotChooseListFile();
	void slotChooseNovitaFile();
	void slotChooseLineFile();
	void slotChooseOutFile();
	void slotCreate();
	void slotTotal(unsigned int n);
	void slotIncProgress();
	void slotDone();

private:
	MainWindow* parentWindow;
	QThread* workerThread;
	CatalogGenerator* generator;

	QLabel* labelSite;
	QLineEdit* editSite;
	QLabel* labelStyleTemplateFile;
	QLineEdit* editStyleTemplateFile;
	QPushButton* buttonStyleTemplateFile;
	QLabel* labelListTemplateFile;
	QLineEdit* editListTemplateFile;
	QPushButton* buttonListTemplateFile;
	QLabel* labelNovitaTemplateFile;
	QLineEdit* editNovitaTemplateFile;
	QPushButton* buttonNovitaTemplateFile;
	QLabel* labelLineTemplateFile;
	QLineEdit* editLineTemplateFile;
	QPushButton* buttonLineTemplateFile;
	QLabel* labelOutFile;
	QLineEdit* editOutFile;
	QPushButton* buttonOutFile;
	QPushButton* buttonCreate;
	QProgressBar* progressBar;
	QGridLayout* layout;

	void resizeWidgets();
	void layoutWidgets();
	void connectWidgets();

	bool verifySite();
	bool verifyStyleFile();
	bool verifyListFile();
	bool verifyNovitaFile();
	bool verifyLineFile();
	bool verifyOutFile();
	bool verifyFields();

	/* - saveSetting salva i settaggi alla pressione del tasto create
	 * - load setting visualizza una finestra di dialogo che permettere di scegliere se caricare gli ultimi settaggi usati
	 */
	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);
	static const std::string configGroupName;
	static const std::string sitoConfigName;
	static const std::string styleConfigName;
	static const std::string listConfigName;
	static const std::string novitaConfigName;
	static const std::string lineConfigName;
	static const std::string outConfigName;

};

class CatalogGenerator : public QObject
{
	Q_OBJECT
public:
	explicit CatalogGenerator(	openDB::BackendConnector* conn,
					const std::string& site,
					const std::string& style,
					const std::string& template_list,
					const std::string& template_novita,
					const std::string& template_line,
					const std::string& out_file);

	virtual ~CatalogGenerator();

signals:
	void totalRecords(unsigned int n);

	void incProgress();

	void done();

public slots:
	void start();

private:
	openDB::BackendConnector* backendConn;
	std::string siteUrl;
	std::string fileStyleTemplate;
	std::string fileListTemplate;
	std::string fileNovitaTemplate;
	std::string fileLineTemplate;
	std::string fileOutput;

	static const std::string sqlQuery_getNewCatalogs;
	static const std::string queryParm_getNewCatalogs_site;

	static const std::string sqlQuery_getCategory;
	static const std::string column_getCategory_CategoryName;
	static const std::string column_getCategory_CategoryCode;

	static const std::string sqlQuery_getCatalogsByCategory;
	static const std::string queryParm_getCatalogsByCategory_site;
	static const std::string queryParm_getCatalogsByCategory_catCode;

	static const std::string lineTemplate_pageUrl;
	static const std::string lineTemplate_imgUrl;
	static const std::string lineTemplate_catalogName;
	static const std::string lineTemplate_catalogDescr;

	static const std::string listTemplate_categoryName;
	static const std::string listTemplate_catalogsList;

	openDB::BackendConnector::Iterator getCategory();

	openDB::BackendConnector::Iterator getCatalogsByCategory(const std::string& category);

	openDB::BackendConnector::Iterator getNewCatalogs();

	void makeNewCatalogsSection(	std::fstream& stream,
					const std::string& templ_new,
					const std::string& templ_line);

	void makeCatalogsByCategory(	std::fstream& stream,
					const std::string& categoryCode,
					const std::string& categoryName,
					const std::string& templ_list,
					const std::string& templ_line);
};

#endif
