/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "DescrGenerator.hpp"
#include "verifyMacro.hpp"

#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>

#include <fstream>
#include <iostream>
#include <thread>

#include "MainWindow.hpp"

DescrGeneratorInterface::DescrGeneratorInterface (QWidget* parent, MainWindow* window) :
	QWidget(parent),
	parentWindow(window),
	labelMainTemplate(new QLabel("Main Template", this)),
	editMainTemplate(new QLineEdit(this)),
	buttonMainTemplate(new QPushButton("Apri", this)),
	labelResourcesTemplate(new QLabel("Res. Template", this)),
	editResourcesTemplate(new QLineEdit(this)),
	buttonResourcesTemplate(new QPushButton("Apri", this)),
	labelVideoResourcesTemplate(new QLabel("Video Res. Template", this)),
	editVideoResourcesTemplate(new QLineEdit(this)),
	buttonVideoResourcesTemplate(new QPushButton("Apri", this)),
	labelVideoTemplate(new QLabel("Video Template", this)),
	editVideoTemplate(new QLineEdit(this)),
	buttonVideoTemplate(new QPushButton("Apri", this)),
	labelCatMerc(new QLabel("Cat. Merc.", this)),
	editCatMerc(new QLineEdit(this)),
	labelNatura(new QLabel("Natura", this)),
	editNatura(new QLineEdit(this)),
	labelFile(new QLabel("File destinazione", this)),
	editFile(new QLineEdit(this)),
	buttonFile(new QPushButton("Scegli", this)),
	buttonCrea(new QPushButton("Crea", this)),
	layoutButton(new QHBoxLayout),
	gridLayout(new QGridLayout(this)),
	progress(new QProgressBar(this)),
	worker_thread(0),
	generator(0)
{
	setLayout(gridLayout);

	resizeWidgets();
	layoutWidgets();
	connectWidgets();
	loadConfig();
}

DescrGeneratorInterface::~DescrGeneratorInterface ()
{
	delete labelMainTemplate;
	delete editMainTemplate;
	delete buttonMainTemplate;
	delete labelResourcesTemplate;
	delete editResourcesTemplate;
	delete buttonResourcesTemplate;
	delete labelVideoResourcesTemplate;
	delete editVideoResourcesTemplate;
	delete buttonVideoResourcesTemplate;
	delete labelVideoTemplate;
	delete editVideoTemplate;
	delete buttonVideoTemplate;
	delete labelCatMerc;
	delete editCatMerc;
	delete labelNatura;
	delete editNatura;
	delete labelFile;
	delete editFile;
	delete buttonFile;
	delete buttonCrea;
	delete layoutButton;
	delete gridLayout;
	delete progress;

	if (worker_thread)
	{
		worker_thread->quit();
		worker_thread->wait();
		delete worker_thread;
		delete generator;
	}
}

void DescrGeneratorInterface::resizeWidgets()
{
	buttonMainTemplate->setFixedSize(buttonMainTemplate->sizeHint());
	buttonResourcesTemplate->setFixedSize(buttonResourcesTemplate->sizeHint());
	buttonVideoResourcesTemplate->setFixedSize(buttonVideoResourcesTemplate->sizeHint());
	buttonVideoTemplate->setFixedSize(buttonVideoTemplate->sizeHint());
	buttonFile->setFixedSize(buttonFile->sizeHint());
}

void DescrGeneratorInterface::layoutWidgets()
{
	gridLayout->addWidget(labelMainTemplate, 0, 0, 1, 1);
	gridLayout->addWidget(editMainTemplate, 0, 1, 1, 2);
	gridLayout->addWidget(buttonMainTemplate, 0, 3, 1, 1);
	gridLayout->addWidget(labelResourcesTemplate, 1, 0, 1, 1);
	gridLayout->addWidget(editResourcesTemplate, 1, 1, 1, 2);
	gridLayout->addWidget(buttonResourcesTemplate, 1, 3, 1, 1);
	gridLayout->addWidget(labelVideoResourcesTemplate, 2, 0, 1, 1);
	gridLayout->addWidget(editVideoResourcesTemplate, 2, 1, 1, 2);
	gridLayout->addWidget(buttonVideoResourcesTemplate, 2, 3, 1, 1);
	gridLayout->addWidget(labelVideoTemplate, 3, 0, 1, 1);
	gridLayout->addWidget(editVideoTemplate, 3, 1, 1, 2);
	gridLayout->addWidget(buttonVideoTemplate, 3, 3, 1, 1);
	gridLayout->addWidget(labelCatMerc, 4, 0, 1, 1);
	gridLayout->addWidget(editCatMerc, 4, 1, 1, 1);
	gridLayout->addWidget(labelNatura, 5, 0, 1, 1);
	gridLayout->addWidget(editNatura, 5, 1, 1, 1);
	gridLayout->addWidget(labelFile, 6, 0, 1, 1);
	gridLayout->addWidget(editFile, 6, 1, 1, 2);
	gridLayout->addWidget(buttonFile, 6, 3, 1, 1);
	layoutButton->addWidget(buttonCrea);
	gridLayout->addLayout(layoutButton, 7, 0, 1, 4, Qt::AlignCenter | Qt::AlignBottom);
	gridLayout->addWidget(progress, 8, 0, 1, 4);
}

void DescrGeneratorInterface::connectWidgets()
{
	connect(buttonMainTemplate, SIGNAL(clicked()), this, SLOT(slotScegliMainTemplate()));
	connect(buttonResourcesTemplate, SIGNAL(clicked()), this, SLOT(slotScegliResourcesTemplate()));
	connect(buttonVideoResourcesTemplate, SIGNAL(clicked()), this, SLOT(slotScegliVideoResourcesTemplate()));
	connect(buttonVideoTemplate, SIGNAL(clicked()), this, SLOT(slotScegliVideoTemplate()));
	connect(buttonFile, SIGNAL(clicked()), this, SLOT(slotScegliFile()));
	connect(buttonCrea, SIGNAL(clicked()), this, SLOT(slotCrea()));
}

bool DescrGeneratorInterface::verificaCampi()
{
	if (
		verificaMainTemplate() &&
		verificaResourcesTemplate() &&
		verificaVideoResourcesTemplate() &&
		verificaVideoTemplate() &&
		verificaFile()
	)
		return true;
	return false;
}

bool DescrGeneratorInterface::verificaMainTemplate() {verifyFile(editMainTemplate, "Non hai impostato nessun main template.")}
bool DescrGeneratorInterface::verificaResourcesTemplate() {verifyFile(editResourcesTemplate, "Non hai impostato nessun resources template.")}
bool DescrGeneratorInterface::verificaVideoResourcesTemplate() {verifyFile(editVideoResourcesTemplate, "Non hai impostato nessun video resources template.")}
bool DescrGeneratorInterface::verificaVideoTemplate() {verifyFile(editVideoTemplate, "Non hai impostato nessun video template.")}
bool DescrGeneratorInterface::verificaFile() {verifyField(editFile, "Non hai impostato nessun file di destinazione.")}


void DescrGeneratorInterface::slotScegliMainTemplate() {chooseOpenFile(parentWindow, editMainTemplate, "Main Template", "*.html")}
void DescrGeneratorInterface::slotScegliResourcesTemplate() {chooseOpenFile(parentWindow, editResourcesTemplate, "Resources Template", "*.html")}
void DescrGeneratorInterface::slotScegliVideoResourcesTemplate() {chooseOpenFile(parentWindow, editVideoResourcesTemplate, "Video Resources Template", "*.html")}
void DescrGeneratorInterface::slotScegliVideoTemplate() {chooseOpenFile(parentWindow, editVideoTemplate, "Video Template", "*.html")}
void DescrGeneratorInterface::slotScegliFile() {chooseSaveFile(parentWindow, editFile, "Salva su file", "*.csv")}


void DescrGeneratorInterface::slotCrea()
{
	if (verificaCampi())
	{
		if (worker_thread == 0 && generator == 0)
		{
			saveConfig();

			openDB::BackendConnector* connector = &parentWindow->backendConnector();
			std::string cat_merc = editCatMerc->text().toStdString();
			std::string natura = editNatura->text().toStdString();
			std::string mainTemFile = editMainTemplate->text().toStdString();
			std::string resTempFile = editResourcesTemplate->text().toStdString();
			std::string videoResTempFile = editVideoResourcesTemplate->text().toStdString();
			std::string videoTempFile = editVideoTemplate->text().toStdString();
			std::string destinationFile = editFile->text().toStdString();

			generator = new DescrGenerator(connector, cat_merc, natura, mainTemFile, resTempFile, videoResTempFile, videoTempFile, destinationFile);
			worker_thread = new QThread;
			generator->moveToThread(worker_thread);

			connect (worker_thread, SIGNAL(started()), generator, SLOT(start()));
			connect (generator, SIGNAL(totalRecords(unsigned int)), this, SLOT(slotTotal(unsigned int)));
			connect (generator, SIGNAL(incProgress()), this, SLOT(slotIncProgress()));
			connect (generator, SIGNAL(done()), this, SLOT(slotDone()));

			worker_thread->start();
		}
		else
			QMessageBox::warning(this, "Attenzione!", "Il processo di generazione e' ancora in corso! Attendi o crea una nuova sessione.", QMessageBox::Ok);
	}
}

void DescrGeneratorInterface::slotDone()
{
	worker_thread->quit();
	worker_thread->wait();
	delete worker_thread;
	delete generator;
	worker_thread = 0;
	generator = 0;
}

void DescrGeneratorInterface::slotTotal(unsigned int n)
{
	progress->setValue(0);
	progress->setMaximum(n);
}
void DescrGeneratorInterface::slotIncProgress()
{
	progress->setValue(progress->value() + 1);
}


const std::string DescrGenerator::parMainTemplateResources = "RESOURCELIST";
const std::string DescrGenerator::parMainTemplateDescription = "DESCRIZIONE";
const std::string DescrGenerator::parMainTemplateVideo = "SEZVIDEO";
const std::string DescrGenerator::parResourcesUrl = "URL";
const std::string DescrGenerator::parResourcesIcon = "ICON";
const std::string DescrGenerator::parResourcesText = "TEXT";
const std::string DescrGenerator::parVideoUrl = "VIDEOURL";
const std::string DescrGenerator::queryGetPubblicati ="\
select a.codice as \"CODICE\", a.codice_mexal as codice_mexal \
from pubblicazione_articoli p join anagrafica_articoli a on p.codice_art=a.codice \
where p.sito='www.decoration.it' and p.pubblica=true";
const std::string DescrGenerator::queryGetPubblicatiCatMerc = " and a.cat_merc in ($CATMERC)";
const std::string DescrGenerator::queryGetPubblicatiNatura = " and a.natura in ($NATURA)";
const std::string DescrGenerator::queryGetRisorse = "\
select url as \"URL\", icon as \"ICON\", testo as \"TEXT\" \
from risorse_articoli \
where codice_art='$CODICE' and sito='www.decoration.it' order by progressivo";
const std::string DescrGenerator::queryGetDescrizione = "\
select descrizione as \"DESCRIZIONE\" \
from descrizioni_articoli \
where codice_art='$CODICE'";
const std::string DescrGenerator::queryGetVideo = "\
select url_video as \"VIDEOURL\" \
from video_articoli \
where codice_art='$CODICE'";
const std::string DescrGenerator::colCodice = "CODICE";
const std::string DescrGenerator::colCoMexal = "codice_mexal";
const std::string DescrGenerator::parCodice = "CODICE";
const std::string DescrGenerator::parCatMerc = "CATMERC";
const std::string DescrGenerator::parNatura = "NATURA";


DescrGenerator::DescrGenerator (
		openDB::BackendConnector* conn,
		const std::string& cat,
		const std::string& nat,
		const std::string& mainTemplate,
		const std::string& resourcesTemplate,
		const std::string& videoResourcesTemplate,
		const std::string& videoTemplate,
		const std::string& destination) :
			QObject(),
			connector(conn),
			catMerc(cat),
			natura(nat),
			mainTemplateFile(mainTemplate),
			resourcesTemplateFile(resourcesTemplate),
			videoResourcesTemplateFile(videoResourcesTemplate),
			videoTemplateFile(videoTemplate),
			destinationFile(destination)
{

}

DescrGenerator::~DescrGenerator()
{

}

void DescrGenerator::start()
{
	std::string main_template, resources_template, videores_template, video_template;
	std::fstream stream(destinationFile.c_str(), std::ios::out);

	stream <<"codice\tdescrizione\tdescrizionehtml\tdescrizionehtml2\tdescrizionehtml3\tmetakeywords\tmetadescription\t" <<std::endl;

	openDB::readTemplate(main_template, mainTemplateFile);
	openDB::readTemplate(resources_template, resourcesTemplateFile);
	openDB::readTemplate(videores_template, videoResourcesTemplateFile);
	openDB::readTemplate(video_template, videoTemplateFile);

	std::string query;
	composeQuery(query);
	openDB::NonTransactional ntxn;
	ntxn.appendCommand(query);

	openDB::BackendConnector::Iterator resPubblic = connector->execTransaction(ntxn);
	openDB::Table& pubblicati = resPubblic.result().resultTable();

	emit totalRecords(pubblicati.storage().records() - 1);

	openDB::Storage::Iterator pubb_it = pubblicati.storage().begin(), pubb_end = pubblicati.storage().end();

	for (; pubb_it != pubb_end; pubb_it++)
	{
		openDB::Record rec_pubb = *pubb_it;

		std::unordered_map<std::string, std::string> article_description, article_video, par;
		std::list<std::unordered_map<std::string, std::string>> article_resources;

		articleDescription(pubb_it, article_description);
		articleResources(pubb_it, article_resources);
		articleVideo(pubb_it, article_video);

		std::string resources, description, video_res, video;

		std::list<std::unordered_map<std::string, std::string>>::iterator	r_it = article_resources.begin(),
											r_end = article_resources.end();
		for (; r_it != r_end; r_it++)
		{
			std::string tmp = resources_template;

			openDB::prepare(tmp, *r_it);
			resources += tmp;
		}

		if (article_video.size() != 0)
		{
			video_res = videores_template;
			openDB::prepare(video_res, article_video);
			resources += video_res;

			video = video_template;
			openDB::prepare(video, article_video);
		}

		if (article_description.size() != 0)
			description = article_description.begin()->second;

		par =
		{
			{parMainTemplateDescription, description},
			{parMainTemplateResources, resources},
			{parMainTemplateVideo, video}
		};

		std::string final_description = main_template;
		openDB::prepare(final_description, par);

		deleteCharacter(final_description, "\n");
		deleteCharacter(final_description, "\t");

		stream << rec_pubb.current(colCoMexal) << "\t" << final_description << std::endl;

		emit incProgress();
	}

	connector->deleteTransaction(resPubblic);
	stream.close();
	emit done();
}

void DescrGenerator::composeQuery(std::string& query)
{
	query = queryGetPubblicati;
	std::unordered_map<std::string, std::string> queryParameters;

	if (!catMerc.empty())
	{
		query += queryGetPubblicatiCatMerc;

		std::string catValue;
		std::list<std::string> catList;
		openDB::tokenize(catList, catMerc, ";, ");
		auto it = catList.begin(), end = catList.end();
		for (; it != end; it++)
		{
			if (!catValue.empty())
				catValue += ", ";
			catValue += "'" + *it + "'";
		}

		queryParameters.insert(std::pair<std::string, std::string>(parCatMerc, catValue));

	}

	if (!natura.empty())
	{
		query += queryGetPubblicatiNatura;

		std::string natureValue;
		std::list<std::string> natureList;
		openDB::tokenize(natureList, natura, ";, ");
		auto it = natureList.begin(), end = natureList.end();
		for (; it != end; it++)
		{
			if (!natureValue.empty())
				natureValue += ", ";
			natureValue += "'" + *it + "'";
		}


		queryParameters.insert(std::pair<std::string, std::string>(parNatura, natureValue));
	}

	openDB::prepare(query, queryParameters);
}


void DescrGenerator::articleResources (	openDB::Storage::Iterator& stor_iterator,
					std::list<std::unordered_map<std::string, std::string>>& resources_list)
{
	resources_list.clear();

	openDB::Record rec_pubb = *stor_iterator;
	std::unordered_map<std::string, std::string> values_pubb, par;
	rec_pubb.current(values_pubb);

	par = {{parCodice, values_pubb[colCodice]}};
	std::string queryResources = queryGetRisorse;
	openDB::prepare(queryResources, par);

	openDB::NonTransactional ntxn_res;
	ntxn_res.appendCommand(queryResources);
	openDB::BackendConnector::Iterator	bit = connector->execTransaction(ntxn_res);
	openDB::Table&	_table = bit.result().resultTable();

	openDB::Storage::Iterator res_it = _table.storage().begin(), res_end = _table.storage().end();
	for (; res_it != res_end; res_it++)
	{
		openDB::Record rec = *res_it;
		std::unordered_map<std::string, std::string> values_tmp;
		rec.current(values_tmp);
		resources_list.push_back(values_tmp);
	}

	connector->deleteTransaction(bit);
}

void DescrGenerator::articleDescription (	openDB::Storage::Iterator& stor_iterator,
						std::unordered_map<std::string, std::string>& description)
{
	description.clear();

	openDB::Record rec_pubb = *stor_iterator;
	std::unordered_map<std::string, std::string> values_pubb, par;
	rec_pubb.current(values_pubb);

	par = {{parCodice, values_pubb[colCodice]}};
	std::string queryDescr = queryGetDescrizione;

	openDB::prepare(queryDescr, par);

	openDB::NonTransactional ntxn_descr;
	ntxn_descr.appendCommand(queryDescr);

	openDB::BackendConnector::Iterator	bit = connector->execTransaction(ntxn_descr);
	openDB::Table&	_table = bit.result().resultTable();

	if (_table.storage().begin() != _table.storage().end())
	{
		openDB::Record	descr_record = *(_table.storage().begin());
		descr_record.current(description);

	}
	connector->deleteTransaction(bit);

}

void DescrGenerator::articleVideo (	openDB::Storage::Iterator& stor_iterator,
					std::unordered_map<std::string, std::string>& video)
{
	video.clear();

	openDB::Record rec_pubb = *stor_iterator;
	std::unordered_map<std::string, std::string> values_pubb, par;
	rec_pubb.current(values_pubb);

	par = {{parCodice, values_pubb[colCodice]}};
	std::string queryVideo = queryGetVideo;
	openDB::prepare(queryVideo, par);
	openDB::NonTransactional ntxn_vid;
	ntxn_vid.appendCommand(queryVideo);
	openDB::BackendConnector::Iterator	bit = connector->execTransaction(ntxn_vid);
	openDB::Table&	_table = bit.result().resultTable();

	if (_table.storage().begin() != _table.storage().end())
	{
		openDB::Record video_record = *(_table.storage().begin());
		video_record.current(video);
	}
	connector->deleteTransaction(bit);

}

void DescrGenerator::deleteCharacter(std::string& str, const std::string& c)
{
	std::string::size_type pos;
	do
	{
		pos = str.find (c);
		if (pos != std::string::npos)
			str.erase ( pos, c.size() );
	}
	while (pos != std::string::npos );
}

const std::string DescrGeneratorInterface::descrGroupName = "description";
const std::string DescrGeneratorInterface::mainSettingName = "main_template";
const std::string DescrGeneratorInterface::resourcesSettingName = "resources_template";
const std::string DescrGeneratorInterface::videoSettingName = "video_template";
const std::string DescrGeneratorInterface::videoResSettingName = "videores_template";
const std::string DescrGeneratorInterface::naturaSettingName = "natura_";
const std::string DescrGeneratorInterface::catSettingName = "cat_merc";
const std::string DescrGeneratorInterface::outFileSettingName = "out_file";

void DescrGeneratorInterface::saveConfig()
{
	libconfig::Config cfg;
	try
	{
		// lettura del file di configurazione
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting &root = cfg.getRoot();

		// se nella root del file di configurazione non esiste il gruppo descrGroupName allora viene aggiunto
		if (!root.exists(descrGroupName))
			addSetting(cfg);
		else
		{
			// se nella root esiste un setting descrGroupName ma non e' un gruppo, viene rimosso quello esistente ed aggiunto
			// il nuovo gruppo configGroupName
			libconfig::Setting& setting = root[descrGroupName];
			if (!setting.isGroup())
			{
				root.remove(descrGroupName);
				addSetting(cfg);
			}
			else
				// se esiste il gruppo descrGroupName nella root, i suoi settaggi vengono aggiornati
				updateSetting(setting);

		}
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void DescrGeneratorInterface::loadConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(descrGroupName) &&
			root[descrGroupName].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[descrGroupName]);
	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void DescrGeneratorInterface::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting& root = cfg.getRoot();
	libconfig::Setting& setting = root.add(descrGroupName, libconfig::Setting::TypeGroup);

	setting.add(mainSettingName, libconfig::Setting::TypeString) = editMainTemplate->text().toStdString();
	setting.add(resourcesSettingName, libconfig::Setting::TypeString) = editResourcesTemplate->text().toStdString();
	setting.add(videoResSettingName, libconfig::Setting::TypeString) = editVideoResourcesTemplate->text().toStdString();
	setting.add(videoSettingName, libconfig::Setting::TypeString) = editVideoTemplate->text().toStdString();
	setting.add(naturaSettingName, libconfig::Setting::TypeString) = editNatura->text().toStdString();
	setting.add(catSettingName, libconfig::Setting::TypeString) = editCatMerc->text().toStdString();
	setting.add(outFileSettingName, libconfig::Setting::TypeString) = editFile->text().toStdString();
}

void DescrGeneratorInterface::updateSetting(libconfig::Setting& setting)
{
	setting[mainSettingName] = editMainTemplate->text().toStdString();
	setting[resourcesSettingName] = editResourcesTemplate->text().toStdString();
	setting[videoResSettingName] = editVideoResourcesTemplate->text().toStdString();
	setting[videoSettingName] = editVideoTemplate->text().toStdString();
	setting[naturaSettingName] = editNatura->text().toStdString();
	setting[catSettingName] = editCatMerc->text().toStdString();
	setting[outFileSettingName] = editFile->text().toStdString();
}

void DescrGeneratorInterface::loadSetting(libconfig::Setting& setting)
{
	editMainTemplate->setText(QString::fromStdString(setting[mainSettingName]));
	editResourcesTemplate->setText(QString::fromStdString(setting[resourcesSettingName]));
	editVideoResourcesTemplate->setText(QString::fromStdString(setting[videoResSettingName]));
	editVideoTemplate->setText(QString::fromStdString(setting[videoSettingName]));
	editNatura->setText(QString::fromStdString(setting[naturaSettingName]));
	editCatMerc->setText(QString::fromStdString(setting[catSettingName]));
	editFile->setText(QString::fromStdString(setting[outFileSettingName]));
}
