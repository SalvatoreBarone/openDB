/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __DESCR_GENERATOR__
#define __DESCR_GENERATOR__

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

#include <QObject>
#include <QWidget>
#include <QThread>

#include <string>
#include <list>
#include <unordered_map>
#include <libconfig.h++>

class QLabel;
class QLineEdit;
class QPushButton;
class QHBoxLayout;
class QGridLayout;
class QProgressBar;

class MainWindow;
class DescrGenerator;

namespace openDB
{
	class BackendConnector;
};

class DescrGeneratorInterface : public QWidget
{
	Q_OBJECT
public:
	explicit DescrGeneratorInterface (QWidget* parent, MainWindow* window);

	virtual ~DescrGeneratorInterface ();

private:
	MainWindow* parentWindow;

	QLabel* labelMainTemplate;
	QLineEdit* editMainTemplate;
	QPushButton* buttonMainTemplate;
	QLabel* labelResourcesTemplate;
	QLineEdit* editResourcesTemplate;
	QPushButton* buttonResourcesTemplate;
	QLabel* labelVideoResourcesTemplate;
	QLineEdit* editVideoResourcesTemplate;
	QPushButton* buttonVideoResourcesTemplate;
	QLabel* labelVideoTemplate;
	QLineEdit* editVideoTemplate;
	QPushButton* buttonVideoTemplate;
	QLabel* labelCatMerc;
	QLineEdit* editCatMerc;
	QLabel* labelNatura;
	QLineEdit* editNatura;
	QLabel* labelFile;
	QLineEdit* editFile;
	QPushButton* buttonFile;
	QPushButton* buttonCrea;
	QHBoxLayout* layoutButton;
	QGridLayout* gridLayout;
	QProgressBar* progress;

	void resizeWidgets();
	void layoutWidgets();
	void connectWidgets();

	bool verificaCampi();
	bool verificaMainTemplate();
	bool verificaResourcesTemplate();
	bool verificaVideoResourcesTemplate();
	bool verificaVideoTemplate();
	bool verificaFile();

	QThread* worker_thread;
	DescrGenerator* generator;

	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);
	static const std::string descrGroupName;
	static const std::string mainSettingName;
	static const std::string resourcesSettingName;
	static const std::string videoSettingName;
	static const std::string videoResSettingName;
	static const std::string naturaSettingName;
	static const std::string catSettingName;
	static const std::string outFileSettingName;

private slots:
	void slotScegliMainTemplate();
	void slotScegliResourcesTemplate();
	void slotScegliVideoResourcesTemplate();
	void slotScegliVideoTemplate();
	void slotScegliFile();
	void slotCrea();

	void slotDone();
	void slotTotal(unsigned int n);
	void slotIncProgress();
};


class DescrGenerator : public QObject
{
	Q_OBJECT
public:
	DescrGenerator (
		openDB::BackendConnector* connector,
		const std::string& cat_merc,
		const std::string& natura,
		const std::string& main_temlate_file,
		const std::string& resources_template_file,
		const std::string& video_Resources_template_file,
		const std::string& video_template_file,
		const std::string& destination_file
	);

	virtual ~DescrGenerator();

signals:
	void totalRecords(unsigned int n);

	void incProgress();

	void done();

public slots:
	void start();

private:
	static const std::string parMainTemplateResources;
	static const std::string parMainTemplateDescription;
	static const std::string parMainTemplateVideo;

	static const std::string parResourcesUrl;
	static const std::string parResourcesIcon;
	static const std::string parResourcesText;

	static const std::string parVideoUrl;

	static const std::string queryGetPubblicati;
	static const std::string queryGetPubblicatiCatMerc;
	static const std::string queryGetPubblicatiNatura;
	static const std::string colCodice;
	static const std::string colCoMexal;

	static const std::string queryGetRisorse;
	static const std::string queryGetDescrizione;
	static const std::string queryGetVideo;

	static const std::string parCodice;
	static const std::string parCatMerc;
	static const std::string parNatura;

	openDB::BackendConnector* connector;
	std::string catMerc;
	std::string natura;
	std::string mainTemplateFile;
	std::string resourcesTemplateFile;
	std::string videoResourcesTemplateFile;
	std::string videoTemplateFile;
	std::string destinationFile;

	void composeQuery(std::string& query);

	void articleResources (	openDB::Storage::Iterator& stor_iterator,
				std::list<std::unordered_map<std::string, std::string>>& resources_list);

	void articleDescription (	openDB::Storage::Iterator& stor_iterator,
					std::unordered_map<std::string, std::string>& description);

	void articleVideo (	openDB::Storage::Iterator& stor_iterator,
				std::unordered_map<std::string, std::string>& video);

	void deleteCharacter(std::string& str, const std::string& c);

};

#endif
