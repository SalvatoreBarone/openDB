/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "DownloadGenerator.hpp"
#include "MainWindow.hpp"
#include "verifyMacro.hpp"
#include "openDB2core.hpp"
#include "openDB2gui.hpp"
#include <unordered_map>
#include <fstream>
#include <QtGui>

typedef std::unordered_map<std::string, std::string> stringMap;

const std::string DownloadAreaGenerator::sqlQuery = "\
select url_destinazione as \"ENTRYURL\", url_image as \"ENTRYIMG\", testo as \"ENTRYTEXT\"\
from componenti.area_download order by testo";

const std::string DownloadAreaGenerator::entriesListParName = "ENTRYLIST";

DownloadAreaGenerator::DownloadAreaGenerator(MainWindow* window, QWidget* parent) :
	QWidget(parent),
	parentWindow(window),
	labelMainTemplate(new QLabel("Template main", this)),
	editMainTemplate(new QLineEdit(this)),
	buttonMainTemplate(new QPushButton("Apri", this)),
	labelLiTemplate(new QLabel("Template Li", this)),
	editLiTemplate(new QLineEdit(this)),
	buttonLiTemplate(new QPushButton("Apri", this)),
	labelOutfile(new QLabel("File Output", this)),
	editOutfile(new QLineEdit(this)),
	buttonOutfile(new QPushButton("Salva", this)),
	buttonCreate(new QPushButton("Crea", this)),
	layout(new QGridLayout(this))
{
	setLayout(layout);

	resizeWidgets();
	layoutWidgets();
	connectWidgets();

	loadConfig();
}

DownloadAreaGenerator::~DownloadAreaGenerator()
{
	delete labelMainTemplate;
	delete editMainTemplate;
	delete buttonMainTemplate;
	delete labelLiTemplate;
	delete editLiTemplate;
	delete buttonLiTemplate;
	delete labelOutfile;
	delete editOutfile;
	delete buttonOutfile;
	delete buttonCreate;
	delete layout;
}

void DownloadAreaGenerator::resizeWidgets()
{
	buttonMainTemplate->setFixedSize(buttonMainTemplate->sizeHint());
	buttonLiTemplate->setFixedSize(buttonLiTemplate->sizeHint());
	buttonOutfile->setFixedSize(buttonOutfile->sizeHint());
	buttonCreate->setFixedSize(buttonCreate->sizeHint());
}

void DownloadAreaGenerator::layoutWidgets()
{
	layout->addWidget(labelMainTemplate, 0, 0, 1, 1);
	layout->addWidget(editMainTemplate, 0, 1, 1, 1);
	layout->addWidget(buttonMainTemplate, 0, 2, 1, 1);
	layout->addWidget(labelLiTemplate, 1, 0, 1, 1);
	layout->addWidget(editLiTemplate, 1, 1, 1, 1);
	layout->addWidget(buttonLiTemplate, 1, 2, 1, 1);
	layout->addWidget(labelOutfile, 2, 0, 1, 1);
	layout->addWidget(editOutfile, 2, 1, 1, 1);
	layout->addWidget(buttonOutfile, 2, 2, 1, 1);
	layout->addWidget(buttonCreate, 3, 0, 1, 3, Qt::AlignCenter | Qt::AlignBottom);
}

void DownloadAreaGenerator::connectWidgets()
{
	connect(buttonMainTemplate, SIGNAL(clicked()), this, SLOT(slotChooseMainTemplate()));
	connect(buttonLiTemplate, SIGNAL(clicked()), this, SLOT(slotChooseLiTemplate()));
	connect(buttonOutfile, SIGNAL(clicked()), this, SLOT(slotChooseOutfile()));
	connect(buttonCreate, SIGNAL(clicked()), this, SLOT(slotCreate()));
}

void DownloadAreaGenerator::slotChooseMainTemplate()
{
	chooseOpenFile(parentWindow, editMainTemplate, "Template Main", "*.html")
}

void DownloadAreaGenerator::slotChooseLiTemplate()
{
	chooseOpenFile(parentWindow, editLiTemplate, "Template Li", "*.html")
}

void DownloadAreaGenerator::slotChooseOutfile()
{
	chooseSaveFile(parentWindow, editOutfile, "File Output", "*.html")
}

void DownloadAreaGenerator::slotCreate()
{
	saveConfig();

	std::string main_template, li_template, entries_list;
	openDB::readTemplate(main_template, editMainTemplate->text().toStdString());
	openDB::readTemplate(li_template, editLiTemplate->text().toStdString());

	openDB::NonTransactional txn(sqlQuery);
	openDB::BackendConnector::Iterator b_it = parentWindow->backendConnector().execTransaction(txn);
	openDB::Table& _table = b_it.result().resultTable();
	openDB::Storage::Iterator s_it = _table.storage().begin(), s_end = _table.storage().end();
	for (; s_it != s_end; s_it++)
	{
		std::string tmp = li_template;
		stringMap par;
		(*s_it).current(par);
		openDB::prepare(tmp, par);
		entries_list += tmp;
	}
	stringMap par = {{entriesListParName, entries_list}};
	openDB::prepare(main_template, par);

	std::fstream stream(editOutfile->text().toStdString().c_str(), std::ios::out);
	stream << main_template << std::endl;
	stream.close();

	parentWindow->backendConnector().deleteTransaction(b_it);
}

const std::string DownloadAreaGenerator::configGroupName = "downloads";
const std::string DownloadAreaGenerator::mainTemplSettingName = "main_template";
const std::string DownloadAreaGenerator::lineTemplSettingName = "li_template";
const std::string DownloadAreaGenerator::outFileSettingName = "out_file";

void DownloadAreaGenerator::saveConfig()
{
	libconfig::Config cfg;
	try
	{
		// lettura del file di configurazione
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting &root = cfg.getRoot();

		// se nella root del file di configurazione non esiste il gruppo descrGroupName allora viene aggiunto
		if (!root.exists(configGroupName))
			addSetting(cfg);
		else
		{
			// se nella root esiste un setting descrGroupName ma non e' un gruppo, viene rimosso quello esistente ed aggiunto
			// il nuovo gruppo configGroupName
			libconfig::Setting& setting = root[configGroupName];
			if (!setting.isGroup())
			{
				root.remove(configGroupName);
				addSetting(cfg);
			}
			else
				// se esiste il gruppo descrGroupName nella root, i suoi settaggi vengono aggiornati
				updateSetting(setting);

		}
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void DownloadAreaGenerator::loadConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(configGroupName) &&
			root[configGroupName].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[configGroupName]);
	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void DownloadAreaGenerator::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting& root = cfg.getRoot();
	libconfig::Setting& setting = root.add(configGroupName, libconfig::Setting::TypeGroup);

	setting.add(mainTemplSettingName, libconfig::Setting::TypeString) = editMainTemplate->text().toStdString();
	setting.add(lineTemplSettingName, libconfig::Setting::TypeString) = editLiTemplate->text().toStdString();
	setting.add(outFileSettingName, libconfig::Setting::TypeString) = editOutfile->text().toStdString();
}

void DownloadAreaGenerator::updateSetting(libconfig::Setting& setting)
{
	setting[mainTemplSettingName] = editMainTemplate->text().toStdString();
	setting[lineTemplSettingName] = editLiTemplate->text().toStdString();
	setting[outFileSettingName] = editOutfile->text().toStdString();
}

void DownloadAreaGenerator::loadSetting(libconfig::Setting& setting)
{
	editMainTemplate->setText(QString::fromStdString(setting[mainTemplSettingName]));
	editLiTemplate->setText(QString::fromStdString(setting[lineTemplSettingName]));
	editOutfile->setText(QString::fromStdString(setting[outFileSettingName]));
}
