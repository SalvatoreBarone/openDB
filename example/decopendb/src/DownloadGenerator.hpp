/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __DOWNLOAD_AREA_GENERATOR_H
#define __DOWNLOAD_AREA_GENERATOR_H

#include <QWidget>
#include <string>
#include <libconfig.h++>

class MainWindow;
class QLabel;
class QLineEdit;
class QPushButton;
class QGridLayout;

class DownloadAreaGenerator : public QWidget
{
	Q_OBJECT
public:
	explicit DownloadAreaGenerator(MainWindow* window, QWidget* parent);

	virtual ~DownloadAreaGenerator();

private:
	MainWindow* parentWindow;

	QLabel* labelMainTemplate;
	QLineEdit* editMainTemplate;
	QPushButton* buttonMainTemplate;
	QLabel* labelLiTemplate;
	QLineEdit* editLiTemplate;
	QPushButton* buttonLiTemplate;
	QLabel* labelOutfile;
	QLineEdit* editOutfile;
	QPushButton* buttonOutfile;
	QPushButton* buttonCreate;
	QGridLayout* layout;

	void resizeWidgets();
	void layoutWidgets();
	void connectWidgets();

	static const std::string sqlQuery;
	static const std::string entriesListParName;

	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);

	static const std::string configGroupName;
	static const std::string mainTemplSettingName;
	static const std::string lineTemplSettingName;
	static const std::string outFileSettingName;

private slots:
	void slotChooseMainTemplate();
	void slotChooseLiTemplate();
	void slotChooseOutfile();
	void slotCreate();
};

#endif
