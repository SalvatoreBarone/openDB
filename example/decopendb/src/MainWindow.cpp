/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "MainWindow.hpp"

#include <string>
#include <unordered_map>
#include <iostream>

#include <QAction>
#include <QMenu>
#include <QFileDialog>
#include <QProgressBar>
#include <QProgressDialog>
#include <QMessageBox>

#include "PgLogin.hpp"
#include "PageGenerator.hpp"
#include "TestUrl.hpp"
#include "DescrGenerator.hpp"
#include "AllArcGenerator.hpp"
#include "AmazonInsertLoader.hpp"
#include "AmazonInsertGenerator.hpp"
#include "AmazonUpdateGenerator.hpp"
#include "EbayInsertLoader.hpp"
#include "EbayInsertGenerator.hpp"
#include "EbayUpdateGenerator.hpp"
#include "CatalogsPageGenerator.hpp"
#include "TestResourcesUrl.hpp"
#include "MenuGenerator.hpp"
#include "DownloadGenerator.hpp"
#include "SliderGenerator.hpp"
#include "AlldecorImgTest.hpp"

MainWindow::MainWindow (QWidget *parent, Qt::WindowFlags flags) : openDB::MainWindow(parent, flags)
{
	modulesConfiguration = workspaceDirectory.toStdString() + "decoration.cfg";
}

MainWindow::~MainWindow()
{
	//deleteToolMenu();
}

void MainWindow::slotLogin()
{
	pgLoginWidget = new PgLogin(database, connector, this, this);
	setCentralWidget(pgLoginWidget);
	connect(pgLoginWidget, SIGNAL(logged()), this, SLOT(slotConnesso()));
	connect(pgLoginWidget, SIGNAL(cancel()), this, SLOT(close()));
}

void MainWindow::createToolMenu()
{
	openDB::MainWindow::createToolMenu();

	createMenuAlldecor();
	createMenuAmazon();
	createMenuDecoration();
//	createMenuEbay();
}

void MainWindow::deleteToolMenu()
{
	if (toolMenu)
	{
		openDB::MainWindow::deleteToolMenu();

		deleteMenuAlldecor();
		deleteMenuDecoration();
		deleteMenuAmazon();
//		deleteMenuEbay();
	}
}


void MainWindow::createMenuAlldecor()
{
	menuAlldecor = new QMenu("Alldecor", this);
	actionCreaTracciati = new QAction("Genera tracciati", this);
	actionCreateCatalogs1 = new QAction("Genera Cataloghi", this);
	actionTestImg = new QAction("Test Immagini", this);

	actionCreateUpdaterGiacenze = new QAction("Crea modello aggiornamento giacenze", this);
	actionImportUpdaterGiacenze = new QAction("Importa modello aggiornamento giacenze", this);

	menuAlldecor->addAction(actionCreaTracciati);
	menuAlldecor->addAction(actionCreateCatalogs1);
	menuAlldecor->addAction(actionTestImg);
	menuAlldecor->addAction(actionCreateUpdaterGiacenze);
	menuAlldecor->addAction(actionImportUpdaterGiacenze);

	actionImportUpdaterGiacenze->setEnabled(false);

	connect (actionCreaTracciati, SIGNAL(triggered()), this, SLOT(slotCreaTracciati()));
	connect (actionCreateCatalogs1, SIGNAL(triggered()), this, SLOT(slotCreateCatalogs()));
	connect (actionTestImg, SIGNAL(triggered()), this, SLOT(slotTestImg()));
	connect (actionCreateUpdaterGiacenze, SIGNAL(triggered()), this, SLOT(slotCreateUpdaterGiacenze()));
	connect (actionImportUpdaterGiacenze, SIGNAL(triggered()), this, SLOT(slotImportUpdaterGiacenze()));

	toolMenu->addMenu(menuAlldecor);

	tableGiacenze = 0;
	updaterGiacenze = 0;
}

void MainWindow::deleteMenuAlldecor()
{
	delete menuAlldecor;
	delete actionCreaTracciati;
	delete actionCreateCatalogs1;
	delete actionTestImg;
	delete actionCreateUpdaterGiacenze;
	delete actionImportUpdaterGiacenze;

	if (tableGiacenze) delete tableGiacenze;
	if (updaterGiacenze) delete updaterGiacenze;
}

void MainWindow::createMenuDecoration()
{
	menuDecoration = new QMenu("Decoration", this);
	actionTestUrl = new QAction("Testa Url", this);
	actionTestResources = new QAction("Test Url Risorse", this);
	actionCreaGuardaAltri = new QAction("Genera pagine Guarda Altri", this);
	actionGeneraDescrizioni = new QAction("Genera Descrizioni", this);
	actionCreateCatalogs2 = new QAction("Genera Cataloghi", this);
	actionMenuGenerator = new QAction("Genera Menu", this);
	actionDownloadArea = new QAction("Genera Download Area", this);
	actionSliderGenerator = new QAction("Genera Slider", this);
	menuDecoration->addAction(actionTestUrl);
	menuDecoration->addAction(actionTestResources);
	menuDecoration->addAction(actionCreaGuardaAltri);
	menuDecoration->addAction(actionGeneraDescrizioni);
	menuDecoration->addAction(actionCreateCatalogs2);
	menuDecoration->addAction(actionMenuGenerator);
	menuDecoration->addAction(actionDownloadArea);
	menuDecoration->addAction(actionSliderGenerator);
	connect (actionTestUrl, SIGNAL(triggered()), this, SLOT(slotTestUrl()));
	connect (actionTestResources, SIGNAL(triggered()), this, SLOT(slotTestResources()));
	connect (actionCreaGuardaAltri, SIGNAL(triggered()), this, SLOT(slotCreaGuardaAltri()));
	connect (actionGeneraDescrizioni, SIGNAL(triggered()), this, SLOT(slotGeneraDescrizioni()));
	connect (actionCreateCatalogs2, SIGNAL(triggered()), this, SLOT(slotCreateCatalogs()));
	connect (actionMenuGenerator, SIGNAL(triggered()), this, SLOT(slotGeneraMenu()));
	connect (actionDownloadArea, SIGNAL(triggered()), this, SLOT(slotDownloadArea()));
	connect (actionSliderGenerator, SIGNAL(triggered()), this, SLOT(slotSliderGenerator()));
	toolMenu->addMenu(menuDecoration);
}

void MainWindow::deleteMenuDecoration()
{
	delete menuDecoration;
	delete actionTestUrl;
	delete actionTestResources;
	delete actionCreaGuardaAltri;
	delete actionGeneraDescrizioni;
	delete actionCreateCatalogs2;
	delete actionMenuGenerator;
}

void MainWindow::createMenuAmazon()
{
	menuAmazon = new QMenu("Amazon", this);
	actionAmazonLoader = new QAction("Recupera informazioni inserimento", this);
	actionAmazonInsert = new QAction("Genera template inserimento", this);
	actionAmazonUpdate = new QAction("Genera template aggiornamento", this);
	menuAmazon->addAction(actionAmazonLoader);
	menuAmazon->addAction(actionAmazonInsert);
	menuAmazon->addAction(actionAmazonUpdate);
	connect (actionAmazonLoader, SIGNAL(triggered()), this, SLOT(slotAmazonLoader()));
	connect (actionAmazonInsert, SIGNAL(triggered()), this, SLOT(slotAmazonInsert()));
	connect (actionAmazonUpdate, SIGNAL(triggered()), this, SLOT(slotAmazonUpdate()));
	toolMenu->addMenu(menuAmazon);

}

void MainWindow::deleteMenuAmazon()
{
	delete menuAmazon;
	delete actionAmazonLoader;
	delete actionAmazonInsert;
	delete actionAmazonUpdate;
}

//void MainWindow::createMenuEbay()
//{
//	menuEbay = new QMenu("Ebay", this);
//	actionEbayLoader = new QAction("Recupera indormazioni inserimento", this);
//	actionEbayInsert = new QAction("Genera template inserimento", this);
//	actionEbayUpdate = new QAction("Genera template aggiornamento", this);
//	menuEbay->addAction(actionEbayLoader);
//	menuEbay->addAction(actionEbayInsert);
//	menuEbay->addAction(actionEbayUpdate);
//	connect(actionEbayLoader, SIGNAL(triggered()), this, SLOT(slotEbayLoader()));
//	connect(actionEbayInsert, SIGNAL(triggered()), this, SLOT(slotEbayInsert()));
//	connect(actionEbayUpdate, SIGNAL(triggered()), this, SLOT(slotEbayUpdate()));
//	toolMenu->addMenu(menuEbay);
//}

//void MainWindow::deleteMenuEbay()
//{
//	delete menuEbay;
//	delete actionEbayLoader;
//	delete actionEbayInsert;
//	delete actionEbayUpdate;
//}


void MainWindow::slotCreaTracciati()
{
	AllArcGeneratorInterface* generator = new AllArcGeneratorInterface(this, this);
	setSessionWidget("Crea tracciati", generator);
}

void MainWindow::slotCreaGuardaAltri()
{
	PageGenerator* pageGenerator = new PageGenerator(this, this);
	setSessionWidget("Crea guarda altri", pageGenerator);
}

void MainWindow::slotTestImg()
{
	AlldecorImgTest* tester = new AlldecorImgTest(this, this);
	setSessionWidget("Test Immagini alldecor", tester);
}

void MainWindow::slotTestUrl()
{
	openDB::Schema& schema = *(database->at("public"));
	openDB::Table& table = *(schema.at("url_articoli"));
	TestUrl* testWidget = new TestUrl(this, this, &table);
	setSessionWidget("Test url", testWidget);
}

void MainWindow::slotTestResources()
{
	openDB::Schema& schema = *(database->at("public"));
	openDB::Table& table = *(schema.at("risorse_articoli"));
	TestResourcesUrl* tester = new TestResourcesUrl(this, &table);
	setSessionWidget("Test url risorse", tester);
}

void MainWindow::slotCreateUpdaterGiacenze()
{
	QString fileName = QFileDialog::getSaveFileName(	this,
								"Crea modello di aggiornamento giacenze",
								getLastDir(),
								"*.csv");
	setLastDir(fileName);

	if (!fileName.isEmpty())
	{
		if (tableGiacenze)
			delete tableGiacenze;

		if (updaterGiacenze)
			delete updaterGiacenze;

		tableGiacenze = new openDB::Table("giacenze_magazzino");
		tableGiacenze->addColumn("codice_art", openDB::Varchar(32), true);
		tableGiacenze->addColumn("giacenza", openDB::Smallint());
		updaterGiacenze = new openDB::CsvUpdateManager(*tableGiacenze);

		openDB::CsvUpdateManager::Setting setting;
		openDB::CsvExportSettingDialog dialog(&setting, this);
		dialog.exec();
		updaterGiacenze->model(fileName.toStdString(), setting);

		actionImportUpdaterGiacenze->setEnabled(true);
	}
}

void MainWindow::slotImportUpdaterGiacenze()
{
	if (tableGiacenze && updaterGiacenze)
	{
		QString fileName = QFileDialog::getOpenFileName(	this,
									"Importa modello di aggiornamento giacenze",
									getLastDir(),
									"*.csv");
		setLastDir(fileName);

		if (!fileName.isEmpty())
		{
			openDB::CsvUpdateManager::Setting setting;
			openDB::CsvExportSettingDialog dialog(&setting, this);
			dialog.exec();

			try
			{
				std::list<std::string> log;
				updaterGiacenze->parse(fileName.toStdString(), log, true, setting);
				if (log.size() != 0 && QMessageBox::warning(	this,
										"Errore durante l'import",
										"Si sono verificati degli errori durante l'import.\nVuoi salvare il log su file?",
										QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
				{
					QString logFileName =  QFileDialog::getSaveFileName (	this,
												"Salva Log",
												"log.txt",
												"txt file (*.txt)" );
					if ( !logFileName.isEmpty() )
					{
						std::fstream logstream(logFileName.toStdString().c_str(), std::ios::out);
						std::list<std::string>::const_iterator it = log.begin(), end = log.end();
						for (; it != end; it++)
							logstream << *it <<std::endl;
						logstream.close();
					}
				}
				delete updaterGiacenze;
				updaterGiacenze = 0;

				openDB::Transaction txn;
				openDB::Storage::ConstIterator	stor_it = tableGiacenze->storage().cbegin(),
								stor_end = tableGiacenze->storage().cend();
				for (; stor_it != stor_end; stor_it++)
				{
					openDB::Record rec = *stor_it;
					std::string command =	"update giacenze_magazzino set giacenza=" + rec.current("giacenza") +
								", aggiornamento=current_timestamp::timestamp(0) where codice_art ='" + rec.current("codice_art") + "';";
					txn.appendCommand(command);
				}
				connector->addTransaction(txn);

				actionImportUpdaterGiacenze->setEnabled(false);

				delete tableGiacenze;
				tableGiacenze = 0;
			}
			catch (openDB::CsvUpdateManagerException& e)
			{
				QMessageBox::warning(	this,
						"Errore durante l'import",
						e.what(),
						QMessageBox::Ok);
			}
		}
	}
}

void MainWindow::slotGeneraDescrizioni()
{
	DescrGeneratorInterface* descrGenerator = new DescrGeneratorInterface(this, this);
	setSessionWidget("Genera descrizioni", descrGenerator);
}

void MainWindow::slotCreateCatalogs()
{
	CatalogGeneratorInterface* generator = new CatalogGeneratorInterface(this, this);
	setSessionWidget("Genera Cataloghi", generator);
}

void MainWindow::slotGeneraMenu()
{
	MenuGeneratorInterface* interface = new MenuGeneratorInterface(this, this);
	setSessionWidget("Genera Menu'", interface);
}

void MainWindow::slotDownloadArea()
{
	DownloadAreaGenerator* generator = new DownloadAreaGenerator(this, this);
	setSessionWidget("Genera Area Download", generator);
}

void MainWindow::slotSliderGenerator()
{
	SliderGenerator* generator = new SliderGenerator(this, this);
	setSessionWidget("Genera Slider", generator);
}

void MainWindow::slotAmazonLoader()
{
	openDB::Schema& schema = *(database->at("public"));
	openDB::Table& table = *(schema.at("articoli_amazon"));
	AmazonInsertLoader* loader = new AmazonInsertLoader(this, &table, this);
	setSessionWidget("Amazon Loader", loader);
}

void MainWindow::slotAmazonInsert()
{
	openDB::Schema& schema = *(database->at("public"));
	openDB::Table& table = *(schema.at("articoli_amazon"));
	AmazonInsertGenerator* generator = new AmazonInsertGenerator(this, &table, this);
	setSessionWidget("Amazon Insert Template", generator);
}

void MainWindow::slotAmazonUpdate()
{
	AmazonUpdateGenerator* updater = new AmazonUpdateGenerator(this, this);
	setSessionWidget("Amazon Update Template", updater);
}

//void MainWindow::slotEbayLoader() {}

//void MainWindow::slotEbayInsert() {}

//void MainWindow::slotEbayUpdate() {}


