/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef APPSPEC_MAIN_WINDOW_H
#define APPSPEC_MAIN_WINDOW_H

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

#include <string>

class QMenu;
class PgLogin;

class MainWindow : public openDB::MainWindow
{
	Q_OBJECT
public:
	explicit MainWindow (QWidget *parent = 0, Qt::WindowFlags flags = 0);

	virtual ~MainWindow();

	const std::string& modulesConfigFile() const noexcept
		{return modulesConfiguration;}

private:
	std::string modulesConfiguration;
	PgLogin* pgLoginWidget;

	virtual void createToolMenu();
	virtual void deleteToolMenu();

	QMenu* menuAlldecor;
	QAction* actionCreaTracciati;
	QAction* actionCreateCatalogs1;
	QAction* actionTestImg;
	QAction* actionCreateUpdaterGiacenze;
	QAction* actionImportUpdaterGiacenze;
	openDB::Table* tableGiacenze;
	openDB::CsvUpdateManager* updaterGiacenze;
	void createMenuAlldecor();
	void deleteMenuAlldecor();

	QMenu* menuDecoration;
	QAction* actionTestUrl;
	QAction* actionTestResources;
	QAction* actionCreaGuardaAltri;
	QAction* actionGeneraDescrizioni;
	QAction* actionCreateCatalogs2;
	QAction* actionMenuGenerator;
	QAction* actionDownloadArea;
	QAction* actionSliderGenerator;
	void createMenuDecoration();
	void deleteMenuDecoration();

	QMenu* menuAmazon;
	QAction* actionAmazonLoader;
	QAction* actionAmazonInsert;
	QAction* actionAmazonUpdate;
	void createMenuAmazon();
	void deleteMenuAmazon();

//	QMenu* menuEbay;
//	QAction* actionEbayLoader;
//	QAction* actionEbayInsert;
//	QAction* actionEbayUpdate;
//	void createMenuEbay();
//	void deleteMenuEbay();


private slots:
	virtual void slotLogin();
	void slotCreaTracciati();
	void slotCreateCatalogs();
	void slotTestImg();
	void slotCreateUpdaterGiacenze();
	void slotImportUpdaterGiacenze();

	void slotTestUrl();
	void slotTestResources();
	void slotCreaGuardaAltri();
	void slotGeneraDescrizioni();
	void slotGeneraMenu();
	void slotDownloadArea();
	void slotSliderGenerator();

	void slotAmazonLoader();
	void slotAmazonInsert();
	void slotAmazonUpdate();

//	void slotEbayLoader();
//	void slotEbayInsert();
//	void slotEbayUpdate();
};

#endif
