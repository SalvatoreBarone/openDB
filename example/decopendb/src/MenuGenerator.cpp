/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "MenuGenerator.hpp"
#include "MainWindow.hpp"
#include "verifyMacro.hpp"


#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>


#include <iostream>
#include <fstream>

MenuGeneratorInterface::MenuGeneratorInterface(MainWindow* window, QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		labelDesktopMain(new QLabel("Desktop main tmpl", this)),
		editDesktopMain(new QLineEdit(this)),
		buttonDesktopMain(new QPushButton("Apri", this)),
		labelDesktopLiWC(new QLabel("Desktop li tmpl child", this)),
		editDesktopLiWC(new QLineEdit(this)),
		buttonDesktopLiWC(new QPushButton("Apri", this)),
		labelDesktopLiWOC(new QLabel("Desktop li tmpl no-child", this)),
		editDesktopLiWOC(new QLineEdit(this)),
		buttonDesktopLiWOC(new QPushButton("Apri", this)),
		labelMobileMain(new QLabel("Mobile main tmpl", this)),
		editMobileMain(new QLineEdit(this)),
		buttonMobileMain(new QPushButton("Apri", this)),
		labelMobileLiWC(new QLabel("Mobile li tmpl child", this)),
		editMobileLiWC(new QLineEdit(this)),
		buttonMobileLiWC(new QPushButton("Apri", this)),
		labelMobileLiWOC(new QLabel("Mobile li tmpl no-child", this)),
		editMobileLiWOC(new QLineEdit(this)),
		buttonMobileLiWOC(new QPushButton("Apri", this)),
		labelOutfileDesktop(new QLabel("Desktop outfile", this)),
		editOutfileDesktop(new QLineEdit(this)),
		buttonOutfileDesktop(new QPushButton("Apri", this)),
		labelOutfileMobile(new QLabel("Mobile outfile", this)),
		editOutfileMobile(new QLineEdit(this)),
		buttonOutfileMobile(new QPushButton("Apri", this)),
		buttonCreate(new QPushButton("Ok", this)),
		layout(new QGridLayout(this)),
		progDialog(0),
		desktopGenerator(0),
		mobileGenerator(0)
{
	setLayout(layout);

	resizeWidgets();
	layoutWidgets();
	connectWidgets();

	loadConfig();
}

void MenuGeneratorInterface::resizeWidgets()
{
	buttonDesktopMain->setFixedSize(buttonDesktopMain->sizeHint());
	buttonDesktopLiWC->setFixedSize(buttonDesktopLiWC->sizeHint());
	buttonDesktopLiWOC->setFixedSize(buttonDesktopLiWOC->sizeHint());
	buttonMobileMain->setFixedSize(buttonMobileMain->sizeHint());
	buttonMobileLiWC->setFixedSize(buttonMobileLiWC->sizeHint());
	buttonMobileLiWOC->setFixedSize(buttonMobileLiWOC->sizeHint());
	buttonOutfileDesktop->setFixedSize(buttonOutfileDesktop->sizeHint());
	buttonOutfileMobile->setFixedSize(buttonOutfileMobile->sizeHint());
	buttonCreate->setFixedSize(buttonCreate->sizeHint());
}

void MenuGeneratorInterface::layoutWidgets()
{
	layout->addWidget(labelDesktopMain, 0, 0, 1, 1);
	layout->addWidget(editDesktopMain, 0, 1, 1, 1);
	layout->addWidget(buttonDesktopMain, 0, 2, 1, 1);
	layout->addWidget(labelDesktopLiWC, 1, 0, 1, 1);
	layout->addWidget(editDesktopLiWC, 1, 1, 1, 1);
	layout->addWidget(buttonDesktopLiWC, 1, 2, 1, 1);
	layout->addWidget(labelDesktopLiWOC, 2, 0, 1, 1);
	layout->addWidget(editDesktopLiWOC, 2, 1, 1, 1);
	layout->addWidget(buttonDesktopLiWOC, 2, 2, 1, 1);
	layout->addWidget(labelMobileMain, 3, 0, 1, 1);
	layout->addWidget(editMobileMain, 3, 1, 1, 1);
	layout->addWidget(buttonMobileMain, 3, 2, 1, 1);
	layout->addWidget(labelMobileLiWC, 4, 0, 1, 1);
	layout->addWidget(editMobileLiWC, 4, 1, 1, 1);
	layout->addWidget(buttonMobileLiWC, 4, 2, 1, 1);
	layout->addWidget(labelMobileLiWOC, 5, 0, 1, 1);
	layout->addWidget(editMobileLiWOC, 5, 1, 1, 1);
	layout->addWidget(buttonMobileLiWOC, 5, 2, 1, 1);
	layout->addWidget(labelOutfileDesktop, 6, 0, 1, 1);
	layout->addWidget(editOutfileDesktop, 6, 1, 1, 1);
	layout->addWidget(buttonOutfileDesktop, 6, 2, 1, 1);
	layout->addWidget(labelOutfileMobile, 7, 0, 1, 1);
	layout->addWidget(editOutfileMobile, 7, 1, 1, 1);
	layout->addWidget(buttonOutfileMobile, 7, 2, 1, 1);
	layout->addWidget(buttonCreate, 8, 0, 1, 4, Qt::AlignCenter | Qt::AlignBottom);
}

void MenuGeneratorInterface::connectWidgets()
{
	connect(buttonDesktopMain, SIGNAL(clicked()), this, SLOT(slotChooseDesktopMain()));
	connect(buttonDesktopLiWC, SIGNAL(clicked()), this, SLOT(slotChooseDesktopLiWC()));
	connect(buttonDesktopLiWOC, SIGNAL(clicked()), this, SLOT(slotChooseDesktopLiWOC()));
	connect(buttonMobileMain, SIGNAL(clicked()), this, SLOT(slotChooseMobileMain()));
	connect(buttonMobileLiWC, SIGNAL(clicked()), this, SLOT(slotChooseMobileLiWC()));
	connect(buttonMobileLiWOC, SIGNAL(clicked()), this, SLOT(slotChooseMobileLiWOC()));
	connect(buttonOutfileDesktop, SIGNAL(clicked()), this, SLOT(slotChooseOutfileDesktop()));
	connect(buttonOutfileMobile, SIGNAL(clicked()), this, SLOT(slotChooseOutfileMobile()));
	connect(buttonCreate, SIGNAL(clicked()), this, SLOT(slotCreate()));
}


MenuGeneratorInterface::~MenuGeneratorInterface()
{
	delete labelDesktopMain;
	delete editDesktopMain;
	delete buttonDesktopMain;
	delete labelDesktopLiWC;
	delete editDesktopLiWC;
	delete buttonDesktopLiWC;
	delete labelDesktopLiWOC;
	delete editDesktopLiWOC;
	delete buttonDesktopLiWOC;
	delete labelMobileMain;
	delete editMobileMain;
	delete buttonMobileMain;
	delete labelMobileLiWC;
	delete editMobileLiWC;
	delete buttonMobileLiWC;
	delete labelMobileLiWOC;
	delete editMobileLiWOC;
	delete buttonMobileLiWOC;
	delete labelOutfileDesktop;
	delete editOutfileDesktop;
	delete buttonOutfileDesktop;
	delete labelOutfileMobile;
	delete editOutfileMobile;
	delete buttonOutfileMobile;
	delete buttonCreate;
	delete layout;

	if (desktopGenerator)
		delete desktopGenerator;

	if (mobileGenerator)
		delete mobileGenerator;

	if (progDialog)
		delete progDialog;
}

void MenuGeneratorInterface::slotChooseDesktopMain() {chooseOpenFile(parentWindow, editDesktopMain, "Desktop Main", "*.html")}
void MenuGeneratorInterface::slotChooseDesktopLiWC() {chooseOpenFile(parentWindow, editDesktopLiWC, "Desktop li with child", "*.html")}
void MenuGeneratorInterface::slotChooseDesktopLiWOC() {chooseOpenFile(parentWindow, editDesktopLiWOC, "Desktop li without child", "*.html")}
void MenuGeneratorInterface::slotChooseMobileMain() {chooseOpenFile(parentWindow, editMobileMain, "Mobile Main", "*.html")}
void MenuGeneratorInterface::slotChooseMobileLiWC() {chooseOpenFile(parentWindow, editMobileLiWC, "Mobile li with child",  "*.html")}
void MenuGeneratorInterface::slotChooseMobileLiWOC() {chooseOpenFile(parentWindow, editMobileLiWOC, "Mobile li with child", "*.html")}
void MenuGeneratorInterface::slotChooseOutfileDesktop() {chooseSaveFile(parentWindow, editOutfileDesktop, "Desktop Outfile", "*.html")}
void MenuGeneratorInterface::slotChooseOutfileMobile() {chooseSaveFile(parentWindow, editOutfileMobile, "Mobile Outfile", "*.html")}

void MenuGeneratorInterface::slotCreate()
{
	if (verifyFields())
	{
		if (
			desktopGenerator == 0 && mobileGenerator == 0
		)
		{
			saveConfig();

			progDialog = new QProgressDialog("Attendi...", "Annulla", 0, 0, this);
			progDialog->setCancelButton(0);
			progDialog->show();

			openDB::BackendConnector* connector = &parentWindow->backendConnector();

			desktopGenerator = new MenuGenerator(
							connector,
							editDesktopMain->text().toStdString(),
							editDesktopLiWC->text().toStdString(),
							editDesktopLiWOC->text().toStdString(),
							editOutfileDesktop->text().toStdString());

			mobileGenerator = new MenuGenerator (
							connector,
							editMobileMain->text().toStdString(),
							editMobileLiWC->text().toStdString(),
							editMobileLiWOC->text().toStdString(),
							editOutfileMobile->text().toStdString());

			desktopGenerator->start();
			mobileGenerator->start();

			delete progDialog;
			delete desktopGenerator;
			delete mobileGenerator;
			desktopGenerator = 0;
			mobileGenerator = 0;
			progDialog = 0;
		}
		else
			QMessageBox::warning(
				this,
				"Attenzione!",
				"Il processo di e' ancora in corso! Attendi o crea una nuova sessione.",
				QMessageBox::Ok);
	}
}

bool MenuGeneratorInterface::verifyFields()
{
	if
	(
		verifyDeskMain() &&
		verifyDeskLiWC() &&
		verifyDeskLiWOC() &&
		verifyMobileMain() &&
		verifyMobileLiWC() &&
		verifyMobileLiWOC() &&
		verifyDeskOutfile() &&
		verifyMobileOutfile()
	)
		return true;
	return false;
}
bool MenuGeneratorInterface::verifyDeskMain() {verifyFile(editDesktopMain, "Imposta il Desktop Main Template")}
bool MenuGeneratorInterface::verifyDeskLiWC() {verifyFile(editDesktopLiWC, "Imposta il Desktop Li WC Template")}
bool MenuGeneratorInterface::verifyDeskLiWOC() {verifyFile(editDesktopLiWOC, "Imposta il Desktop Li WOC Template")}
bool MenuGeneratorInterface::verifyMobileMain() {verifyFile(editMobileMain, "Imposta il Mobile Main Template")}
bool MenuGeneratorInterface::verifyMobileLiWC() {verifyFile(editMobileLiWC, "Imposta il Mobile WC Template")}
bool MenuGeneratorInterface::verifyMobileLiWOC() {verifyFile(editMobileLiWOC, "Imposta il Mobile WOC Template")}
bool MenuGeneratorInterface::verifyDeskOutfile() {verifyField(editOutfileDesktop, "Imposta il Desktop Outfile")}
bool MenuGeneratorInterface::verifyMobileOutfile() {verifyField(editOutfileMobile, "Imposta il Mobile Outfile")}

// comando sql per ottenere le voci principali del menu
const std::string MenuGenerator::getMainEntries_sql = "\
select id_voce as \"ENTRYCODE\", url_destinazione as \"ENTRYURL\", url_image as \"ENTRYIMG\", testo as \"ENTRYTEXT\" \
from componenti.main_menu \
where id_voce_padre is null";

// comando sql per ottenere le sotto-voci di una voce del menu
const std::string MenuGenerator::getSubEntries_sql = "\
select id_voce as \"ENTRYCODE\", url_destinazione as \"ENTRYURL\", url_image as \"ENTRYIMG\", testo as \"ENTRYTEXT\" \
from componenti.main_menu \
where id_voce_padre = '$ENTRYCODE'";

// nome del parametro attraverso il quale specificare il codice di una voce "padre"
const std::string MenuGenerator::getSubEntries_codePar = "ENTRYCODE";

const std::string MenuGenerator::entryCodeColumn = "ENTRYCODE"; // nome della colonna contente il codice di una voce menu
const std::string MenuGenerator::entryUrlColumn = "ENTRYURL"; // nome della colonna contenente l'url di destinazione di una voce menu
const std::string MenuGenerator::entryImgColumn = "ENTRYIMG"; // nome della colonna contenente l'immagine associata ad una voce del menu
const std::string MenuGenerator::entryTextColumn = "ENTRYTEXT"; // nome della colonna contenente il testo di una voce del menu

const std::string MenuGenerator::entriesListPar = "ENTRYLIST"; // nome del parametro "lista di entries" nei template con figli

MenuGenerator::MenuGenerator(
		openDB::BackendConnector* connector,
		const std::string& file_template_main,
		const std::string& file_template_li_wc,
		const std::string& file_template_li_woc,
		const std::string& file_output) :
			QObject(),
			backConnector(connector),
			outfile(file_output)
{
	openDB::readTemplate(template_main, file_template_main); // lettura da file template principale
	openDB::readTemplate(template_li_wc, file_template_li_wc);  // lettura da file template li con figli
	openDB::readTemplate(template_li_woc, file_template_li_woc); // lettura da file template li senza figli
}

MenuGenerator::~MenuGenerator()
{
}

void MenuGenerator::start()
{
	// interrogazione per ottenere le voci del menu' principali
	openDB::NonTransactional ntxn(getMainEntries_sql);
	openDB::BackendConnector::Iterator main_it = backConnector->execTransaction(ntxn);
	openDB::Table& _mainEntries = main_it.result().resultTable();

	openDB::Storage::Iterator 	it = _mainEntries.storage().begin(),
					end = _mainEntries.storage().end();
	std::string entryList; // conterra' tutte le voci generate

	for (; it != end; it++)
	{
		stringMap entry;
		(*it).current(entry);
		entryList += buildEntries(entry);
	}

	backConnector->deleteTransaction(main_it);

	stringMap par = {{entriesListPar, entryList}};
	openDB::prepare (template_main, par);

	std::fstream stream (outfile.c_str(), std::ios::out);
	stream << template_main << std::endl;
	stream.close();
	emit done();
}

openDB::BackendConnector::Iterator MenuGenerator::getSubEntries(const std::string& parentEntryCode)
{
	std::string sqlCommand = getSubEntries_sql;
	stringMap par = {{getSubEntries_codePar, parentEntryCode}};
	openDB::prepare(sqlCommand, par);

	openDB::NonTransactional ntxn(sqlCommand);
	openDB::BackendConnector::Iterator it = backConnector->execTransaction(ntxn);
	return it;
}

std::string MenuGenerator::buildEntries(const stringMap& entry)
{
	std::string entryList;

	openDB::BackendConnector::Iterator sub_it = getSubEntries(entry.at(entryCodeColumn));
	openDB::Table& _subEntry = sub_it.result().resultTable();

	if (_subEntry.storage().records()  == 0)
	{
		// questa entry non ha delle sotto-entry
		entryList = template_li_woc;
		openDB::prepare(entryList, entry);
	}
	else
	{
		// questa entry ha delle sotto-entry
		entryList = template_li_wc;
		// costruzione della lista di sub-entry
		openDB::Storage::Iterator	it = _subEntry.storage().begin(),
						end = _subEntry.storage().end();
		std::string subEntryStr;
		for (; it != end; it++)
		{
			stringMap values;
			(*it).current(values);
			// le sotto-entry verranno costruite ricorsivamente
			subEntryStr += buildEntries(values);
		}
		stringMap par = entry;
		par.insert (stringPair(entriesListPar, subEntryStr));
		openDB::prepare(entryList, par);
	}

	backConnector->deleteTransaction(sub_it);
	return entryList;
}

const std::string MenuGeneratorInterface::configGroupName = "menu";
const std::string MenuGeneratorInterface::desktopMainSettingName = "desktop_main";
const std::string MenuGeneratorInterface::desktopLiWCSettingName = "desktop_li_wc";
const std::string MenuGeneratorInterface::desktopLiWOCSettingName = "desktop_li_woc";
const std::string MenuGeneratorInterface::mobileMainSettingName = "mobile_main";
const std::string MenuGeneratorInterface::mobileLiWCSettingName = "mobile_li_wc";
const std::string MenuGeneratorInterface::mobileLiWOCSettingName = "mobile_li_woc";
const std::string MenuGeneratorInterface::desktopOutFileSettingName = "desktop_out";
const std::string MenuGeneratorInterface::mobileOutFileSettingName = "mobile_out";

void MenuGeneratorInterface::saveConfig()
{
	libconfig::Config cfg;

	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();

		if (!root.exists(configGroupName))
			addSetting(cfg);
		else
		{
			libconfig::Setting& setting = root[configGroupName];
			if (!setting.isGroup())
			{
				root.remove(configGroupName);
				addSetting(cfg);
			}
			else
				updateSetting(setting);
		}
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());

	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parsing error alla linea " + std::to_string(e.getLine()) + ":\n\t" + e.getError() + "\n(" + e.what() + ").";
		QMessageBox::warning(this, "Parsing error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void MenuGeneratorInterface::loadConfig()
{
	libconfig::Config cfg;

	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(configGroupName) &&
			root[configGroupName].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[configGroupName]);
	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parsing error alla linea " + std::to_string(e.getLine()) + ":\n\t" + e.getError() + "\n(" + e.what() + ").";
		QMessageBox::warning(this, "Parsing error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}

}

void MenuGeneratorInterface::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting& root = cfg.getRoot();
	libconfig::Setting& setting = root.add(configGroupName, libconfig::Setting::TypeGroup);

	setting.add(desktopMainSettingName, libconfig::Setting::TypeString) = editDesktopMain->text().toStdString();
	setting.add(desktopLiWCSettingName, libconfig::Setting::TypeString) = editDesktopLiWC->text().toStdString();
	setting.add(desktopLiWOCSettingName, libconfig::Setting::TypeString) = editDesktopLiWOC->text().toStdString();
	setting.add(mobileMainSettingName, libconfig::Setting::TypeString) = editMobileMain->text().toStdString();
	setting.add(mobileLiWCSettingName, libconfig::Setting::TypeString) = editMobileLiWC->text().toStdString();
	setting.add(mobileLiWOCSettingName, libconfig::Setting::TypeString) = editMobileLiWOC->text().toStdString();
	setting.add(desktopOutFileSettingName, libconfig::Setting::TypeString) = editOutfileDesktop->text().toStdString();
	setting.add(mobileOutFileSettingName, libconfig::Setting::TypeString) = editOutfileMobile->text().toStdString();
}

void MenuGeneratorInterface::updateSetting(libconfig::Setting& setting)
{
	setting[desktopMainSettingName] = editDesktopMain->text().toStdString();
	setting[desktopLiWCSettingName] = editDesktopLiWC->text().toStdString();
	setting[desktopLiWOCSettingName] = editDesktopLiWOC->text().toStdString();
	setting[mobileMainSettingName] = editMobileMain->text().toStdString();
	setting[mobileLiWCSettingName] = editMobileLiWC->text().toStdString();
	setting[mobileLiWOCSettingName] = editMobileLiWOC->text().toStdString();
	setting[desktopOutFileSettingName] = editOutfileDesktop->text().toStdString();
	setting[mobileOutFileSettingName] = editOutfileMobile->text().toStdString();
}

void MenuGeneratorInterface::loadSetting(libconfig::Setting& setting)
{
	editDesktopMain->setText(QString::fromStdString(setting[desktopMainSettingName]));
	editDesktopLiWC->setText(QString::fromStdString(setting[desktopLiWCSettingName]));
	editDesktopLiWOC->setText(QString::fromStdString(setting[desktopLiWOCSettingName]));
	editMobileMain->setText(QString::fromStdString(setting[mobileMainSettingName]));
	editMobileLiWC->setText(QString::fromStdString(setting[mobileLiWCSettingName]));
	editMobileLiWOC->setText(QString::fromStdString(setting[mobileLiWOCSettingName]));
	editOutfileDesktop->setText(QString::fromStdString(setting[desktopOutFileSettingName]));
	editOutfileMobile->setText(QString::fromStdString(setting[mobileOutFileSettingName]));
}

