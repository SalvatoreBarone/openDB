/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __DECORATION_MENU_GENERATOR_H
#define __DECORATION_MENU_GENERATOR_H

#include <QObject>
#include <QWidget>

#include <string>
#include <unordered_map>
#include <libconfig.h++>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class MainWindow;
class MenuGenerator;

class QLabel;
class QLineEdit;
class QPushButton;
class QHBoxLayout;
class QGridLayout;
class QProgressDialog;
class QThread;

class MenuGeneratorInterface : public QWidget
{
	Q_OBJECT
public:
	explicit MenuGeneratorInterface(MainWindow* window, QWidget* parent);

	virtual ~MenuGeneratorInterface();

private slots:
	void slotChooseDesktopMain();
	void slotChooseDesktopLiWC();
	void slotChooseDesktopLiWOC();
	void slotChooseMobileMain();
	void slotChooseMobileLiWC();
	void slotChooseMobileLiWOC();
	void slotChooseOutfileDesktop();
	void slotChooseOutfileMobile();
	void slotCreate();

private:
	MainWindow* parentWindow;

	// componenti per la scelta del main-template per la versione desktop
	QLabel* labelDesktopMain;
	QLineEdit* editDesktopMain;
	QPushButton* buttonDesktopMain;
	// componenti per la scelta del li-template con sottovoci (WC = With Child)
	QLabel* labelDesktopLiWC;
	QLineEdit* editDesktopLiWC;
	QPushButton* buttonDesktopLiWC;
	// componenti per la scelta del li-template senza sottovoci (WOC = WithOut Child)
	QLabel* labelDesktopLiWOC;
	QLineEdit* editDesktopLiWOC;
	QPushButton* buttonDesktopLiWOC;
	// componenti per la scelta del main-template per la versione Mobile
	QLabel* labelMobileMain;
	QLineEdit* editMobileMain;
	QPushButton* buttonMobileMain;
	// componenti per la scelta del li-template con sottovoci (WC = With Child)
	QLabel* labelMobileLiWC;
	QLineEdit* editMobileLiWC;
	QPushButton* buttonMobileLiWC;
	// componenti per la scelta del li-template senza sottovoci (WOC = WithOut Child)
	QLabel* labelMobileLiWOC;
	QLineEdit* editMobileLiWOC;
	QPushButton* buttonMobileLiWOC;
	// componenti per la scelta del file di destinazione desktop
	QLabel* labelOutfileDesktop;
	QLineEdit* editOutfileDesktop;
	QPushButton* buttonOutfileDesktop;
	// componenti per la scelta del file di destinazione mobile
	QLabel* labelOutfileMobile;
	QLineEdit* editOutfileMobile;
	QPushButton* buttonOutfileMobile;
	// pulsante per avviare la creazione
	QPushButton* buttonCreate;
	// layout del widget
	QGridLayout* layout;
	// progress-dialog
	QProgressDialog* progDialog;
	// generatore menu-desktop
	MenuGenerator* desktopGenerator;
	// generatore mobile
	MenuGenerator* mobileGenerator;

	void resizeWidgets();
	void layoutWidgets();
	void connectWidgets();

	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);
	static const std::string configGroupName;
	static const std::string desktopMainSettingName;
	static const std::string desktopLiWCSettingName;
	static const std::string desktopLiWOCSettingName;
	static const std::string mobileMainSettingName;
	static const std::string mobileLiWCSettingName;
	static const std::string mobileLiWOCSettingName;
	static const std::string desktopOutFileSettingName;
	static const std::string mobileOutFileSettingName;

	bool verifyFields();
	bool verifyDeskMain();
	bool verifyDeskLiWC();
	bool verifyDeskLiWOC();
	bool verifyMobileMain();
	bool verifyMobileLiWC();
	bool verifyMobileLiWOC();
	bool verifyDeskOutfile();
	bool verifyMobileOutfile();


};



class MenuGenerator : public QObject
{
	Q_OBJECT
public:
	typedef std::unordered_map<std::string, std::string> stringMap;
	typedef std::pair<std::string, std::string> stringPair;

	explicit MenuGenerator (
			openDB::BackendConnector* connector,
			const std::string& file_template_main,
			const std::string& file_template_li_wc,
			const std::string& file_template_li_woc,
			const std::string& file_output
		);

	virtual ~MenuGenerator();

signals:
	void done();

public slots:
	void start();

private:
	// puntatore all'oggetto BackendConnector che verra' usato per interrogare il database
	openDB::BackendConnector* backConnector;

	std::string template_main; // template principale
	std::string template_li_wc; // template li con figli
	std::string template_li_woc; // template li senza figli
	std::string outfile;	// file output

	static const std::string getMainEntries_sql; // comando sql per ottenere le voci principali del menu
	static const std::string getSubEntries_sql; // comando sql per ottenere le sotto-voci di una voce del menu
	static const std::string getSubEntries_codePar; // nome del parametro attraverso il quale specificare il codice di una voce "padre"

	static const std::string entryCodeColumn; // nome della colonna contente il codice di una voce menu
	static const std::string entryUrlColumn; // nome della colonna contenente l'url di destinazione di una voce menu
	static const std::string entryImgColumn; // nome della colonna contenente l'immagine associata ad una voce del menu
	static const std::string entryTextColumn; // nome della colonna contenente il testo di una voce del menu

	static const std::string entriesListPar; // nome del parametro "lista di entries" nei template con figli

	// interroga il db per ottenere le sotto-voci della voce identificata da parentEntryCode
	openDB::BackendConnector::Iterator getSubEntries(const std::string& parentEntryCode);

	// costruisce la stringa contenente il codice HTML di una voce menu, sottovoci incluse, usando la ricorsione
	// il parametro entry e' cio' che si ottiene chiamando la funzione current su un oggetto openDB::Record
	std::string buildEntries (const stringMap& entry);

};

#endif
