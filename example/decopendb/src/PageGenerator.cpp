/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "PageGenerator.hpp"
#include "MainWindow.hpp"
#include "verifyMacro.hpp"
#include "openDB2core.hpp"
#include "openDB2gui.hpp"

#include <QtGui>
#include <QFileDialog>

#include <fstream>
#include <unordered_map>


const std::string PageGenerator::sqlQuery_getArticles = "\
select art.codice as codice, url.relative_url as url \
from anagrafica_articoli art join url_articoli url on art.codice=url.codice_art \
where art.natura='$NATURA' and art.cat_merc<>'1006' and url.sito='www.decoration.it' \
order by art.codice";
const std::string PageGenerator::sqlQuery_getArticles_parNatura = "NATURA";
const std::string PageGenerator::getArticle_codeColumnName = "codice";
const std::string PageGenerator::getArticle_urlColumnName = "url";

const std::string PageGenerator::sqlQuery_getListino = "\
select ra.url as \"LINKTOLISTINO\", ra.icon as \"IMGLISTINO\" \
from risorse_articoli ra join anagrafica_articoli aa on ra.codice_art = aa.codice \
where aa.cat_merc='1006' and ra.sito='www.decoration.it' and ra.testo='Listino' and aa.natura='$NATURA'";
const std::string PageGenerator::sqlQuery_getListino_parNatura = "NATURA";

const std::string PageGenerator::parMainTemplate_nomeCampionario = "NOMECAMPIONARIO";
const std::string PageGenerator::parMainTemplate_fileComponente = "FILECOMPONENTE";
const std::string PageGenerator::parCompTemplate_nomeCampionario = "NOMECAMPIONARIO";
const std::string PageGenerator::parCompTemplate_listArticoli = "LISTAARTICOLI";
const std::string PageGenerator::parCompTemplate_linkListino = "LINKTOLISTINO";
const std::string PageGenerator::parCompTemplate_imgListino = "IMGLISTINO";
const std::string PageGenerator::parLineTemplate_urlArticolo = "URLARTICOLO";
const std::string PageGenerator::parLineTemplate_imgArticolo = "IMGARTICOLO";
const std::string PageGenerator::parLineTemplate_codiceArticolo = "CODICEARTICOLO";

PageGenerator::PageGenerator(QWidget* parent, MainWindow* window) :
		QWidget(parent),
		parentWindow(window),
		labelTemplateMainFile(new QLabel("Template file", this)),
		editTemplateMainFile(new QLineEdit(this)),
		buttonTemplateMainFile(new QPushButton("Apri", this)),
		labelTemplateCompFile(new QLabel("Template comp.", this)),
		editTemplateCompFile(new QLineEdit(this)),
		buttonTemplateCompFile(new QPushButton("Apri", this)),
		labelTemplateLine(new QLabel("Template linea", this)),
		editTemplateLine(new QLineEdit(this)),
		buttonTemplateLine(new QPushButton("Apri", this)),
		labelTitoloCampionario(new QLabel("Nome campionario", this)),
		editTitoloCampionario(new QLineEdit(this)),
		labelNatura(new QLabel("Natura", this)),
		editNatura(new QLineEdit(this)),
		labelBaseDirectory(new QLabel("Cart. dest.", this)),
		editBaseDirectory(new QLineEdit(this)),
		buttonBaseDirectory(new QPushButton("Apri", this)),
		labelNomeFile(new QLabel("Nome file", this)),
		editNomeFile(new QLineEdit(this)),
		buttonNomeFile(new QPushButton("Apri", this)),
		labelNomeFileComponente(new QLabel("Nome file comp.", this)),
		editNomeFileComponente(new QLineEdit(this)),
		buttonNomeFileComponente(new QPushButton("Apri", this)),
		labelImgDirectory(new QLabel("Cart. Img", this)),
		editImgDirectory(new QLineEdit(this)),
		buttonImgDirectory(new QPushButton("Apri", this)),
		buttonCrea(new QPushButton("Crea", this)),
		layout(new QGridLayout(this))
{
	setLayout(layout);

	resizeWidgets();
	layoutWidgets();
	connectWidgets();
	loadConfig();
}

PageGenerator::~PageGenerator()
{
	delete labelTemplateMainFile;
	delete editTemplateMainFile;
	delete buttonTemplateMainFile;
	delete labelTemplateCompFile;
	delete editTemplateCompFile;
	delete buttonTemplateCompFile;
	delete labelTemplateLine;
	delete editTemplateLine;
	delete buttonTemplateLine;
	delete labelTitoloCampionario;
	delete editTitoloCampionario;
	delete labelNatura;
	delete editNatura;
	delete labelBaseDirectory;
	delete editBaseDirectory;
	delete buttonBaseDirectory;
	delete labelNomeFile;
	delete editNomeFile;
	delete buttonNomeFile;
	delete labelNomeFileComponente;
	delete editNomeFileComponente;
	delete buttonNomeFileComponente;
	delete labelImgDirectory;
	delete editImgDirectory;
	delete buttonImgDirectory;
	delete buttonCrea;
	delete layout;
}

void PageGenerator::resizeWidgets()
{
	buttonTemplateMainFile->setFixedSize(buttonTemplateMainFile->sizeHint());
	buttonTemplateCompFile->setFixedSize(buttonTemplateCompFile->sizeHint());
	buttonTemplateLine->setFixedSize(buttonTemplateLine->sizeHint());
	buttonBaseDirectory->setFixedSize(buttonBaseDirectory->sizeHint());
	buttonNomeFile->setFixedSize(buttonNomeFile->sizeHint());
	buttonNomeFileComponente->setFixedSize(buttonNomeFileComponente->sizeHint());
	buttonImgDirectory->setFixedSize(buttonImgDirectory->sizeHint());
	buttonCrea->setFixedSize(buttonCrea->sizeHint());
}

void PageGenerator::layoutWidgets()
{
	layout->addWidget(labelTemplateMainFile, 0, 0, 1, 1);
	layout->addWidget(editTemplateMainFile, 0, 1, 1, 2);
	layout->addWidget(buttonTemplateMainFile, 0, 3, 1, 1);
	layout->addWidget(labelTemplateCompFile, 1, 0, 1, 1);
	layout->addWidget(editTemplateCompFile, 1, 1, 1, 2);
	layout->addWidget(buttonTemplateCompFile, 1, 3, 1, 1);
	layout->addWidget(labelTemplateLine, 2, 0, 1, 1);
	layout->addWidget(editTemplateLine, 2, 1, 1, 2);
	layout->addWidget(buttonTemplateLine, 2, 3, 1, 1);
	layout->addWidget(labelTitoloCampionario, 4, 0, 1, 1);
	layout->addWidget(editTitoloCampionario, 4, 1, 1, 1);
	layout->addWidget(labelNatura, 5, 0, 1, 1);
	layout->addWidget(editNatura, 5, 1, 1, 1);
	layout->addWidget(labelBaseDirectory, 6, 0, 1, 1);
	layout->addWidget(editBaseDirectory, 6, 1, 1, 2);
	layout->addWidget(buttonBaseDirectory, 6, 3, 1, 1);
	layout->addWidget(labelNomeFile, 7, 0, 1, 1);
	layout->addWidget(editNomeFile, 7, 1, 1, 2);
	layout->addWidget(buttonNomeFile, 7, 3, 1, 1);
	layout->addWidget(labelNomeFileComponente,8, 0, 1, 1);
	layout->addWidget(editNomeFileComponente, 8, 1, 1, 2);
	layout->addWidget(buttonNomeFileComponente, 8, 3, 1, 1);
	layout->addWidget(labelImgDirectory, 9, 0, 1, 1);
	layout->addWidget(editImgDirectory, 9, 1, 1, 2);
	layout->addWidget(buttonImgDirectory, 9, 3, 1, 1);
	layout->addWidget(buttonCrea, 10, 0, 1, 4, Qt::AlignCenter | Qt::AlignBottom);
}

void PageGenerator::connectWidgets()
{
	connect(buttonTemplateMainFile, SIGNAL(clicked()), this, SLOT(slotScegliTemplateFile()));
	connect(buttonTemplateCompFile, SIGNAL(clicked()), this, SLOT(slotScegliTemplateComp()));
	connect(buttonTemplateLine, SIGNAL(clicked()), this, SLOT(slotScegliTemplateLine()));
	connect(buttonBaseDirectory, SIGNAL(clicked()), this, SLOT(slotScegliBaseDirectory()));
	connect(buttonNomeFile, SIGNAL(clicked()), this, SLOT(slotScegliFileName()));
	connect(buttonNomeFileComponente, SIGNAL(clicked()), this, SLOT(slotScegliCompFileName()));
	connect(buttonImgDirectory, SIGNAL(clicked()), this, SLOT(slotScegliImgDirectory()));
	connect(buttonCrea, SIGNAL(clicked()), this, SLOT(slotCrea()));
}

void PageGenerator::slotScegliTemplateFile() {chooseOpenFile(parentWindow, editTemplateMainFile, "Main Template", "*.html")}
void PageGenerator::slotScegliTemplateComp() {chooseOpenFile(parentWindow, editTemplateCompFile, "Template componente", "*.html")}
void PageGenerator::slotScegliTemplateLine() {chooseOpenFile(parentWindow, editTemplateLine, "Template line", "*.html")}

void PageGenerator::slotScegliBaseDirectory()
{
	QString fileName = QFileDialog::getExistingDirectory(	this,
								"Directory di destinazione",
								(editBaseDirectory->text().isEmpty() ? QDir::currentPath() :  editBaseDirectory->text()));
	if ( !fileName.isEmpty() )
		editBaseDirectory->setText(fileName + "/");
}

void PageGenerator::slotScegliFileName()
{
	chooseSaveFile(	parentWindow, editNomeFile, "File destinazione", "*.html")
}

void PageGenerator::slotScegliCompFileName()
{
	chooseSaveFile(	parentWindow, editNomeFileComponente, "File Componente", "*.html")
}

void PageGenerator::slotScegliImgDirectory()
{
	QString fileName = QFileDialog::getExistingDirectory(	this,
								"Directory immagini",
								(editBaseDirectory->text().isEmpty() ? QDir::currentPath() :  editBaseDirectory->text()));
	if ( !fileName.isEmpty() )
		editImgDirectory->setText(fileName + "/");
}

void PageGenerator::slotCrea()
{
	if (verificaCampi())
	{
		saveConfig();
		createComp();
		createMain();
	}
}

bool PageGenerator::verificaCampi()
{
	if (
		verificaMainTemplate() &&
		verificaCompTemplate() &&
		verificaLineTemplate() &&
		verificaNomeCampionario() &&
		verificaNatura() &&
		verificaBaseDirectory() &&
		verificaNomeFile() &&
		verificaNomeFileComp() &&
		verificaImgDirectory()
	)
		return true;
	return false;
}

bool PageGenerator::verificaMainTemplate() {verifyFile(editTemplateMainFile, "Scegli il file Template principale!")}
bool PageGenerator::verificaCompTemplate() {verifyFile(editTemplateCompFile, "Scegli il file Template componente!")}
bool PageGenerator::verificaLineTemplate() {verifyFile(editTemplateLine, "Scegli il file Template per le linee di elenco!")}
bool PageGenerator::verificaNomeCampionario() {verifyField(editTitoloCampionario, "Inserisci il nome del campionario!")}
bool PageGenerator::verificaNatura() {verifyField(editNatura, "Inserisci il codice della natura!")}
bool PageGenerator::verificaBaseDirectory() {verifyField(editBaseDirectory, "Scegli la directory di destinazione!")}
bool PageGenerator::verificaNomeFile() {verifyField(editNomeFile, "Scegli il file di destinazione principale!")}
bool PageGenerator::verificaNomeFileComp() {verifyField(editNomeFileComponente, "Scegli il file di destinazione componente!")}
bool PageGenerator::verificaImgDirectory() {verifyField(editImgDirectory, "Scegli la directory per le immagini!")}

/* La funzione createMain genera il file HTML a cui gli articoli devono fare riferimento tramite link alla pagina guarda altri.
 * Il file non contiene la lista degli articoli ma definisce solo la struttura della pagine. La lista degli articoli e' contenuta nel
 * file componente caricato tramite libreria jquery.
 */
void PageGenerator::createMain()
{

	QString fileName = editNomeFile->text();
	QString fileComp = editNomeFileComponente->text();
	fileComp.replace(fileName.indexOf(editBaseDirectory->text()), editBaseDirectory->text().size(), "");

	std::string main_template;
	openDB::readTemplate(main_template, editTemplateMainFile->text().toStdString());

	stringsmap par =
	{
		{parCompTemplate_nomeCampionario, editTitoloCampionario->text().toStdString()},
		{parMainTemplate_fileComponente, fileComp.toStdString()}
	};
	openDB::prepare(main_template, par);

	std::fstream stream;
	stream.open(fileName.toStdString().c_str(), std::ios::out);
	stream 	<< main_template << std::endl;
	stream.close();
}

/* createComp si occupa della creazione del file componente contenente informazioni circa il nome del campionario,
 * il link al listino pdf, la lista degli articoli che fanno parte del campionario corredata di codice e immagine rappresentativa.
 *
 */

void PageGenerator::createComp()
{
	std::string comp_template;
	openDB::readTemplate(comp_template, editTemplateCompFile->text().toStdString());

	std::string articleList;
	createArticleLinksList(articleList);

	stringsmap parListino;
	createListinoParm(parListino);

	stringsmap par =
	{
		{parCompTemplate_nomeCampionario, editTitoloCampionario->text().toStdString()},
		{parCompTemplate_listArticoli, articleList}
	};
	par.insert(parListino.begin(), parListino.end());
	openDB::prepare(comp_template, par);

	QString fileComp = editNomeFileComponente->text();
	std::fstream stream;
	stream.open(fileComp.toStdString().c_str(), std::ios::out);
	stream 	<< comp_template << std::endl;
	stream.close();
}

/* crea la lista di articoli in codice HTML con riferimenti all'articolo, codice dell'articolo e immagine rappresentativa.
 *
 */
void PageGenerator::createArticleLinksList(std::string& list)
{
	list.clear();

	std::string line_template;
	openDB::readTemplate(line_template, editTemplateLine->text().toStdString());
	QString imgDir = editImgDirectory->text();
	imgDir.replace(imgDir.indexOf(editBaseDirectory->text()), editBaseDirectory->text().size(), "");

	openDB::NonTransactional query;
	std::string sqlCommand = sqlQuery_getArticles;
	stringsmap par = { {sqlQuery_getArticles_parNatura, editNatura->text().toStdString()} };
	openDB::prepare(sqlCommand, par);
	query.appendCommand(sqlCommand);
	openDB::BackendConnector::Iterator result_it = parentWindow->backendConnector().execTransaction(query);

	openDB::Table& _resultTable = result_it.result().resultTable();
	openDB::Storage::Iterator storage_it = _resultTable.storage().begin(), storage_end = _resultTable.storage().end();
	for (; storage_it != storage_end; storage_it++)
	{
		std::string tmp = line_template;
		openDB::Record record = *storage_it;
		stringsmap values, par;
		record.current(values);

		par =
		{
			{parLineTemplate_urlArticolo, values[getArticle_urlColumnName]},
			{parLineTemplate_imgArticolo,  imgDir.toStdString() + values[getArticle_codeColumnName] + ".jpg"},
			{parLineTemplate_codiceArticolo, values[getArticle_codeColumnName]},
		};

		openDB::prepare(tmp, par);
		list += tmp;
	}

	parentWindow->backendConnector().deleteTransaction(result_it);
}

/*
 * recupera i parametri da utilizzare per includere il link al listino.
 */
void PageGenerator::createListinoParm(stringsmap& par)
{
	par.clear();

	openDB::NonTransactional query;
	std::string sqlCommand = sqlQuery_getListino;
	stringsmap queryPar = {{sqlQuery_getListino_parNatura, editNatura->text().toStdString()}};
	openDB::prepare(sqlCommand, queryPar);
	query.appendCommand(sqlCommand);
	openDB::BackendConnector::Iterator result_it = parentWindow->backendConnector().execTransaction(query);

	openDB::Table& _resultTable = result_it.result().resultTable();
	if (_resultTable.storage().records() > 0)
	{
		openDB::Storage::Iterator storage_it = _resultTable.storage().begin();
		openDB::Record record = *storage_it;
		record.current(par);
	}

	parentWindow->backendConnector().deleteTransaction(result_it);
}


const std::string PageGenerator::configGroupName = "panoramica_articoli";
const std::string PageGenerator::mainTemplSettingName = "main_tmpl";
const std::string PageGenerator::compTemplSettingName = "comp_tmpl";
const std::string PageGenerator::lineTemplSettingName = "line_tmpl";
const std::string PageGenerator::titoloSettingName = "titolo";
const std::string PageGenerator::naturaSettingName = "natura";
const std::string PageGenerator::baseDirSettingName = "base_dir";
const std::string PageGenerator::fileSettingName = "file_output";
const std::string PageGenerator::fileCompSettingName = "file_comp";
const std::string PageGenerator::imgDirSettingName = "img_dir";

void PageGenerator::saveConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());

		libconfig::Setting& root = cfg.getRoot();
		if (!root.exists(configGroupName))
			addSetting(cfg);
		else
		{
			libconfig::Setting& setting = root[configGroupName];
			if (!setting.isGroup())
			{
				root.remove(configGroupName);
				addSetting(cfg);
			}
			else
				updateSetting(setting);
		}

		cfg.writeFile(parentWindow->modulesConfigFile().c_str());

	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message =	"Parsing error alla linea " + std::to_string(e.getLine()) + ":\n" +
					e.getError() + "\n(" + e.what() + ").";
		QMessageBox::warning(this, "Parsing error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void PageGenerator::loadConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();

		if
		(
			root.exists(configGroupName) &&
			root[configGroupName].isGroup() &&
			QMessageBox::question(this, "", "Caricare gli ultimi settaggi usati?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[configGroupName]);
	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void PageGenerator::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting& setting = cfg.getRoot().add(configGroupName, libconfig::Setting::TypeGroup);
	setting.add(mainTemplSettingName, libconfig::Setting::TypeString) = editTemplateMainFile->text().toStdString();
	setting.add(compTemplSettingName, libconfig::Setting::TypeString) = editTemplateCompFile->text().toStdString();
	setting.add(lineTemplSettingName, libconfig::Setting::TypeString) = editTemplateLine->text().toStdString();
	setting.add(titoloSettingName, libconfig::Setting::TypeString) = editTitoloCampionario->text().toStdString();
	setting.add(naturaSettingName, libconfig::Setting::TypeString) = editNatura->text().toStdString();
	setting.add(baseDirSettingName, libconfig::Setting::TypeString) = editBaseDirectory->text().toStdString();
	setting.add(fileSettingName, libconfig::Setting::TypeString) = editNomeFile->text().toStdString();
	setting.add(fileCompSettingName, libconfig::Setting::TypeString) = editNomeFileComponente->text().toStdString();
	setting.add(imgDirSettingName, libconfig::Setting::TypeString) = editImgDirectory->text().toStdString();
}

void PageGenerator::updateSetting(libconfig::Setting& setting)
{
	setting[mainTemplSettingName] = editTemplateMainFile->text().toStdString();
	setting[compTemplSettingName] = editTemplateCompFile->text().toStdString();
	setting[lineTemplSettingName] = editTemplateLine->text().toStdString();
	setting[titoloSettingName] = editTitoloCampionario->text().toStdString();
	setting[naturaSettingName] = editNatura->text().toStdString();
	setting[baseDirSettingName] = editBaseDirectory->text().toStdString();
	setting[fileSettingName] = editNomeFile->text().toStdString();
	setting[fileCompSettingName] = editNomeFileComponente->text().toStdString();
	setting[imgDirSettingName] = editImgDirectory->text().toStdString();
}

void PageGenerator::loadSetting(libconfig::Setting& setting)
{
	editTemplateMainFile->setText(QString::fromStdString(setting[mainTemplSettingName]));
	editTemplateCompFile->setText(QString::fromStdString(setting[compTemplSettingName]));
	editTemplateLine->setText(QString::fromStdString(setting[lineTemplSettingName]));
	editTitoloCampionario->setText(QString::fromStdString(setting[titoloSettingName]));
	editNatura->setText(QString::fromStdString(setting[naturaSettingName]));
	editBaseDirectory->setText(QString::fromStdString(setting[baseDirSettingName]));
	editNomeFile->setText(QString::fromStdString(setting[fileSettingName]));
	editNomeFileComponente->setText(QString::fromStdString(setting[fileCompSettingName]));
	editImgDirectory->setText(QString::fromStdString(setting[imgDirSettingName]));
}

