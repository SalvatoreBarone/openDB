/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __PAGE_GENERATOR_H
#define __PAGE_GENERATOR_H

#include <QWidget>

#include <string>
#include <unordered_map>

#include <libconfig.h++>

class QLabel;
class QLineEdit;
class QTextEdit;
class QGridLayout;
class QPushButton;
class QHBoxLayout;

class MainWindow;


class PageGenerator : public QWidget
{
	Q_OBJECT
public:
	explicit PageGenerator(QWidget* parent, MainWindow* window);

	virtual ~PageGenerator();

private slots:
	void slotScegliTemplateFile();
	void slotScegliTemplateComp();
	void slotScegliTemplateLine();
	void slotScegliBaseDirectory();
	void slotScegliFileName();
	void slotScegliCompFileName();
	void slotScegliImgDirectory();
	void slotCrea();

private:
	typedef std::unordered_map<std::string, std::string> stringsmap;

	static const std::string sqlQuery_getArticles;
	static const std::string sqlQuery_getArticles_parNatura;
	static const std::string getArticle_urlColumnName;
	static const std::string getArticle_codeColumnName;
	static const std::string sqlQuery_getListino;
	static const std::string sqlQuery_getListino_parNatura;

	static const std::string parMainTemplate_nomeCampionario;
	static const std::string parMainTemplate_fileComponente;
	static const std::string parCompTemplate_nomeCampionario;
	static const std::string parCompTemplate_linkListino;
	static const std::string parCompTemplate_imgListino;
	static const std::string parCompTemplate_listArticoli;
	static const std::string parLineTemplate_urlArticolo;
	static const std::string parLineTemplate_imgArticolo;
	static const std::string parLineTemplate_codiceArticolo;

	MainWindow* parentWindow;
	QLabel* labelTemplateMainFile;
	QLineEdit* editTemplateMainFile;
	QPushButton* buttonTemplateMainFile;
	QLabel* labelTemplateCompFile;
	QLineEdit* editTemplateCompFile;
	QPushButton* buttonTemplateCompFile;
	QLabel* labelTemplateLine;
	QLineEdit* editTemplateLine;
	QPushButton* buttonTemplateLine;
	QLabel* labelTitoloCampionario;
	QLineEdit* editTitoloCampionario;
	QLabel* labelNatura;
	QLineEdit* editNatura;
	QLabel* labelBaseDirectory;
	QLineEdit* editBaseDirectory;
	QPushButton* buttonBaseDirectory;
	QLabel* labelNomeFile;
	QLineEdit* editNomeFile;
	QPushButton* buttonNomeFile;
	QLabel* labelNomeFileComponente;
	QLineEdit* editNomeFileComponente;
	QPushButton* buttonNomeFileComponente;
	QLabel* labelImgDirectory;
	QLineEdit* editImgDirectory;
	QPushButton* buttonImgDirectory;
	QPushButton* buttonCrea;
	QGridLayout* layout;

	void resizeWidgets();
	void layoutWidgets();
	void connectWidgets();

	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);

	static const std::string configGroupName;
	static const std::string mainTemplSettingName;
	static const std::string compTemplSettingName;
	static const std::string lineTemplSettingName;
	static const std::string titoloSettingName;
	static const std::string naturaSettingName;
	static const std::string baseDirSettingName;
	static const std::string fileSettingName;
	static const std::string fileCompSettingName;
	static const std::string imgDirSettingName;

	bool verificaCampi();
	bool verificaMainTemplate();
	bool verificaCompTemplate();
	bool verificaLineTemplate();
	bool verificaNomeCampionario();
	bool verificaNatura();
	bool verificaBaseDirectory();
	bool verificaNomeFile();
	bool verificaNomeFileComp();
	bool verificaImgDirectory();

	void createMain();
	void createComp();
	void createArticleLinksList(std::string& list);
	void createListinoParm(stringsmap& par);


};

#endif
