/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "PgLogin.hpp"
#include "MainWindow.hpp"
#include "openDB2core.hpp"
#include "openDB2gui.hpp"

#include <QtGui>

const std::string PgLogin::hostname = "milky.system-ns.org";
const std::string PgLogin::port = "5432";
const std::string PgLogin::dbname = "decoration";


PgLogin::PgLogin(	openDB::Database*& __database,
			openDB::BackendConnector*& __connector,
			MainWindow* __window,
			QWidget* __parent) :
			QWidget(__parent),
		database(__database),
		connector(__connector),
		window(__window),
		labelUsername(new QLabel("Username", this)),
		editUsername( new QLineEdit(this)),
		labelPassword(new QLabel("Password", this)),
		editPassword(new QLineEdit(this)),
		editLayout(new QGridLayout),
		buttonConnetti(new QPushButton("Connetti", this)),
		buttonAnnulla(new QPushButton("Annulla", this)),
		buttonLayout(new QHBoxLayout),
		gridLayout(new QGridLayout(this))
{
	setLayout(gridLayout);
	editPassword->setEchoMode(QLineEdit::Password);


	buttonConnetti->setFixedSize(buttonConnetti->sizeHint());
	buttonAnnulla->setFixedSize(buttonAnnulla->sizeHint());

	editLayout->addWidget(labelUsername, 0, 0);
	editLayout->addWidget(editUsername, 0, 1);
	editLayout->addWidget(labelPassword, 1, 0);
	editLayout->addWidget(editPassword, 1, 1);

	buttonLayout->addWidget(buttonConnetti);
	buttonLayout->addWidget(buttonAnnulla);

	gridLayout->addLayout(editLayout, 1, 0, 1, 1, Qt::AlignCenter);
	gridLayout->addLayout(buttonLayout, 1, 0, 1, 2, Qt::AlignCenter | Qt::AlignBottom);

	connect(buttonConnetti, SIGNAL(clicked()), this, SLOT(login()));
	connect(buttonAnnulla, SIGNAL(clicked()), this, SIGNAL(cancel()));

}

PgLogin::~PgLogin()
{
	delete labelUsername;
	delete editUsername;
	delete labelPassword;
	delete editPassword;
	delete buttonConnetti;
	delete buttonAnnulla;
	delete editLayout;
	delete buttonLayout;
	delete gridLayout;
}

void PgLogin::login() noexcept
{
	openDB::PgConnector* _connector = new openDB::PgConnector;
	_connector->hostaddr(hostname);
	_connector->port(port);
	_connector->dbname(dbname);
	_connector->username(editUsername->text().toStdString());
	_connector->password(editPassword->text().toStdString());
	_connector->timeout(timeout);

	try {
		_connector->connect();

		std::string querycmd = openDB::PgConnection::pgDbStructureQuery;
		openDB::NonTransactional txn;
		txn.appendCommand(querycmd);
		openDB::BackendConnector::Iterator bit = _connector->execTransaction(txn);
		openDB::Table& table = bit.result().resultTable();

		openDB::DatabaseBuilder dbuilder(_connector->dbname());
		openDB::Database* _database = new openDB::Database;
		*_database = dbuilder.fromTable(table);
		_connector->deleteTransaction(bit);

		if (connector)
			delete connector;
		connector = _connector;

		if (database)
			delete database;
		database = _database;

		emit logged();
	}
	catch (openDB::BackendException& e) {
		QMessageBox::warning(this, "Errore durante la Connessione", e.what(), QMessageBox::Ok);
		delete _connector;
	}
}
