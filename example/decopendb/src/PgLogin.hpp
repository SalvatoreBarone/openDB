/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __DECORATION_LOGIN_WIDGET_H
#define __DECORATION_LOGIN_WIDGET_H


#include <QWidget>
#include <string>

class QLabel;
class QLineEdit;
class QPushButton;
class QHBoxLayout;
class QGridLayout;
class MainWindow;

namespace openDB {
	class Database;
	class BackendConnector;
};

class PgLogin : public QWidget
{
	Q_OBJECT
public:
	explicit PgLogin (	openDB::Database*& __database,
				openDB::BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent);

	virtual ~PgLogin();

signals:
	void logged();
	void cancel();

private:
	openDB::Database*& database;
	openDB::BackendConnector*& connector;
	MainWindow* window;

	static const std::string hostname;
	static const std::string port;
	static const std::string dbname;
	static const int timeout = 5;

	QLabel* labelUsername;
	QLineEdit* editUsername;
	QLabel* labelPassword;
	QLineEdit* editPassword;
	QGridLayout* editLayout;
	QPushButton* buttonConnetti;
	QPushButton* buttonAnnulla;
	QHBoxLayout* buttonLayout;
	QGridLayout* gridLayout;

private slots:
	void login() noexcept; // slot di connessione per postgres

};


#endif
