/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "SliderGenerator.hpp"
#include "MainWindow.hpp"
#include "verifyMacro.hpp"

#include <QtGui>
#include <QFileDialog>

#include <fstream>
#include <unordered_map>
typedef std::unordered_map<std::string, std::string> stringMap;


const std::string SliderGenerator::sqlQuery_getSliderID = "select distinct id_slider from componenti.slider order by id_slider";

const std::string SliderGenerator::idColumnName = "id_slider";

const std::string SliderGenerator::sqlQuery_getSliderDef = "\
select \
id_slider as \"SLIDERID\", \
case when width_um='px' then width::integer else width end as \"SLIDERWIDTH\", \
width_um as \"UMSLIDERWIDTH\", \
case when height_um='px' then height::integer else height end as \"SLIDERHEIGHT\", \
height_um as \"UMSLIDERHEIGHT\", \
case when content_maxwidth_um='px' then content_maxwidth::integer else content_maxwidth end as \"CONTENTMAXWIDTH\", \
content_maxwidth_um as \"UMCONTENTMAXWIDTH\", \
case when content_maxheight_um='px' then content_maxheight::integer else content_maxheight end as \"CONTENTMAXHEIGHT\", \
content_maxheight_um as \"UMCONTENTMAXHEIGHT\", \
animated, \
animation_name as \"ANIMATIONNAME\", \
slide_duration as \"SLIDEDURATION\", \
animation_count as \"ANIMATIONCOUNT\", \
(select count(id_slider) from componenti.slide where id_slider='$IDSLIDER') as \"NUMOFSLIDES\" \
from componenti.slider \
where id_slider='$IDSLIDER'";

const std::string SliderGenerator::sqlQuery_getSlidersSlide = "\
select \
html_code as \"HTMLCODE\", \
label_icon as \"LABELICON\" \
from componenti.slide \
where id_slider = '$IDSLIDER' \
order by progressivo";

const std::string SliderGenerator::idColumnNamePar = "IDSLIDER";

const std::string SliderGenerator::animatedColumnName = "animated";

const std::string SliderGenerator::slideIndexParName = "INDEX";

const std::string SliderGenerator::slideOffsetParName = "OFFSET";

const std::string SliderGenerator::slidesTemplateParName = "SLIDESLIST";

const std::string SliderGenerator::labelTemplateParName = "LABELSLIST";

const std::string SliderGenerator::radioTemplateParName = "RADIOBUTTONSLIST";

const std::string SliderGenerator::cssTemplateParName = "CSSCOMMANDSLIST";

SliderGenerator::SliderGenerator(MainWindow* window, QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		labelSliderID(new QLabel("Slider ID", this)),
		comboBoxSliderID(new QComboBox(this)),
		labelMainTemplate(new QLabel("Main Template", this)),
		editMainTemplate(new QLineEdit(this)),
		buttonMainTemplate(new QPushButton("Apri", this)),
		labelRadioTemplate(new QLabel("Radio Template", this)),
		editRadioTemplate(new QLineEdit(this)),
		buttonRadioTemplate(new QPushButton("Apri", this)),
		labelRadioCheckedTemplate(new QLabel("Checked Radio Template", this)),
		editRadioCheckedTemplate(new QLineEdit(this)),
		buttonRadioCheckedTemplate(new QPushButton("Apri", this)),
		labelSlideTemplate(new QLabel("Slide Template", this)),
		editSlideTemplate(new QLineEdit(this)),
		buttonSlideTemplate(new QPushButton("Apri", this)),
		labelLabelTemplate(new QLabel("Label Template", this)),
		editLabelTemplate(new QLineEdit(this)),
		buttonLabelTemplate(new QPushButton("Apri", this)),
		labelCSSControlTemplate(new QLabel("CSS-Control Template", this)),
		editCSSControlTemplate(new QLineEdit(this)),
		buttonCSSControlTemplate(new QPushButton("Apri", this)),
		labelOutFile(new QLabel("Output File", this)),
		editOutFile(new QLineEdit(this)),
		buttonOutFile(new QPushButton("Salva", this)),
		buttonCreate(new QPushButton("Crea", this)),
		layout(new QGridLayout(this))
{
	setLayout(layout);
	createComboBoxSliderID();
	resizeButton();
	connectButton();
	layoutWidget();
	loadConfig();
}

SliderGenerator::~SliderGenerator()
{
	delete labelSliderID;
	delete comboBoxSliderID;
	delete labelMainTemplate;
	delete editMainTemplate;
	delete buttonMainTemplate;
	delete labelRadioTemplate;
	delete editRadioTemplate;
	delete buttonRadioTemplate;
	delete labelRadioCheckedTemplate;
	delete editRadioCheckedTemplate;
	delete buttonRadioCheckedTemplate;
	delete labelSlideTemplate;
	delete editSlideTemplate;
	delete buttonSlideTemplate;
	delete labelLabelTemplate;
	delete editLabelTemplate;
	delete buttonLabelTemplate;
	delete labelCSSControlTemplate;
	delete editCSSControlTemplate;
	delete buttonCSSControlTemplate;
	delete labelOutFile;
	delete editOutFile;
	delete buttonOutFile;
	delete buttonCreate;
	delete layout;
}

void SliderGenerator::slotChooseMainTemplate()
{
	chooseOpenFile(parentWindow, editMainTemplate, "Main Template", "*.html")
}

void SliderGenerator::slotChooseRadioTemplate()
{
	chooseOpenFile(parentWindow, editRadioTemplate, "Radio Template", "*.html")
}

void SliderGenerator::slotChooseRadioCheckedTemplate()
{
	chooseOpenFile(parentWindow, editRadioCheckedTemplate, "Checked Radio Template", "*.html")
}

void SliderGenerator::slotChooseSlideTemplate()
{
	chooseOpenFile(parentWindow, editSlideTemplate, "Slide Template", "*.html")
}

void SliderGenerator::slotChooseLabelTemplate()
{
	chooseOpenFile(parentWindow, editLabelTemplate, "Label Template", "*.html")
}

void SliderGenerator::slotChooseCSSControlTemplate()
{
	chooseOpenFile(parentWindow, editCSSControlTemplate, "CSS template", "*.css")
}


void SliderGenerator::slotChooseOutfile()
{
	chooseSaveFile(parentWindow, editOutFile, "Output file", "*.html")
}

void SliderGenerator::slotCreate()
{
	if (verifyFields())
	{
		saveConfig();
		openDB::BackendConnector::Iterator slider_it = getSliderParameter();
		openDB::Table& _slider = slider_it.result().resultTable();

		if (_slider.storage().records() > 0)
		{
			// lettura dei vari template
			std::string main_template, slide_template, label_template, radio_template, checkedradio_template, css_template;
			openDB::readTemplate(main_template, editMainTemplate->text().toStdString());
			openDB::readTemplate(slide_template, editSlideTemplate->text().toStdString());
			openDB::readTemplate(label_template, editLabelTemplate->text().toStdString());
			openDB::readTemplate(radio_template, editRadioTemplate->text().toStdString());
			openDB::readTemplate(checkedradio_template, editRadioCheckedTemplate->text().toStdString());
			openDB::readTemplate(css_template, editCSSControlTemplate->text().toStdString());


			std::string	slides_list,	// conterra' il codice html completo di tutte le slide
					label_list,	// conterra' il codice html di tutte le label
					radio_list,	// conterra' il codice html di tutti i radio-button
					css_list;	// conterra' il codice css di tutti i controlli per lo slider
			unsigned	index = 1;	// contatore/indice usato per identificare univocamente slide, label, radio-button e
							// controlli css

			openDB::BackendConnector::Iterator slides_it = getSlidersSlides();
			openDB::Table& slides = slides_it.result().resultTable();
			openDB::Storage::Iterator s_it = slides.storage().begin(), s_end = slides.storage().end();
			for (; s_it != s_end; s_it++, index++)
			{
				std::string 	slide_tmp = slide_template,
						label_tmp = label_template,
						radio_tmp = (s_it == slides.storage().begin() ? checkedradio_template : radio_template),
						css_tmp = css_template;

				stringMap par;
				(*s_it).current(par);
				par.insert({{slideIndexParName, std::to_string(index)}}); 		// aggiunta del parametro indice
				par.insert({{slideOffsetParName, std::to_string((index - 1) * 100)}}); 	// aggiunta del parametro offset

				// risoluzione dei parametri
				openDB::prepare(slide_tmp, par);
				openDB::prepare(label_tmp, par);
				openDB::prepare(radio_tmp, par);
				openDB::prepare(css_tmp, par);

				// aggiunta alla lista
				slides_list += slide_tmp;
				label_list += label_tmp;
				radio_list += radio_tmp;
				css_list += css_tmp;
			}

			// eliminazione del result per le slide non piu' necessari
			parentWindow->backendConnector().deleteTransaction(slides_it);

			/* per ottenere il codice finale dello slider sono necessari due passaggi di preparazione:
			 *	1) sostituire ai parametri slidesTemplateParName, labelTemplateParName, ecc.. i rispettivi valori;
			 *	2) essendo i valori dei parametri di cui sopra anch'essi parametrici si procede a sostituire i parametri coi
			 *	   valori memorizzati nel database;
			 */

			// passo di preparazione 1
			stringMap htmlCSS_par =
			{
				{slidesTemplateParName, slides_list},
				{labelTemplateParName, label_list},
				{radioTemplateParName, radio_list},
				{cssTemplateParName, css_list}
			};
			openDB::prepare(main_template, htmlCSS_par);

			// passo di preparazione 2
			stringMap slider_par;
			(*_slider.storage().begin()).current(slider_par);
			openDB::prepare(main_template, slider_par);

			// scrittura su file dello slider
			std::fstream stream(editOutFile->text().toStdString().c_str(), std::ios::out);
			stream << main_template << std::endl;
			stream.close();

		}

		// eliminazione del result per lo slider
		parentWindow->backendConnector().deleteTransaction(slider_it);
	}
}

bool SliderGenerator::verifyMainTemplate()
{
	verifyFile(editMainTemplate, "Scegli il main template")
}

bool SliderGenerator::verifyRadioTemplate()
{
	verifyFile(editRadioTemplate, "Scegli il radio-button template")
}

bool SliderGenerator::verifyRadioCheckedTemplate()
{
	verifyFile(editRadioCheckedTemplate, "Scegli il checked radio-button template")
}

bool SliderGenerator::verifySlideTemplate()
{
	verifyFile(editSlideTemplate, "Scegli lo slide template")
}

bool SliderGenerator::verifyLabelTemplate()
{
	verifyFile(editLabelTemplate, "Scegli il label template")
}

bool SliderGenerator::verifyCSSControlTemplate()
{
	verifyFile(editCSSControlTemplate, "Scegli il css-control template")
}

bool SliderGenerator::verifyFields()
{
	if
	(
		verifyMainTemplate() &&
		verifyRadioTemplate() &&
		verifyRadioCheckedTemplate() &&
		verifySlideTemplate() &&
		verifyLabelTemplate() &&
		verifyCSSControlTemplate()
	)
		return true;
	return false;
}

void SliderGenerator::createComboBoxSliderID()
{
	openDB::NonTransactional txn(sqlQuery_getSliderID);
	openDB::BackendConnector::Iterator it = parentWindow->backendConnector().execTransaction(txn);
	openDB::Table& _table = it.result().resultTable();
	openDB::Storage::Iterator	s_it = _table.storage().begin(),
					s_end = _table.storage().end();
	for (; s_it != s_end; s_it++)
	{
		QString value = QString::fromStdString((*s_it).current(idColumnName));
		comboBoxSliderID->addItem(value, QVariant(value));
	}

	comboBoxSliderID->setFixedSize(comboBoxSliderID->sizeHint());

	parentWindow->backendConnector().deleteTransaction(it);
}

void SliderGenerator::resizeButton()
{
	buttonMainTemplate->setFixedSize(buttonMainTemplate->sizeHint());
	buttonRadioTemplate->setFixedSize(buttonRadioTemplate->sizeHint());
	buttonRadioCheckedTemplate->setFixedSize(buttonRadioCheckedTemplate->sizeHint());
	buttonSlideTemplate->setFixedSize(buttonSlideTemplate->sizeHint());
	buttonLabelTemplate->setFixedSize(buttonLabelTemplate->sizeHint());
	buttonCSSControlTemplate->setFixedSize(buttonCSSControlTemplate->sizeHint());
	buttonOutFile->setFixedSize(buttonOutFile->sizeHint());
	buttonCreate->setFixedSize(buttonCreate->sizeHint());
}

void SliderGenerator::layoutWidget()
{
	layout->addWidget(labelSliderID, 0, 0, 1, 1);
	layout->addWidget(comboBoxSliderID, 0, 1, 1, 1);
	layout->addWidget(labelMainTemplate, 1, 0, 1, 1);
	layout->addWidget(editMainTemplate, 1, 1, 1, 1);
	layout->addWidget(buttonMainTemplate, 1, 2, 1, 1);
	layout->addWidget(labelRadioTemplate, 2, 0, 1, 1);
	layout->addWidget(editRadioTemplate, 2, 1, 1, 1);
	layout->addWidget(buttonRadioTemplate, 2, 2, 1, 1);
	layout->addWidget(labelRadioCheckedTemplate, 3, 0, 1, 1);
	layout->addWidget(editRadioCheckedTemplate, 3, 1, 1, 1);
	layout->addWidget(buttonRadioCheckedTemplate, 3, 2, 1, 1);
	layout->addWidget(labelSlideTemplate, 4, 0, 1, 1);
	layout->addWidget(editSlideTemplate, 4, 1, 1, 1);
	layout->addWidget(buttonSlideTemplate, 4, 2, 1, 1);
	layout->addWidget(labelLabelTemplate, 5, 0, 1, 1);
	layout->addWidget(editLabelTemplate, 5, 1, 1, 1);
	layout->addWidget(buttonLabelTemplate, 5, 2, 1, 1);
	layout->addWidget(labelCSSControlTemplate, 6, 0, 1, 1);
	layout->addWidget(editCSSControlTemplate, 6, 1, 1, 1);
	layout->addWidget(buttonCSSControlTemplate, 6, 2, 1, 1);
	layout->addWidget(labelOutFile, 7, 0, 1, 1);
	layout->addWidget(editOutFile, 7, 1, 1, 1);
	layout->addWidget(buttonOutFile, 7, 2, 1, 1);
	layout->addWidget(buttonCreate, 8, 0, 1, 4, Qt::AlignCenter | Qt::AlignBottom);
}

void SliderGenerator::connectButton()
{
	connect(buttonMainTemplate, SIGNAL(clicked()), this, SLOT(slotChooseMainTemplate()));
	connect(buttonRadioTemplate, SIGNAL(clicked()), this, SLOT(slotChooseRadioTemplate()));
	connect(buttonRadioCheckedTemplate, SIGNAL(clicked()), this, SLOT(slotChooseRadioCheckedTemplate()));
	connect(buttonSlideTemplate, SIGNAL(clicked()), this, SLOT(slotChooseSlideTemplate()));
	connect(buttonLabelTemplate, SIGNAL(clicked()), this, SLOT(slotChooseLabelTemplate()));
	connect(buttonCSSControlTemplate, SIGNAL(clicked()), this, SLOT(slotChooseCSSControlTemplate()));
	connect(buttonOutFile, SIGNAL(clicked()), this, SLOT(slotChooseOutfile()));
	connect(buttonCreate, SIGNAL(clicked()), this, SLOT(slotCreate()));
}

openDB::BackendConnector::Iterator SliderGenerator::getSliderParameter()
{
	std::string sql_query = sqlQuery_getSliderDef;
	stringMap par = {{idColumnNamePar, comboBoxSliderID->currentText().toStdString()}};
	openDB::prepare(sql_query, par);
	openDB::NonTransactional tx(sql_query);

	openDB::BackendConnector::Iterator it = parentWindow->backendConnector().execTransaction(tx);
	return it;
}

openDB::BackendConnector::Iterator SliderGenerator::getSlidersSlides()
{
	std::string sql_query = sqlQuery_getSlidersSlide;
	stringMap par = {{idColumnNamePar, comboBoxSliderID->currentText().toStdString()}};
	openDB::prepare(sql_query, par);
	openDB::NonTransactional tx(sql_query);

	openDB::BackendConnector::Iterator it = parentWindow->backendConnector().execTransaction(tx);
	return it;
}

const std::string SliderGenerator::configGroupName = "slider";
const std::string SliderGenerator::mainTmplSettingName = "main_tmpl";
const std::string SliderGenerator::radioTmplSettingName = "radio_tmpl";
const std::string SliderGenerator::radioCheckedSettingName = "checked_radio_tmpl";
const std::string SliderGenerator::slideTmplSettingName = "slide_tmpl";
const std::string SliderGenerator::labelTmplSettingName = "label_tmpl";
const std::string SliderGenerator::cssSettingName = "css_tmpl";
const std::string SliderGenerator::outfileSettingName = "outfile";

void SliderGenerator::saveConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());

		libconfig::Setting& root = cfg.getRoot();
		if (!root.exists(configGroupName))
			addSetting(cfg);
		else
		{
			libconfig::Setting& setting = root[configGroupName];
			if (!setting.isGroup())
			{
				root.remove(configGroupName);
				addSetting(cfg);
			}
			else
				updateSetting(setting);
		}

		cfg.writeFile(parentWindow->modulesConfigFile().c_str());

	}
	catch (libconfig::FileIOException& e)
	{
		addSetting(cfg);
		cfg.writeFile(parentWindow->modulesConfigFile().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message =	"Parsing error alla linea " + std::to_string(e.getLine()) + ":\n" +
					e.getError() + "\n(" + e.what() + ").";
		QMessageBox::warning(this, "Parsing error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void SliderGenerator::loadConfig()
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(parentWindow->modulesConfigFile().c_str());
		libconfig::Setting& root = cfg.getRoot();

		if
		(
			root.exists(configGroupName) &&
			root[configGroupName].isGroup() &&
			QMessageBox::question(this, "", "Caricare gli ultimi settaggi usati?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			loadSetting(root[configGroupName]);
	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void SliderGenerator::addSetting(libconfig::Config& cfg)
{
	libconfig::Setting& setting = cfg.getRoot().add(configGroupName, libconfig::Setting::TypeGroup);
	setting.add(mainTmplSettingName, libconfig::Setting::TypeString) = editMainTemplate->text().toStdString();
	setting.add(radioTmplSettingName, libconfig::Setting::TypeString) = editRadioTemplate->text().toStdString();
	setting.add(radioCheckedSettingName, libconfig::Setting::TypeString) = editRadioCheckedTemplate->text().toStdString();
	setting.add(slideTmplSettingName, libconfig::Setting::TypeString) = editSlideTemplate->text().toStdString();
	setting.add(labelTmplSettingName, libconfig::Setting::TypeString) = editLabelTemplate->text().toStdString();
	setting.add(cssSettingName, libconfig::Setting::TypeString) = editCSSControlTemplate->text().toStdString();
	setting.add(outfileSettingName, libconfig::Setting::TypeString) = editOutFile->text().toStdString();
}

void SliderGenerator::updateSetting(libconfig::Setting& setting)
{
	setting[mainTmplSettingName] = editMainTemplate->text().toStdString();
	setting[radioTmplSettingName] = editRadioTemplate->text().toStdString();
	setting[radioCheckedSettingName] = editRadioCheckedTemplate->text().toStdString();
	setting[slideTmplSettingName] = editSlideTemplate->text().toStdString();
	setting[labelTmplSettingName] = editLabelTemplate->text().toStdString();
	setting[cssSettingName] = editCSSControlTemplate->text().toStdString();
	setting[outfileSettingName] = editOutFile->text().toStdString();
}

void SliderGenerator::loadSetting(libconfig::Setting& setting)
{
	editMainTemplate->setText(QString::fromStdString(setting[mainTmplSettingName]));
	editRadioTemplate->setText(QString::fromStdString(setting[radioTmplSettingName]));
	editRadioCheckedTemplate->setText(QString::fromStdString(setting[radioCheckedSettingName]));
	editSlideTemplate->setText(QString::fromStdString(setting[slideTmplSettingName]));
	editLabelTemplate->setText(QString::fromStdString(setting[labelTmplSettingName]));
	editCSSControlTemplate->setText(QString::fromStdString(setting[cssSettingName]));
	editOutFile->setText(QString::fromStdString(setting[outfileSettingName]));
}


