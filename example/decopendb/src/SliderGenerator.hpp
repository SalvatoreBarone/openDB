/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __SLIDER_GENERATOR_H
#define __SLIDER_GENERATOR_H

#include <QWidget>
#include <QObject>

#include <string>
#include <libconfig.h++>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class MainWindow;
class QLabel;
class QLineEdit;
class QComboBox;
class QPushButton;
class QCheckBox;
class QProgressBar;
class QGridLayout;

class SliderGenerator : public QWidget
{
	Q_OBJECT
public:
	explicit SliderGenerator(MainWindow* window, QWidget* parent);

	virtual ~SliderGenerator();

private slots:
	void slotChooseMainTemplate();
	void slotChooseRadioTemplate();
	void slotChooseRadioCheckedTemplate();
	void slotChooseSlideTemplate();
	void slotChooseLabelTemplate();
	void slotChooseCSSControlTemplate();
	void slotChooseOutfile();
	void slotCreate();

private:
	MainWindow* parentWindow;

	QLabel* labelSliderID;
	QComboBox* comboBoxSliderID;
	QLabel* labelMainTemplate;
	QLineEdit* editMainTemplate;
	QPushButton* buttonMainTemplate;
	QLabel* labelRadioTemplate;
	QLineEdit* editRadioTemplate;
	QPushButton* buttonRadioTemplate;
	QLabel* labelRadioCheckedTemplate;
	QLineEdit* editRadioCheckedTemplate;
	QPushButton* buttonRadioCheckedTemplate;
	QLabel* labelSlideTemplate;
	QLineEdit* editSlideTemplate;
	QPushButton* buttonSlideTemplate;
	QLabel* labelLabelTemplate;
	QLineEdit* editLabelTemplate;
	QPushButton* buttonLabelTemplate;
	QLabel* labelCSSControlTemplate;
	QLineEdit* editCSSControlTemplate;
	QPushButton* buttonCSSControlTemplate;
	QLabel* labelOutFile;
	QLineEdit* editOutFile;
	QPushButton* buttonOutFile;
	QPushButton* buttonCreate;
	QGridLayout* layout;

	void resizeButton();
	void layoutWidget();
	void connectButton();

	void saveConfig();
	void loadConfig();
	void addSetting(libconfig::Config& cfg);
	void updateSetting(libconfig::Setting& setting);
	void loadSetting(libconfig::Setting& setting);

	static const std::string configGroupName;
	static const std::string mainTmplSettingName;
	static const std::string radioTmplSettingName;
	static const std::string radioCheckedSettingName;
	static const std::string slideTmplSettingName;
	static const std::string labelTmplSettingName;
	static const std::string cssSettingName;
	static const std::string outfileSettingName;

	bool verifyMainTemplate();
	bool verifyRadioTemplate();
	bool verifyRadioCheckedTemplate();
	bool verifySlideTemplate();
	bool verifyLabelTemplate();
	bool verifyCSSControlTemplate();
	bool verifyFields();

	/* sqlQuery_getSliderID contiene la query sql eseguita per ottenere gli id di tutti gli slider
	 * definiti nel database. idColumnName è il nome della colonna contenente gli id.
	 * Vengono entrambi usati dalla funzione createComboBoxSliderID per creare il combo-box attraverso
	 * il quale scegliere lo slider da creare.
	 */
	static const std::string sqlQuery_getSliderID;
	static const std::string idColumnName;
	void createComboBoxSliderID();


	/* sqlQuery_getSliderDef contiene la query usata dalla funzione getSliderParameter per ottenere i
	 * parametri con cui è definito uno slider, tra i quali le dimensioni, le informazioni circa i
	 * contenuti e l'eventuale animazione
	 * La query è parametrica riguardo l'id dello slider. Il nome del parametro contenente il nome dello
	 * slider è idColumnNamePar.
	 */
	static const std::string sqlQuery_getSliderDef;
	openDB::BackendConnector::Iterator getSliderParameter();

	/* sqlQuery_getSlidersSlide contiene la query usata dalla funzione getSlidersSlides per ottenere tutte
	 * le informazioni circa le slide di uno slider.
	 * La query è parametrica riguardo l'id dello slider. Il nome del parametro contenente il nome dello
	 * slider è idColumnNamePar.
	 */
	static const std::string sqlQuery_getSlidersSlide;
	openDB::BackendConnector::Iterator getSlidersSlides();

	static const std::string idColumnNamePar;

	/* - animatedColumnName è il nome della colonna che contiene true/false a seconda se lo slider deve
	 *   scorrere automaticamente (true) oppure deve scorrere con comando manuale. Tale colonna appartiene
	 *   alla tabella che si ottiene eseguendo la query parametrica il cui codice è contenuto in
	 *   sqlQuery_getSliderDef
	 */
	static const std::string animatedColumnName;

	/* slideIndexParName è il nome del parametro nei template che permette di specificare un indice per le
	 * slide, le label, i controlli e i radiobutton.
	 * slideOffsetParName è il nome del parametro nel template css che permette di specificare lo spiazzamento dello
	 * slide quando un particolare comando viene attivato
	 */
	static const std::string slideIndexParName;
	static const std::string slideOffsetParName;

	/*  - slidesTemplateParName è il nome del parametro del main template dove dovranno essere inserite le slide
	 *  - labelTemplateParName è il nome del parametro del main template dove dovranno essere inserite le label
	 *  - radioTemplateParName è il nome del parametro del main template dove dovranno essere inserite i radio-button
	 *  - cssTemplateParName è il nome del parametro del main template dove dovranno essere inserite i comando css
	 */
	static const std::string slidesTemplateParName;
	static const std::string labelTemplateParName;
	static const std::string radioTemplateParName;
	static const std::string cssTemplateParName;

};

#endif
