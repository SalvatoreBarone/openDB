/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "TestResourcesUrl.hpp"
#include "MainWindow.hpp"

#include <QtGui>
#include <QProgressDialog>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

const std::string TestResourcesUrl::getResourcesSqlQuery = "\
select codice_art, sito, progressivo, url, icon, testo \
from risorse_articoli \
where sito='www.decoration.it' \
order by codice_art, progressivo";
const std::string TestResourcesUrl::columnCode = "codice_art";
const std::string TestResourcesUrl::columnSite = "sito";
const std::string TestResourcesUrl::columnProg = "progressivo";
const std::string TestResourcesUrl::columnUrl = "url";
const std::string TestResourcesUrl::columnIcon = "icon";
const std::string TestResourcesUrl::columnText = "testo";

TestResourcesUrl::TestResourcesUrl(	MainWindow* window,
					openDB::Table* resourcesTable) :
		QWidget(window),
		parentWindow(window),
		table(resourcesTable),
		netManager(new QNetworkAccessManager),
		textBrowser(new QTextBrowser(this)),
		progressBar(new QProgressBar(this)),
		layout(new QVBoxLayout(this))

{
	setLayout(layout);
	layout->addWidget(textBrowser);
	layout->addWidget(progressBar);
	connect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(testUrl(QNetworkReply*)));

	//esecuzione della query per ottenere tutte le informazioni relative alle risorse articoli memorizzate in database.
	//non viene utilizzata la funzione loadQuery in modo da non sovrascrivere eventuali risorse presenti nella tabella locale e non ancora
	//rese permanenti su database.
	openDB::NonTransactional query;
	query.appendCommand(getResourcesSqlQuery);
	result_it = parentWindow->backendConnector().execTransaction(query);
	openDB::Table& _resultTable = result_it.result().resultTable();

	progressBar->setMinimum(0);
	progressBar->setMaximum(_resultTable.storage().records() - 1);

	//per ciascuna risorsa viene creata una richiesta a QNetworkManager
	openDB::Storage::Iterator storage_it = _resultTable.storage().begin(), storage_end = _resultTable.storage().end();
	for (; storage_it != storage_end; storage_it++)
	{
		openDB::Storage::Iterator it_tmp = storage_it;
		openDB::Record record = *storage_it;
		std::unordered_map<std::string, std::string> values;
		record.current(values);

		QString url;
		if (isRelative(values[columnUrl]))
			url = "http://" + QString::fromStdString(values[columnSite]) + "/" + QString::fromStdString(values[columnUrl]);
		else
			url = QString::fromStdString(values[columnUrl]);

		QNetworkReply* reply = netManager->head(QNetworkRequest(QUrl(url)));
		testMap.insert(std::pair<QNetworkReply*, openDB::Storage::Iterator> (reply, it_tmp));
	}
}

TestResourcesUrl::~TestResourcesUrl()
{
	delete netManager;
	delete textBrowser;
	delete progressBar;
	delete layout;

	auto it = testMap.begin(), end = testMap.end();
	for (; it != end; it++)
		delete it->first;
	parentWindow->backendConnector().deleteTransaction(result_it);
}

/* Il test per stabilire se un url sia relativo o assoluto e' molto semplice: se l'url contiene la seguenza di caratteri "://"
 * es. (http://www.facebook.com/) allora si tratta di un url assoluto (per cui viene la funzione restituisce false). Se la sequenza non viene
 * trovata allora PROBABILMENTE si tratta di un url relativo.
 */
bool TestResourcesUrl::isRelative(const std::string& url)
{
	if ( url.find ( "://" ) != std::string::npos )
		return false;
	else
		return true;
}

void TestResourcesUrl::copyWrongUrl()
{
	table->storage().clear();
	table->loadRecord(result_it.result().resultTable());
}

void TestResourcesUrl::testUrl(QNetworkReply* reply)
{
	openDB::Storage::Iterator it = testMap[reply];

	/* Se head non restituisce http_OK (200) allora il link non e' valido.
	 * Se il link e' valido il record corretto viene eliminato dall'elenco.
	 */
	if ( reply->attribute ( QNetworkRequest::HttpStatusCodeAttribute ).toInt() == http_OK)
	{
		textBrowser->append(QString::fromStdString((*it).current(columnCode)) + "\turl: " + reply->url().path() + "\tok");
		result_it.result().resultTable().storage().eraseRecord(it);
	}
	else
		textBrowser->append("<b>" + QString::fromStdString((*it).current(columnCode)) + "\turl: " + reply->url().path() + "\turl error</b>");

	/* Viene predisposta l'eliminazione di reply
	 */
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator>::iterator mit = testMap.find(reply);
	testMap.erase(mit);
	reply->deleteLater();

	progressBar_mutex.lock();
		progressBar->setValue(progressBar->value() + 1);
	progressBar_mutex.unlock();

	if (progressBar->value() == progressBar->maximum())
		copyWrongUrl();
}
