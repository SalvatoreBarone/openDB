/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __TEST_RESOURCES_URL_H
#define __TEST_RESOURCES_URL_H

#include <QWidget>

#include <string>
#include <unordered_map>
#include <mutex>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class MainWindow;

class QNetworkAccessManager;
class QNetworkRequest;
class QNetworkReply;
class QTextBrowser;
class QProgressBar;
class QVBoxLayout;

class TestResourcesUrl : public QWidget
{
	Q_OBJECT
public:
	explicit TestResourcesUrl(MainWindow* window, openDB::Table* resourcesTable);

	virtual ~TestResourcesUrl();

private:
	MainWindow* parentWindow;
	openDB::Table* table;
	QNetworkAccessManager* netManager;
	QTextBrowser* textBrowser;
	QProgressBar* progressBar;
	QVBoxLayout* layout;

	/**
	 *  - getResourcesSqlQuery contiene la query sql per ottenere tutte le risorse collegate ad i vari articoli
	 *
	 *  - columnCode e' il nome della colonna contenente il codice dell'articolo cui e' collegata una risorsa;
	 *
	 *  - columnSite e' il nome della colonna contenente il sito cui la risorsa fa riferimento;
	 *
	 *  - columnProg e' il nome della colonna contenente un indice progressivo utilizzato per ordinare le risorse associate ad un certo articolo;
	 *
	 *  - columnUrl e' il nome della colonna contenente l'url relativo/assoluto della risorsa. I link relativi iniziano per /, quelli assoluti
	 *    con il classico http://www.sito.xxx/url_risorsa. Gli url relativi fanno riferimento al sito contenuto nella colonna columnSite;
	 *
	 *  - columnIcon e' il nome della colonna contenente l'url relativo/assoluto dell'icona usata nel codice html per raggiungere la risorsa;
	 *    I link assoluti cominciano col classico http:// mentre gli url relativi cominciano per /. Gli url relativi fanno riferimento al sito
	 *    contenuto nella colonna columnSite;
	 *
	 *  - columnText e' il nome della colonna contenente il testo usato nel codice HTML per il link alla risorsa. Viene caricato ugualmente
	 *    da database anche se non viene mai utilizzato nelle operazioni di test;
	 *
	 *  - result_it e' un iteratore di BackendConnector che punta al result della query utilizzata per ottenere le risorse collegate agli articolo.
	 *    E' necessario conservare questo iteratore perche' il test per ogni singola risorsa viene effettuato in split-phase utilizzando l'interfaccia
	 *    offerta da QNetworkManager. Al termine di tutti i test sara' possibile eliminare il result.
	 */
	static const std::string getResourcesSqlQuery;
	static const std::string columnCode;
	static const std::string columnSite;
	static const std::string columnProg;
	static const std::string columnUrl;
	static const std::string columnIcon;
	static const std::string columnText;
	openDB::BackendConnector::Iterator result_it;

	/* httpStatusCode definisce i codici identificativi delle risposte ai comandi Head o Get previste dal protocollo HTTP  */
	enum httpStatusCode
	{
		http_OK = 200,
		http_unauthorized = 401,
		http_forbidden = 403,
		http_notFound = 404,
		http_serverError = 500
	};

	/* Il seguente mutex viene utilizzato per l'accesso in mutua esclusione all'aggiornamento della progressBar */
	std::mutex progressBar_mutex;

	/* La mappa testMap conserva le corrispondenze QNetworkReply-Storage::Iterator ossia le corrispondenze tra le risposte di QNetworkManager
	 * relative al test generato a partire dalle informazioni contenute in un record puntato da un'istanza di un oggetto Storage::Iterator.
	 * La mappa viene utilizzata dalla funzione test ogni volta che QNetworkManager completa il prelievo della risorsa associata ad un particolare
	 * url per risalire al record che l'ha generata.
	 */
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator> testMap;

	/* Stabilisce se un url e' relativo o assoluto. Restituisce true se l'ulr e' relativo.
	 * Il test per stabilire se un url sia relativo o assoluto e' molto semplice: se l'url contiene la seguenza di caratteri "://"
	 * es. (http://www.facebook.com/) allora si tratta di un url assoluto (per cui viene la funzione restituisce false). Se la sequenza
	 * non viene trovata allora PROBABILMENTE si tratta di un url relativo.
	 */
	bool isRelative(const std::string& url);

	/* Copia i record che non hanno passato il test nella tabella table affinche' sia possibile correggerli */
	void copyWrongUrl();

private slots:

	void testUrl(QNetworkReply* reply);

};


#endif

