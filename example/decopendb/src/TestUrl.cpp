/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "TestUrl.hpp"
#include "MainWindow.hpp"

#include <QtGui>
#include <QProgressDialog>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QByteArray>

#include <fstream>
#include <iostream>
#include <unordered_map>

const std::string TestUrl::sqlQuery = "select codice_art, sito, relative_url, creazione, aggiornamento from url_articoli order by codice_art";
const std::string TestUrl::codeColumn = "codice_art";
const std::string TestUrl::siteColumn = "sito";
const std::string TestUrl::urlColumn = "relative_url";

TestUrl::TestUrl(QWidget* parent, MainWindow* window, openDB::Table* urlTable):
		QWidget(parent),
		parentWindow(window),
		table(urlTable),
		manager(new QNetworkAccessManager),
		progress(new QProgressBar(this)),
		remainingTest(0),
		textBrowser(new QTextBrowser(this)),
		layout(new QVBoxLayout(this))
{
	setLayout(layout);
	layout->addWidget(textBrowser);
	layout->addWidget(progress);
	connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(testUrl(QNetworkReply*)));

	openDB::NonTransactional query;
	query.appendCommand(sqlQuery);
	result_it = parentWindow->backendConnector().execTransaction(query);
	openDB::Table& _resultTable = result_it.result().resultTable();
	openDB::Storage::Iterator storage_it = _resultTable.storage().begin(), storage_end = _resultTable.storage().end();

	remainingTest = _resultTable.storage().records();
	progress->setMinimum(0);
	progress->setMaximum(remainingTest - 1);
	progress->show();

	for (; storage_it != storage_end; storage_it++)
	{
		openDB::Record record = *storage_it;
		std::unordered_map<std::string, std::string> values;
		record.current(values);

		std::string url = "http://" + values[siteColumn] + values[urlColumn];
		openDB::Storage::Iterator it_tmp = storage_it;
		QNetworkReply* reply = manager->get(QNetworkRequest(QUrl(QString::fromStdString(url))));
		std::pair<QNetworkReply*, openDB::Storage::Iterator> mapPair(reply, it_tmp);
		replyMap.insert(mapPair);

	}
}

TestUrl::~TestUrl()
{
	delete manager;
	delete progress;
	delete textBrowser;
	delete layout;

	parentWindow->backendConnector().deleteTransaction(result_it);
	auto it = replyMap.begin(), end = replyMap.end();
	for (; it != end; it++)
		delete it->first;
}

void TestUrl::testUrl(QNetworkReply* reply)
{
	openDB::Storage::Iterator it = replyMap[reply];
	openDB::Record record = *it;
	std::unordered_map<std::string, std::string> values;
	record.current(values);
	switch ( reply->attribute ( QNetworkRequest::HttpStatusCodeAttribute ).toInt() )
	{
		case http_OK:
			{
				if (codeTest(reply, values[codeColumn]))
				{
					result_it.result().resultTable().storage().eraseRecord(it);
					textBrowser->append(QString::fromStdString(values[codeColumn] + "\turl: http://" + values[siteColumn] + values[urlColumn] + "\tok"));
				}
				else
					textBrowser->append(QString::fromStdString(values[codeColumn] + "\turl: http://" + values[siteColumn] + values[urlColumn] + "\turl error"));
			}
			break;
		default:
			textBrowser->append(QString::fromStdString(values[codeColumn] + "\turl: http://" + values[siteColumn] + values[urlColumn] + "\turl error"));
			break;
	}

	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator>::iterator mit = replyMap.find(reply);
	replyMap.erase(mit);
	reply->deleteLater();

	mtx.lock();
		progress->setValue(progress->value() + 1);
		remainingTest--;
	mtx.unlock();

	if (remainingTest == 0)
		copyWrongUrl();
}

bool TestUrl::codeTest(QNetworkReply* reply, const std::string& code)
{
	QByteArray resurce = reply->readAll();
	std::string file ( resurce.data() );
	std::list<std::string> line_list;
	openDB::tokenize (line_list, file, '\0' );
	std::list<std::string>::const_iterator it = line_list.begin();
	bool codeFound = false;

	while ( it != line_list.end() && ! codeFound )
	{
		if ( it->find ( code ) != std::string::npos )
			codeFound = true;
		else
			it++;
	}
	return codeFound;
}

void TestUrl::copyWrongUrl()
{
	table->storage().clear();
	table->loadRecord(result_it.result().resultTable());
}
