/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef TEST_PAGE_URL
#define TEST_PAGE_URL

#include <QWidget>

#include <string>
#include <mutex>
#include <unordered_map>

#include "openDB2core.hpp"
#include "openDB2gui.hpp"

class MainWindow;
class QProgressBar;
class QNetworkAccessManager;
class QNetworkRequest;
class QNetworkReply;
class QTextBrowser;
class QVBoxLayout;

class TestUrl : public QWidget
{
	Q_OBJECT
public:
	explicit TestUrl(QWidget* parent, MainWindow* window, openDB::Table* urlTable);

	virtual ~TestUrl();

private:

	MainWindow* parentWindow;
	openDB::Table* table;
	QNetworkAccessManager* manager;
	QProgressBar* progress;
	std::mutex mtx;
	unsigned long remainingTest;
	std::unordered_map<QNetworkReply*, openDB::Storage::Iterator> replyMap;

	QTextBrowser* textBrowser;
	QVBoxLayout* layout;

	openDB::BackendConnector::Iterator result_it;
	static const std::string sqlQuery;
	static const std::string codeColumn;
	static const std::string siteColumn;
	static const std::string urlColumn;

	enum httpStatusCode
	{
		http_OK = 200,
		http_unauthorized = 401,
		http_forbidden = 403,
		http_notFound = 404,
		http_serverError = 500
	};

	bool codeTest(QNetworkReply* reply, const std::string& code);

	void copyWrongUrl();

private slots:
	void testUrl(QNetworkReply* reply);

};

#endif
