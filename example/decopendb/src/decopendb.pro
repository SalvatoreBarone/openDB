
TEMPLATE = app
TARGET = ../decopendb
QT += network
DEPENDPATH +=
INCLUDEPATH +=  /usr/include/postgresql /usr/include/openDB2
LIBS += -L.. -L. -lpthread -lopenDB2core -lopenDB2core -lopenDB2gui -lpq -lconfig++ -lsqlite3
QMAKE_CXXFLAGS += -std=c++11 -Wextra -ggdb3

HEADERS += \ 
PgLogin.hpp \	
MainWindow.hpp \
PageGenerator.hpp \
TestUrl.hpp \
DescrGenerator.hpp \
AllArcGenerator.hpp \
AmazonInsertLoader.hpp \
AmazonInsertGenerator.hpp \
AmazonUpdateGenerator.hpp \
EbayInsertGenerator.hpp \
EbayInsertLoader.hpp \
EbayUpdateGenerator.hpp \
CatalogsPageGenerator.hpp \
TestResourcesUrl.hpp \
MenuGenerator.hpp \
DownloadGenerator.hpp \
SliderGenerator.hpp \
AlldecorImgTest.hpp \

SOURCES += \
main.cpp \
PgLogin.cpp \
MainWindow.cpp \
PageGenerator.cpp \
TestUrl.cpp \
DescrGenerator.cpp \
AllArcGenerator.cpp \
AmazonInsertLoader.cpp \ 
AmazonInsertGenerator.cpp \
AmazonUpdateGenerator.cpp \
EbayInsertGenerator.cpp \
EbayInsertLoader.cpp \
EbayUpdateGenerator.cpp \
CatalogsPageGenerator.cpp \
TestResourcesUrl.cpp \
MenuGenerator.cpp \
DownloadGenerator.cpp \
SliderGenerator.cpp \
AlldecorImgTest.cpp \

