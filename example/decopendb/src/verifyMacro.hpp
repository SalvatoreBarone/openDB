/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __VERIFY_MACRO_H
#define __VERIFY_MACRO_H

#include <QFileDialog>

/**
 * La macro verifyFile viene utilizzata dai membri di verifica per verificare che sia stato settato un nome del file, al variare dei casi,
 * e che il file sia effettivamente accessibile. In caso non sia stato settato il file viene visualizzata una QMessageBox riportante un
 * messaggio di avviso. Nel caso in cui il file non sia accessibile un'altra QMessageBox riporta la situazione.
 *
 * La macro va usata in funzioni che devono restituire un valore booleano true se non viene rilevato nessun errore, false qualsiasi errore
 * venga rilevato.
 *
 * @param component componente dell'interfaccia QT su cui il test deve essere effettuato
 * @param message messaggio da visualizzare nel caso in cui component contenga una stringa vuota.
 */
#define verifyFile(component, message) \
if (component->text().isEmpty())\
{\
	QMessageBox::warning (	this,\
					"Attenzione!",\
					message,\
					QMessageBox::Ok);\
			return false;\
}\
else\
{\
	std::fstream stream(component->text().toStdString().c_str(), std::ios::in);\
	if (!stream.is_open())\
	{\
		QMessageBox::warning (	this,\
					"Attenzione!",\
					"Non e' stato possibile aprire il file" + component->text(),\
					QMessageBox::Ok);\
		stream.close();\
		return false;\
	}\
	stream.close();\
}\
return true;\



/**
 * La macro verifyFile viene utilizzata dai membri di verifica per verificare che sia stato settato un valore per un campo.
 * In caso non sia stato settato il valore viene visualizzata una QMessageBox riportante un messaggio di avviso.
 *
 * La macro va usata in funzioni che devono restituire un valore booleano true se non viene rilevato nessun errore, false qualsiasi errore
 * venga rilevato.
 *
 * @param component componente dell'interfaccia QT su cui il test deve essere effettuato
 * @param message messaggio da visualizzare nel caso in cui component contenga una stringa vuota.
 */
#define verifyField(component, message) \
if (component->text().isEmpty())\
{\
	QMessageBox::warning (	this,\
				"Attenzione!",\
				message,\
				QMessageBox::Ok);\
	return false;\
}\
return true;\

#endif
