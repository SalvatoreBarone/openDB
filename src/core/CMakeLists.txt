include_directories(
"."
".."
"Exceptions"
"SqlTypes"
"Structure"
"Structure/Storage"
"Connectors"
"UI"
"/usr/include/postgres/"
)



set(CORE_SRC
sha.cpp
common.cpp)

set(SQLTYPES_SRC
SqlTypes/Bigint.cpp
SqlTypes/Boolean.cpp
SqlTypes/Character.cpp
SqlTypes/Date.cpp
SqlTypes/DoublePrecision.cpp
SqlTypes/Integer.cpp
SqlTypes/Numeric.cpp
SqlTypes/Real.cpp
SqlTypes/SmallInt.cpp
SqlTypes/SqlType.cpp
SqlTypes/Time.cpp
SqlTypes/Timestamp.cpp
SqlTypes/Varchar.cpp)

set(STRUCTURE_SRC
Structure/Column.cpp
Structure/QuerySetting.cpp
Structure/Record.cpp
Structure/StorageConstIterator.cpp
Structure/Storage.cpp
Structure/StorageIterator.cpp
Structure/StorageOrderedConstIterator.cpp
Structure/Table.cpp
Structure/TableConstIterator.cpp
Structure/TableIterator.cpp
Structure/Schema.cpp
Structure/SchemaConstIterator.cpp
Structure/SchemaIterator.cpp
Structure/Database.cpp
Structure/DatabaseConstIterator.cpp
Structure/DatabaseIterator.cpp)

set(CONNECTORS_SRC 
Connectors/BackendConnector.cpp
Connectors/BackendConstIterator.cpp
Connectors/BackendIterator.cpp
Connectors/PgConnection.cpp
Connectors/PgConnector.cpp
Connectors/Sqlite3Connection.cpp
Connectors/Sqlite3Connector.cpp)

set(UI_SRC
UI/CsvUpdater.cpp
#~ UI/DatabaseBuilder.cpp
)

add_library(${PROJECT_NAME}core SHARED ${CORE_SRC} ${SQLTYPES_SRC} ${STRUCTURE_SRC} ${CONNECTORS_SRC} ${UI_SRC})




