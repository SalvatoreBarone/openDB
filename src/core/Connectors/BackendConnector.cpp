/**
 * @file BackendConnector.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "BackendConnector.hpp"
#include "PgConnection.hpp"
#include "Sqlite3Connection.hpp"
#include <typeinfo>
#include <thread>
using namespace openDB;

#ifdef __DEBUG
#include <iostream>
using namespace std;
#endif

BackendConnector::BackendConnector (unsigned connections) :
		connectionsNumber (connections),
		lastOperationId(0),
		connectionVector(connectionsNumber),
		freeConnectionNum(connectionsNumber)
{

}

BackendConnector::~BackendConnector()
{
	auto conn_end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != conn_end; it++ )
		delete it->connection;

}

BackendConnector::Iterator BackendConnector::begin() noexcept
{
	BackendConnector::Iterator it;
	it.parent = (BackendConnector*) this;
	it.iter = operationMap.begin();
	return it;
}


BackendConnector::ConstIterator BackendConnector::cbegin() const noexcept
{
	BackendConnector::ConstIterator it;
	it.parent = (BackendConnector*) this;
	it.iter = operationMap.begin();
	return it;
}

BackendConnector::Iterator BackendConnector::end() noexcept
{
	BackendConnector::Iterator it;
	it.parent = (BackendConnector*) this;
	it.iter = operationMap.end();
	return it;
}

BackendConnector::ConstIterator BackendConnector::cend() const noexcept
{
	BackendConnector::ConstIterator it;
	it.parent = (BackendConnector*) this;
	it.iter = operationMap.end();
	return it;
}

void BackendConnector::connect() throw ( BackendException& )
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		it->connection->connect();
}

void BackendConnector::reset() throw ( BackendException& )
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		it->connection->reset();
}

void BackendConnector::disconnect() noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		it->connection->disconnect();
}

enum Connection::Status BackendConnector::test() throw (BackendException&)
{
	Connection::Status __status = Connection::connectionOk;
	auto it = connectionVector.begin(), end = connectionVector.end();
	while (it != end && __status == Connection::connectionOk)
		__status = it++->connection->test();

	return __status;
}

BackendConnector::Iterator BackendConnector::exec(	const std::string command,
													const std::string &name,
													bool transaction,
													bool async) throw (BackendException&) {

	lastIdSafeIncrement();
	std::string newCommand = (transaction ? "begin;" + command + "end;" : command);
	BackendOperation newOperation(lastOperationId, newCommand, name);
	BackendConnector::Iterator it;
	it.parent = this;
	it.iter = operationMap.emplace(std::pair<unsigned long, BackendOperation>(lastOperationId, newOperation)).first;

	if (async) {
		std::thread thr(&BackendConnector::executeOperation, this, lastOperationId);
		thr.detach();
	} else {
		executeOperation(lastOperationId);
	}

	return it;
}

BackendConnector::Iterator BackendConnector::exec(	const std::list<std::string> &commands,
													const std::string &name,
													bool transaction,
													bool async) throw (BackendException&) {
	std::string command;
	for (std::list<std::string>::const_iterator it = commands.begin(); it != commands.end(); it++) {
		command += *it;
		if (command.back() != ';')
			command += ";";
	}
	return exec(command, name, transaction, async);
}


void BackendConnector::deleteResult(BackendConnector::Iterator& it) noexcept
{
	if ( it.brother(begin()) && it != end()) {
		operationMap.erase(it.iter);
		it = end();
	}
}

BackendConnector::Iterator BackendConnector::getStructure() throw (BackendException&)
{
	lastIdSafeIncrement();
	BackendOperation newOperation(lastOperationId, "", "Structure");
	BackendConnector::Iterator it;
	it.parent = this;
	it.iter = operationMap.emplace(std::pair<unsigned long, BackendOperation>(lastOperationId, newOperation)).first;

	Result& result = it.iter->second.result;
	result.setStatus(Result::running);

	/*
	 * Verify that there is a free connection that you can use.
	 */
	freeConnectionMtx.lock();
	while (freeConnectionNum == 0)
		/*
		 * The wait operations atomically release the lock and suspend the execution of the thread.
		 * When the condition variable is notified, the thread is awakened, and the lock is reacquired.
		 */
		freeConnection.wait(freeConnectionMtx);
	freeConnectionNum--; 			// decrementing the number of free connection
	freeConnectionMtx.unlock();		// mutex unlocking

	/*
	 * Find which connection is free, using the try_lock () function.
	 */
	std::vector<CuncurrentConnection>::iterator conn_it = connectionVector.begin();
	std::vector<CuncurrentConnection>::iterator conn_end = connectionVector.end();
	while (conn_it != conn_end && !(conn_it->connectionMtx.try_lock()))
		conn_it++;
	conn_it->connection->dbStructure(result);	// execution of the structure retrieval procedure
	conn_it->connectionMtx.unlock();			// connection mutex unlocking

	/*
	 * At the end, increase the number of free connections and signals the availability of a new free
	 * connection, if necessary.
	 */
	freeConnectionMtx.lock();
	freeConnectionNum++;
	if (freeConnectionNum == 1)
		freeConnection.notify_all();
	freeConnectionMtx.unlock();


	return it;
}

void BackendConnector::lastIdSafeIncrement() noexcept {
	while (operationMap.find(lastOperationId) != operationMap.end())
		lastOperationId++;
}

void BackendConnector::executeOperation (unsigned long id) throw (BackendException&)
{
	std::unordered_map<unsigned long, BackendOperation>::iterator op_it = operationMap.find(id);
	std::string command = op_it->second.command;
	Result& result = op_it->second.result;
	result.setStatus(Result::running);

	/*
	 * Verify that there is a free connection that you can use.
	 */
	freeConnectionMtx.lock();
	/*
	 * The wait operations atomically release the lock and suspend the execution of the thread.
	 * When the condition variable is notified, the thread is awakened, and the lock is reacquired.
	 * We need to retest the condition, so... a while is required!
	 */
	while (freeConnectionNum == 0)
		freeConnection.wait(freeConnectionMtx);
	freeConnectionNum--; 			// decrementing the number of free connection
	freeConnectionMtx.unlock();		// mutex unlocking

	/*
	 * Find which connection is free, using the try_lock () function.
	 */
	std::vector<CuncurrentConnection>::iterator conn_it = connectionVector.begin();
	std::vector<CuncurrentConnection>::iterator conn_end = connectionVector.end();
	while (conn_it != conn_end && !(conn_it->connectionMtx.try_lock()))
		conn_it++;
	conn_it->connection->exec(command, result);		// query/command execution
	conn_it->connectionMtx.unlock();				// connection mutex unlocking

	/*
	 * At the end, increase the number of free connections and signals the availability of a new free
	 * connection, if necessary.
	 */
	freeConnectionMtx.lock();
	freeConnectionNum++;
	if (freeConnectionNum == 1)
		freeConnection.notify_all();
	freeConnectionMtx.unlock();
}

