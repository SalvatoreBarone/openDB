/**
 * @file BackendConstIterator.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "BackendConnector.hpp"
#include <iterator>
using namespace openDB;

BackendConnector::ConstIterator BackendConnector::ConstIterator::operator++(int) noexcept
{
	BackendConnector::ConstIterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

BackendConnector::ConstIterator& BackendConnector::ConstIterator::operator++() noexcept
{
	next();
	return *this;
}

bool BackendConnector::ConstIterator::operator == (const BackendConnector::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent && iter == __it.iter)
		return true;
	return false;
}

bool BackendConnector::ConstIterator::operator != (const BackendConnector::ConstIterator& __it) const noexcept
{
	if (parent != __it.parent || iter != __it.iter)
		return true;
	return false;
}

bool BackendConnector::ConstIterator::brother(const BackendConnector::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent)
		return true;
	return false;
}

const BackendConnector::BackendOperation& BackendConnector::ConstIterator::operator*() const throw ( BackendException& ) {
		if (!parent)
		throw BackendException (
			BackendException::iteratorParent,
			__PRETTY_FUNCTION__,
			"Error using BackendConnector::ConstIterator::result(): no valid parent pointer;");

	if (iter == parent->operationMap.end())
		throw BackendException (
			BackendException::iteratorAccess,
			__PRETTY_FUNCTION__,
			"Error using BackendConnector::ConstIterator::result(): can't access to end();");

	return iter->second;
}

const BackendConnector::BackendOperation* BackendConnector::ConstIterator::operator->() const throw ( BackendException& ) {
		if (!parent)
		throw BackendException (
			BackendException::iteratorParent,
			__PRETTY_FUNCTION__,
			"Error using BackendConnector::ConstIterator::result(): no valid parent pointer;");

	if (iter == parent->operationMap.end())
		throw BackendException (
			BackendException::iteratorAccess,
			__PRETTY_FUNCTION__,
			"Error using BackendConnector::ConstIterator::result(): can't access to end();");

	return &(iter->second);
}

BackendConnector::ConstIterator& BackendConnector::ConstIterator::next () noexcept
{
	if (parent && iter != parent->operationMap.end())
		iter++;
	return* this;
}

