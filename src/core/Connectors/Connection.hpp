/**
 * @file Connection.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_CONNECTION
#define OPENDB2_CONNECTION

/**
 * @addtogroup Connectors
 * @{
 */

#include <string>
#include "Result.hpp"
#include "../Exceptions/BackendException.hpp"

namespace openDB
{

/**
 * @brief Interface for connection classes.
 *
 * @details The Connection class defines the interface that needs to be implemented by classes that are
 * responsible for creating remote database connections (managed by a DBMS) to send SQL statements generated
 * by the library.
 */
class Connection
{
public:

	/**
	 * @brief Connection status.
	 *
	 * @details The classes that implement the Connection interface must use this enumerative type to indicate
	 * the status of the connection with the DBMS.
	 */
	enum Status {
		connectionOk,	/**< connectionOk, the connection is active: you can send sql statements to the DBMS.
		 	 	 	 	 	 */

		connectionBad	/**< connectionBad, the connection isn't active: you can't send sql statements to the
							 DBMS. */
	};

	/**
	 * @brief Constructor.
	 *
	 * @details Initializes the connection status, setting it to connectionBad.
	 */
	Connection () noexcept : status(connectionBad) {}

	virtual ~Connection () {}

	/**
	 * @brief Connect to the DBMS
	 *
	 * @details The function must implement the connection mechanisms to the remote DBMS.
	 *
	 * @exception May fire a BackendException exception in case an error occurs during the connection attempt.
	 */
	virtual void connect() throw ( BackendException& ) = 0;

	/**
	 * @brief Reset a connection
	 *
	 * @details The function must implement the mechanisms for resetting a connection.
	 */
	virtual void reset() throw ( BackendException& ) = 0;

	/**
	 * @brief Disconnect from DBMS
	 *
	 * @details The function must implement mechanisms for disconnecting from databases.
	 */
	virtual void disconnect() noexcept = 0;

	/**
	 * @brief Test of a connection status.
	 *
	 * @details The function implements any connection status test mechanisms.
	 *
	 * @return Returns the status of the connection.
	 *
	 * @exception May fire a BackendException exception if errors occur during the test.
	 */
	virtual enum Connection::Status test() throw ( BackendException& )  = 0;

	/**
	 * @brief Send an sql statement to the DBMS for it to run.
	 *
	 * @details The function must implement the mechanisms that allow you to send SQL statement to the remote
	 * DBMS and retrieve the execution result.
	 *
	 * @param [in] 	command SQL statement(s);
	 * @param [out] res 	a Result object, it must contain the result of execution.
	 *
	 * @exception May fire a BackendException exception in case an error occurs during execution.
	 */
	virtual void exec (const std::string& command, Result& res) throw ( BackendException& ) = 0;

	/**
	 * @brief Retrieve the remote database structure
	 *
	 * @details The function must implement the mechanism that retrieves the entire database structure
	 * directly from the DBMS.
	 *
	 * @param [out] dbstruct a reference to a Result object. It must contain a row for each column in the
	 * database, each of which containing the following fields:
	 * - schema_name: name of the schema;
	 * - table_name: name of the table;
	 * - column_name: name of the column;
	 * - udt_name: type of the column;
	 * - length: maximum length for varchar and character data type;
	 * - precision: precision for numeric data type;
	 * - scale: scale for numeric data type;
	 * - key: 'yes'/'no', yes means that this column compose the key for the table to which it belongs.
	 *
	 * @exception May fire a BackendException exception in case an error occurs during execution.
	 */
	virtual void dbStructure(Result& dbstruct) throw ( BackendException& ) = 0;

protected:

	Status status; /**< Connection status */
};

/**
 * @}
 */


};

#endif
