/**
 * @file PgConnection.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "PgConnection.hpp"
using namespace openDB;

const std::string PgConnection::pgDbStructureQuery = "select \
col.table_schema as schema_name, \
col.table_name as table_name, \
col.column_name as column_name, \
col.udt_name, \
col.character_maximum_length as length, \
col.numeric_precision as precision, \
col.numeric_scale as scale, \
case \
when (col.table_schema, col.table_name, col.column_name) in \
( \
select \
col.table_schema, \
col.table_name, \
col.column_name \
from information_schema.columns col \
join information_schema.constraint_column_usage ccl \
on col.table_schema=ccl.table_schema \
and col.table_name=ccl.table_name \
and col.column_name=ccl.column_name \
join information_schema.table_constraints ts \
on ccl.constraint_name=ts.constraint_name \
where ts.constraint_type = 'PRIMARY KEY' \
and col.table_schema not in ('pg_catalog','information_schema') \
) \
then 'yes' \
else 'no' end \
	as key \
	from information_schema.columns col \
	where col.table_schema not in('pg_catalog', 'information_schema') \
	order by col.table_schema, col.table_name, col.ordinal_position";

void PgConnection::connect() throw ( BackendException& )
{
	std::string connection_string;
	connection_string =	"host='" + hostaddr +
				"' port='" + port +
				"' dbname='" + dbname +
				"' user='" +  username +
				"' password='" + password +
				"' connect_timeout='" + std::to_string(timeout) + "'";
	if (pgconnection == 0)
	{
		pgconnection = PQconnectdb(connection_string.c_str());
		if  (pgconnection == 0)
			throw BackendException(
					BackendException::memoryError,
					__PRETTY_FUNCTION__,
					"Can't establish a connection to the database server: not enough memory.");
		else
			if (PQstatus(pgconnection) != CONNECTION_OK)
			{
				std::string msg = std::string(PQerrorMessage(pgconnection));
				PQfinish(pgconnection);
				pgconnection = 0;
				throw BackendException(
					BackendException::connectionError,
					__PRETTY_FUNCTION__,
					"Can't establish a connection to the database server: " + msg);
			}
			else
				status = connectionOk;
	}
}

void PgConnection::reset() throw ( BackendException& )
{
	if (pgconnection != 0)
		PQreset(pgconnection);
	else
		throw BackendException(
		BackendException::connectionError,
		__PRETTY_FUNCTION__,
		"Can't reset a non-made connection to the database");
}

void PgConnection::disconnect() noexcept
{
	if (pgconnection != 0)
	{
		PQfinish(pgconnection);
		pgconnection = 0;
		status = connectionBad;
	}
}

enum Connection::Status PgConnection::test() throw ( BackendException& )
{
	if  (pgconnection == 0)
	{
		status = connectionBad;
		throw BackendException(
				BackendException::memoryError,
				__PRETTY_FUNCTION__,
				"Can't connect to the server: not enough memory.");
	}
	else
	{
		if (PQstatus(pgconnection) != CONNECTION_OK)
		{
			std::string msg = std::string(PQerrorMessage(pgconnection));
			status = connectionBad;
			PQfinish(pgconnection);
			pgconnection = 0;
			throw BackendException(
				BackendException::connectionError,
				__PRETTY_FUNCTION__,
				"Can't connect to the server: " + msg);
		}
		else
			status = connectionOk;
	}

	return status;
}

void PgConnection::exec (const std::string& command, Result& res) throw ( BackendException& )
{
	if (pgconnection == 0 || status == connectionBad)
		throw BackendException(
				BackendException::execBadConnection,
				__PRETTY_FUNCTION__,
				"Sql command execution on a bad connection.");

	PGresult* pgresult = PQexec(pgconnection, command.c_str());
	if (pgresult != 0)
	{
		ExecStatusType pgstatus = PQresultStatus(pgresult);
		switch (pgstatus)
		{
			case PGRES_COMMAND_OK:
				{
					res.setStatus(Result::commandOk);
					std::string cmdStatus = std::string(PQcmdStatus(pgresult));
					std::string cmdTuples = std::string(PQcmdTuples(pgresult));
					res.setInfo(cmdStatus + "\n" + cmdTuples);
					break;
				}

			case PGRES_TUPLES_OK:
				{
					res.setStatus(Result::tupleOk);
					std::string cmdStatus = std::string(PQcmdStatus(pgresult));
					std::string cmdTuples = std::string(PQcmdTuples(pgresult));
					res.setInfo(cmdStatus + "\n" + cmdTuples);
					createColumns(pgresult, res);
					createRecords(pgresult, res);
				}
				break;

			case PGRES_FATAL_ERROR:
			default:
				{
					res.setStatus(Result::error);
					std::string severity = std::string(PQresultErrorField(pgresult ,PG_DIAG_SEVERITY));
					std::string sqlState = std::string(PQresultErrorField(pgresult ,PG_DIAG_SQLSTATE));
					std::string messagePrimary = std::string(PQresultErrorField(pgresult ,PG_DIAG_MESSAGE_PRIMARY));
					std::string errorMessage = std::string(PQresultErrorMessage(pgresult));
					res.setInfo(	errorMessage + "\n" +
									severity + "\n" +
									sqlState + "\n" +
									messagePrimary);

				}
				break;
		}
		PQclear(pgresult);

	}
	else
		throw BackendException(
			BackendException::memoryError,
			__PRETTY_FUNCTION__,
			"Can't execute the command: not enough memory.");
}


void PgConnection::dbStructure(Result& dbstruct) throw ( BackendException& ) {

	if (pgconnection == 0 || status == connectionBad)
		throw BackendException(
				BackendException::execBadConnection,
				__PRETTY_FUNCTION__,
				"Sql command execution on a bad connection.");

	PGresult* pgresult = PQexec(pgconnection, pgDbStructureQuery.c_str());
	if (pgresult != 0)
	{
		ExecStatusType pgstatus = PQresultStatus(pgresult);
	switch (pgstatus)
		{

			case PGRES_TUPLES_OK:
				{
					dbstruct.setStatus(Result::tupleOk);
					std::string cmdStatus = std::string(PQcmdStatus(pgresult));
					std::string cmdTuples = std::string(PQcmdTuples(pgresult));
					dbstruct.setInfo(cmdStatus + "\n" + cmdTuples);
					createColumns(pgresult, dbstruct);
					createRecords(pgresult, dbstruct);
				}
				break;

			case PGRES_FATAL_ERROR:
			default:
				{
					dbstruct.setStatus(Result::error);
					std::string severity = std::string(PQresultErrorField(pgresult ,PG_DIAG_SEVERITY));
					std::string sqlState = std::string(PQresultErrorField(pgresult ,PG_DIAG_SQLSTATE));
					std::string messagePrimary = std::string(PQresultErrorField(pgresult ,PG_DIAG_MESSAGE_PRIMARY));
					std::string errorMessage = std::string(PQresultErrorMessage(pgresult));
					dbstruct.setInfo(	errorMessage + "\n" +
										severity + "\n" +
										sqlState + "\n" +
										messagePrimary);
				}
				break;
		}
		PQclear(pgresult);

	}
	else
		throw BackendException(
			BackendException::memoryError,
			__PRETTY_FUNCTION__,
			"Can't execute the command: not enough memory.");

}

void PgConnection::createColumns(PGresult* pgresult, Table& table) const
{
	int numcol = PQnfields(pgresult);
	for (int col = 0; col < numcol; col++)
		table.addColumn(std::string(PQfname(pgresult, col)), Varchar());
}

void PgConnection::createRecords(PGresult* pgresult, Table& table) const
{
	int numcol = PQnfields(pgresult), numrow = PQntuples(pgresult);
	for (int row = 0; row < numrow; row++)
	{
		Record rec = table.getStorage().newRecord();
		std::unordered_map<std::string, std::string> valuesMap;
		for (int col = 0; col < numcol; col++)
			valuesMap.insert(std::pair<std::string, std::string>(std::string(PQfname(pgresult, col)), std::string(PQgetvalue(pgresult, row, col))));
		rec.edit(valuesMap);
		rec.setStatus(Record::loaded);
		table.getStorage().addRecord(rec);
	}
}
