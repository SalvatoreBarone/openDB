/**
 * @file PgConnection.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_PGCONNECTION
#define OPENDB2_PGCONNECTION

/**
 * @addtogroup Core
 * @{
 * @addtogroup Connectors
 * @{
 */

#include "Connection.hpp"
#include <postgresql/libpq-fe.h>

namespace openDB
{

/**
 * @brief Connector for PostgreSQL DBMS.
 *
 * @details This class implements the Connection interface, specializing it for the PostgreSQL DBMS. Allows
 * you to connect to a database managed by a local or remote DBSM server by specifying several connection
 * parameters, such as the host address on which PostgreSQL is running, the port on which the server is
 * listening, the particular database to interact, user name and password for user
 * identification.
 * The class also implements the mechanism of database structure retrieval directly through the DBMS.
 */
class PgConnection : public Connection
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @details Builds an empty object, it will be the task of the programmer (or user) to set the connection
	 * parameters.
	 */
	PgConnection() : Connection(), pgconnection(0), timeout(5) {}

	/**
	 * @brief Destructor.
	 *
	 * @details Closes connection with the database before destroying the object.
	 */
	virtual ~PgConnection()	{ disconnect(); }

	/**
	 * @brief Sets the host address on which PostgreSQL is running.
	 *
	 * @param [in] str the host address on which PostgreSQL is running
	 */
	void setHostaddr ( const std::string& str ) noexcept { hostaddr = str;}

	/**
	 * @brief Sets the port on which the server is listening.
	 *
	 * @param [in] str the port on which the server is listening.
	 */
	void setPort ( const std::string& str) noexcept {port = str;}

	/**
	 * @brief Sets the name of the database to interact with.
	 *
	 * @param [in] str the name of the database.
	 */
	void setDbName (const std::string& str) noexcept {dbname = str;}

	/**
	 * @brief Sets the username, for login purpose
	 *
	 * @param [in] str the username
	 */
	void setUsername (const std::string& str) noexcept {username = str;}

	/**
	 * @brief Sets the password, for login purpose
	 *
	 * @param [in] str the password
	 */
	void setPassword (const std::string& str) noexcept {password = str;}

	/**
	 * @brief Sets the maximum wait for connection, in seconds.
	 *
	 * @param [in] t maximum wait for connection, in seconds.
	 */
	void setTimeout (int t) noexcept {timeout = t;}

	/**
	 * @brief Connect to the database.
	 *
	 * @exception BackendException if an error occurs while attempting to connect. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 *
	 * @note The connection is made in blocking mode.
	 */
	virtual void connect() throw ( BackendException& );

	/**
	 * @brief Reset a connection to a database.
	 *
	 * @exception May fire a BackendException exception in case an error occurs during the connection attempt.
	 */
	virtual void reset() throw ( BackendException& );

	/**
	 * @brief Disconnect from a database.
	 *
	 * @details The function also frees the memory used by the object managing the connection.
	 */
	virtual void disconnect() noexcept;

	/**
	 * @brief Test the status of a connection.
	 *
	 * @retval Connection::connectionOk if the connection is active;
	 * @retval Connection::connectionBad if the connection isn't active.
	 *
	 * @note The test is made in blocking mode.
	 *
	 * @exception BackendException if an error occurs while attempting to test the connection. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 */
	virtual enum Connection::Status test()  throw ( BackendException& );

	/**
	 * @brief Sends an SQL statement to the DBMS and returns the execution result.
	 *
	 * @param [in] command the SQL statement. It can also be made up of multiple SQL commands, separated by
	 * a ';';
	 * @param [out] res a Result object, it will contain the result of execution.
	 *
	 * @note The execution is made in blocking mode.
	 *
	 * @exception BackendException if an error occurs while attempting to exec the statements. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 */
	virtual void exec (const std::string& command, Result& res) throw ( BackendException& );

	/**
	 * @brief Retrieve the remote database structure.
	 *
	 * @param [out] dbstruct a reference to a Result object. It will contain a row for each column in the
	 * database, each of which containing the following fields:
	 * - schema_name: name of the schema;
	 * - table_name: name of the table;
	 * - column_name: name of the column;
	 * - udt_name: type of the column;
	 * - length: maximum length for varchar and character data type;
	 * - precision: precision for numeric data type;
	 * - scale: scale for numeric data type;
	 * - key: 'yes'/'no', yes means that this column compose the key for the table to which it belongs.
	 *
	 * @note The execution is made in blocking mode.
	 *
	 * @exception BackendException if an error occurs while attempting to exec the statements. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 */
	virtual void dbStructure(Result& dbstruct) throw ( BackendException& );

private:
	PGconn* pgconnection;		/**< pointer to the object used to access PostgreSQL DBMS, using the libpq
									 interface */

	std::string hostaddr; 		/**< the host address on which PostgreSQL is running */
	std::string port;			/**< the port on which the server is listening */
	std::string dbname;			/**< the particular database to interact */
	std::string username;		/**< username, for login purpose */
	std::string password;		/**< password, for login purpose */
	int			timeout;		/**< maximum wait for connection, in seconds */

	/**
	 * Contains the sql query used by the dbStructure() function to retrieve the database structure.
	 */
	static const std::string pgDbStructureQuery;

	/**
	 * @brief Create the column structure for the Table object that contains the result of a SQL query.
	 *
	 * @param [in]	pgresult	pointer to the object used to access PostgreSQL result, using the libpq
								interface.
	 *
	 * @param [out]	table		Table object to define the structure, in terms of columns. The data type will
	 * 							be set to varchar, regardless of which column type is in the remote database.
	 */
	void createColumns(PGresult* pgresult, Table& table) const;

	/**
	 * @brief Copy records from the object returned from the libpq library to a Table object.
	 *
	 * @param [in]	pgresult	pointer to the object used to access PostgreSQL result, using the libpq
								interface.
	 *
	 * @param [out]	table		Table object in which the records will be copied. Its structure must first be
	 * 							created by the PgConnection::createColumn() function.
	 */
	void createRecords(PGresult* pgresult, Table& table) const;
};

/**
 * @}
 * @}
 */


};

#endif
