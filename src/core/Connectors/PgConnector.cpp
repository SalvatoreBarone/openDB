/**
 * @file PgConnector.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "PgConnector.hpp"
#include "PgConnection.hpp"
using namespace openDB;

PgConnector::PgConnector(unsigned nconn) noexcept :
		BackendConnector(nconn)
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		it->connection = new PgConnection();
}

void PgConnector::setHostaddr ( const std::string& str ) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setHostaddr(str);
}

void PgConnector::setPort ( const std::string& str) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setPort(str);
}

void PgConnector::setDbName (const std::string& str) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setDbName(str);
}

void PgConnector::setUsername (const std::string& str) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setUsername(str);
}

void PgConnector::setPassword (const std::string& str) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setPassword(str);
}

void PgConnector::setTimeout (int t) noexcept
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		dynamic_cast<PgConnection*>(it->connection)->setTimeout(t);
}


