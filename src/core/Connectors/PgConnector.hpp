/**
 * @file PgConnector.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB_POSTGRESQL_CONNECTOR_H
#define __OPENDB_POSTGRESQL_CONNECTOR_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Connectors
 * @{
 */

#include "BackendConnector.hpp"
#include "Connection.hpp"

namespace openDB
{

/**
 * @brief PortgreSQL database connector
 *
 * @details This class extends the BackendConnector class, providing the features that allow you to connect
 * and interact with a database managed by the PostgreSQL DBMS. The class allows to set the connection
 * parameters for all the different PgConnection objects managed by an object of this class.
 */
class PgConnector : public BackendConnector
{
public:
	explicit PgConnector (unsigned nconn = 5) noexcept;

	virtual ~PgConnector() {}

	/**
	 * @brief Sets the host address on which PostgreSQL is running.
	 *
	 * @param [in] str the host address on which PostgreSQL is running
	 */
	void setHostaddr ( const std::string& str ) noexcept;

	/**
	 * @brief Sets the port on which the server is listening.
	 *
	 * @param [in] str the port on which the server is listening.
	 */
	void setPort ( const std::string& str) noexcept;

	/**
	 * @brief Sets the name of the database to interact with.
	 *
	 * @param [in] str the name of the database.
	 */
	void setDbName (const std::string& str) noexcept;

	/**
	 * @brief Sets the username, for login purpose
	 *
	 * @param [in] str the username
	 */
	void setUsername (const std::string& str) noexcept;

	/**
	 * @brief Sets the password, for login purpose
	 *
	 * @param [in] str the password
	 */
	void setPassword (const std::string& str) noexcept;

	/**
	 * @brief Sets the maximum wait for connection, in seconds.
	 *
	 * @param [in] t maximum wait for connection, in seconds.
	 */
	void setTimeout (int t) noexcept;

};

/**
 * @}
 * @}
 */

};

#endif
