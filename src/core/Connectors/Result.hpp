/**
 * @file Result.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_RESULT
#define OPENDB2_RESULT

/**
 * @addtogroup Core
 * @{
 * @addtogroup Connectors
 * @{
 */

#include <string>
#include "../Structure/Table.hpp"

namespace openDB
{

/**
 * @brief Class to manage SQL statement execution result.
 *
 * @details The class constitutes a wrapper around the Table class, which adds only some useful features to
 * managing the different scenarios that may occur during executing sql commands. Such scenarios include error
 * handling, the management of the execution result of commands that do not generate a result (such as insert,
 * update, and delete), and the execution of the query execution result (such as select).
 */
class Result : public Table
{
public:

	/**
	 * @brief Result status
	 *
	 * @details The status of the result is updated by the classes that are physically interfacing with the
	 * DBMS (currently PgConnection and Sqlite3Connection).
	 */
	enum Status {
		empty,    /**< 	empty, the result is empty (probably because the sql command that generate it has not
						yet been executed) */
		running,  /**< 	running, the result is incomplete, (probably because the sql command that generate it has not
						yet been completed) */
		commandOk,/**< 	commandOk, the execution of an insert, update, or delete command has been successfully
						completed */
		tupleOk,  /**< tupleOk, the execution of a query has been successfully completed */
		error     /**< error, an error occurred while executing the sql command. */
	};

	/**
	 * @brief Constructor
	 *
	 * @details Builds an empty Result object
	 *
	 * @param [in] name an (optional) name for the internal Table object managing the result.
	 */
	explicit Result (const std::string& name = "result") : Table(name), status(empty) {}

	virtual ~Result() {}

	/**
	 * @brief Gets the status of a Result object.
	 *
	 * @return a Result::Status element, status of the Result object.
	 */
	enum Result::Status getStatus() const noexcept {return status;}

	/**
	 * @brief Sets the status of a Result object.
	 *
	 * @param [in] s a Result::Status element, the new status for the Result object.
	 *
	 * @note You should not use this function. The status of a Record object is automatically managed by the
	 * classes that interact with the DBMS
	 */
	void setStatus(enum Result::Status s) noexcept {status = s;}

	/**
	 * @brief Returns some useful information about the result.
	 *
	 * @return some useful information about the result.
	 */
	std::string getInfo() const noexcept {return infoMessage;}

	/**
	 * @brief Sets the internal information message.
	 *
	 * @param  [in] str an information message about the result.
	 *
	 * @note You should not use this function. The Record object is internally managed by the classes that
	 * interact with the DBMS.
	 */
	void setInfo(const std::string& str) noexcept {infoMessage = str;}

private:
	Status status;				/**< Result status */
	std::string infoMessage;	/**< Some useful information about the result */
};

/**
 * @}
 * @}
 */


};

#endif
