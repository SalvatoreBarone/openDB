/**
 * @file Sqlite3Connection.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Sqlite3Connection.hpp"
using namespace openDB;

/* Le seguenti sono usate dalla funzione membro exec.
 * Non possono essere dichiarate funzioni membro a causa della signature della funzione sqlite3_exec()
 */
int callback (void* res, int columns_number, char** row_values,char** columns_name) noexcept;
void createColumns(Table *table, int columns_number, char** columns_name) noexcept;
void loadRecord(Table *table, int columns_number, char** row_values, char** columns_name) noexcept;

openDB::Sqlite3Connection::Sqlite3Connection(const std::string& dbFileName) noexcept :
	fileName(dbFileName),
	dbConnection(0)
{
}

openDB::Sqlite3Connection::~Sqlite3Connection()
{
	disconnect();
}

void openDB::Sqlite3Connection::createAndConnect() throw ( BackendException& ) {
	if (dbConnection)
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"Connection already made!");

	if (fileName.empty())
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"You must specify a database file name!");

	int errorCode;
	if ((errorCode = sqlite3_open_v2(
				fileName.c_str(),
				&dbConnection,
				SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,
				0)) != SQLITE_OK)
	{
		status = Connection::connectionBad;
		std::string message = std::string(sqlite3_errmsg(dbConnection)) + "\n " + std::string(sqlite3_errstr(errorCode));
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"Can't establish a connection to the database \"" + fileName + "\": " + message);
	}
	else
		status = Connection::connectionOk;
}

void openDB::Sqlite3Connection::connect() throw (BackendException&)
{
	if (dbConnection)
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"Connection already made!");

	if (fileName.empty())
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"You must specify a database file name!");

	int errorCode;
	if ((errorCode = sqlite3_open_v2(
				fileName.c_str(),
				&dbConnection,
				SQLITE_OPEN_READWRITE,
				0)) != SQLITE_OK)
	{
		status = Connection::connectionBad;
		std::string message = std::string(sqlite3_errmsg(dbConnection)) + "\n " + std::string(sqlite3_errstr(errorCode));
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"Can't establish a connection to the database \"" + fileName + "\": " + message);
	}
	else
		status = Connection::connectionOk;
}

void openDB::Sqlite3Connection::reset() throw ( BackendException& )
{
	if (status == Connection::connectionOk) {
		disconnect();
		connect();
	} else
	throw BackendException(
		BackendException::connectionError,
		__PRETTY_FUNCTION__,
		"Can't reset a non-made connection to the database \"" + fileName);
}

void openDB::Sqlite3Connection::disconnect() noexcept
{
	if (dbConnection)
	{
		while (sqlite3_close(dbConnection) != SQLITE_OK);
		dbConnection = 0;
	}
}

enum Connection::Status openDB::Sqlite3Connection::test() throw ( BackendException& )
{
	int errorCode;
	if ((errorCode = sqlite3_errcode(dbConnection)) == SQLITE_OK)
		return (status = Connection::connectionOk);
	else
	{
		status = Connection::connectionBad;
		std::string message = std::string(sqlite3_errmsg(dbConnection)) + "\n" + std::string(sqlite3_errstr(errorCode));
		throw BackendException(
			BackendException::connectionError,
			__PRETTY_FUNCTION__,
			"Database connection test on \"" + fileName + "\": " + message);
	}
}


void openDB::Sqlite3Connection::exec(const std::string& command, Result& res) throw (BackendException&)
{
	if (dbConnection && status == Connection::connectionOk)
	{
		res.setStatus(Result::running);
		char* msg = 0;
		int exitCode = sqlite3_exec(
				dbConnection,
				command.c_str(),
				callback,
				(void*)&res,
				&msg);

		std::string cmdStatus;
		if (exitCode != SQLITE_OK)
		{
			res.setStatus(Result::error);
			cmdStatus = std::string(msg);
		}
		else
		{
			if (command.find("select") == std::string::npos)
				res.setStatus(Result::commandOk);
			else
				res.setStatus(Result::tupleOk);
			cmdStatus = "Ok";
		}
		res.setInfo(cmdStatus);
	}
}

void openDB::Sqlite3Connection::dbStructure(Result& dbstruct) throw ( BackendException& ) {

}

void createColumns(	Table *table,
					int columns_number,
					char** columns_name) noexcept
{
	for (int i = 0; i < columns_number; i++)
	{
		std::string name = std::string(columns_name[i]);
		table->addColumn(name, openDB::Varchar(), false);
	}
}

void loadRecord(	Table *table,
					int columns_number,
					char** row_values,
					char** columns_name) noexcept
{
	Record rec = table->getStorage().newRecord();
	std::unordered_map<std::string, std::string> map;
	for (int i = 0; i < columns_number; i++)
	{
		std::string	col_name = std::string(columns_name[i]),
				value = std::string((row_values[i] ? row_values[i] : "")); // row value e' nullo se il valore sql e' nullo
		std::pair<std::string, std::string> pair(col_name, value);
		map.insert(pair);
	}
	rec.edit(map);
	rec.setStatus(Record::loaded);
	table->getStorage().addRecord(rec);
}

int callback (	void* void_res,
				int columns_number,
				char** row_values,
				char** column_name) noexcept
{
	openDB::Result* res = (openDB::Result*)void_res;

	if (res->getStorage().recordsNum() == 0)
		createColumns(res, columns_number, column_name);

	loadRecord(res, columns_number, row_values, column_name);

	return 0;
}
