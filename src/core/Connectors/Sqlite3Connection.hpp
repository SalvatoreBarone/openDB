/**
 * @file Sqlite3Connection.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB_SQLITE3_CONNECTION_H
#define __OPENDB_SQLITE3_CONNECTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Connectors
 * @{
 */

#include "Connection.hpp"
#include <sqlite3.h>
#include <string>

namespace openDB
{

/**
 * @brief Connector for PostgreSQL DBMS.
 *
 * @details This class implements the Connection interface, specializing it for the PostgreSQL DBMS. Allows
 * you to connect to a database managed by a local Sqlite3 server, specifying the name of the file containing
 * the database.
 */
class Sqlite3Connection : public Connection
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @details Initializes the connection status, setting it to connectionBad.
	 */
	explicit Sqlite3Connection(const std::string& dbFileName = std::string()) noexcept;

	virtual ~Sqlite3Connection();

	/**
	 * @brief Set the name of the file containing the database, if it was empty.
	 *
	 * @param [in] dbFileName name of the file
	 */
	void setFileName(const std::string& dbFileName) noexcept {fileName = dbFileName;}

	/**
	 * @brief Create a database, if it doesn't exists, and connect to it.
	 *
	 * @exception BackendException if an error occurs while attempting to connect. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 *
	 * @note The connection is made in blocking mode.
	 */
	void createAndConnect() throw ( BackendException& );

	/**
	 * @brief Connect to the database.
	 *
	 * @exception BackendException if an error occurs while attempting to connect. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 *
	 * @note The connection is made in blocking mode.
	 */
	virtual void connect() throw ( BackendException& );

	/**
	 * @brief Reset a connection to a database.
	 *
	 * @exception May fire a BackendException exception in case an error occurs during the connection attempt.
	 */
	virtual void reset() throw ( BackendException& );

	/**
	 * @brief Disconnect from a database.
	 *
	 * @details The function also frees the memory used by the object managing the connection.
	 */
	virtual void disconnect() noexcept;

	/**
	 * @brief Test of a connection status.
	 *
	 * @details The function implements any connection status test mechanisms.
	 *
	 * @return Returns the status of the connection.
	 *
	 * @exception May fire a BackendException exception if errors occur during the test.
	 */
	virtual enum Connection::Status test() throw ( BackendException& );

	/**
	 * @brief Sends an SQL statement to the DBMS and returns the execution result.
	 *
	 * @param [in] command the SQL statement. It can also be made up of multiple SQL commands, separated by
	 * a ';';
	 * @param [out] res a Result object, it will contain the result of execution.
	 *
	 * @note The execution is made in blocking mode.
	 *
	 * @exception BackendException if an error occurs while attempting to exec the statements. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 */
	virtual void exec (const std::string& command, Result& res) throw ( BackendException& );

	/**
	 * @brief Retrieve the remote database structure.
	 *
	 * @param [out] dbstruct a reference to a Result object. It will contain a row for each column in the
	 * database, each of which containing the following fields:
	 * - schema_name: name of the schema;
	 * - table_name: name of the table;
	 * - column_name: name of the column;
	 * - udt_name: type of the column;
	 * - length: maximum length for varchar and character data type;
	 * - precision: precision for numeric data type;
	 * - scale: scale for numeric data type;
	 * - key: 'yes'/'no', yes means that this column compose the key for the table to which it belongs.
	 *
	 * @note The execution is made in blocking mode.
	 *
	 * @exception BackendException if an error occurs while attempting to exec the statements. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 */
	virtual void dbStructure(Result& dbstruct) throw ( BackendException& );

private:

	std::string fileName; /**< name of the file containing the database */

	sqlite3* dbConnection; /**< pointer to the object used to access Sqlite3 DBMS, using the libsqlite3
								interface */


};

/**
 * @}
 * @}
 */


};

#endif
