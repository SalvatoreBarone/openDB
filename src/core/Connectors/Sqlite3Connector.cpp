/**
 * @file Sqlite3Connector.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Sqlite3Connector.hpp"
#include "Sqlite3Connection.hpp"
using namespace openDB;

openDB::Sqlite3Connector::Sqlite3Connector() :
		BackendConnector(1)
{
	auto end = connectionVector.end();
	for (auto it = connectionVector.begin(); it != end; it++ )
		it->connection = new Sqlite3Connection();
}

void openDB::Sqlite3Connector::setFileName(const std::string& file_name) noexcept
{
	if (__fileName.empty())
	{
		__fileName = file_name;
		auto end = connectionVector.end();
		for (auto it = connectionVector.begin(); it != end; it++ )
			dynamic_cast<Sqlite3Connection*>(it->connection)->setFileName(file_name);
	}
}

void openDB::Sqlite3Connector::createAndConnect() throw ( BackendException& ) {
	auto it = connectionVector.begin();
	auto end = connectionVector.end();
	dynamic_cast<Sqlite3Connection*>(it->connection)->createAndConnect();
	for (++it; it != end; it++)
		dynamic_cast<Sqlite3Connection*>(it->connection)->connect();
}
