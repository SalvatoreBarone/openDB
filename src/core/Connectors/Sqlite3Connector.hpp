/**
 * @file Sqlite3Connector.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDBB_SQLITE3_CONNECTOR_H
#define __OPENDBB_SQLITE3_CONNECTOR_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Connectors
 * @{
 */

#include "BackendConnector.hpp"

namespace openDB
{

/**
 * @brief Sqlite3 database connector
 *
 * @details This class extends the BackendConnector class, providing the features that allow you to connect
 * and interact with a database managed by the Sqlite3 DBMS. The class allows to set the connection
 * parameters for all the different Sqlite3 objects managed by an object of this class.
 *
 * @note The Sqlite3Connector class uses only one connection to the "remote" Sqlite3 database. This means that
 * the "async" parameter of the exec() functions is ignored.
 */
class Sqlite3Connector : public BackendConnector
{
public:
	explicit Sqlite3Connector ();

	virtual ~Sqlite3Connector() {}

	/**
	 * @brief Set the name of the file containing the database.
	 *
	 * @param [in] dbFileName name of the file
	 */
	void setFileName(const std::string& dbFileName) noexcept;

	/**
	 * @brief Create a database, if it doesn't exists, and connect to it.
	 *
	 * @exception BackendException if an error occurs while attempting to connect. Using the
	 * BackendException::getExcpCode() method, you can trace the cause of the error. The object will also
	 * contain a description of the error.
	 *
	 * @note The connection is made in blocking mode.
	 */
	void createAndConnect() throw ( BackendException& );

private:
	std::string __fileName;

};

/**
 * @}
 * @}
 */


};

#endif
