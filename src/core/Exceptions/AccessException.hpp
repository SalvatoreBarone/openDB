/**
 * @file AccessException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __ACCESS_EXCEPTION_H
#define __ACCESS_EXCEPTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Exceptions
 * @{
 */

#include "BasicException.hpp"

namespace openDB
{

/**
 * @brief Exception class related to error in access to structures or fields.
 *
 * @details This type of exception is raised when errors occur in accessing structures such as schemas,
 * tables, columns, or fields in a record.
 */
class AccessException : public BasicException
{
public:

	/**
	 * @brief Error code.
	 *
	 * @details It allows you to quickly identify the cause of error and make it easier to manage.
	 */
	typedef enum
	{
		ColumnName,       	/**< you used an empty string as the name of a column you want to access */
		ColumnDoesntExist,	/**< the column you want to access doesn't exist */
		SchemaName,       	/**< you used an empty string as the name of a schema you want to access */
		TableName,        	/**< you used an empty string as the name of a column you want to access */
		IteratorNoParent, 	/**< when you are trying to access a structure through an iterator that is not
								 associated with any "parent" structure */
		IteratorAccessEnd 	/**< when you are trying to access to 'end()' position */

	} AccessExceptionCode;

	/**
	 * @brief Constructor.
	 *
	 * @param [in] code		error code related to an exception.
	 * @param [in] fname	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg		brief description message for the exception fired.
	 */
	AccessException (	AccessExceptionCode code,
						const std::string& fname,
						const std::string& msg) :
							BasicException (fname, msg),
							code(code) {}

	/**
	 * @brief Returns the error code related to an exception.
	 *
	 * @retval ColumnName: you used an empty string as the name of a column you want to	 access;
	 * @retval ColumnDoesntExist: the column you want to acces doesn't exist;
	 * @retval SchemaName: you used an empty string as the name of a schema you want to access;
	 * @retval TableName: you used an empty string as the name of a column you want to access;
	 * @retval IteratorNoParent: when you are trying to access a structure through an iterator that is not
	 * associated with any "parent" structure;
	 * @retval IteratorAccessEnd: then you are trying to access to 'end()' position.
	 */
	AccessExceptionCode getCode() { return code; }

private:
	AccessExceptionCode code; /**< Error code related to an exception. */

};

/**
 * @}
 * @}
 */

};
#endif
