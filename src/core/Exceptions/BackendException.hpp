/**
 * @file BackendException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_BACKEND_EXCEPTION
#define OPENDB2_BACKEND_EXCEPTION

/**
 * @addtogroup Core
 * @{
 * @addtogroup Exceptions
 * @{
 *
 */

#include "BasicException.hpp"

namespace openDB
{

/**
 * @brief Backend (DBMS) related exception
 *
 * @details This type of exception is raised when errors occur in connecting or accessing local or remote
 * databases.
 */
class BackendException : public BasicException
{
public:

	/**
	 * @brief Error code.
	 *
	 * @details It allows you to quickly identify the cause of error and make it easier to manage.
	 */
	typedef enum
	{
		connectionError, 	/**< cannot establish a connection with the database server for any reason; the
		 	 	 	 	 	 	 message field contains a brief description of the error */
		memoryError,		/**< not enough memory to establish a connection with the database server */
		execBadConnection,	/**< you are trying to execute a sql statement on an non-configured connection or
		 	 	 	 	 	 	 on a no longer valid connection */
		iteratorParent,		/**< when you are trying to access a structure through an iterator that is not
								 associated with any "parent" structure */
		iteratorAccess		/**< when you are trying to access to an invalid position */
	} ExceptionCode;

	/**
	 * @brief Constructor.
	 *
	 * @param [in] code 	error code related to the exception
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 */
	BackendException (	ExceptionCode code,
						const std::string& fname,
						const std::string& msg) :
							BasicException (fname, msg), excpCode(code) {}

	/**
	 * @brief Returns the error code related to an exception.
	 *
	 * @return an ExceptionCode error code, related to the exception fired.
	 */
	ExceptionCode getExcpCode() const
	{
		return excpCode;
	}

protected:
	ExceptionCode excpCode;	/**< Error code related to an exception. */

};
/**
 * @}
 * @}
 */
};
#endif
