/**
 * @file BasicException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB2_BASIC_EXCEPTION_H
#define __OPENDB2_BASIC_EXCEPTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Exceptions
 * @{
 *
 */

#include <string>
#include <exception>

namespace openDB
{

/**
 * @brief Exception base class
 *
 * @details All classes used to manage exceptions are derived from this class. This class derives from
 * std::exception.
 */
class BasicException : public std::exception
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 */
	BasicException (const std::string& fname,
					const std::string& msg) :	function(fname), message(msg) {}

	/**
	 * @brief Returns a null terminated character sequence that may be used to identify the exception.
	 *
	 * @details The particular representation pointed by the returned value is implementation-defined.
	 *
	 * @return A pointer to a c-string with content related to the exception. This is guaranteed to be valid
	 * at least until the exception object from which it is obtained is destroyed.
	 */
	virtual const char* what() const noexcept {return message.c_str();}

	/**
	 * @brief Returns a brief description message for the exception fired.
	 *
	 * @return a (const) reference to the internal std::string 'message' object.
	 */
	virtual const std::string& getMessage() const {return message;}

	/**
	 * @brief Set brief description message for the exception fired.
	 *
	 * @param [in] msg brief description message for the exception fired.
	 */
	virtual void setMessage(const std::string& msg) {message = msg;}

	/**
	 * @brief Returns the name of the function within which the exception was fired.
	 *
	 * @return a (const) reference to the internal std::string 'function' object.
	 */
	virtual const std::string& getFunction() const {return function;}

protected:

	std::string function;	/**< Name of the function within which the exception was fired */
	std::string message; 	/**< Brief description message for the exception fired. */
};

/**
 * @brief Exception fired when a key field is assuming a null value.
 */
class EmptyKey : public BasicException
{
public:
	/**
	 * @brief Constructor.
	 *
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg 		brief description message for the exception fired.
	 */
	EmptyKey (	const std::string& fname,
				const std::string& msg) : BasicException (fname, msg) {}
};


/**
 * @}
 * @}
 */
};
#endif
