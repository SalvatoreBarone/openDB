/**
 * @file CsvUpdaterException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __CSV_UPDATE_MANAGER_EXCEPTION_H
#define __CSV_UPDATE_MANAGER_EXCEPTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup UI
 * @{
 */

#include "BasicException.hpp"

namespace openDB
{

class CsvUpdater;

class CsvUpdaterException : public BasicException
{
public:
	typedef enum
	{
		streamError,
		controlStringError,
		tableError,
		columnsError,
		lineError,
		opCodeError,
		idError

	} ExceptionCode;

	CsvUpdaterException (	CsvUpdater* man,
								ExceptionCode code,
								const std::string& __funcName,
								const std::string& __message ) :
									BasicException(__funcName, __message),
									manager(man),
									excpCode(code) {}

	ExceptionCode getExcpCode() const {
		return excpCode;
	}

	const CsvUpdater* getManager() const {
		return manager;
	}
		
protected:
	CsvUpdater* manager;
	ExceptionCode excpCode;

};

/**
 * @}
 * @}
 */

};
#endif
