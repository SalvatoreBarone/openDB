/**
 * @file DataException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB2_DATA_EXCEPTION_H
#define __OPENDB2_DATA_EXCEPTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Exceptions
 * @{
 */

#include "BasicException.hpp"

namespace openDB
{

/**
 * @brief Exception class related to sql data type error.
 *
 * @details This type of exception is raised when errors occur in validate values
 */
class DataException : public BasicException
{
public:

	/**
	 * @brief Error code.
	 *
	 * @details It allows you to quickly identify the cause of error and make it easier to manage.
	 */
	typedef enum
	{
		DataExceptionEC = 0,
		InvalidArgument = 1,
		BooleanValue = 1001,	/**< used by the Boolean::validate() function when the string cannot be
									 translated neither as "true" or "false. */

		SmallintValue = 1002,	/**< used by the Smallint::validate() function when the string cannot be
									 translated to a Smallint */

		IntegerValue = 1003,	/**< used by the Integer::validate() function when the string cannot be
									 translated to an Integer; */

		BigintValue = 1004,		/**< used by the Bigint::validate() function when the string cannot be
									 translated to a Bigint; */

		RealValue = 1005,		/**< used by the Real::validate() function when the string cannot be
									 translated to a Real; */

		DoubleValue = 1006,		/**< used by the DoublePrecision::validate() function when the string cannot
									 be translated to a DoublePrecision; */

		NumericValue = 1007,	/**< used by the Numeric::validate() function when the string cannot be
									 translated to Numeric(p, s), respecting the precision and/or scale
									 paramether; */

		TimeValue = 1008,		/**< used by the Time::validate() function when the string cannot be
									 translated to a Time, neither in the hh:mm nor in the hh:mm:ss format */

		DateValue = 1009,		/**< used by the Date::validate() function when the string cannot be
									 translated to a Date, neither in the yyyy/mm/dd nor in the dd/mm/yyyy
									 format */

		AmbiguousDate = 2,		/**< used by the Date::validate() function when the string can be translated
									 to a Date, but the result is ambiguous */

		InvalidDate = 3,		/**< used by the Date::validate() function when the string can be translated
		 	 	 	 	 	 	 	 in a Date, but the result is meaningless */

		InvalidTime = 4,		/**< used by the Time::validate() function when the string can be translated
		 	 	 	 	 	 	 	 in a Time, but the result is meaningless */
		OutOfBound = 5,

		SmallintBound  = 5001,	/**< used by the Smallint::validate() function when the bounds arent met; */

		IntegerBound  = 5002,	/**< used by the Integer::validate() function when the bounds aren't met; */

		BigintBound  = 5003,	/**< used by the Bigint::validate() function when the bounds aren't met; */

		RealBound  = 5004,		/**< used by the Real::validate() function when the bounds arent' met; */

		DoubleBound  = 5005,	/**< used by the DoublePrecision::validate() function when the bounds aren't
									 met; */

		NumericBound  = 5006,	/**< used by the Numeric::validate() function when the bounds aren't met; */

		NumericPrecision  = 5007,	/**< used by the Numeric::validate() function when the constraint on the
										 precision paramether isn't respected; */

		NumericScale  = 5008,	/**< used by the Numeric::validate() function when the constraint on the scale
									 paramether is not respected */
		ValueTooLong = 6,
		VarcharLength = 6001,	/**< used by the Varchar::validate() function when the constraint on the
		 	 	 	 	 	 	 	 string length is not respected */
		CharacterLength = 6002	/**< used by the Character::validate() functionfunction when the constraint on
		 	 	 	 	 	 	 	 the string length is not respected; */
	} DataExceptionCode;


	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	DataException (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::DataExceptionEC ) :
						BasicException (fname, msg),
						value(value),
						code(code) {}

	/**
	 * @brief Returns the string triggering the exception.
	 *
	 * @return a string containing the value triggering the exception.
	 */
	const std::string& getValue() const noexcept {return value;}

	/**
	 * @brief Returns the error code related to the exception
	 *
	 * @return a DataExceptionCode, the error code related to the exception.
	 */
	DataExceptionCode getCode() const noexcept {return code;}


protected:
	std::string value;		/**< value triggering the exception. */
	DataExceptionCode code; /**< error code related to the exception */
};

/**
 * @brief Invalid Argument exception.
 *
 * @details Generated from
 * 	 - Boolean::validate() when the string cannot be translated neither as "true" or "false
 * 	 - Smallint::validate() when the string cannot be translated to Smallint
 * 	 - Integer::validate() when the string cannot be translated to Integer;
 * 	 - BigInt::validate() when the string cannot be translated to BigInt;
 * 	 - Real::validate()when the string cannot be translated to Real;
 * 	 - DoublePrecision::validate() when the string cannot be translated to DoublePrecision;
 * 	 - Numeric::validate() when the string cannot be translated to Numeric(p, s);
 * 	 - Time::validate() when the string cannot be translated to Time;
 * 	 - Date::validate() when the string cannot be translated to Date;
 */
class InvalidArgument : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	InvalidArgument ( 	const std::string& value,
						const std::string& fname,
						const std::string& msg,
						DataExceptionCode code = DataException::InvalidArgument ) :
							DataException(value, fname,  msg, code) {}
};

/**
 * @brief Ambiguous Date exception.
 *
 * @details Fired from Date::validate() function when the string can be translated to a Date, but the result
 * is ambiguous
 */
class AmbiguousDate : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	AmbiguousDate (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::AmbiguousDate ) :
						DataException(value, fname,  msg, code) {}
};

/**
 * @brief Invalud Date exception.
 *
 * @details Fired from Date::validate() function when the string can be translated to a Date, but the result
 * is meaningless
 */
class InvalidDate : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	InvalidDate (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::InvalidDate ) :
						DataException(value, fname,  msg, code) {}
};

/**
 * @brief Invalud Time exception.
 *
 * @details Fired from Time::validate() function when the string can be translated to a Time, but the result
 * is meaningless
 */
class InvalidTime : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	InvalidTime (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::InvalidTime) :
						DataException(value, fname,  msg, code) {}
};

/**
 * @brief Out of Bound exception
 *
 * @details Fired from
 *  - SmallInt::validate()
 * 	- Integer::validate()
 * 	- BigInt::validate()
 * 	- Real::validate()
 * 	- DoublePrecision::validate()
 * 	- Numeric::validate()
 * 	- Numeric::validate()
 *
 * 	when the constraints on the specific bound arent' met.
 */
class OutOfBound : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	OutOfBound (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::OutOfBound) :
					DataException(value, fname,  msg, code) {}
};

/**
 * @brief Value too long exception
 *
 * @details Fired from
 * - Varchar::validate();
 * - Character::validate();
 *
 * when the constraint on the string length is not met.
 */
class ValueTooLong : public DataException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] value	source value triggering the exception.
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 * @param [in] code		error code related to an exception.
	 */
	ValueTooLong (	const std::string& value,
					const std::string& fname,
					const std::string& msg,
					DataExceptionCode code = DataException::ValueTooLong) :
						DataException(value, fname,  msg, code) {}
};



/**
 * @}
 * @}
 */

};
#endif
