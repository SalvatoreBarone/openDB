/**
 * @file DatabaseBuilderException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB2_DBUILDER_EXCEPTION_H
#define __OPENDB2_DBUILDER_EXCEPTION_H

/**
 * @addtogroup UI
 * @{
 */

#include "openDBcore.hpp"

namespace openDB {

/**
 * @brief Exception class related to the DatabaseBuilder class.
 */
class DatabaseBuilderException : public BasicException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] fname	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg		brief description message for the exception fired.
	 */
	DatabaseBuilderException (	const std::string& fname,
								const std::string& msg) : BasicException (fname, msg) {}
};

/**
 * @}
 */

};

#endif
