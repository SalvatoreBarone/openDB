/**
 * @file StorageException.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __OPENDB2_STORAGE_EXCEPTION_H
#define __OPENDB2_STORAGE_EXCEPTION_H

/**
 * @addtogroup Core
 * @{
 * @addtogroup Exceptions
 * @{
 *
 */

#include "BasicException.hpp"



namespace openDB
{

class Storage;
class Record;

/**
 * @brief Exception class related to memory errors record errors.
 */
class StorageException : public BasicException
{
public:

	/**
	 * @brief Constructor.
	 *
	 * @param [in] stor		a pointer to the Storage object (tipically a MemStorage object) source triggering
	 * 						the exception;
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 */
	StorageException (	Storage* stor,
						const std::string& fname,
						const std::string& msg )  :
							BasicException (fname, msg),
							storage(stor),
							record(0) {}

	/**
	 * @brief Constructor.
	 *
	 * @param [in] rec		a pointer to the Record object source triggering the exception;
	 * @param [in] fname 	name of the function within which the exception was fired. Typically obtained
	 * 						by using the __PRETTY_FUNCTION__ macro.
	 * @param [in] msg	 	brief description message for the exception fired.
	 */
	StorageException (	Record* rec,
						const std::string& fname,
						const std::string& msg ) :
							BasicException (fname, msg),
							storage(0),
							record(rec) {}

	/**
	 * @brief Returns the Record object, source triggering the exception.
	 *
	 * @return a pointer to the Record object source triggering the exception.
	 */
	const Record* getRecord() const {
		return record;
	}

	/**
	 * @brief Returns the Storage object (tipically a MemStorage object) source triggering the exception;
	 * @return a pointer to the Storage object source triggering the exception.
	 */
	const Storage* getStorage() const {
		return storage;
	}
		
protected:
	Storage* storage; /**< pointer to the Storage object source triggering the exception */
	Record* record;	/**< pointer to the Record object source triggering the exception */
};


/**
 * @}
 * @}
 */

};
#endif
