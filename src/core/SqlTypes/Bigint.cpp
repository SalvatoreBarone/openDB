/**
 * @file BigInt.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Bigint.hpp"
#include <stdexcept>
#include <typeinfo>
#include <limits>

using namespace openDB;

const std::string Bigint::typeName = "bigint";
const std::string Bigint::udtName = "int8";
const long long Bigint::min = std::numeric_limits<long long>::min();
const long long Bigint::max = std::numeric_limits<long long>::max();

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType*  Bigint::clone() const noexcept {
	return new Bigint;
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as a BigInt value. In case one of the constraints is not respected, an exception is raised.
 * In this case, if the value can't be converted to an integer an InvalidArgument exception is raised, and
 * if the value can be converted to an integer but it is not within range of allowed values an OutOfBound
 * exception is raised.
 *
 * @exception InvalidArgument, if value can't be converted to an integer.
 * @exception OutOfBound, if the value can be converted to an integer but it is not within range of
 * allowed values.
 */
void Bigint::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		long long _value;
		try { _value = std::stoll ( value ); }
		catch ( std::out_of_range& e )
		{
			throw OutOfBound (value,
						__PRETTY_FUNCTION__,
						"value out of range for Big Int data type.",
						DataException::BigintBound);
		}

		catch ( std::invalid_argument& e )
		{
			throw InvalidArgument (	value,
						__PRETTY_FUNCTION__,
						"invalid value for Big Int data type.",
						DataException::BigintValue);
		}
		if ( ! ( _value >= min && _value <= max ) )
			throw OutOfBound (value,
						__PRETTY_FUNCTION__,
						"value out of range for Big Int data type.",
						DataException::BigintBound);
	}
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a bigint value. The function must only be used after the same string
 * has been verified using the validate() function, so that the validity of the value can be assumed by
 * hypothesis. In this case, no operation is required: if the string contains a valid value, it can be
 * used directly in an sql statement.
 */
std::string Bigint::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;
	return value;
}


/**
 * @brief Returns information about the internal representation of a Bigint object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Bigint object.
 */
struct SqlType::TypeInfo Bigint::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Bigint::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	return info;
}
