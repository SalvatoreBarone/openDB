/**
 * @file BigInt.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef BIGINT_SQL_TYPE
#define BIGINT_SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{

/**
 * @brief Bigint sql type.
 *
 * @details The Bigint sql type may be equivalent to the <i>long long int</i> data type.
 */
class Bigint : public SqlType
{
public:
	Bigint() noexcept {}
	
	virtual SqlType* clone() const noexcept;
	
	virtual ~Bigint() noexcept {}

	virtual void validate ( const std::string& value ) const throw ( DataException& );

	virtual std::string prepare ( const std::string& value ) const noexcept;

	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 7;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< BigInt UDT name */
	static const long long min;						/**< Lower bound for the set of allowed values */
	static const long long max;						/**< Upper bound for the set of allowed values */
};



/**
 * @}
 * @}
 */

};
#endif
