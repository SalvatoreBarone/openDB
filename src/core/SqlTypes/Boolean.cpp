/**
 * @file Boolean.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Boolean.hpp"
using namespace openDB;

const std::string Boolean::typeName = "boolean";
const std::string Boolean::udtName = "bool";
const std::string Boolean::trueDefault = "t";
const std::string Boolean::falseDefault = "f";
const std::list<std::string> Boolean::trueValues = {trueDefault, "TRUE", "true", "y", "yes", "on", "1", "Y", "S", "s", "T"};
const std::list<std::string> Boolean::falseValues = {falseDefault, "FALSE", "false", "n", "no", "off", "0", "N", "F"};

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Boolean::clone() const noexcept {return new Boolean;}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as a Boolean value. In case one of the constraints is not respected, an exception is raised.
 * Values ​​admitted as "true" are "true", "TRUE", "t", "y", "yes", "on", "1", "Y", "S", "s" or "T"; values
 * admitted as "false" are "false", "FALSE", "f", "n", "no", "off", "0", "N" or "F".
 * If the value is not among those allowed, an InvalidArgument exception is raised.
 *
 * @exception InvalidArgument, if the value is not among those allowed.
 */
void Boolean::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		bool valid_value = false;

		std::list<std::string>::const_iterator true_it = trueValues.begin();
		std::list<std::string>::const_iterator true_end = trueValues.end();
		while (true_it != true_end)
			if ( value == *true_it && !valid_value)
				valid_value = true;
			else true_it++;

		if (!valid_value)
		{
			std::list<std::string>::const_iterator false_it = falseValues.begin();
			std::list<std::string>::const_iterator false_end = falseValues.end();
			while (false_it != false_end && !valid_value )
				if ( value == *false_it )
					valid_value = true;
				else
					false_it++;
		}

		if (!valid_value)
			throw InvalidArgument (value,
						__PRETTY_FUNCTION__,
						"invalid value for Boolean data type.",
						DataException::BooleanValue );
	}
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a boolean value. The function must only be used after the same string
 * has been verified using the validate() function, so that the validity of the value can be assumed by
 * hypothesis.
 */
std::string Boolean::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;

	std::list<std::string>::const_iterator it = trueValues.begin();
	std::list<std::string>::const_iterator end = trueValues.end();
	for ( ; it != end; it++ )
		if ( value == *it )
			return "'" + trueDefault + "'";

	return  "'" +  falseDefault + "'";
}

/**
 * @brief Test for "true" values.
 *
 * @note The function must only be used after that the string has been verified using the validate() function,
 * so that the validity of the value can be assumed by hypothesis.
 *
 * @param [in] value string to test.
 *
 * @retval true if the string contains a value that can be translated as sql "true";
 * @retval false if the string doesn't contain a value that can be translated as sql "false";
 */
bool Boolean::isTrue ( std::string value ) noexcept
{
	bool istrue = false;
	std::list<std::string>::const_iterator it = trueValues.begin();

	while ( it != trueValues.end() && !istrue )
		if ( value == *it )
			istrue = true;
		else
			it++;

	return istrue;
}

/**
 * @brief Returns information about the internal representation of a Boolean object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Boolean object.
 */
struct SqlType::TypeInfo Boolean::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Boolean::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	return info;
}
