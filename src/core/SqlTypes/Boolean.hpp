/**
 * @file Boolean.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef BOOLEAN_SQL_TYPE
#define BOOLEAN_SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB {

/**
 * @brief Boolean is a sql equivalent of c++ <i>bool</i> data type.
 */
class Boolean : public SqlType {
public:

	Boolean() noexcept {}

	virtual SqlType* clone() const noexcept;
	
	virtual ~Boolean() noexcept {}
	
	virtual void validate ( const std::string& value ) const throw ( DataException& );

	virtual std::string prepare ( const std::string& value ) const noexcept;

	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 0;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Boolean UDT name */
	static const std::string trueDefault;			/**< Default value for "true" */
	static const std::string falseDefault;			/**< Default value for "false" */
	static const std::list<std::string> trueValues;	/**< Admitted values for "true": "true", "TRUE", "t", "y",
														 "yes", "on", "1", "Y", "S", "s" and "T"*/
	static const std::list<std::string> falseValues; /**< Admitted values fot "false": "false", "FALSE", "f",
														 "n", "no", "off", "0", "N" or "F"*/
	static bool isTrue(std::string value) noexcept;
};


/**
 * @}
 * @}
 */

};
#endif
