/**
 * @file Character.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Character.hpp"
using namespace openDB;

const std::string Character::typeName = "character";
const std::string Character::udtName = "bpchar";

/**
 * @brief Allocate a new Character object.
 *
 * @param [in] __length	Maximum length in terms of number of characters. Default is equal to the
 * 						defaultLength member.
 */
Character::Character (unsigned __length) noexcept {
	(__length <= maxLength ? length = __length : length = maxLength);
}

/**
 * @brief Clones the object on which it is called, returning an identical, correctly initialized copy.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism. The cloned object will contain the
 * same maximum number of characters of the object from which it was obtained.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Character::clone() const noexcept {
	return new Character(length);
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as a Character value. In case one of the constraints is not respected, an exception is raised.
 * For example, if the value in the string exceeds the maximum number of characters allowed, a
 * ValueTooLong exception is raised.
 *
 * @exception ValueTooLong, if the value in the string exceeds the maximum number of characters allowed.
 */
void Character::validate ( const std::string& value ) const throw ( DataException& )
{

	if (!SqlType::isNull(value) && value.size() > length)
		throw ValueTooLong (value,
					__PRETTY_FUNCTION__,
					"value too long for Character(" + std::to_string(length) + ").",
					DataException::CharacterLength);
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of this function is to prepare the value in the "value" string so that it can be used
 * within an sql statement. The function must be used after the same string has been verified using the
 * validate() function, so that the validity of the value can be assumed by hypothesis.
 * This function, in particular, adds a single-quote at the beginning and at the end of the string. In
 * addition, all the single-quotes in the string will be escaped.
 */
std::string Character::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;
	else
	{
		std::string tmp = value;

		size_t index = 0;
		while ( (index = tmp.find_first_of('\'', index)) != std::string::npos)
		{
			tmp.replace(index, 1, "''", 0, std::string::npos);
			index += 2;
		}

		return "'" + tmp + "'";
	}
}

/**
 * @brief Returns information about the internal representation of a Character object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Character object.
 */
struct SqlType::TypeInfo Character::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Character::internalID;
	info.typeName = typeName;
	info.vcharLength = length;
	info.udtName = udtName;
	return info;
}
