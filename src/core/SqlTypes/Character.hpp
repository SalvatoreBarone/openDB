/**
 * @file Character.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_CHARACTER_SQL_TYPE
#define OPENDB2_CHARACTER_SQL_TYPE


/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{


/**
 * @brief Character is a sql equivalent of <i>char*</i> o <i>std::string</i>.
 *
 * @details The sql character type assumes that if a string has a length less than the maximum length allowed,
 * the remaining characters are filled with a white space. This operation is not executed by the library,
 * because it is performed automatically by the DBMS upon insertion.
 */
class Character : public SqlType {
public:
	Character (unsigned __length = defaultLength) noexcept;
	virtual SqlType* clone() const noexcept;
	virtual ~Character() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 3;			/**< Internal ID of the translated type. */
	static const std::string typeName;			/**< Internal type name */
	static const std::string udtName;			/**< Character UDT name */
	static const unsigned maxLength = 1048576;	/**< Maximum length for a sql character value */
	static const unsigned defaultLength = 1;	/**< Default length for a sql character value */

protected:
	unsigned length;	/**< Maximum length for values of this type */
};


/**
 * @}
 * @}
 */

};
#endif
