/**
 * @file Date.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Date.hpp"
#include "common.hpp"
using namespace openDB;

const std::string Date::typeName = "date";
const std::string Date::udtName = "date";
const std::string Date::separators = "/-";
const std::string Date::format = "yyyy-MM-dd";

#include <sstream>
#include <iomanip>
#ifdef __DEBUG
#include <iostream>
#endif

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism. The cloned object will have the
 * same precision and scale of the object from which it was obtained.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Date::clone() const noexcept {
	return new Date;
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as date. In case one of the constraints is not respected, an exception is raised.
 * In this case, the function the function verifies that the string contains a date in yyyy/mm/dd or
 * dd/mm/yyyy format, otherwise an exception is raised.
 *
 *
 * @exception InvalidArgument, if the string content is not a date in the format yyyy/mm/dd or dd/mm/yyyy;
 * @exception AmbiguousDate, if the date, expressed in a format like yy/mm/dd or dd/mm/yy, is ambiguous.
 * For example 12/11/15 could be interpreted as November 12 2015 as well as as November 15 2012, and as
 * December 11, 2015;
 * @exception InvalidDate, if the date is meaningless, such as February 30 or November 31.
 */
void Date::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		DateInteger date(value);
		if ( !date.validate())
			throw InvalidDate (value,
						__PRETTY_FUNCTION__,
						"invalid date for Date data type.",
						DataException::DateValue);

	}
}


/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a date. The function must only be used after the same string has been
 * verified using the validate() function, so that the validity of the value can be assumed by hypothesis.
 * In this case the function returns a date in the yyyy-mm-dd format.
 */
std::string Date::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;

	DateInteger _date(value);

	std:: stringstream ss;
	ss 		<< std::setfill('0') << std::setw(4) << _date.year <<"-"
			<< std::setfill('0') << std::setw(2) << _date.month <<"-"
			<< std::setfill('0') << std::setw(2) << _date.day;
	return "'" + ss.str() + "'";
}

/**
 * @brief Try to convert a string to a date expressed in numeric format.
 *
 * @warning The function may fire an exception of type
 * - InvalidArgument: if the string content is not a date in the format yyyy/mm/dd or dd/mm/yyyy;
 * - AmbiguousDate: if the date, expressed in a format like yy/mm/dd or dd/mm/yy, is ambiguous. For
 *   example 12/11/15 could be interpreted both as November 12, 2015 as well as as November 15, 2012,
 *   and as December 11, 2015;
 */
Date::DateInteger::DateInteger( std::string __value ) throw ( DataException& )
{
	std::list<std::string> token;
	tokenize ( token , __value, separators );
	if ( token.size() != 3 )
		throw InvalidArgument (__value,
					__PRETTY_FUNCTION__,
					"invalid date for Date data type.",
					DataException::DateValue);

	std::list<std::string>::iterator token_it = token.begin();
	std::list<std::string>::iterator token_end = token.end();

	// verifico che i token siano numerici
	for (; token_it != token_end; token_it++) {
		try {
			std::stoul(*token_it);
		}
		catch ( std::exception& e ) {
			throw InvalidArgument (__value,
						__PRETTY_FUNCTION__,
						"invalid date for Date data type.",
						DataException::DateValue);
		}
	}

	token_it = token.begin();
	if ( token_it->size() == 4 )
	{
		//la data è nel formato yyyy/mm/dd
		year = std::stoul ( *token_it++ );
		month = std::stoul ( *token_it++ );
		day = std::stoul ( *token_it );
	}
	else {
		//la data è nel formato dd/mm/yyyy ?
		day = std::stoul ( *token_it++ );
		month = std::stoul ( *token_it++ );

		if ( token_it->size() == 4 )
			//la data è nel formato dd/mm/yyyy
			year = std::stoul ( *token_it );
		else
			//la data è ambigua.
			throw AmbiguousDate (__value,
						__PRETTY_FUNCTION__,
						"the date is ambiguous.", DataException::AmbiguousDate );
	}
}

/**
 * @brief Check that a date makes sense.
 *
 * @retval true if the date makes sense;
 * @retval false if the date in meaningless.
 */

bool Date::DateInteger::validate () const noexcept
{
	switch (month)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day >= 1 && day <= 31)
				return true;
			else
				return false;

		case 4:
		case 6:
		case 9:
		case 11:
			if (day >= 1 && day <= 30)
				return true;
			else
				return false;

		case 2:
			if (day >= 1 && day <= 28 )
				return true;
			else
			{
				if ( day == 29 && ( (year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0 ) )
					return true;
				else
					return false;
			}

		default
				:
			return false;
	}
}


/**
 * @brief Returns information about the internal representation of a Date object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Date object.
 */
struct SqlType::TypeInfo Date::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Date::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	return info;
}
