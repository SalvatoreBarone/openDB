/**
 * @file Date.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef DATE_SQL_TYPE
#define DATE_SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB {

/**
 * @brief Date data type
 *
 * @details There is no c++ equivalent for Date date type. This is a data type designed to contain dates in
 * yyyy/mm/dd format. Using the standard is uncomfortable for most people, so it is possible to enter a date
 * in both yyyy/mm/dd (or yyyy-mm-dd) and dd/mm/yyyy (or dd-mm-yyyy) format.
 */
class Date : virtual public SqlType {
public:
	Date () noexcept {}
	virtual SqlType* clone() const noexcept;
	virtual ~Date() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 1;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Date UDT name */
	static const std::string separators;			/**< Characters used as separator */
	static const std::string format;				/**< Format used the prepare() function */

protected:

	/**
	 * @brief Contains a date in numeric format.
	 *
	 * @details The structure is used by the verification functions.
	 */
	struct DateInteger {
		unsigned day;		/**< Day of the month */
		unsigned month;		/**< Month */
		unsigned year;		/**< Year */
		DateInteger() : day(0), month(0), year(0) {}
		DateInteger(std::string __value) throw (DataException&);
		bool validate () const noexcept;
	};




};



/**
 * @}
 * @}
 */

};
#endif
