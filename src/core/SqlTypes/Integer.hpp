/**
 * @file Integer.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_INTEGER_SQL_TYPE
#define OPENDB2_INTEGER_SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{

/**
 * @brief Integer sql type.
 *
 * @details The Integer sql type may be equivalent to the <i>int</i> data type.
 */
class Integer : public SqlType
{
public:
	Integer () noexcept {}
	virtual SqlType* clone() const noexcept;
	virtual ~Integer() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 6;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Integer UDT name */
	static const long int min;						/**< Lower bound for the set of allowed values */
	static const long int max;						/**< Upper bound for the set of allowed values */
};



/**
 * @}
 * @}
 */

};
#endif
