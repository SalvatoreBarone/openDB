/**
 * @file Numeric.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Numeric.hpp"
#include "common.hpp"
#include <stdexcept>
#include <typeinfo>
#include <limits>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <cstdio>
using namespace openDB;

const std::string Numeric::typeName = "numeric";
const std::string Numeric::udtName = "numeric";

/**
 * @brief Constructor
 *
 * @details The Numeric data type is an integer or real data type, defined by the user using two
 * parameters:
 * - precision: is the total number of significant digits of the whole number ie the total number of
 *  digits to the right and left of the point separating the integer from the fractional part;
 * - scale: is the number of significant digits of the fractional part, that is, the number of digits to
 * the right of the point.
 *
 * Thus, for example, 123.4567 has a 7-digit precision and a 4-digit scale.
 *
 * @param [in] __precision total number of significant digits of the whole number;
 * @param [in] __scale number of significant digits of the fractional part;
 *
 */
Numeric::Numeric ( unsigned __precision, unsigned __scale) noexcept
{
	( __precision <= maxPrecision ? precision = __precision : precision = maxPrecision );
	( __scale <= maxScale ? scale = __scale : scale = maxScale );
}

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism. The cloned object will have the
 * same precision and scale of the object from which it was obtained.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Numeric::clone() const noexcept {
	return new Numeric(precision, scale);
}


/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as an integer on a real number. In case one of the constraints is not respected, an
 * exception is raised.
 * In this case, if the value can't be converted to an integer or to a real number an InvalidArgument
 * exception is raised. If the constraint on precision or scale aren't respected an OutOfBound
 * exception is raised.
 *
 * @warning If the value can't be converted to an integer or to a real number an InvalidArgument
 * exception is raised. If the constraint on precision or scale aren't respected an OutOfBound
 * exception is raised.
 *
 * @exception InvalidArgument, if value can't be converted to an integer or fp-number.
 * @exception OutOfBound, if the constraint on precision or scale aren't respected.
 */
void Numeric::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		std::string valueUpper = value;
		std::transform(valueUpper.begin(), valueUpper.end(),valueUpper.begin(), ::toupper);
		if (	valueUpper != "NAN" &&
				(
					value.find_first_not_of("-+.,0123456789") != std::string::npos ||
					std::count(valueUpper.begin(), valueUpper.end(), '+') > 1 ||
					std::count(valueUpper.begin(), valueUpper.end(), '-') > 1 ||
					std::count(valueUpper.begin(), valueUpper.end(), ',') > 1 ||
					std::count(valueUpper.begin(), valueUpper.end(), '.') > 1 ||
					(std::count(valueUpper.begin(), valueUpper.end(), '+') && std::count(valueUpper.begin(), valueUpper.end(), '-'))
				)
			)
			throw InvalidArgument (value,
						__PRETTY_FUNCTION__,
						"invalid value for Numeric data type.",
						DataException::NumericValue);


		std::vector<std::string> token;
		tokenize (token, value, ".," );
		unsigned fractLength = 0, intLength = 0;
		switch (token.size()) {
		case 2:
			 fractLength = token[1].size();
		case 1:
			intLength = token[0].size();
			break;
		default:
			// due to previous checks, the following can be supefluous
			throw InvalidArgument (value,
						__PRETTY_FUNCTION__,
						"invalid value for Numeric data type.",
						DataException::NumericValue );
		}

		if (intLength + fractLength > precision)
			throw OutOfBound (value,
					__PRETTY_FUNCTION__,
					"allowable precision exceeded.",
					DataException::NumericPrecision);

		if ( fractLength > scale )
			throw OutOfBound (value,
				__PRETTY_FUNCTION__,
				"allowable scale exceeded.",
				DataException::NumericScale);

	}
}


/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as an integer or a real number. The function must only be used after the
 * same string has been verified using the validate() function, so that the validity of the value can be
 * assumed by hypothesis.
 * The function discards any whitespace characters until first non-whitespace character is found. Then it
 * takes as many characters as possible to form a valid floating-point representation and converts them to
 * a floating-point value.
 */
std::string Numeric::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;
	else
	{
		std::string tmp = value;
		size_t index = value.find_first_of(',');
		if (index != std::string::npos)
			tmp[index]='.';
		return tmp;

		// std::ostringstream ss;
		// ss << std::fixed << std::setprecision(scale) << std::stod (tmp);
		// return ss.str();
		// you can delete the following lines if all TC on the Numeric class goes OK!
		// char *buf = new char [tmp.length() + 2];
		// std::string format = "%." + std::to_string(scale) + "f";
		// std::sprintf(buf, format.c_str(), std::stod (value));
		// tmp = std::string(buf);
		// delete [] buf;
		// return tmp;
	}
}

/**
 * @brief Returns information about the internal representation of a Numeric object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Numeric object.
 */
struct SqlType::TypeInfo Numeric::typeInfo() const noexcept {
	SqlType::TypeInfo info;
	info.internalID = Numeric::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	info.numericPrecision = precision;
	info.numericScale = scale;
	return info;
}
