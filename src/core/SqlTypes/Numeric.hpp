/**
 * @file Numeric.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_NUMERIC_SQL_TYPE
#define OPENDB2_NUMERIC_SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{

/**
 * @brief Numeric data type
 *
 * @details There is no c ++ equivalent for Numeric data type. This is a numeric, integer or real data type,
 * defined by the user using two parameters:
 * - precision: is the total number of significant digits of the whole number ie the total number of digits to
 *   the right and left of the point separating the integer from the fractional part;
 * - scale: is the number of significant digits of the fractional part, that is, the number of digits to the
 *   right of the point.
 *
 * Thus, for example, 123.4567 has a 7-digit precision and a 4-digit scale. The Numeric type is an "exact"
 * type of data, that is, there is no approximation.
 */
class Numeric : public SqlType
{
public:
	Numeric ( unsigned __precision = defaultPrecision, unsigned __scale = defaultScale ) noexcept;
	virtual SqlType* clone() const noexcept;
	virtual ~Numeric() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 10;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Numeric UDT name */
	static const unsigned maxPrecision = 1000;		/**< Maximum value for precision */
	static const unsigned defaultPrecision = 1000;	/**< Default value for precision	*/
	static const unsigned maxScale = 1000;			/**< Maximum value for scale */
	static const unsigned defaultScale = 0;			/**< Default value for scale */

protected:
	unsigned precision;	/**< Total number of significant digits of the whole number */
	unsigned scale;		/**< Number of significant digits of the fractional part */
};


/**
 * @}
 * @}
 */

};
#endif
