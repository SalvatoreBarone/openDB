/**
 * @file Real.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Real.hpp"
#include <stdexcept>
#include <typeinfo>
#include <limits>
#include <algorithm>
#include <cstdio>
using namespace openDB;

const std::string Real::typeName = "real";
const std::string Real::udtName = "float4";
const float Real::smallest = std::numeric_limits<float>::lowest();
const float Real::max = std::numeric_limits<float>::max();


/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Real::clone() const noexcept {
	return new Real;
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as a Real value. In case one of the constraints is not respected, an exception is raised.
 * In this case, if the value can't be converted to a real number an InvalidArgument exception is raised;
 * if the value can be converted to a real number but it is not within range of allowed values an
 * OutOfBound exception is raised.
 *
 * @exception InvalidArgument, if value can't be converted to a fp-number.
 * @exception OutOfBound, if the value can be converted to a fp-number but it is not within range of
 * allowed values.
 */
void Real::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		std::string valueUpper = value;
		std::transform(valueUpper.begin(), valueUpper.end(),valueUpper.begin(), ::toupper);
		if (
			(	valueUpper != "INF" &&
				valueUpper != "+INF" &&
				valueUpper != "-INF" &&
				valueUpper != "INFINITY" &&
				valueUpper != "+INFINITY" &&
				valueUpper != "-INFINITY" &&
				valueUpper != "NAN" &&
				valueUpper != "+NAN" &&
				valueUpper != "-NAN"
			) &&
			(	valueUpper.find_first_not_of("-+.,0123456789E") != std::string::npos ||
				std::count(valueUpper.begin(), valueUpper.end(), '+') > 2 ||
				std::count(valueUpper.begin(), valueUpper.end(), '-') > 2 ||
				std::count(valueUpper.begin(), valueUpper.end(), ',') > 1 ||
				std::count(valueUpper.begin(), valueUpper.end(), '.') > 1 ||
				std::count(valueUpper.begin(), valueUpper.end(), 'E') > 1
			)
			)
			throw InvalidArgument (value,
						__PRETTY_FUNCTION__,
						"invalid value for Real data type.",
						DataException::RealValue);

		try {
			std::stof ( value );
		}
		catch ( std::out_of_range& e )
		{
			throw OutOfBound (value,
						__PRETTY_FUNCTION__,
						"value out of range for Real data type.",
						DataException::RealBound);
		}
		catch ( std::invalid_argument& e ) {
			throw InvalidArgument (value,
						__PRETTY_FUNCTION__,
						"invalid value for Real data type.",
						DataException::RealValue);
		}
	}
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a real number. The function must only be used after the same string
 * has been verified using the validate() function, so that the validity of the value can be assumed by
 * hypothesis.
 */
std::string Real::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;
	else {
		std::string tmp = value;
		size_t index = value.find_first_of(',');
		if (index != std::string::npos)
			tmp[index]='.';
		return std::to_string(stof(tmp));
	}
}

/**
 * @brief Returns information about the internal representation of a Real object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Real object.
 */
struct SqlType::TypeInfo Real::typeInfo() const throw () {
	SqlType::TypeInfo info;
	info.internalID = Real::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	return info;
}
