/**
 * @file Real.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_REAL_SQL_TYPE
#define OPENDB2_REAL_SQL_TYPE


/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{

/**
 * @brief Real sql data type.
 *
 * @details The Real sql type is the equivalent of the <i>float</i> type in c++. This is a floating point
 * number in single precision, ie with 6 significant digits after the decimals separator. Such a number is not
 * always expressed exactly: it can be approximated with a real number, represented by the same number of bits
 * for mantissa and exponent, which minimizes the error. The maximum error that is made during approximation
 * varies from implementation to implementation, and is called epsilon. It can always be obtained by using the
 * @code
 * std::numeric_limits<long double>::epsilon()
 * @endcode
 * function, defined in "limits".
 */
class Real : public SqlType
{
public:
	Real() noexcept {}
	virtual SqlType* clone() const noexcept;
	virtual ~Real() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 8;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Real UDT name */
	static const float smallest;					/**< Lower bound for the set of allowed values */
	static const float max;							/**< Upper bound for the set of allowed values */
};


/**
 * @}
 * @}
 */

};
#endif
