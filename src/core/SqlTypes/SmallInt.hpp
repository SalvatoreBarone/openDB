/**
 * @file Smallint.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_SMALLINT_SQL_TYPE
#define OPENDB2_SMALLINT_SQL_TYPE


/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"

namespace openDB
{

/**
 * @brief Smallint sql type.
 *
 * @details The Smallint sql type doesn't have a true c++ equivalent. It is a signed integer type represented
 *  on 16 bits, so its values are beetwen -32768 a 32767.
 */

class Smallint : public SqlType
{
public:
	Smallint() noexcept {}
	virtual SqlType* clone() const noexcept;
	virtual ~Smallint() noexcept {}
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct SqlType::TypeInfo typeInfo() const noexcept;

	static const int internalID = 5;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Smallint UDT name */
	static const int min = -32768;					/**< Lower bound for the set of allowed values */
	static const int max = 32767;					/**< Upper bound for the set of allowed values */
};


/**
 * @}
 * @}
 */

};
#endif
