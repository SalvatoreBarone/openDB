/**
 * @file SqlType.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "SqlType.hpp"
using namespace openDB;

const std::string SqlType::null = "NULL";
const std::list<std::string> SqlType::nullValues = {"null", "NULL", ""};


/**
 * @brief Test for "null" values.
 * @param [in] str string to test
 *
 * @retval true if the string contains a value that can be translated as sql NULL;
 * @retval false if the string doesn't contain a value that can be translated as sql NULL;
 */
bool SqlType::isNull(const std::string& str) noexcept
{
	std::list<std::string>::const_iterator it = nullValues.begin();
	std::list<std::string>::const_iterator end = nullValues.end();
	while (it != end)
		if (*it == str)
			return true;
		else
			it++;
	return false;
}
