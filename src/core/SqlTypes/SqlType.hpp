/**
 * @file SqlType.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef SQL_TYPE
#define SQL_TYPE

/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "../Exceptions/DataException.hpp"

#include <list>
#include <string>

namespace openDB {
	
/**
 * @brief Base class for sql type conversion.
 *
 * @details This is the base class from which all types used to translate into sql are derived.
 */
class SqlType {
public:
	SqlType() noexcept {}
	
	/**
	 * @brief Allocate a copy of an object.
	 *
	 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
	 * returns useful when you need to allocate objects using polymorphism. This is only an interface
	 * definition: each class derived from this class must implement this method.
	 *
	 * @return a pointer to SqlType, pointing to the cloned object.
	 */
	virtual SqlType* clone() const noexcept = 0;
	
	virtual ~SqlType() noexcept {}

	/**
	 * @brief Value validation.
	 *
	 * @param [in] value string to validate;
	 *
	 * @details The function defines the interface for all validation functions. The task of these functions
	 * will be to verify that the value in the "value" string respects certain constraints. These constraints
	 * determine whether the value of the string can be used within an sql statement.
	 * Each class derived from this class must implement this method.
	 */
	virtual void validate ( const std::string& value ) const throw ( DataException& ) = 0;

	/**
	 * @brief Value preparation.
	 *
	 * @param [in] value string to prepare.
	 *
	 * @return prepared value;
	 *
	 * @details The function defines the interface for all preparation functions. The task of these functions
	 * will be to prepare the value in the "value" string so that it can be used within an sql statement.
	 * The function must only be used after the same string has been verified using the validate() function,
	 * so that the validity of the value can be assumed by hypothesis.
	 * Each class derived from this class must implement this method.
	 */
	virtual std::string prepare ( const std::string& value ) const noexcept = 0;

	/**
	 * @brief Internal sql type description.
	 *
	 * @details This structure contains information about the translated sql type.
	 */
	struct TypeInfo {
		int internalID;				/**< Internal ID of the translated type. Each derived class must define
										 its unique identifier. */
		std::string typeName;		/**< Internal name of the translated type. Each derived class must define
										 its name. */
		std::string udtName;		/**< User Defined Type name. Each derived class must define its UDT name.*/
		unsigned numericPrecision;	/**< Numeric precision for Numeric datatype. For all other types it makes
										 no sense. */
		unsigned numericScale;		/**< Numeric scale for Numeric datatype. For all other types it makes
										 no sense. */
		unsigned vcharLength;		/**< Maximum length in terms of number of characters for the Varchar and
										 Character type. For all other types it makes no sense.*/
		
		TypeInfo() : internalID(-1), numericPrecision(0), numericScale(0), vcharLength(0) {}
	};

	/**
	 * @brief Returns information about the internal representation of a sql type.
	 *
	 * @details Each class derived from this class must implement this method.
	 *
	 * @return a TypeInfo struct, which contains information about the internal representation of a sql type.
	 */
	virtual struct TypeInfo typeInfo () const noexcept = 0;

	/**
	 * @brief This member contains a representation of the sql "value" NULL.
	 *
	 * @details This value is used in every sql statements generated from this library.
	 */
	static const std::string null;

	/**
	 * @brief This member contains a list of representations for the sql "value" NULL.
	 *
	 * @details These values are used to translate a NULL value retrieved from a database into the library
	 * internal NULL value representation.
	 */
	static const std::list<std::string> nullValues;

	static bool isNull(const std::string& str) noexcept;
};

/**
 * @}
 * @}
 */
	
};

#endif
