/**
 * @file Time.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Time.hpp"
#include "common.hpp"
using namespace openDB;

#include <sstream>
#include <iomanip>

const std::string Time::typeName = "time";
const std::string Time::udtName = "time";
const std::string Time::separator = ":";
const std::string Time::format = "hh:mm:ss";

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism. The cloned object will have the
 * same precision and scale of the object from which it was obtained.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Time::clone() const noexcept{
	return new Time;
}


/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as time. In case one of the constraints is not respected, an exception is raised.
 * In this case, the function the function verifies that the string contains a time in the hh:mm:ss or
 * hh:mm format, otherwise an exception is raised.
 *
 * @exception InvalidArgument, if the string content is not a time in the format hh:mm:ss or hh:mm;
 * @exception InvalidTime, if the time is meaningless;
 */
void Time::validate ( const std::string& value ) const throw ( DataException& )
{
	if (!SqlType::isNull(value))
	{
		TimeInteger time(value);
		if ( !time.validate())
			throw InvalidTime (value,
						__PRETTY_FUNCTION__,
						"invalid time value for Time data type.",
						DataException::InvalidTime);
	}
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a time. The function must only be used after the same string has been
 * verified using the validate() function, so that the validity of the value can be assumed by hypothesis.
 * In this case the function returns a time in the hh:mm:ss format.
 */
std::string Time::prepare ( const std::string& value ) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;

	TimeInteger _time(value);

	std:: stringstream ss;
	ss 		<< std::setfill('0') << std::setw(2) << _time.hour <<":"
			<< std::setfill('0') << std::setw(2) << _time.minute <<":"
			<< std::setfill('0') << std::setw(2) << _time.second;

	return 	"'" + ss.str() +"'";
}

/**
 * @brief Try to convert a string to a time expressed in numeric format.
 *
 * @param [in] __value string to convert.
 *
 * @warning The function may fire an exception of type InvalidArgument if the string content is not a time
 * in the hh:mm:ss or hh:mm format.
 */
Time::TimeInteger::TimeInteger ( std::string __value ) throw ( DataException& )
{
	std::vector<std::string> token;
	tokenize ( token, __value, separator );

	if ( token.size() != 3 && token.size() != 2 )
		throw InvalidArgument (__value,
					__PRETTY_FUNCTION__,
					"invalid time value for Time data type.",
					DataException::TimeValue);

	std::vector<std::string>::iterator token_it = token.begin(), token_end = token.end();
	// verifico che i token siano numerici
	for (; token_it != token_end; token_it++) {
		try {
			std::stoul(*token_it);
		}
		catch ( std::exception& e ) {
			throw InvalidArgument (__value,
						__PRETTY_FUNCTION__,
						"invalid date for Time data type.",
						DataException::TimeValue);
		}
	}

	hour = std::stoul(token[0]);
	minute = std::stoul(token[1]);
	if ( token.size() == 2 )
		second = 0;
	else
		second = std::stoul(token[2]);

}

/**
 * @brief Check that a time makes sense.
 *
 * @param [in] __time time to test
 *
 * @retval true if the time makes sense;
 * @retval false if the time in meaningless.
 */
bool Time::TimeInteger::validate () const noexcept
{
	if (hour <= 23 && minute <= 59 && second <= 60)
		return true;
	else
		return false;
}


/**
 * @brief Returns information about the internal representation of a Time object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Time object.
 */
struct SqlType::TypeInfo Time::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Time::internalID;
	info.typeName = typeName;
	info.udtName = udtName;
	return info;
}
