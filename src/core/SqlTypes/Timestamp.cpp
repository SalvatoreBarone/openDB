/**
 * @file Timestamp.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Timestamp.hpp"
#include "common.hpp"
using namespace openDB;

#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>

const std::string Timestamp::typeName = "timestamp";
const std::string Timestamp::udtName = "timestamp";
const std::string Timestamp::separator = " ";
const std::string Timestamp::format = "yyyy-MM-dd hh:mm:ss";

#define datePosition 0
#define timePosition 1

/**
 * @brief Allocate a copy of an object.
 *
 * @details Appropriately allocate a new copy of the object, initializing it correctly. This function
 * returns useful when you need to allocate objects using polymorphism. The cloned object will have the
 * same precision and scale of the object from which it was obtained.
 *
 * @return a pointer to SqlType, pointing to the cloned object.
 */
SqlType* Timestamp::clone() const noexcept {
	return new Timestamp;
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as timestamp. In case one of the constraints is not respected, an exception is raised.
 * In this case, the function the function verifies that the string contains a timestamp in the
 * yyyy/mm/dd hh:mm:ss format.Tormat variations of Date and Time types are allowed.
 *
 * @exception InvalidArgument, if the string content is not a date in the format yyyy/mm/dd or dd/mm/yyyy;
 * @exception AmbiguousDate, if the date, expressed in a format like yy/mm/dd or dd/mm/yy, is ambiguous.
 * For example 12/11/15 could be interpreted as November 12 2015 as well as as November 15 2012, and as
 * December 11, 2015;
 * @exception InvalidDate, if the date is meaningless, such as February 30 or November 31.
 * @exception InvalidArgument, if the string content is not a time in the format hh:mm:ss or hh:mm;
 * @exception InvalidDate, if the time is meaningless;
 */
void Timestamp::validate(const std::string& value) const throw (DataException&)
{
	if (SqlType::isNull(value))
		return;

	std::vector<std::string> stringVec;
	tokenize(stringVec, value, Timestamp::separator);

	if (stringVec.size() != 1 && stringVec.size() != 2)
		throw InvalidArgument(
			__PRETTY_FUNCTION__,
			value,
			"invalid value for Timestamp data type",
			DataException::InvalidArgument);


	std::string date_str = stringVec[datePosition];
	Date::validate(date_str);
	if (stringVec.size() == 2) {
		std::string time = stringVec[timePosition];
		Time::validate(time);
	}
}

/**
 * @brief Value preparation.
 *
 * @param [in] value string to prepare.
 *
 * @return prepared value;
 *
 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
 * used within an sql statement as a timestamp. The function must only be used after the same string has
 * been verified using the validate() function, so that the validity of the value can be assumed by
 * hypothesis. In this case the function returns a timestamp in the "yyyy-mm-dd hh:mm:ss" format.
 */
std::string Timestamp::prepare(const std::string& value) const noexcept
{
	if (SqlType::isNull(value))
		return SqlType::null;

	std::vector<std::string> stringVec;
	tokenize(stringVec, value, Timestamp::separator);
	std::string date = stringVec[datePosition];

	Date::DateInteger _date(date);
	std:: stringstream ss;
	ss 		<< std::setfill('0') << std::setw(4) << _date.year <<"-"
			<< std::setfill('0') << std::setw(2) << _date.month <<"-"
			<< std::setfill('0') << std::setw(2) << _date.day;

	std::string time;
	if (stringVec.size() == 2)
	{
		std::string time = stringVec[timePosition];
		Time::TimeInteger _time(time);
		ss 		<< " "
				<< std::setfill('0') << std::setw(2) << _time.hour <<":"
				<< std::setfill('0') << std::setw(2) << _time.minute <<":"
				<< std::setfill('0') << std::setw(2) << _time.second;
	}

	return "'" + ss.str() + "'";
}


/**
 * @brief Returns information about the internal representation of a Timestamp object.
 *
 * @return a SqlType::TypeInfo struct, which contains information about the internal representation of a
 * Timestamp object.
 */
struct SqlType::TypeInfo Timestamp::typeInfo() const noexcept
{
	SqlType::TypeInfo info;
	info.internalID = Timestamp::internalID;
	info.typeName = Timestamp::typeName;
	info.udtName = Timestamp::udtName;
	return info;
}

