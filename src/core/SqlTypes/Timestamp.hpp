/**
 * @file Timestamp.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SQL_TIMESTAMP_H
#define __SQL_TIMESTAMP_H


/**
 * @addtogroup Core
 * @{
 * @addtogroup SqlTypes
 * @{
 */

#include "SqlType.hpp"
#include "Date.hpp"
#include "Time.hpp"

#include <string>

namespace openDB
{

class Timestamp : public Date, public Time
{
public:
	explicit Timestamp() noexcept {}
	virtual ~Timestamp() {}
	virtual SqlType* clone() const noexcept;
	virtual void validate ( const std::string& value ) const throw ( DataException& );
	virtual std::string prepare ( const std::string& value ) const noexcept;
	virtual struct TypeInfo typeInfo () const noexcept;

	static const int internalID = 11;				/**< Internal ID of the translated type. */
	static const std::string typeName;				/**< Internal type name */
	static const std::string udtName;				/**< Time UDT name */
	static const std::string separator;				/**< Characters used as separator */
	static const std::string format;				/**< Format used the prepare() function */

};


/**
 * @}
 * @}
 */

};
#endif
