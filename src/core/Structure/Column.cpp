/**
 * @file Column.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Column.hpp"
#include "common.hpp"

#include <typeinfo>
#include <algorithm>
#include <iostream>
using namespace openDB;

#define defineType(Type) { type = new Type; return; }

/**
 * @brief Constructor.
 *
 * @details
 * Defines a new Column object, specifying the name and type.
 * For example
 * @code
 * openDB::Column columnBoolean ("Boolean", openDB::Boolean());
 * openDB::Column columnDate ("Date", openDB::Date());
 * openDB::Column columnTime ("Timestamp", openDB::Timestamp());
 * openDB::Column columnTime ("Time", openDB::Time());
 * openDB::Column columnSmall ("Small", openDB::Smallint());
 * openDB::Column columnInt ("Int", openDB::Integer());
 * openDB::Column columnBig ("Bigint", openDB::Bigint());
 * openDB::Column columnReal ("Real", openDB::Real());
 * openDB::Column columnDouble ("Double", openDB::DoublePrecision());
 * openDB::Column columnNumeric ("Numeric", openDB::Numeric(10, 5));
 * openDB::Column columnVchar ("VChar", openDB::Varchar(100));
 * openDB::Column columnChar ("Char", openDB::Character(100));
 * @endcode
 *
 * @param [in] columnName	name of the column; it identifies the column within the table to which it
 * 							belongs;
 * @param [in] columnType	sql type of the column;
 *
 * @param [in] key			if is set to "true" it means that this column compose the key for the table
 * 							to which it belongs.
 *
 * @exception AccessException if columnName is an empty string or if it contains any non-alphanumerical
 * character
 **/
Column::Column(std::string columnName, const SqlType& columnType, bool key)  throw (AccessException&) :
	name ( columnName ), isKey ( key )
{
	if (columnName.empty())
		throw AccessException(
			AccessException::ColumnName,
			__PRETTY_FUNCTION__,
			"Can't create a Column object without a name!");

	std::string nameUpper = columnName;
	std::transform(nameUpper.begin(), nameUpper.end(), nameUpper.begin(), ::toupper);
	if (nameUpper.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_") != std::string::npos)
		throw AccessException(
			AccessException::ColumnName,
			__PRETTY_FUNCTION__,
			"The column name contains an illegal non-alphanumerical character");

	if ( typeid ( columnType ) == typeid ( Boolean ) )			defineType(Boolean)
	if ( typeid ( columnType ) == typeid ( Date ) )				defineType(Date)
	if ( typeid ( columnType ) == typeid ( Time ) )				defineType(Time)
	if ( typeid ( columnType ) == typeid ( Timestamp ) )		defineType(Timestamp)
	if ( typeid ( columnType ) == typeid ( Smallint ) ) 		defineType(Smallint)
	if ( typeid ( columnType ) == typeid ( Integer ) )			defineType(Integer)
	if ( typeid ( columnType ) == typeid ( Bigint ) )			defineType(Bigint)
	if ( typeid ( columnType ) == typeid ( Real ) )				defineType(Real)
	if ( typeid ( columnType ) == typeid ( DoublePrecision ) )	defineType(DoublePrecision);
	if ( typeid ( columnType ) == typeid ( Numeric ) )			defineType(Numeric(columnType.typeInfo().numericPrecision, columnType.typeInfo().numericScale))
	if ( typeid ( columnType ) == typeid ( Varchar ) )			defineType(Varchar (columnType.typeInfo().vcharLength))
	if ( typeid ( columnType ) == typeid ( Character ) )		defineType(Character ( columnType.typeInfo().vcharLength ))
}

Column::Column( const Column& c ) noexcept :
	name ( c.name ),
	type ( c.type->clone() ),
	isKey ( c.isKey ),
	querySetting ( c.querySetting )
{}

const Column& Column::operator= (const Column& c) noexcept
{
	if (&c != this)
	{
		name = c.name;
		isKey = c.isKey;
		type = c.type->clone();
		querySetting = c.querySetting;
	}
	return *this;
}

/**
 * @brief Value validation.
 *
 * @param [in] value string to validate;
 *
 * @details The goal of this function is to verify that the value in the "value" string respects certain
 * constraints. These constraints determine whether the value of the string can be used within an sql
 * statement as date. In case one of the constraints is not respected, an exception is raised.
 *
 * @warning DO NOT use the validate() function defined by SqlType derived types! USE THIS ONE!
 **/
void Column::validate(const std::string& value) const throw (BasicException&)
{
	if ( isKey && SqlType::isNull(value))
		throw EmptyKey (__PRETTY_FUNCTION__, "'" + name + "' is key! Do not insert empty values!" );
	type->validate (value);
}

/**
 * @brief Returns a select-condition, for 'where' sql statements, based on the column usage parameters.
 *
 * @return a std::string object containing the select condition for this column.
 */
std::string Column::getSelectCondition() const noexcept
{
	std::string condition = name + querySetting.getCompareOperator();
	std::list<std::string> valueList;

	switch (querySetting.getCompareOperatorEnum())
	{
		case QuerySetting::more:
		case QuerySetting::moreOrEqual:
		case QuerySetting::equal:
		case QuerySetting::notEqual:
		case QuerySetting::lessOrEqual:
		case QuerySetting::less:
		case QuerySetting::like:
		case QuerySetting::notLike:
			condition += prepare(querySetting.getSelectValue());
			break;

		case QuerySetting::in:
		case QuerySetting::notIn:
			{
				std::string conditionValue;
				tokenize(valueList, querySetting.getSelectValue(), ";,");
				auto it = valueList.begin(), end = valueList.end();
				for (; it != end; it++)
				{
					if (!conditionValue.empty())
						conditionValue += ", ";
					conditionValue += prepare(*it);
				}
				condition += "(" + conditionValue + ")";
			}
			break;
	}
	return condition;
}
