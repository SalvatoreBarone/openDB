/**
 * @file Column.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_COLUMN
#define OPENDB2_COLUMN

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 */

#include "../Exceptions/AccessException.hpp"
#include "../SqlTypes/SqlTypes.hpp"
#include "QuerySetting.hpp"

namespace openDB
{

/**
 * @brief Abstraction of the column concept
 *
 * The point of view that you have to keep, looking at this class, is not the column view seen as a set of
 * values, but as a set of semantic properties and common features to a set of values ​​that, in fact,
 * represent a column of a table. This object does not retain any value relative to the column, but only its
 * properties, such as the name, sql type, and use of values ​​that are contained in the column when
 * queries are sent to the database.
 **/

class Column
{
public:
	Column (	std::string columnName,
				const SqlType& columnType,
				bool key = false ) throw ( AccessException& );

	Column( const Column& c ) noexcept;

	const Column& operator= (const Column& c) noexcept;

	~Column ()
		{delete type;}

	/**
	 * @brief Returns the name of the column.
	 *
	 * @return an std::string object, containing the name of the column.
	 **/
	std::string getName() const noexcept {return name;}

	/**
	 * @brief Returns the sql type of the column
	 *
	 * @return a pointer to SqlType, internal sql type of the column; the returned pointer remains valid
	 * until the column object to which it belongs is deallocated.
	 */
	const SqlType* getSqlType() const noexcept {return type;}

	/**
	 * @brief Sets the use of the column in the key composition.
	 *
	 * @param [in] key if is set to "true" it means that this column compose the key for the table to which it
	 * belongs.
	 **/
	void setIsKey ( bool key ) noexcept {isKey = key;}

	/**
	 * @brief Returns information about the use of the column in the key composition.
	 *
	 * @retval true if the column compose the key for the table to which it belongs;
	 * @retval false if the column doesn't compose the key for the table to which it belongs;
	 **/
	bool getIsKey() const noexcept {return isKey;}

	void validate ( const std::string& value ) const throw ( BasicException& );

	/**
	 * @brief Value preparation.
	 *
	 * @param [in] value string to prepare.
	 *
	 * @return prepared value;
	 *
	 * @details The goal of of this function is to prepare the value in the "value" string so that it can be
	 * used within an sql statement as a date. The function must only be used after the same string has been
	 * verified using the validate() function, so that the validity of the value can be assumed by hypothesis.
	 **/
	std::string prepare (const std::string& value ) const noexcept {return type->prepare ( value );}

	/**
	 * @brief Allows you to set column usage parameters in queries.
	 *
	 * @param [in] _attr column usage parameters in queries
	 **/
	void setQuerySetting ( const QuerySetting& _attr ) noexcept {querySetting = _attr;}

	/**
	 * @brief Return information about the usage of this column in queries.
	 *
	 * @return a QuerySetting object containing column usage parameters in queries.
	 **/
	const QuerySetting& getQuerySetting () const noexcept {return querySetting;}

	std::string getSelectCondition() const noexcept;

private:
	std::string name;			/**< column name */
	SqlType* type;				/**< column sql type */
	bool isKey;					/**< if is set to "true" it means that this column compose the key for the
									 table to which it belongs.  */
	QuerySetting querySetting;	/**< column usage parameters in queries */
}; //Column Class

/**
 * @}
 * @}
 */


}; //openDB namespace


#endif
