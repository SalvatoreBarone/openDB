/**
 * @file Database.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Database.hpp"
using namespace openDB;

Database::Database ( const std::string& databaseName ) noexcept :
		name(databaseName),
		schemasMap( new std::unordered_map<std::string, Schema>)
{}

Database::Database (const Database& database) noexcept :
		name(database.name),
		schemasMap(database.schemasMap)
{}

const Database& Database::operator= (const Database& database) noexcept
{
	if (&database != this)
	{
		 name = database.name;
		 schemasMap = database.schemasMap;
	}
	return * this;
}

Database::Iterator Database::addSchema (std::string schemaName) throw ( AccessException& )
{
	Database::Iterator schema_it = at(schemaName);
	if (schema_it == end())
	{
		schemasMap->insert(std::pair<std::string, Schema>(schemaName, Schema(schemaName, (Database*) this)));
		return at(schemaName);
	}
	return end();
}

void Database::deleteSchema (Database::Iterator& it) noexcept
{
	if (it.parent == this && it != end())
		it.iter = schemasMap->erase(it.iter);
}

Database::Iterator Database::begin() noexcept
{
	Database::Iterator it;
	it.parent = (Database*) this;
	it.iter =  schemasMap->begin();
	return it;
}

Database::Iterator Database::end() noexcept
{
	Database::Iterator it;
	it.parent = (Database*) this;
	it.iter =  schemasMap->end();
	return it;
}


Database::ConstIterator Database::cbegin() const noexcept
{
	Database::ConstIterator it;
	it.parent = (Database*) this;
	it.iter =  schemasMap->cbegin();
	return it;
}

Database::ConstIterator Database::cend() const noexcept
{
	Database::ConstIterator it;
	it.parent = (Database*) this;
	it.iter =  schemasMap->cend();
	return it;
}

Database::Iterator Database::at(const std::string& schemaName) noexcept
{
	Database::Iterator it;
	it.parent = this;
	it.iter = schemasMap->find(schemaName);
	return it;
}

Database::ConstIterator Database::cat(const std::string& schemaName) const noexcept
{
	Database::ConstIterator it;
	it.parent = (Database*)this;
	it.iter = schemasMap->find(schemaName);
	return it;
}
