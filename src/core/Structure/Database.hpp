/**
 * @file Database.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_DATABASE
#define OPENDB2_DATABASE

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 */


#include "Schema.hpp"

namespace openDB
{

/**
 * @brief A Database object represents a databas as a set of schemas and tables.
 *
 * @details
 * The object, by itself, is little more than a container of Schema objects, which in turn are little more
 * than Table containers.
 *
 * @section iterator Accessing Schemas
 * This class implements some useful methods and a mechanism that governs access to Schema objects belonging
 * to a Database object. The mechanism is based on the concept of "iterator".
 * An iterator is any object that, pointing to some element in a range of elements (such as an array or a
 * container), has the ability to iterate through the elements of that range using a set of operators
 * (with at least the increment (++) and dereference (*) operators).
 * Iterators are available in a "normal" version, without restriction on access to the object on which they
 * are placed, and in a "const" version, which does not allow changes to the object, implemented,
 * respectively, by the Database::Iterator and Database::ConstIterator class.
 */
class Database
{

public:

	/**
	 * @brief Iterator class
	 *
	 * @details The Database class implements a simple but efficient mechanism that governs access to schemas.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the element on which the iterator is placed.
	 */
	class Iterator
	{
		friend class Database;
	public:

		/**
		 * @brief Constructor
		 *
		 * @details You can build a Database::Iterator object, but you can not associate it with a Database
		 * object. This is to further regulate how to access schemas belonging to a database. You can get a
		 * Database::Iterator object associated with a Database object only by using the Database::begin(),
		 * Database::at() or Database::end() functions.
		 */
		Iterator() noexcept : parent(0) {}

		/**
		 * @brief Post-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a new iterator to the previous
		 * position.
		 *
		 * @return a new iterator placed on the previous position.
		 */
		Iterator operator++(int) noexcept;		//i++

		/**
		 * @brief Pre-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a reference to the same iterator on
		 * which the operator acted.
		 *
		 * @return a reference to the same iterator.
		 */
		Iterator& operator++() noexcept;		//++i

		/**
		 * @brief Checks if two iterators are placed on the same element.
		 *
		 * @param [in] __it a Database::Iterator object;
		 *
		 * @retval true if two iterators are placed on the same element;
		 * @retval false if two iterators aren't placed on the same element;
		 */
		bool operator == (const Iterator& __it) const noexcept;

		/**
		 * @brief Verify that two iterators are not placed on the same element.
		 *
		 * @param [in] __it a Database::Iterator object;
		 *
		 * @retval true if two iterators aren't placed on the same element;
		 * @retval false if two iterators are placed on the same element;
		 */
		bool operator != (const Iterator& __it) const noexcept;

		/**
		 * @brief Check if two iterators are associated with the same Schema object.
		 *
		 * @param [in] __it a Database::Iterator object;
		 *
		 * @retval true if two iterators are associated with the same Database object;
		 * @retval false  if two iterators aren't associated with the same Database object;
		 */
		bool brother (const Iterator& __it) const noexcept;

		/**
		 * @brief Dereference operator
		 *
		 * @return a reference to the Schema object on which the iterator is placed. The reference remains
		 * valid until the Database object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		Schema& operator* () throw ( AccessException& );

		/**
		 * @brief Arrow operator
		 *
		 * @return a pointer to the Schema object on which the iterator is placed. The pointer remains
		 * valid until the Database object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		Schema* operator-> () throw ( AccessException& );

	private:
		Database* parent;	/**< pointer to the Database object to which the iterator is associated */
		std::unordered_map<std::string, Schema>::iterator iter;

		/**
		 * @brief Moves the iterator one position forward.
		 *
		 * @return a reference to the same object.
		 */
		Iterator& next () noexcept;
	};

	/**
	 * @brief ConstIterator class
	 *
	 * @details The Database class implements a simple but efficient mechanism that governs access to schemas.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the record on which the iterator is placed.
	 * This class of iterators belongs to the class of random access iterators and has read-only access
	 * restrictions to records.
	 */
	class ConstIterator
	{
		friend class Database;
	public:

		/**
		 * @brief Constructor
		 *
		 * @details You can build a Database::ConstIterator object, but you can not associate it with a
		 * Database object. This is to further regulate how to access schemas belonging to a database. You can
		 * get a Database::ConstIterator object associated with a Database object only by using the
		 * Database::cbegin(), Database::at() or Database::cend() functions.
		 * This class of iterators belongs to the class of random access iterators class and has read-only
		 * access restrictions to columns.
		 */
		ConstIterator() noexcept : parent(0) {}

		/**
		 * @brief Post-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a new iterator to the previous
		 * position.
		 *
		 * @return a new iterator placed on the previous position.
		 */
		ConstIterator operator++(int) noexcept;		//i++

		/**
		 * @brief Pre-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a reference to the same iterator on
		 * which the operator acted.
		 *
		 * @return a reference to the same iterator.
		 */
		ConstIterator& operator++() noexcept;		//++i

		/**
		 * @brief Checks if two iterators are placed on the same element.
		 *
		 * @param [in] __it a Database::ConstIterator object;
		 *
		 * @retval true if two iterators are placed on the same element;
		 * @retval false if two iterators aren't placed on the same element;
		 */
		bool operator == (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Verify that two iterators are not placed on the same element.
		 *
		 * @param [in] __it a Database::ConstIterator object;
		 *
		 * @retval true if two iterators aren't placed on the same element;
		 * @retval false if two iterators are placed on the same element;
		 */
		bool operator != (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Check if two iterators are associated with the same Schema object.
		 *
		 * @param [in] __it a Database::ConstIterator object;
		 *
		 * @retval true if two iterators are associated with the same Schema object;
		 * @retval false  if two iterators aren't associated with the same Schema object;
		 */
		bool brother (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Dereference operator
		 *
		 * @return a reference to the Schema object on which the iterator is placed. The reference remains
		 * valid until the Database object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		const Schema& operator* () const throw ( AccessException& );

		/**
		 * @brief Arrow operator
		 *
		 * @return a pointer to the Schema object on which the iterator is placed. The pointer remains
		 * valid until the Database object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		const Schema* operator-> () const throw ( AccessException& );

	private:
		Database* parent;	/**< pointer to the Database object to which the iterator is associated */
		std::unordered_map<std::string, Schema>::const_iterator iter;

		/**
		 * @brief Moves the iterator one position forward.
		 *
		 * @return a reference to the same object.
		 */
		ConstIterator& next () noexcept;
	};

	friend class Iterator;
	friend class ConstIterator;

	/**
	 * @brief Constructor.
	 *
	 * @details Builds a new Database object, assigning it a name.
	 *
	 * @param [in] databaseName name of the new Database object;
	 */
	explicit Database ( const std::string& databaseName = "") noexcept;

	/**
	 * @brief Make a quick copy of the object
	 *
	 * @details The Database object is not copied entirely.
	 * The main advantages offered by this type of solution are many. First of all, you can pass a Database
	 * object by copy very quickly from having to copy entirely every time. Any changes to the "copy" will
	 * affect also the original object. Second, in this way, two Database objects can share the same Storage
	 * and the same records, creating a dual access mechanism to the same information content.
	 *
	 * @param [in] database Database object to be copied.
	 */
	Database (const Database& database) noexcept;

	/**
	 * @brief Make a quick copy of the object
	 *
	 * @details The Database object is not copied entirely.
	 * The main advantages offered by this type of solution are many. First of all, you can pass a Database
	 * object by copy very quickly from having to copy entirely every time. Any changes to the "copy" will
	 * affect also the original object. Second, in this way, two Database objects can share the same Storage
	 * and the same records, creating a dual access mechanism to the same information content.
	 *
	 * @param [in] database Database object to be copied.
	 */
	const Database& operator= (const Database& database) noexcept;

	/**
	 * @brief Returns the database name;
	 *
	 * @return a std::string object, containing the database name;
	 */
	std::string getName() const noexcept {return name;}

	/**
	 * @brief Returns the number of schemas belonging to the database;
	 *
	 * @return an unsigned number representing the number of schemas belonging to the database.
	 */
	unsigned schemasNum() const noexcept {return schemasMap->size();}

	/**
	 * @brief Returns a Database::Iterator object placed on the first schema.
	 *
	 * @note There is no special order for Schema objects;
	 *
	 * @return a Database::Iterator placed on the first Schema object;
	 */
	Iterator begin() noexcept;

	/**
	 * @brief Returns a Database::Iterator pointing to the "past-the-end" schema.
	 *
	 * @note the "past-the-end" schema does not really exist, so the iterator returned by this function
	 * points to the first invalid position after all valid positions.
	 *
	 * @return a Database::Iterator pointing to the "past-the-end" schema.
	 */
	Iterator end() noexcept;

	/**
	 * @brief Returns a Database::ConstIterator object placed on the first Schema object.
	 *
	 * @note There is no special order for Schema objects;
	 *
	 * @return a Database::ConstIterator placed on the first Schema object;
	 */
	ConstIterator cbegin() const noexcept;

	/**
	 * @brief Returns a Schema::ConstIterator pointing to the "past-the-end" schema.
	 *
	 * @note the "past-the-end" schema does not really exist, so the iterator returned by this function
	 * points to the first invalid position after all valid positions.
	 *
	 * @return a Schema::ConstIterator pointing to the "past-the-end" schema.
	 */
	ConstIterator cend() const noexcept;

	/**
	 * @brief Returns an iterator pointing to a specific Schema object
	 *
	 * @param [in] schemaName name of the Schema object.
	 *
	 * @return a Database::Iterator object, pointing to the Schema object whose name is schemaName, or end()
	 * if there is no Schema object with that name.
	 */
	Iterator at(const std::string& schemaName) noexcept;

	/**
	 * @brief Returns an iterator pointing to a specific Schema object
	 *
	 * @param [in] schemaName name of the Schema object.
	 *
	 * @return a Database::ConstIterator object, pointing to the Schema object whose name is schemaName, or
	 * cend() if there is no Schema object with that name.
	 */
	ConstIterator cat(const std::string& schemaName) const noexcept;

	/**
	 * @brief Creates and adds a Schema object to the Database.
	 *
	 * @param [in] schemaName name for the new Schema object.
	 *
	 * @return a Database::Iterator object, pointing to the inserted Schema object, if it was actually
	 * inserted, or end() if no insertion occurred.
	 */
	Iterator addSchema (std::string schemaName) throw ( AccessException& );

	/**
	 * @brief Remove and delete a Schema object from the Database.
	 *
	 * @param [in] it a Database::Iterator, pointing to the Schema object to be removed and deleted.
	 */
	void deleteSchema (Iterator& it) noexcept;


private:
	std::string name;	/**< Name of the database */

	/**
	 * @brief Structural view of the database.
	 */
	std::shared_ptr<std::unordered_map<std::string, Schema>> schemasMap;
};

/**
 * @}
 * @}
 */



};

#endif
