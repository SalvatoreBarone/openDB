/**
 * @file DatabaseConstIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Database.hpp"
using namespace openDB;

Database::ConstIterator Database::ConstIterator::operator++(int) noexcept
{
	Database::ConstIterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

Database::ConstIterator& Database::ConstIterator::operator++() noexcept
{
	next();
	return *this;
}

bool Database::ConstIterator::operator == (const Database::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent && iter == __it.iter)
		return true;
	return false;
}

bool Database::ConstIterator::operator != (const Database::ConstIterator& __it) const noexcept
{
	return !operator==(__it);
}

bool Database::ConstIterator::brother(const Database::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent)
		return true;
	return false;
}

const Schema& Database::ConstIterator::operator* () const throw ( AccessException& )
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Database::ConstIterator::operator*: no valid parent pointer;");

	if (iter == parent->schemasMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Database::ConstIterator::operator*: can't access to end();");

	return iter->second;
}

const Schema* Database::ConstIterator::operator-> () const throw (AccessException&)
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Database::ConstIterator::operator->: no valid parent pointer;");

	if (iter == parent->schemasMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Database::ConstIterator::operator->: can't access to end();");

	return &(iter->second);
}

Database::ConstIterator& Database::ConstIterator::next () noexcept
{
	if (parent && iter != parent->schemasMap->cend())
		iter++;
	return* this;
}



