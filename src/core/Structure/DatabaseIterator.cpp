/**
 * @file DatabaseIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Database.hpp"
using namespace openDB;

Database::Iterator Database::Iterator::operator++(int) noexcept
{
	Database::Iterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

Database::Iterator& Database::Iterator::operator++() noexcept
{
	next();
	return *this;
}


bool Database::Iterator::operator == (const Database::Iterator& __it) const noexcept
{
	if (parent == __it.parent && iter == __it.iter)
		return true;
	return false;
}

bool Database::Iterator::operator != (const Database::Iterator& __it) const noexcept
{
	return !operator==(__it);
}

bool Database::Iterator::brother(const Database::Iterator& __it) const noexcept
{
	if (parent == __it.parent)
		return true;
	return false;
}

Schema& Database::Iterator::operator* () throw ( AccessException& )
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Database::Iterator::operator*: no valid parent pointer;");

	if (iter == parent->schemasMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Database::Iterator::operator*: can't access to end();");

	return iter->second;
}

Schema* Database::Iterator::operator-> () throw (AccessException&)
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Database::Iterator::operator->: no valid parent pointer;");

	if (iter == parent->schemasMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Database::Iterator::operator->: can't access to end();");

	return &(iter->second);
}

Database::Iterator& Database::Iterator::next () noexcept
{
	if (parent && iter != parent->schemasMap->end())
		iter++;
	return* this;
}



