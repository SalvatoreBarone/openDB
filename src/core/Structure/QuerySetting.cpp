/**
 * @file QuerySetting.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "QuerySetting.hpp"
using namespace openDB;

std::string QuerySetting::getCompareOperator() const noexcept {
	switch (compareOperator) {
		case more:	return " > ";
		case moreOrEqual:	return " >= ";
		case equal:	return " = ";
		case notEqual:	return " != ";
		case like:	return " like ";
		case notLike:	return " not like ";
		case lessOrEqual:	return " <= ";
		case less:	return " < ";
		case in:	return " in ";
		case notIn:	return " not in ";
		default :	return "";
	};
	return "";
}

std::string QuerySetting::getOrderMode () const noexcept {
	if (orderMode == ascending)
		return " asc ";
	else
		return " desc ";
}


