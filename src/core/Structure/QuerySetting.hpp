/**
 * @file QuerySetting.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_QUERY_SETTING
#define OPENDB2_QUERY_SETTING

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 */


#include <string>

namespace openDB
{

/**
 * @brief This class defines column usage parameters in sql queries.
 *
 * @details Thi class contains informations about column usage in sql queries, such as:
 * - if the column is to be projected;
 * - if there are selection constraints on the column and, in this case,
 *	 	 - which compare operator to use
 *	 	 - which selection constraints to use
 *	 	 .
 * - if the result of the query is to be sorted according to the values ​​in the column in question and, in this
 *   case, which order (ascending or descending) to use.
 **/
class QuerySetting
{
public:
	/**
	 * @brief Sql compare operators.
	 **/
	enum SqlCompareOperator {
		more, 		/**< ">" operator; */
		moreOrEqual, 	/**< ">=" operator; */
		equal, 		/**< "==" operator; */
		notEqual, 	/**< "!=" operator; */
		like, 		/**< "like" operator, only for Character and Varchar data type; */
		notLike, 	/**< "not like" operator, only for Character and Varchar data type; */
		lessOrEqual, 	/**< "<=" operator; */
		less, 		/**< "<" operator; */
		in, 		/**< "in (set of value)" operator; */
		notIn 		/**< "not in (set of value) operator; */
	};

	/**
	 * @brief Sql order mode.
	 **/
	enum SqlOrder {
		ascending,	/**< ascending */
		descendign	/**< descending */
	};

	/**
	 * @brief Constructor.
	 *
	 * @details It builds an object so that the column is not used either in projection, selection or sorting
	 * operations.
	 **/
	QuerySetting() :
		proiect ( false ),
		select ( false ),
		selectValue ( "" ),
		compareOperator ( equal ),
		orderBy ( false ),
		orderMode ( ascending )
	{}

	/**
	 * @brief Sets the projection parameter.
	 *
	 * @param [in] value if set to "true" the column will be used in projection operations;
	 **/
	void setProiect ( bool value ) noexcept { proiect = value; }

	/**
	 * @brief Returns the settings for use in projection operations.
	 *
	 * @retval "true" if the column will be used in projection operations;
	 * @retval "false" if the column will not be used in projection operations;
	 **/
	bool getProiect () const noexcept { return proiect; }

	/**
	 * @brief Sets the selection parameter.
	 *
	 * @param [in] value if set to "true" the column will be used in selection operations;
	 **/
	void setSelect ( bool value ) noexcept { select = value; }

	/**
	 * @brief Returns the settings for use in selection operations.
	 *
	 * @retval "true" if the column will be used in selection operations;
	 * @retval "false" if the column will not be used in selection operations;
	 **/
	bool getSelect () const noexcept { return select; }

	/**
	 * @brief Sets the selection constraints.
	 *
	 * @param [in] value a std::string object containing the selection value or constraints;
	 *
	 * @warning The selection constraints will be used in sql statements as they are, so it is the user's
	 * burden to verify that they are correct, according to the sql type of the column.
	 *
	 **/
	void setSelectValue ( std::string value ) noexcept {selectValue = value;}

	/**
	 * @brief Returns the selection constraints.
	 *
	 * @return a std::string object containing the selection value or constraints;
	 **/
	std::string getSelectValue () const noexcept {	return selectValue; }

	/**
	 * @brief Sets the compare operator to be used.
	 *
	 * @param [in] op a sqlCompareOperator element, the compare operator to be used;
	 **/
	void setCompareOperator ( SqlCompareOperator op ) noexcept {compareOperator = op;}

	/**
	 * @brief Returns the compare operator to be used in string format.
	 *
	 * @return a std::string, containing the compare operator to be used, translated in string.
	 **/
	std::string getCompareOperator() const noexcept;

	/**
	 * @brief Returns the compare operator to be used.
	 *
	 * @return a sqlCompareOperator element, the compare operator to be used;
	 **/
	enum SqlCompareOperator getCompareOperatorEnum () const noexcept {return compareOperator;}

	/**
	 * @brief Sets the sorting parameter.
	 *
	 * @param [in] value if sets to "true" the column will be used in sorting operations.
	 **/
	void setOrderBy ( bool value ) noexcept { orderBy = value; }

	/**
	 * @brief Returns the settings for use in sorting operations.
	 *
	 * @retval "true" if the column will be used in sorting operations;
	 * @retval "false" if the column will not be used in sorting operations;
	 **/
	bool getOrderBy () const noexcept	{return orderBy;}

	/**
	 * @brief Sets the ordering mode to be used.
	 *
	 * @param [in] order a sqlOrder element, the order mode to be used;
	 **/
	void setOrderMode ( enum SqlOrder order ) noexcept	{orderMode = order;}

	/**
	 * @brief Returns the order mode to be used, in string format.
	 *
	 * @return a std::string containing the order mode to be used in string format.
	 **/
	std::string getOrderMode () const noexcept;

	/**
	 * @brief Returns the order mode to be used.
	 *
	 * @return a sqlOrder element, the order mode to be used.
	 **/
	enum SqlOrder getOrderModeEnum () const noexcept {return orderMode;}

protected:
	bool 					proiect;			/**< true if the column will be used in proiection operation */
	bool 					select;				/**< true if the column will be used in selection operation */
	std::string	 			selectValue;		/**< selection constraints */
	enum SqlCompareOperator	compareOperator;	/**< compare operator to be used */
	bool 					orderBy;			/**< true if the column will be used in sorting operation*/
	enum SqlOrder			orderMode;			/**< order mode to be used */
};

/**
 * @}
 * @}
 */



};

#endif
