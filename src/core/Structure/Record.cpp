/**
 * @file Record.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Record.hpp"
#include "Storage.hpp"
#include "Table.hpp"
#include "common.hpp"
#include "sha.hpp"

#include <sstream>
#include <iomanip>

#ifdef __DEBUG
#include <iostream>
using namespace std;
#endif

using namespace openDB;

/**
 * @brief Constructor
 *
 * @details You can build a new Record object, but you can't bind it to a Storage object. You must use the
 * Storage::newRecord() function.
 */
Record::Record() noexcept :
	parent(0),
	columnsMap(0),
	state(empty) {}

/**
 * @brief Make a quick copy of the object
 *
 * @details The Record object is not copied entirely. The result of the copy is not a totally different
 * object, but a different object that, however, "points" to the same data.
 *
 * @param [in] rec object to be copied.
 */
Record::Record (const Record& rec) noexcept :
	parent(rec.parent),
	columnsMap(rec.columnsMap),
	valuesMap(rec.valuesMap),
	state(rec.state) {}

/**
 * @brief Make a quick copy of the object
 *
 * @details The Record object is not copied entirely. The result of the copy is not a totally different
 * object, but a different object that, however, "points" to the same data.
 *
 * @param [in] rec object to be copied.
 */
const Record& Record::operator= (const Record& rec) noexcept {
	if (&rec != this) {
		parent = rec.parent;
		columnsMap = rec.columnsMap;
		valuesMap = rec.valuesMap;
		state = rec.state;
	}
	return *this;
}

/**
 * @brief Allow you to update one of the value in the record.
 *
 * @details Allows you to edit one of the values ​​that make up the record, specifying the column to which
 * the value to edit refers. Changes made to the records remain local until the next commit (ie when the
 * changes that are made locally will be made effective on the database).
 * If a column compose the key for the table to which it belongs, editing is performed only if the record
 * is in the "empty" state.
 *
 * @param [in] columnName	name of the column to which the value to edit refers;
 * @param [in] value		new value for the "columnName" field of the record;
 *
 * @warning This function is designed to perform changes after that the validation of the values and column
 * name has been performed.
 **/
void Record::performChanges (	const std::string& columnName,
								const std::string& value) noexcept {
	std::unordered_map<std::string, Column>::const_iterator it = columnsMap->find(columnName);
	switch (state) {
		case empty:
			{
				std::string new_value = value;
				valuesMap->at(columnName).current = new_value;
			}
			break;
		case inserting:
			{
				std::string new_value = value;
				if (!it->second.getIsKey())
					valuesMap->at(columnName).current = new_value;
			}
			break;
		case loaded:
		case updating:
			{
				std::string new_value = value;
				std::string old_value = valuesMap->at(columnName).current;
				if (!it->second.getIsKey()) {
					if (state == loaded)
						valuesMap->at(columnName).old = old_value;
					valuesMap->at(columnName).current = new_value;
					valuesMap->at(columnName).changed = true;
				}
			}
			break;

		default:
			break;
	}
}

/**
 * @brief Allow you to update <b>atomically</b> one or more values in the record using column-value pairs.
 *
 * @details Allows you to edit one or more of the values ​​that make up the record, specifying the column
 * to which each value to edit refers using a column-value pairs. Changes made to the records remain local
 * until the next commit (ie when the changes that are made locally will be made effective on the
 * database).
 * The following table shows the possible status transitions for a record when the edit() function is
 * called on them.
 * <table>
 * <tr><th>Current state</th><th>Next state</td></tr>
 * <tr><td>empty</td><td>inserting</td></tr>
 * <tr><td>loaded</td><td>updating</td></tr>
 * <tr><td>inserting</td><td>inserting</td></tr>
 * <tr><td>updating</td><td>updating</td></tr>
 * <tr><td>deleting</td><td>deleting (no operation performed)</td></tr>
 * </table>
 *
 * If a column compose the key for the table to which it belongs, editing is performed only if the record
 * is in the "empty" or state. Changes to the fields that make up the key are ignored.
 *
 * @exception AccessException;
 * @exception EmptyKey;
 * @exception InvalidArgument;
 * @exception AmbiguousDate;
 * @exception InvalidDate;
 * @exception InvalidTime;
 * @exception OutOfBound;
 * @exception ValueTooLong;
 *
 * @param [in] valuesMap	a reference to a std::unordered_map<std::string, std::string> object
 * 							containing a set of "column name - value" pair, each of which specifies the
 * 							column to which each value to edit refers;
 **/
void Record::edit (const std::unordered_map<std::string, std::string>& values ) throw ( BasicException& ) {
	if (state != deleting) {
		std::unordered_map<std::string, std::string>::const_iterator values_it = values.begin();
		std::unordered_map<std::string, std::string>::const_iterator values_end = values.end();
		std::unordered_map<std::string, Column>::const_iterator col_end = columnsMap->end();

		/*
		 * controllo prima che le colonne corrispondano e che i valori siano compatibili in modo da assicurare
		 * l'atomicita'
		 */
		for (; values_it != values_end; values_it++) {
			std::unordered_map<std::string, Column>::const_iterator it = columnsMap->find(values_it->first);
			if (it == col_end)
				throw AccessException (	AccessException::ColumnDoesntExist,
										__PRETTY_FUNCTION__,
										"Can't access to column " + values_it->first);
			else {
				std::string tmp = values_it->second;
				switch (state) {
				case empty:
					it->second.validate(tmp);
					break;
				case inserting:
				case loaded:
				case updating:
					if (!it->second.getIsKey())
						it->second.validate(tmp);
					break;
				default: break;
				}
			}
		}
		/*
		 * dopo aver verificato corrispondenza delle colonne e coerenza dei valori, effettuo l'aggiornamento
		 */
		values_it = values.begin();
		for (; values_it != values_end; values_it++)
				performChanges(values_it->first, values_it->second);
		stateMachine();
	}
}

/**
 * @brief Gets the current value belonging to a column.
 *
 * @warning This function throws an AccessException if the column name is empty, null or non existent.
 *
 * @param [in] columnName name of the column;
 *
 * @return an std::string object, containing the value.
 */
std::string Record::getCurrentValue (const std::string& columnName) const throw( AccessException& ) {
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->find(columnName);
	if (it != valuesMap->end())
		return it->second.current;
	else
		throw AccessException (	AccessException::ColumnDoesntExist,
								__PRETTY_FUNCTION__,
								"Can't access to column " + columnName);
}

/**
 * @brief Gets all the current values belonging to a Record object.
 *
 * @details The values belonging to the Record object will be returned as "column name - value" pairs in a
 * std::unordered_map<std::string, std::string> object.
 *
 * @param [out] values a std::unordered_map<std::string, std::string> object containing the
 * "column name - value" pairs;
 */
void Record::getCurrentValues (	std::unordered_map<std::string,	std::string>& values ) const noexcept {
	values.clear();
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->begin();
	std::unordered_map<std::string, Value>::const_iterator end = valuesMap->end();
	for (; it != end; it++)
		values.insert(std::pair<std::string, std::string>(it->first, it->second.current));
}


/**
 * @brief Gets the old value belonging to a column.
 *
 * @warning This function throws an AccessException if the column name is empty, null or non existent.
 *
 * @param [in] columnName name of the column;
 *
 * @return an std::string object, containing the value.
 */
std::string Record::getOldValue (const std::string& columnName) const throw( AccessException& ) {
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->find(columnName);
	if (it != valuesMap->end())
		return it->second.old;
	else
		throw AccessException (	AccessException::ColumnDoesntExist,
								__PRETTY_FUNCTION__,
								"Can't access to column " + columnName);
}

/**
 * @brief Gets all the old values belonging to a Record object.
 *
 * @details The values belonging to the Record object will be returned as "column name - value" pairs in a
 * std::unordered_map<std::string, std::string> object.
 *
 * @param [out] values a std::unordered_map<std::string, std::string> object containing the
 * "column name - value" pairs;
 */
void Record::getOldValues ( std::unordered_map<std::string, std::string>& values ) const noexcept {
	values.clear();
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->begin();
	std::unordered_map<std::string, Value>::const_iterator end = valuesMap->end();
	for (; it != end; it++)
		values.insert(std::pair<std::string, std::string>(it->first, it->second.old));
}

/**
 * @brief Checks whether the value associated with a column has been changed.
 *
 * @param [in] columnName column name
 *
 * @retval "true" if the value has been changed;
 * @retval "false" if the value has not been changed;
 */
bool Record::getChanged (const std::string& columnName) const throw( AccessException& )
{
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->find(columnName);
	if (it != valuesMap->end())
		return it->second.changed;
	else
		throw AccessException (	AccessException::ColumnDoesntExist,
								__PRETTY_FUNCTION__,
								"Can't access to column " + columnName);
}

/**
 * @brief Checks whether the values associated with a Record object has been changed.
 *
 * @param [out] values a std::unordered_map object
 */
void Record::getChanged (std::unordered_map<std::string, bool>& values ) const noexcept {
	values.clear();
	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->begin();
	std::unordered_map<std::string, Value>::const_iterator end = valuesMap->end();
	for (; it != end; it++)
		values.insert(std::pair<std::string, bool>(it->first, it->second.changed));
}

/**
 * @brief Gets the size of a Record object, in byte
 *
 * @return size of the whole Record object (not just the content of the fields), in byte
 */
std::streamoff Record::size() const noexcept {
	unsigned long size = 3 * sizeof(unsigned) * valuesMap->size() + sizeof(unsigned) + sizeof(bool) + sizeof(enum Status);

	std::unordered_map<std::string, Value>::const_iterator it = valuesMap->begin();
	std::unordered_map<std::string, Value>::const_iterator end = valuesMap->end();
	for (; it != end; it++)
		size += it->first.size() + it->second.current.size() + it->second.old.size();
	return (std::streamoff) size;
}

/**
 * @brief Returns a string that can be used to uniquely identify a record.
 *
 * @details The string is obtained by concatenating the value assumed by the fields that make up the key for
 * the table object to which the record belongs. the value thus obtained is obfuscated using SHA-256 and
 * represented as a string whose characters are the hashsum in hexadecimal.
 *
 * @return a std::string object, containing a string that can be used to uniquely identify a record.
 */
std::string Record::getKey() const noexcept {
	std::string key;
	Table::ConstIterator it = parent->getParent().cbegin();
	Table::ConstIterator end = parent->getParent().cend();


	for (; it != end; it++) {
		if (it->getIsKey())
			key += valuesMap->at(it->getName()).current;
	}
	if (key.empty()) {
		it = parent->getParent().cbegin();
		for (; it != end; it++)
			key += valuesMap->at(it->getName()).current;
	}
	SHA256_sum_t hashsum;
	SHA256_hashsum((uint8_t*)key.c_str(), key.size(), &hashsum);
	std::stringstream str;
	for (int i = 0; i < 8; i++)
		str << std::hex << std::setfill('0') << std::setw(8) << hashsum.byte_array[i];
	return str.str();
}

/**
 * @brief This function must be called only by the parent Storage object
 *
 * @param [in] structure The structure of a Table object
 */
void Record::setStructure(std::shared_ptr<std::unordered_map<std::string, Column>> structure) noexcept {
	columnsMap = structure;
	valuesMap = std::shared_ptr<std::unordered_map<std::string, Value>>(new std::unordered_map<std::string, Value>);
	std::unordered_map<std::string, Column>::const_iterator it = columnsMap->begin();
	std::unordered_map<std::string, Column>::const_iterator end = columnsMap->end();
	for (; it != end; it++)
		valuesMap->insert (std::pair<std::string, Value>(it->first, Value()));
}

/**
 * @brief Changes the state of a record after a call to the edit() function.
 *
 * @details
 * The following table shows the possible status transitions for a record when the edit() function is
 * called on them.
 * <table>
 * <tr><th>Current state</th><th>Next state</td></tr>
 * <tr><td>empty</td><td>inserting</td></tr>
 * <tr><td>loaded</td><td>updating</td></tr>
 * <tr><td>inserting</td><td>inserting</td></tr>
 * <tr><td>updating</td><td>updating</td></tr>
 * <tr><td>deleting</td><td>deleting (no operation performed)</td></tr>
 * </table>
 */
void Record::stateMachine() {
	switch (state) {
		case empty:
		case inserting:
			state = Record::inserting;
			break;
		case loaded:
		case updating:
			state = Record::updating;
			break;
		default:
			break;
	}
}

