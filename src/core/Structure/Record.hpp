/**
 * @file Record.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_RECORD
#define OPENDB2_RECORD

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 * @addtogroup Storage
 * @{
 */

#include "Column.hpp"

#include <memory>
#include <unordered_map>
#include <string>
#include <fstream>

namespace openDB
{

class Storage;

/**
 * @brief This class represents a record and manages its status.
 *
 * @details Here, for records, we mean the row of a database table. The Record class holds a reference to the
 * table whose record belongs by retaining a pointer to the Storage object that manages all records in a Table
 * object.
 * This class allows you to perform controlled operations such as inserting a new record into a table,
 * editing an existing record, marking a record so that it is deleted. Changes made to the records remain
 * local until the next commit (ie when the changes that are made locally will be made effective on the
 * database).
 * For controlled operations we mean, in this case, operations that maintain the ACID properties.
 */
class Record
{
	friend class Storage;
public:

	/**
	 * @brief Record state
	 */
	enum Status {
		empty,		/**< empty record; initial state of each Record object*/
		loaded,		/**< record loaded from database and unmodified */
		inserting,	/**< record inserted (not loaded from database) */
		updating,	/**< record loaded from database and modified */
		deleting	/**< record loaded from database and marked for deletion */
	};


	Record() noexcept;

	Record (const Record& rec) noexcept;

	const Record& operator= (const Record& rec) noexcept;

	void edit (const std::unordered_map<std::string, std::string>& valuesMap) throw ( BasicException& );

	/**
	 * @brief Sets the state of the record.
	 *
	 * @param [in] s new record state.
	 */
	void setStatus (enum Status s) noexcept { state = s; }

	/**
	 * @brief Gets the state of the record.
	 *
	 * @return a Record::State element, the state of the record.
	 */
	enum Status getStatus () const noexcept {return state;}

	std::string getCurrentValue (const std::string& columnName) const throw( AccessException& );

	void getCurrentValues ( std::unordered_map<std::string, std::string>& values ) const noexcept;

	std::string getOldValue (const std::string& columnName) const throw( AccessException& );

	void getOldValues ( std::unordered_map<std::string, std::string>& values ) const noexcept;

	bool getChanged (const std::string& columnName) const throw( AccessException& );

	void getChanged (std::unordered_map<std::string, bool>& values ) const noexcept;

	std::streamoff size() const noexcept;

	std::string getKey() const noexcept;

private:
	Storage* parent;	/**< pointer to the Storage object that the record refers to */

	/**
	 * Pointer to the "structural view" of a Table (and Storage) object
	 *
	 * @warning DO NOT COPY THIS POINTER MANUALLY! Use the setStructure() function.
	 */
	std::shared_ptr<std::unordered_map<std::string, Column>> columnsMap;

	void setStructure(std::shared_ptr<std::unordered_map<std::string, Column>> structure) noexcept;

	/**
	 * @brief This structure keeps vital information on the value belonging to a column of the record.
	 */
	struct 	Value
	{
		std::string current;	/**< current value of the field */
		std::string old;		/**< old value of the field */
		bool changed;			/**< "true" if the value has been changed */

		/**
		 * @brief Constructor
		 * @param [in] c current value
		 * @param [in] o old value
		 * @param [in] v varied
		 */
		Value (	std::string c = "",
				std::string o = "",
				bool v = false) :
					current(c), old(o), changed(v) {}
	};

	std::shared_ptr<std::unordered_map<std::string, Value>> valuesMap; /**< keeps "column name - value" pairs */

	enum Status state;	/**< record state */

	void performChanges (const std::string& columnName, const std::string& value) noexcept;

	void stateMachine();
};

/**
 * @}
 * @}
 * @}
 */

};

#endif
