/**
 * @file Schema.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Schema.hpp"
#include "Database.hpp"
using namespace openDB;

Schema::Schema (	const std::string& schemaName,
			Database* parent) throw ( AccessException& ) :
		name(schemaName),
		parent(parent),
		tablesMap(new std::unordered_map<std::string, Table>)
{
	if (name == "")
		throw AccessException (AccessException::SchemaName, __func__, "A name is necessary for the schema!");
}

Schema::Schema (const Schema& schema) noexcept :
		name(schema.name),
		parent(schema.parent),
		tablesMap(schema.tablesMap)
{}

const Schema& Schema::operator= (const Schema& schema) noexcept
{
	if (&schema != this)
	{
		name = schema.name;
		parent = schema.parent;
		tablesMap = schema.tablesMap;
	}
	return *this;
}

Schema::Iterator Schema::addTable(std::string tableName)  throw (AccessException&)
{
	Schema::Iterator tab_it = at(tableName);
	if (tab_it == end())
	{
		tablesMap->insert(std::pair<std::string, Table>(tableName, Table(tableName, (Schema*) this)));
		return at(tableName);
	}
	return end();
}

void Schema::deleteTable (Schema::Iterator& it) noexcept {
	if (it.parent == this && it != end())
		it.iter =tablesMap->erase(it.iter);
}

Schema::Iterator Schema::begin() noexcept
{
	Schema::Iterator it;
	it.parent = this;
	it.iter =  tablesMap->begin();
	return it;
}

Schema::Iterator Schema::end() noexcept
{
	Schema::Iterator it;
	it.parent = this;
	it.iter =  tablesMap->end();
	return it;
}

Schema::ConstIterator Schema::cbegin() const noexcept
{
	ConstIterator it;
	it.parent = (Schema*) this;
	it.iter =  tablesMap->cbegin();
	return it;
}

Schema::ConstIterator Schema::cend() const noexcept
{
	ConstIterator it;
	it.parent = (Schema*) this;
	it.iter =  tablesMap->cend();
	return it;
}

Schema::Iterator Schema::at(const std::string& tableName) noexcept
{
	Schema::Iterator it;
	it.parent = this;
	it.iter =  tablesMap->find(tableName);
	return it;
}

Schema::ConstIterator Schema::cat(const std::string& tableName) const noexcept
{
	Schema::ConstIterator it;
	it.parent = (Schema*)this;
	it.iter =  tablesMap->find(tableName);
	return it;
}
