/**
 * @file Schema.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_SCHEMA
#define OPENDB2_SCHEMA

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 */


#include "Table.hpp"

namespace openDB
{

class Database;

/**
 * @brief Abstraction of a database schema.
 *
 * @details
 * The Schema class provides the abstraction of a schema in a database. The term "schema" refers to the
 * organization of data as a blueprint of how the database is constructed (divided into database tables in
 * the case of relational databases).
 *
 * section tables Table management
 * Table objects are identified from their own name (table with the same name are not allowed in a schema),
 * but the structure used in Schema objects to hold Table object, a std::unordered_map, would alter the order
 * in which Table objects have been added to the Schema object. For this reason, there is a std::list
 * object in which the Table objects names are progressively inserted in order.
 *
 * @section iteratorsec Accessing Table
 * This class implements some useful methods and a mechanism that governs access to Table belonging to a
 * Schema object. The mechanism is based on the concept of "iterator".
 * An iterator is any object that, pointing to some element in a range of elements (such as an array or a
 * container), has the ability to iterate through the elements of that range using a set of operators
 * (with at least the increment (++) and dereference (*) operators).
 * Iterators are available in a "normal" version, without restriction on access to the object on which they
 * are placed, and in a "const" version, which does not allow changes to the object, implemented,
 * respectively, by the Schema::Iterator and Schema::ConstIterator class.
 */
class Schema 
{
public:
	
	/**
	 * @brief Iterator class
	 *
	 * @details The Schema class implements a simple but efficient mechanism that governs access to tables.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the element on which the iterator is placed.
	 */
	class Iterator
	{
		friend class Schema;
	public:

		/**
		 * @brief Constructor
		 *
		 * @details You can build a Schema::Iterator object, but you can not associate it with a Schema
		 * object. This is to further regulate how to access tables belonging to a schema. You can get a
		 * Schema::Iterator object associated with a Schema object only by using the Schema::begin(),
		 * Schema::at() or Schema::end() functions.
		 */
		Iterator() noexcept : parent(0) {}

		/**
		 * @brief Post-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a new iterator to the previous
		 * position.
		 *
		 * @return a new iterator placed on the previous position.
		 */
		Iterator operator++(int) noexcept;		//i++

		/**
		 * @brief Pre-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a reference to the same iterator on
		 * which the operator acted.
		 *
		 * @return a reference to the same iterator.
		 */
		Iterator& operator++() noexcept;		//++i

		/**
		 * @brief Checks if two iterators are placed on the same element.
		 *
		 * @param [in] __it a Schema::ConstIterator object;
		 *
		 * @retval true if two iterators are placed on the same element;
		 * @retval false if two iterators aren't placed on the same element;
		 */
		bool operator == (const Iterator& __it) const noexcept;

		/**
		 * @brief Verify that two iterators are not placed on the same element.
		 *
		 * @param [in] __it a Schema::ConstIterator object;
		 *
		 * @retval true if two iterators aren't placed on the same element;
		 * @retval false if two iterators are placed on the same element;
		 */
		bool operator != (const Iterator& __it) const noexcept;

		/**
		 * @brief Check if two iterators are associated with the same Schema object.
		 *
		 * @param [in] __it a Schema::ConstIterator object;
		 *
		 * @retval true if two iterators are associated with the same Schema object;
		 * @retval false  if two iterators aren't associated with the same Schema object;
		 */
		bool brother (const Iterator& __it) const noexcept;

		/**
		 * @brief Dereference operator
		 *
		 * @return a reference to the Table object on which the iterator is placed. The reference remains
		 * valid until the Schema object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		Table& operator* () throw ( AccessException& );

		/**
		 * @brief Arrow operator
		 *
		 * @return a pointer to the Table object on which the iterator is placed. The pointer remains
		 * valid until the Schema object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
		 */
		Table* operator-> () throw ( AccessException& );

	private:
		Schema* parent;	/**< pointer to the Schema object to which the iterator is associated */
		std::unordered_map<std::string, Table>::iterator iter;

		/**
		 * @brief Moves the iterator one position forward.
		 *
		 * @return a reference to the same object.
		 */
		Iterator& next () noexcept;
	};

	/**
	 * @brief ConstIterator class
	 *
	 * @details The Schema class implements a simple but efficient mechanism that governs access to tables.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the record on which the iterator is placed.
	 * This class of iterators belongs to the class of random access iterators and has read-only access
	 * restrictions to records.
	 */
	class ConstIterator
	{
		friend class Schema;
	public:

		/**
		 * @brief Constructor
		 *
		 * @details You can build a Schema::ConstIterator object, but you can not associate it with a Schema
		 * object. This is to further regulate how to access tables belonging to a schema. You can get a
		 * Schema::ConstIterator object associated with a Schema object only by using the Schema::cbegin(),
		 * Schema::at() or Schema::cend() functions.
		 * This class of iterators belongs to the class of random access iterators class and has read-only
		 * access restrictions to columns.
		 */
		ConstIterator() noexcept : parent(0) {}

		/**
		 * @brief Post-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a new iterator to the previous
		 * position.
		 *
		 * @return a new iterator placed on the previous position.
		 */
		ConstIterator operator++(int) noexcept;		//i++

		/**
		 * @brief Pre-increase operator
		 *
		 * @details Moves the iterator one position forward, returning a reference to the same iterator on
		 * which the operator acted.
		 *
		 * @return a reference to the same iterator.
		 */
		ConstIterator& operator++() noexcept;		//++i

		/**
		 * @brief Checks if two iterators are placed on the same element.
		 *
		 * @param [in] __it a Schema::ConstIterator object;
		 *
		 * @retval true if two iterators are placed on the same element;
		 * @retval false if two iterators aren't placed on the same element;
		 */
		bool operator == (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Verify that two iterators are not placed on the same element.
		 *
		 * @param [in] __it a Schema::ConstIterator object;
		 *
		 * @retval true if two iterators aren't placed on the same element;
		 * @retval false if two iterators are placed on the same element;
		 */
		bool operator != (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Check if two iterators are associated with the same Schema object.
		 *
		 * @param [in] __it a Table::ConstIterator object;
		 *
		 * @retval true if two iterators are associated with the same Schema object;
		 * @retval false  if two iterators aren't associated with the same Schema object;
		 */
		bool brother (const ConstIterator& __it) const noexcept;

		/**
		 * @brief Dereference operator
		 *
		 * @return a reference to the Table object on which the iterator is placed. The reference remains
		 * valid until the Schema object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end ()).
		 */
		const Table& operator* () const throw ( AccessException& );

		/**
		 * @brief Arrow operator
		 *
		 * @return a pointer to the Table object on which the iterator is placed. The pointer remains
		 * valid until the Schema object to which the iterator belongs is deallocated.
		 *
		 * @exception AccessException if the iterator is placed in an invalid position (eg end ()).
		 */
		const Table* operator-> () const throw ( AccessException& );

	private:
		Schema* parent;	/**< pointer to the Table object to which the iterator is associated */
		std::unordered_map<std::string, Table>::const_iterator iter;

		/**
		 * @brief Moves the iterator one position forward.
		 *
		 * @return a reference to the same object.
		 */
		ConstIterator& next () noexcept;
	};

	friend class Iterator;
	friend class ConstIterator;

	/**
	 * @brief Constructor.
	 *
	 * @details Builds a new Schema object, assigning it a name and optionally assigning it a Database object
	 * as parent object.
	 *
	 * @param [in] name		name of the new Schema object;
	 * @param [in] parent	parent Database object.
	 *
	 * @exception AccessException if name is an empty string.
	 */
	explicit Schema (	const std::string& name,
						Database* parent = 0) throw ( AccessException& );

	/**
	 * @brief Make a quick copy of the object
	 *
	 * @details The Schema object is not copied entirely.
	 * The main advantages offered by this type of solution are many. First of all, you can pass a Schema type
	 * object by copy very quickly from having to copy entirely every time. Any changes to the "copy" will
	 * affect the original object. Second, in this way, two Schema objects can share the same Storage and the
	 * same records, creating a dual access mechanism to the same information content.
	 *
	 * @param [in] schema Schema object to be copied.
	 */
	Schema (const Schema& schema) noexcept;

	/**
	 * @brief Make a quick copy of the object
	 *
	 * @details The Schema object is not copied entirely.
	 * The main advantages offered by this type of solution are many. First of all, you can pass a Schema type
	 * object by copy very quickly from having to copy entirely every time. Any changes to the "copy" will
	 * affect the original object. Second, in this way, two Schema objects can share the same Storage and the
	 * same records, creating a dual access mechanism to the same information content.
	 *
	 * @param [in] schema Schema object to be copied.
	 */
	const Schema& operator= (const Schema& schema) noexcept;

	/**
	 * @brief Returns the schema name;
	 *
	 * @return a std::string object, containing the table name;
	 */
	std::string getName() const noexcept {return name;}

	/**
	 * @brief Returns the number of tables belonging to the schema;
	 *
	 * @return an unsigned number representing the number of tables belonging to the schema.
	 */
	unsigned tablesNum() const noexcept {return tablesMap->size();}

	/**
	 * @brief Returns a Schema::Iterator object placed on the first table.
	 *
	 * @note There is no special order for Table objects, except the one in which they are added to the schema
	 * or loaded from databases.
	 *
	 * @return a Schema::Iterator placed on the first Table object;
	 */
	Iterator begin() noexcept;

	/**
	 * @brief Returns a Schema::Iterator pointing to the "past-the-end" table.
	 *
	 * @note the "past-the-end" table does not really exist, so the iterator returned by this function
	 * points to the first invalid position after all valid positions.
	 *
	 * @return a Schema::Iterator pointing to the "past-the-end" table.
	 */
	Iterator end() noexcept;

	/**
	 * @brief Returns a Schema::ConstIterator object placed on the first table.
	 *
	 * @note There is no special order for Table objects, except the one in which they are added to the schema
	 * or loaded from databases.
	 *
	 * @return a Schema::ConstIterator placed on the first Table object;
	 */
	ConstIterator cbegin() const noexcept;

	/**
	 * @brief Returns a Schema::ConstIterator pointing to the "past-the-end" table.
	 *
	 * @note the "past-the-end" table does not really exist, so the iterator returned by this function
	 * points to the first invalid position after all valid positions.
	 *
	 * @return a Schema::ConstIterator pointing to the "past-the-end" table.
	 */
	ConstIterator cend() const noexcept;

	/**
	 * @brief Returns an iterator pointing to a specific Table object
	 *
	 * @param [in] tableName name of the Table object.
	 *
	 * @return a Schema::Iterator object, pointing to the Table object whose name is tableName, or end() if
	 * there is no Table object with that name.
	 */
	Iterator at(const std::string& tableName) noexcept;

	/**
	 * @brief Returns an iterator pointing to a specific Table object
	 *
	 * @param [in] tableName name of the Table object.
	 *
	 * @return a Schema::ConstIterator object, pointing to the Table object whose name is tableName, or end()
	 * if there is no Table object with that name.
	 */
	ConstIterator cat(const std::string& tableName) const noexcept;

	/**
	 * @brief Creates and adds a Table object to the Schema.
	 *
	 * @param [in] tableName name for the new Table object.
	 *
	 * @return a Schema::Iterator object, pointing to the inserted Table object, if it was actually inserted,
	 * or end() if no insertion occurred.
	 */
	Iterator addTable(std::string tableName) throw (AccessException&);

	/**
	 * @brief Remove and delete a Table object from the Schema.
	 *
	 * @param [in] it a Schema::Iterator, pointing to the Table object to be removed and deleted.
	 */
	void deleteTable (Iterator& it) noexcept;

private:
/**
 * @brief Schema name.
 *
 * @details Schemas in the same database are identified through their name.
 */
	std::string name;
/**
 * @brief Pointer to the Database object which the Schema object belongs.
 */
	Database* parent;
/**
 * @brief Structural view of a Schema.
 *
 * @details The set of table objects that make up a schema are contained in an unordered_map object, which
 * allows access to internal Table objects using their name.
 */
	std::shared_ptr<std::unordered_map<std::string, Table>> tablesMap;
};

/**
 * @}
 * @}
 */


	
};

#endif
