/**
 * @file SchemaConstIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Schema.hpp"
using namespace openDB;

Schema::ConstIterator Schema::ConstIterator::operator++(int) noexcept
{
	Schema::ConstIterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

Schema::ConstIterator& Schema::ConstIterator::operator++() noexcept
{
	next();
	return *this;
}

bool Schema::ConstIterator::operator == (const Schema::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent && iter == __it.iter)
		return true;
	return false;
}

bool Schema::ConstIterator::operator != (const Schema::ConstIterator& __it) const noexcept
{
	return !operator==(__it);
}

bool Schema::ConstIterator::brother(const Schema::ConstIterator& __it) const noexcept
{
	if (parent == __it.parent)
		return true;
	return false;
}

const Table& Schema::ConstIterator::operator* () const throw ( AccessException& )
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Schema::ConstIterator::operator*: no valid parent pointer;");

	if (iter == parent -> tablesMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Schema::ConstIterator::operator*: can't access to end();");

	return iter->second;
}

const Table* Schema::ConstIterator::operator-> () const throw (AccessException&)
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Schema::ConstIterator::operator->: no valid parent pointer;");

	if (iter == parent->tablesMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Schema::ConstIterator::operator->: can't access to end();");

	return &(iter->second);
}

Schema::ConstIterator& Schema::ConstIterator::next () noexcept
{
	if (parent && iter != parent->tablesMap->cend())
		iter++;
	return* this;
}



