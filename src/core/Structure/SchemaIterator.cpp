/**
 * @file SchemaIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Schema.hpp"
using namespace openDB;

Schema::Iterator Schema::Iterator::operator++(int) noexcept
{
	Schema::Iterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

Schema::Iterator& Schema::Iterator::operator++() noexcept
{
	next();
	return *this;
}

bool Schema::Iterator::operator == (const Schema::Iterator& __it) const noexcept
{
	if (parent == __it.parent && iter == __it.iter)
		return true;
	return false;
}

bool Schema::Iterator::operator != (const Schema::Iterator& __it) const noexcept
{
	return !operator==(__it);
}

bool Schema::Iterator::brother(const Schema::Iterator& __it) const noexcept
{
	if (parent == __it.parent)
		return true;
	return false;
}

Table& Schema::Iterator::operator* () throw ( AccessException& )
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Schema::Iterator::operator*: no valid parent pointer;");

	if (iter == parent->tablesMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Schema::Iterator::operator*: can't access to end();");

	return iter->second;
}

Table* Schema::Iterator::operator-> () throw (AccessException&)
{
	if (!parent)
		throw AccessException (
				AccessException::IteratorNoParent,
				__PRETTY_FUNCTION__,
				"Error using Schema::Iterator::operator->: no valid parent pointer;");

	if (iter == parent -> tablesMap->end())
		throw AccessException (
				AccessException::IteratorAccessEnd,
				__PRETTY_FUNCTION__,
				"Error using Schema::Iterator::operator->: can't access to end();");

	return &(iter->second);
}

Schema::Iterator& Schema::Iterator::next () noexcept
{
	if (parent && iter != parent->tablesMap->cend())
		iter++;
	return* this;
}




