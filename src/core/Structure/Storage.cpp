/**
 * @file Storage.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Storage.hpp"
#include "Table.hpp"
#include <cstdlib>
#include <cassert>
using namespace openDB;

#ifdef __DEBUG
#include <iostream>
using namespace std;
#endif

/**
 * @brief Constructor.
 *
 * @details You can build a new Storage object, but you can't bind it to a Table object.
 */
Storage::Storage() noexcept :
		parent (0),
		columnsMap(),
		recordMap(new std::unordered_map<std::string, Record>),
		recordOrder(new std::list<std::string>)
{}

Storage::Storage(const Storage& s) noexcept :
	parent(s.parent),
	columnsMap(s.columnsMap),
	recordMap(s.recordMap),
	recordOrder(s.recordOrder)
	{}


const Storage& Storage::operator=(const Storage& s) noexcept {
	if (&s != this) {
		parent = s.parent;
		columnsMap = s.columnsMap;
		recordMap = s.recordMap;
		recordOrder = s.recordOrder;
	}
	return *this;
}


/**
 * @brief Cleans the object.
 *
 * @details Brings back the object to its initial state.
 */
void Storage::clean () noexcept {
	recordMap->clear();
	recordOrder->clear();
}

/**
 * @brief Returns a Storage::Iterator object placed on the first record managed by the Storage object.
 *
 * @note The order is the one defined by the order relations existing between the unique identifiers of the
 * inserted Record objects.
 *
 * @return a Storage::Iterator placed on the first record;
 */
Storage::Iterator Storage::begin() noexcept
{
	Storage::Iterator it;
	it.parent = (Storage*) this;
	it.iter = ((Storage*)this)->recordMap->begin();
	return it;
}


/**
 * @brief Returns a Storage::Iterator pointing to the "past-the-end" record.
 *
 * @note the "past-the-end" record does not really exist, so the iterator returned by this function
 * points to the first invalid position after all valid positions.
 *
 * @return a Storage::Iterator pointing to the "past-the-end" record.
 */
Storage::Iterator Storage::end() noexcept
{
	Storage::Iterator it;
	it.parent = (Storage*) this;
	it.iter = ((Storage*)this)->recordMap->end();
	return it;
}

/**
 * @brief Returns a Storage::ConstIterator object placed on the first record managed by the Storage object.
 *
 * @note The order is the one defined by the order relations existing between the unique identifiers of the
 * inserted Record objects.
 *
 * @return a Storage::ConstIterator placed on the first record;
 */
Storage::ConstIterator Storage::cbegin() const noexcept
{
	Storage::ConstIterator it;
	it.parent = (Storage*)this;
	it.iter = recordMap->cbegin();
	return it;
}

/**
 * @brief Returns a Storage::ConstIterator pointing to the "past-the-end" record.
 *
 * @note the "past-the-end" record does not really exist, so the iterator returned by this function
 * points to the first invalid position after all valid positions.
 *
 * @return a Storage::ConstIterator pointing to the "past-the-end" record.
 */
Storage::ConstIterator Storage::cend() const noexcept
{
	Storage::ConstIterator it;
	it.parent = (Storage*)this;
	it.iter = recordMap->cend();
	return it;
}

/**
 * @brief Returns a Storage::OrderedConstIterator object placed on the first record managed by the Storage object.
 *
 * @note There is no special order for records managed by a Storage object, except the one in which they
 * are inserted or loaded from databases.
 *
 * @return a Storage::OrderedConstIterator placed on the first record;
 */
Storage::OrderedConstIterator Storage::obegin() const noexcept {
	Storage::OrderedConstIterator it;
	it.parent = (Storage*)this;
	it.iter = recordOrder->cbegin();
	return it;
}

/**
 * @brief Returns a Storage::OrderedConstIterator pointing to the "past-the-end" record.
 *
 * @note the "past-the-end" record does not really exist, so the iterator returned by this function
 * points to the first invalid position after all valid positions.
 *
 * @return a Storage::OrderedConstIterator pointing to the "past-the-end" record.
 */
Storage::OrderedConstIterator Storage::oend() const noexcept {
	Storage::OrderedConstIterator it;
	it.parent = (Storage*)this;
	it.iter = recordOrder->cend();
	return it;
}

/**
 * @brief Returns an iterator pointing to a specific Record object
 *
 * @param [in] id internal ID of the Record object.
 *
 * @return a Storage::Iterator object, pointing to the Record object whose internal ID is "id", or end() if
 * there is no Record object with that id.
 */
Storage::Iterator Storage::at(const std::string& id) noexcept {
	Storage::Iterator it;
	it.parent = this;
	it.iter =  recordMap->find(id);
	return it;
}

/**
 * @brief Returns an iterator pointing to a specific Record object
 *
 * @param [in] id internal ID of the Record object.
 *
 * @return a Storage::ConstIterator object, pointing to the Record object whose internal ID is "id", or end()
 * if there is no Record object with that id.
 */
Storage::ConstIterator Storage::cat(const std::string& id) const noexcept {
	Storage::ConstIterator it;
	it.parent = (Storage*)this;
	it.iter =  recordMap->find(id);
	return it;
}

/**
 * @brief Create a new Record object.
 *
 * @details The purpose of this function is to create a new Record type object and to bind it to a
 * MemStorage object.
 *
 * @param [in] recState element of type Record::State, state for the new Record object.
 *
 * @return a Record object, binded to the MemStorage object through which it was created.
 */
Record Storage::newRecord(enum Record::Status recState) const noexcept {
	Record rec;
	rec.parent = (Storage*)this;
	rec.state = recState;
	rec.setStructure(columnsMap);
	return rec;
}

/**
 * @brief Insert a Record object.
 *
 * @details The purpose of this function is to insert a new Record object between those managed by the
 * MemStorage object so that the information contained in the Record object is made persistent in the
 * database at commit. The Record object must necessarily have been created with the newRecord() function,
 * or no insertion will take place.
 *
 * @param [in] record 	a Record object, the record to be inserted; must be previously created with the
 * 						newRecord() function;
 *
 * @warning If the Record object has not been created with the newRecord() function before, no insertion
 * will be made, the function will return end().
 *
 * @return a Storage::Iterator object, placed on the new inserted record, or end() if no insertion is made.
 */
Storage::Iterator Storage::addRecord(const Record& record) noexcept {
	std::string recordID = record.getKey();
	if (record.parent == this && recordMap->find(recordID) == recordMap->end()) {
		Storage::Iterator it;
		it.parent = this;
		it.iter = recordMap->insert(std::pair<std::string, Record>(recordID, record)).first;
		recordOrder->push_back(recordID);
		return it;
	}
	return end();
}

/**
 * @brief Erase a Record object.
 *
 * @details The purpose of this function is to erase a Record object between those managed by the
 * MemStorage object. For erased objects no operation on the database will be executed at commit: an
 * erased record is as if it never existed in the local MemStorage object.
 * The record that will be erased will be the one to which the iterator object passed to the function.
 * If the iterator is not associated with the MemStorage object, no changes will take place.
 *
 * @param [in] it	Storage::Iterator object, associated with the MemStorage object, pointing to the
 * 					Record object to be erased.
 *
 * @warning If the Iterator object is not associated with the MemStorage object, no changes will take
 * place.
 */
void Storage::removeRecord(Storage::Iterator& it) noexcept
{
	if (it.brother(begin()) && it != end()) {
		recordOrder->remove(it.iter->first);
		it.iter = recordMap->erase(it.iter);
	}
}
