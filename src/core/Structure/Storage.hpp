/**
 * @file Storage.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_STORAGE
#define OPENDB2_STORAGE

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 * @addtogroup Storage
 * @{
 */

#include "Column.hpp"
#include "Record.hpp"
#include "../Exceptions/StorageException.hpp"

#include <memory>
#include <unordered_map>
#include <list>
#include <string>

namespace openDB
{

class Table;

/**
 * @brief Defines interface implemented by record storage classes.
 *
 * @details The Storage class  provides the record storage features, which are:
 * - creation of new records belonging to a Storage object;
 * - insertion of a new records into a Storage object;
 * - erasing of a record belonging to a Storage object;
 *
 * In essence, you can see a Storage object (or an object of a Storage-derived class) as a local copy of
 * records to be inserted into a remote database, or loaded from database and to be updated or deleted.
 *
 * @section Erasing-vs-deletion Erasing vs deletion.
 * Inserting a record into a Storage object will, upon commit, insert the record into the appropriate table on
 * the database. Same thing for modified or deleted records. For erased objects, however, no operation on the
 * database will be executed at commit: an erased record is as if it never existed in the local Storage object.
 *
 * @section Record-id Record identification.
 * The class uses an identifier to uniquely identify and distinguish internally managed records objects. You
 * will never need to use these IDs to access records because the Storage class implements and provides a
 * much simpler and more disciplined mechanism for accessing records, which is the iterator mechanism.
 *
 * @section Iterators Iterators.
 * The class is not limited to defining the interface alone: it implements some useful methods and a mechanism
 * that governs access to records stored by a particular class object derived from it.
 * The mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head on a
 * tape: it can be used to read and/or write the record on which the iterator is placed.
 * An iterator is any object that, pointing to some element in a range of elements (such as an array or a
 * container), has the ability to iterate through the elements of that range using a set of operators
 * (with at least the increment (++) and dereference (*) operators).
 *
 *
 *
 * Iterators are available in a "normal" version, without restriction on access to the record
 * on which they are placed, and in a "const" version, which does not allow changes to the record.
 */
class Storage
{
public:

	/**
	 * @brief Iterator class
	 *
	 * @details The Storage class implements a simple but efficient mechanism that governs access to records
	 * stored by a particular Storage object. This mechanism is based on the concept of "iterator". You can
	 * imagine an iterator as a read/write head on a tape: it can be used to read and/or write the record on
	 * which the iterator is placed.
	 */
	class Iterator
	{
		friend class Storage;
	public:
		Iterator() noexcept;
		Iterator operator++ ( int ) noexcept;	//i++
		Iterator& operator++() noexcept;		//++i
		bool operator == ( const Iterator& __it ) const noexcept;
		bool operator != ( const Iterator& __it ) const noexcept;
		bool brother ( const Iterator& __it ) const noexcept;
		Record& operator* () throw ( StorageException& );
		Record* operator-> () throw ( StorageException& );
	private:
		Storage* parent;	/**< pointer to the Storage object to which the iterator is associated */
		std::unordered_map<std::string, Record>::iterator iter;
		Iterator& next () noexcept;
	};

	/**
	 * @brief ConstIterator class
	 *
	 * @details The Storage class implements a simple but efficient mechanism that governs access to records
	 * stored by a particular Storage object. This mechanism is based on the concept of "iterator". You can
	 * imagine an iterator as a read/write head on a tape: it can be used to read and/or write the record on
	 * which the iterator is placed.
	 * This class of iterators belongs to the class of bidirectional iterators and has read-only access
	 * restrictions to records.
	 */
	class ConstIterator
	{
	public:
		friend class Storage;
		ConstIterator() noexcept;
		ConstIterator operator++ ( int ) noexcept;	//i++
		ConstIterator& operator++() noexcept;		//++i
		bool operator == ( const ConstIterator& __it ) const noexcept;
		bool operator != ( const ConstIterator& __it ) const noexcept;
		bool brother ( const ConstIterator& __it ) const noexcept;
		const Record& operator* () const throw ( StorageException& );
		const Record* operator-> () const throw ( StorageException& );

	private:
		Storage* parent;	/**< pointer to the Storage object to which the iterator is associated */
		std::unordered_map<std::string, Record>::const_iterator iter;
		ConstIterator& next () noexcept;
	};

	/**
	 * @brief ConstIterator class
	 *
	 * @details The Storage class implements a simple but efficient mechanism that governs access to records
	 * stored by a particular Storage object. This mechanism is based on the concept of "iterator". You can
	 * imagine an iterator as a read/write head on a tape: it can be used to read and/or write the record on
	 * which the iterator is placed.
	 * This class of iterators belongs to the class of bidirectional iterators and has read-only access
	 * restrictions to records.
	 */
	class OrderedConstIterator
	{
	public:
		friend class Storage;
		OrderedConstIterator() noexcept;
		OrderedConstIterator operator++ ( int ) noexcept;	//i++
		OrderedConstIterator& operator++() noexcept;		//++i
		bool operator == ( const OrderedConstIterator& __it ) const noexcept;
		bool operator != ( const OrderedConstIterator& __it ) const noexcept;
		bool brother ( const OrderedConstIterator& __it ) const noexcept;
		const Record& operator* () const throw ( StorageException& );
		const Record* operator-> () const throw ( StorageException& );

	private:
		Storage* parent;	/**< pointer to the Storage object to which the iterator is associated */
		std::list<std::string>::const_iterator iter;
		OrderedConstIterator& next () noexcept;
	};

	friend class Iterator;
	friend class ConstIterator;
	friend class OrderedConstIterator;
	friend class Table;

	Storage() noexcept;
	Storage(const Storage& s) noexcept;
	const Storage& operator=(const Storage& s) noexcept;
	virtual ~Storage () {}
	virtual void clean () noexcept;

	/**
	 * @brief Returns the number of records managed by this object.
	 */
	virtual unsigned recordsNum() const noexcept {return recordMap->size();}

	/**
	 * @brief Returns a reference to the parent Table object.
	 *
	 * @return a reference to the parent Table object.
	 */
	const Table& getParent() const noexcept {return *parent;}

	Iterator begin() noexcept;
	Iterator end() noexcept;
	ConstIterator cbegin() const noexcept;
	ConstIterator cend() const noexcept;
	OrderedConstIterator obegin() const noexcept;
	OrderedConstIterator oend() const noexcept;
	Iterator at(const std::string& id) noexcept;
	ConstIterator cat(const std::string& id) const noexcept;

	virtual Record newRecord(enum Record::Status recState = Record::empty) const noexcept;
	virtual Iterator addRecord(const Record& record) noexcept;
	virtual void removeRecord(Iterator& it) noexcept;

protected:

	Table* parent; /**< Pointer to the Object table to which the Storage object handles the records */

	 /**
	  * Pointer to the "structural view" of a Table object
	  */
	std::shared_ptr<std::unordered_map<std::string, Column>> columnsMap;
	/**
	 * Each record is associated with an unique identifier obtained through the Record::getKey() function.
	 * The key-value match is implemented through an object std::unordered_map. The fields of the
	 * unordered_map object are of the long type for the key and Record for the value.
	 */
	std::shared_ptr<std::unordered_map<std::string, Record>> recordMap;

	/**
	 * To preserve the order in which Record objects are added to the Storage object, their identifiers are
	 * placed in the recordList list. This list is used by the Storage::OrderedConstIterator class of iterator
	 */
	std::shared_ptr<std::list<std::string>> recordOrder;
};

/**
 * @}
 * @}
 * @}
 */

};

#endif
