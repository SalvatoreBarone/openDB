/**
 * @file StorageIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Storage.hpp"
using namespace openDB;


/**
 * @brief Constructor
 *
 * @details You can build a Storage::Iterator object, but you can not associate it with a Storage
 * object, or with an object of a Storage derived class. This is to further regulate how to access
 * records managed from a Storage object. You can get a Storage::Iterator object associated with a
 * Storage object only by using the Storage::begin() or Storage::end() functions.
 * This class of iterators belongs to the class of bidirectional iterators class and has no access
 * restrictions to records.
 */
Storage::Iterator::Iterator() noexcept : parent ( 0 ) {}

/**
 * @brief Post-increase operator
 *
 * @details Moves the iterator one position forward, returning a new iterator to the previous
 * position.
 *
 * @return a new iterator placed on the previous position.
 */
Storage::Iterator Storage::Iterator::operator++(int) noexcept
{
	Storage::Iterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

/**
 * @brief Pre-increase operator
 *
 * @details Moves the iterator one position forward, returning a reference to the same iterator on
 * which the operator acted.
 *
 * @return a reference to the same iterator.
 */
Storage::Iterator& Storage::Iterator::operator++() noexcept
{
	next();
	return *this;
}

/**
 * @brief Checks if two iterators are placed on the same element.
 *
 * @param [in] __it a Storage::Iterator object;
 *
 * @retval true if two iterators are placed on the same element;
 * @retval false if two iterators aren't placed on the same element;
 */
bool Storage::Iterator::operator == (const Storage::Iterator& __it) const noexcept
{
	if (brother(__it) && iter == __it.iter)
		return true;
	return false;
}

/**
 * @brief Verify that two iterators are not placed on the same element.
 *
 * @param [in] __it a Storage::Iterator object;
 *
 * @retval true if two iterators aren't placed on the same element;
 * @retval false if two iterators are placed on the same element;
 */
bool Storage::Iterator::operator != (const Storage::Iterator& __it) const noexcept
{
	return !(operator==(__it));
}

/**
 * @brief Check if two iterators are associated with the same Storage object.
 *
 * @param [in] __it a Storage::Iterator object;
 *
 * @retval true if two iterators are associated with the same Storage object;
 * @retval false  if two iterators aren't associated with the same Storage object;
 */
bool Storage::Iterator::brother(const Storage::Iterator& __it) const noexcept
{
	if (parent->recordMap.get() == __it.parent->recordMap.get())
		return true;
	return false;
}

/**
 * @brief Dereference operator
 *
 * @return a reference to the Record object on which the iterator is placed. The reference remains
 * valid until the Storage object to which the iterator belongs is deallocated.
 *
 * @exception StorageException if the iterator is placed in an invalid position (eg end ()).
 */
Record& Storage::Iterator::operator* () throw ( StorageException& )
{
	if (!parent)
		throw StorageException (
				parent,
				__PRETTY_FUNCTION__,
				"Error using Storage::Iterator::operator*: no valid parent pointer;");

	if (iter == parent->recordMap->end())
		throw StorageException (
				parent,
				__PRETTY_FUNCTION__,
				"Error using Storage::Iterator::operator*: can't access to end();");

	return iter->second;
}


/**
 * @brief Arrow operator
 *
 * @return a pointer to the Record object on which the iterator is placed.
 *
 * @exception StorageException if the iterator is placed in an invalid position (eg end ()).
 */
Record* Storage::Iterator::operator-> () throw ( StorageException& )
{
	if (!parent)
		throw StorageException (
				const_cast<Storage*>(parent),
				__PRETTY_FUNCTION__,
				"Error using Storage::Iterator::operator*() const: no valid parent pointer;");

	if (iter == parent->recordMap->end())
		throw StorageException (
				const_cast<Storage*>(parent),
				__PRETTY_FUNCTION__,
				"Error using Storage::Iterator::operator*() const: can't access to end();");

	return &(iter->second);
}

/**
 * @brief Moves the iterator one position forward.
 *
 * @return a reference to the same object.
 */
Storage::Iterator& Storage::Iterator::next () noexcept
{
	if (parent && iter != parent->recordMap->end())
		iter++;
	return* this;
}

