/**
 * @file Table.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Table.hpp"
#include "Schema.hpp"
#include "common.hpp"
using namespace openDB;

#include <iostream>
#include <algorithm>

/**
 * @brief Constructor.
 *
 * @details Builds a new Table object, assigning it a name and optionally assigning it a schema object as
 * the parent object.
 *
 * @param [in] _name		name of the new Table object;
 * @param [in] parent	parent Schema.
 *
 * @exception AccessException if _name is an empty string or if contains non-alphanumerical character
 */
Table::Table (	const std::string& _name,
				Schema* parent ) throw ( AccessException& ) :
			name ( _name ),
			parent (parent),
			columnsMap (),
			columnsOrder(),
			storage()
{
	if (name.empty())
		throw AccessException(
				AccessException::TableName,
				__PRETTY_FUNCTION__,
				"Error creating a table: can't create a table without name.");

	std::string nameUpper = name;
	std::transform(nameUpper.begin(), nameUpper.end(), nameUpper.begin(), ::toupper);
	if (nameUpper.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_") != std::string::npos)
		throw AccessException(
			AccessException::ColumnName,
			__PRETTY_FUNCTION__,
			"The table name contains an illegal non-alphanumerical character");

	columnsMap = std::shared_ptr<std::unordered_map<std::string, Column>>(new std::unordered_map<std::string, Column>);
	columnsOrder = std::shared_ptr<std::list <std::string>>(new std::list <std::string>);
	storage = std::shared_ptr<Storage>(new Storage);
	storage->parent = this;
	storage->columnsMap = columnsMap;
}

/**
 * @brief Make a quick copy of the object
 *
 * @details The Table object is not copied entirely. A Table object can be seen as consisting of a
 * header and a payload. The header consists of information such as the name of the object, its structural
 * view, while the payload is the set of records contained in the object. Only the header is copied by
 * this copy constructor, the payload is "shared" between the different copied objects without being
 * copied.
 * The main advantages offered by this type of solution are many. First of all, you can pass a Table type
 * object by copy very quickly from having to copy entirely it every time, obviously paying attention to
 * the use it makes, as any changes to the "copy" will affect the original object. Second, in this way,
 * two Table objects can share the same Storage and the same records, creating a dual access mechanism to
 * the same information content.
 *
 * @param [in] table object to be copied.
 */
Table::Table(const Table& _table) noexcept :
	name(_table.name),
	parent(_table.parent),
	columnsMap(_table.columnsMap),
	columnsOrder(_table.columnsOrder),
	storage(_table.storage)
{}

/**
 * @brief Make a quick copy of the object
 *
 * @details The Table object is not copied entirely. A Table object can be seen as consisting of a
 * header and a payload. The header consists of information such as the name of the object, its structural
 * view, while the payload is the set of records contained in the object. Only the header is copied by
 * this operator, the payload is "shared" between the different copied objects without being
 * copied.
 * The main advantages offered by this type of solution are many. First of all, you can pass a Table type
 * object by copy very quickly from having to copy entirely it every time, obviously paying attention to
 * the use it makes, as any changes to the "copy" will affect the original object. Second, in this way,
 * two Table objects can share the same Storage and the same records, creating a dual access mechanism to
 * the same information content.
 *
 * @param [in] table object to be copied.
 */
const Table& Table::operator= (const Table& _table) noexcept
{
	if (&_table != this)
	{
		name = _table.name;
		parent = _table.parent;
		columnsOrder = _table.columnsOrder;
		columnsMap = _table.columnsMap;
		storage = _table.storage;
	}
	return *this;
}

/**
 * @brief Allows you to add a column to the table.
 *
 * @code
 * table.addColumn ("Boolean", openDB::Boolean());
 * table.addColumn ("Date", openDB::Date());
 * table.addColumn ("Time", openDB::Time());
 * table.addColumn ("Small", openDB::Smallint());
 * table.addColumn ("Int", openDB::Integer());
 * table.addColumn ("Bigint", openDB::Bigint());
 * table.addColumn ("Real", openDB::Real());
 * table.addColumn ("Double", openDB::DoublePrecision());
 * table.addColumn ("Numeric", openDB::Numeric(10, 5));
 * table.addColumn ("Varchar", openDB::Varchar(100));
 * table.addColumn ("Character", openDB::Character(100));
 * @endcode
 *
 * @param [in] name	name of the column; it identifies the column within the table to which it belongs;
 * @param [in] type	sql type of the column;
 * @param [in] key	if is set to "true" it means that this column compose the key for the table to which
 * 					it belongs.
 *
 * @exception AccessException if columnName is an empty string.
 *
 * @return an std::pair<Table::Iterator, bool> object, the first field is an Iterator object pointing to the
 * Column object whose name is "name", the second field is a boolean information whose value is "true" if the
 * Column object was actually inserted, or "false" if no insertion occurred.
 **/
std::pair<Table::Iterator, bool> Table::addColumn(const std::string& name, const SqlType& type, bool key) throw (AccessException&) {
	if (name.empty())
		throw AccessException(
			AccessException::ColumnName,
			__PRETTY_FUNCTION__,
			"Error: can't create a column without name.");

	Table::Iterator c = at (name);
	if (c == end()) {
		columnsMap->insert(std::pair<std::string, Column>(name, Column(name, type, key)));
		columnsOrder->push_back(name);
		return std::pair<Iterator, bool>(at(name), true);
	} else
		return std::pair<Iterator, bool>(c, false);
}

/**
 * @brief Returns a Table::Iterator object placed on the first column.
 *
 * @note There is no special order for Column objects, except the one in which they are added to the table
 * or loaded from databases.
 *
 * @return a Table::Iterator placed on the first Column object;
 */
Table::Iterator Table::begin() noexcept {
	Table::Iterator it;
	it.parent = this;
	it.iter = this -> columnsOrder->begin();
	return it;
}

/**
 * @brief Returns a Table::Iterator pointing to the "past-the-end" column.
 *
 * @note the "past-the-end" column does not really exist, so the iterator returned by this function
 * points to the first invalid position after all valid positions.
 *
 * @return a Table::Iterator pointing to the "past-the-end" column.
 */
Table::Iterator Table::end() noexcept {
	Table::Iterator it;
	it.parent = this;
	it.iter = this -> columnsOrder->end();
	return it;
}

/**
 * @brief Returns an iterator pointing to a specific Column object
 *
 * @param [in] ColumnName name of the Column object.
 *
 * @return a Table::Iterator object,  pointing to the Column object whose name is ColumnName, or end() if
 * there is no Column object with that name.
 */
Table::Iterator Table::at ( const std::string& columnName ) noexcept {
	Table::Iterator it = begin();
	Table::Iterator end_it = end();
	while (it != end_it && it->getName() != columnName)
		it++;
	return it;
}

/**
 * @brief Returns a Table::ConstIterator object placed on the first column.
 *
 * @note There is no special order for Column objects, except the one in which they are added to the table
 * or loaded from databases.
 *
 * @return a Table::ConstIterator placed on the first Column object;
 */
Table::ConstIterator Table::cbegin() const noexcept {
	Table::ConstIterator it;
	it.parent = (Table*) this;
	it.iter =  columnsOrder->cbegin();
	return it;
}

/**
 * @brief Returns a Table::Iterator pointing to the "past-the-end" column.
 *
 * @note the "past-the-end" column does not really exist, so the iterator returned by this function
 * points to the first invalid position after all valid positions.
 *
 * @return a Table::Iterator pointing to the "past-the-end" record.
 */
Table::ConstIterator Table::cend() const noexcept {
	Table::ConstIterator it;
	it.parent = (Table*) this;
	it.iter =  columnsOrder->cend();
	return it;
}

/**
 * @brief Returns an iterator pointing to a specific Column object
 *
 * @param [in] ColumnName name of the Column object.
 *
 * @return a Table::ConstIterator object, pointing to the Column object whose name is ColumnName, or end()
 * if there is no Column object with that name.
 */
Table::ConstIterator Table::cat(const std::string& columnName) const noexcept {
	Table::ConstIterator it = cbegin();
	Table::ConstIterator end_it = cend();
	while (it != end_it && it->getName() != columnName)
		it++;
	return it;
}

/**
 * @brief Generates an SQL statement to load records from DBMS.
 *
 * @details It takes into account the QuerySetting parameters set for each of the Column objects
 * belonging to the Table object.
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement
 *
 * @return a std:string, object containint the sql statement.
 **/
std::string Table::loadCommand(bool addSchemaName) const noexcept
{
	std::string sql_column_name;
	std::string sql_where;
	std::string sql_order;

	std::list<std::string>::const_iterator list_it = columnsOrder->begin();
	std::list<std::string>::const_iterator list_end = columnsOrder->end();
	for (; list_it != list_end; list_it++)
	{
		QuerySetting _attribute = columnsMap->at(*list_it).getQuerySetting();
		if(!sql_column_name.empty())
			sql_column_name += ", ";
		sql_column_name += *list_it;
		if (_attribute.getSelect())
		{
			if (!sql_where.empty())
				sql_where+= " and ";
			sql_where += columnsMap->at(*list_it).getSelectCondition();
		}
		if (_attribute.getOrderBy())
		{
			if (!sql_order.empty())
				sql_order+= ", ";
			sql_order += *list_it + " " + _attribute.getOrderMode ();
		}
	}
	std::string command = "select " + sql_column_name + " from " + ( parent != 0 && addSchemaName && parent->getName() != "public" ? (parent->getName() + ".") : "")  +
	name + ((!sql_where.empty()) ? (" where " + sql_where) : "" ) + ((!sql_order.empty()) ? (" order by " + sql_order) : "" );

	return command;
}

/**
 * @brief Allows you to load records from another Table object.
 *
 * @warning If the Table already contains some other records, they will be erased!
 *
 * @param [in] table object from which to load records.
 *
 * @exception AccessException when errors occur in accessing Column object;
 * @exception EmptyKey when a key field is assuming a null value;
 * @exception InvalidArgument when the string cannot be translated to an sql type;
 * @exception AmbiguousDate when the string can be translated to a Date, but the result is ambiguous;
 * @exception InvalidDate when the string can be translated to a Date, but the result is meaningless;
 * @exception InvalidTime when the string can be translated to a Time, but the result is meaningless;
 * @exception OutOfBound when the constraints on the bound for numerical value arent' met;
 * @exception ValueTooLong when the constraint on the string length is not met;
 **/
void Table::loadRecord ( const Table& records ) throw ( BasicException& )
{
	// first check if load is possible
	Storage::OrderedConstIterator it = records.storage->obegin();
	Storage::OrderedConstIterator end = records.storage->oend();
	for (; it != end; it++)
	{
		Record rec = storage->newRecord();
		std::unordered_map<std::string, std::string> values;
		it->getCurrentValues(values);
		rec.edit(values);
	}

	// if load isn't possible the Record::edit() function thrown an exception...

	storage->clean();
	it = records.storage->obegin();
	for (; it != end; it++)
	{
		Record rec = storage->newRecord();
		std::unordered_map<std::string, std::string> values;
		it->getCurrentValues(values);
		rec.edit(values);
		rec.setStatus(Record::loaded);
		std::string key = rec.getKey();
		storage->recordMap->insert(std::pair<std::string, Record>(key, rec));
		storage->recordOrder->push_back(key);
	}
}

/**
 * @brief Generates a list of sql commands to make local changes to dbms.
 *
 * @param [out] commandList 	a std::list<std::string> object, it will contain the sql commands;
 *
 * @param [in] appendCommand	if true, commandList will not be emptied before adding another sql
 * 								statement;
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement;
 **/
void Table::commit (	std::list<std::string>& commandList,
						bool append,
						bool addSchemaName ) const noexcept
{
	if (!append)
		commandList.clear();

	Storage::ConstIterator it = storage->cbegin();
	Storage::ConstIterator end = storage->cend();
	for (; it != end; it++)
	{
		switch(it->getStatus()) {
			case Record::inserting:
				commandList.push_back(sqlInsert(it, addSchemaName));
				break;

			case Record::updating:
				commandList.push_back(sqlUpdate(it, addSchemaName));
				break;

			case Record::deleting:
				commandList.push_back(sqlDelete(it, addSchemaName));
				break;

			default: break;
		}
	}
}

/**
 * @brief Generates a sql command to insert a record into the database.
 *
 * @param [in] it				iterator placed on the record to use to generate the command;
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement
 *
 * @return a std:string, object containing the sql statement.
 **/
std::string Table::sqlInsert(const Storage::ConstIterator& it, bool addSchemaName) const noexcept
{
	std::string command;
	if (it != storage->cend())
	{
		std::string sql_column;
		std::string sql_values;

		std::unordered_map<std::string, std::string> value_map;
		(*it).getCurrentValues(value_map);

		std::list<std::string>::const_iterator col_it = columnsOrder->begin();
		std::list<std::string>::const_iterator col_end = columnsOrder->end();
		for (; col_it != col_end; col_it++)
			if (!SqlType::isNull(value_map.at(*col_it)))
			{
				if (!sql_column.empty())
					sql_column+= ", ";

				sql_column += *col_it;

				if (!sql_values.empty())
					sql_values += ", ";

				sql_values += columnsMap->at(*col_it).prepare( value_map.at(*col_it));
			}

		command = "insert into " + ( parent != 0 && addSchemaName && parent->getName() != "public" ? (parent->getName() + ".") : "")  + name + " (" + sql_column + ") values (" + sql_values + ")";
	}
	return command;
}

/**
 * @brief Generates a sql command to update a record into the database.
 *
 * @param [in] it				iterator placed on the record to use to generate the command;
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement
 *
 * @return a std:string, object containing the sql statement.
 **/
std::string Table::sqlUpdate(const Storage::ConstIterator& it, bool addSchemaName) const noexcept
{
	std::string command;
	if (it != storage->cend())
	{
		std::string sql_value;
		std::string sql_where;
		std::unordered_map<std::string, std::string> value_map, old_map;
		std::unordered_map<std::string, bool> varied_map;
		(*it).getCurrentValues(value_map);
		(*it).getOldValues(old_map);
		(*it).getChanged(varied_map);

		std::unordered_map<std::string, std::string>::const_iterator value_it = value_map.begin();
		std::unordered_map<std::string, std::string>::const_iterator value_end = value_map.end();
		for (; value_it != value_end; value_it++)
		{
			if (columnsMap->at(value_it->first).getIsKey())
			{
				if (!sql_where.empty())
					sql_where += " and ";
				sql_where += value_it -> first + "=" + columnsMap->at(value_it->first).prepare(value_it -> second);
			}
			else
			{
				//controllo che i valori del record non coincidano prima generare il comando per il loro aggiornamento
				if (varied_map.at(value_it->first) == true)
				{
					if (!sql_value.empty())
						sql_value += ", ";
					sql_value += value_it -> first + "=" + columnsMap->at(value_it->first).prepare(value_it -> second);
				}
			}
		}

		if (sql_where.empty())
		{
			std::unordered_map<std::string, std::string>::const_iterator old_it = old_map.begin();
			std::unordered_map<std::string, std::string>::const_iterator old_end = old_map.end();
			for (; old_it != old_end; old_it++)
			{
				if (!sql_where.empty())
					sql_where += " and ";
				sql_where += old_it -> first + "=" + columnsMap->at(old_it->first).prepare(old_it -> second);
			}
		}

		if (sql_value.empty())
			command = "";
		else
			command = "update "  + ( parent != 0 && addSchemaName && parent->getName() != "public"  ? (parent->getName() + ".") : "")  + name + " set " + sql_value + " where " + sql_where;
	}
	return command;
}

/**
 * @brief Generates a sql command to delete a record into the database.
 *
 * @param [in] it				iterator placed on the record to use to generate the command;
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement;
 *
 * @return a std:string, object containing the sql statement.
 **/
std::string Table::sqlDelete(const Storage::ConstIterator& it, bool addSchemaName) const noexcept
{
	std::string command;
	if (it != storage->cend())
	{

		std::string sql_value;
		std::string sql_where;
		std::unordered_map<std::string, std::string> value_map;
		(*it).getCurrentValues(value_map);

		std::unordered_map<std::string, std::string>::const_iterator value_it = value_map.begin();
		std::unordered_map<std::string, std::string>::const_iterator value_end = value_map.end();
		for (; value_it != value_end; value_it++)
			if (columnsMap->at(value_it->first).getIsKey())
			{
				if (!sql_where.empty())
					sql_where += " and ";
				sql_where += value_it -> first + "=" + columnsMap->at(value_it->first).prepare(value_it -> second);
			}


		if (sql_where.empty())
		{
			value_it = value_map.begin();
			value_end = value_map.end();
			for (; value_it != value_end; value_it++)
			{
				if (!sql_where.empty())
					sql_where += " and ";
				sql_where += value_it -> first + "=" + columnsMap->at(value_it->first).prepare(value_it -> second);
			}
		}

		command = "delete from "  + ( parent != 0 && addSchemaName && parent->getName() != "public"  ? (parent->getName() + ".") : "")  +name +  " where " + sql_where;
	}
	return command;
}

/**
 * @brief Generates a sql query.
 *
 * @param [in] addSchemaName	if true, the function adds the name of the schema which the table belongs
 * 								to the sql statement
 *
 * @return a std:string, object containing the sql statement.
 **/
std::string Table::sqlSelect(bool addSchemaName) const noexcept
{
	std::string command;
	std::string sql_project;
	std::string sql_where;
	std::string sql_order;

	std::list<std::string>::const_iterator it = columnsOrder->begin();
	std::list<std::string>::const_iterator end = columnsOrder->end();
	for (; it != end; it++)
	{
		QuerySetting _attribute = columnsMap->at(*it).getQuerySetting();
		if (_attribute.getProiect())
		{
			if(!sql_project.empty())
				sql_project += ", ";
			sql_project += *it;
		}
		if (_attribute.getSelect())
		{
			if (!sql_where.empty())
				sql_where+= " and ";
			sql_where += columnsMap->at(*it).getSelectCondition();
		}
		if (_attribute.getOrderBy())
		{
			if (!sql_order.empty())
				sql_order+= ", ";
			sql_order += *it + " " + _attribute.getOrderMode();
		}
	}

	command = "select " + sql_project + " from " + ( parent != 0 && addSchemaName && parent->getName() != "public"  ? (parent->getName() + ".") : "")  + name + ((!sql_where.empty()) ? (" where " + sql_where) : "" ) + ((!sql_order.empty()) ? (" order by " + sql_order) : "" );
	return command;
}
