/**
 * @file Table.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_TABLE
#define OPENDB2_TABLE

/**
 * @addtogroup Core
 * @{
 * @addtogroup Structure
 * @{
 */


#include "Storage.hpp"
#include <memory>

namespace openDB
{
class Schema;
/**
 * @brief Abstraction of a Table in a database
 *
 * @details A table type object is an abstraction whose goal is to emulate a database table. It consists of a
 * set of Column objects, which retain the common properties of the semantic homologous elements inside the
 * table, and contains a reference to a Storage object, or to a derived class, which has the task of managing
 * the records belonging to the table. These records are Record objects, which are therefore the tuples
 * contained in the table.
 *
 * @section columns Column management
 * Column objects are identified from their own name (columns with the same name are not allowed in a table),
 * but the structure used in Table objects to hold Column object, a std::unordered_map, would alter the order
 * in which Column objects have been added to the Table object. For this reason, there is a std::list
 * object in which the Column objects names are progressively inserted in order.
 *
 * @section iteratorsec Accessing columns
 * This class implements some useful methods and a mechanism that governs access to Column belonging to a
 * Table object. The mechanism is based on the concept of "iterator".
 * An iterator is any object that, pointing to some element in a range of elements (such as an array or a
 * container), has the ability to iterate through the elements of that range using a set of operators
 * (with at least the increment (++) and dereference (*) operators).
 * Iterators are classified into five categories depending on the functionality they implement:
 * - Input and output iterators are the most limited types of iterators: they can perform sequential
 *   single-pass input or output operations; they do not find application in this library;
 * - Forward iterators have all the functionality of input iterators and - if they aren't constant iterators -
 *   also the functionality of output iterators, although they are limited to one direction in which to
 *   iterate through a range (forward);
 * - Bidirectional iterators are like forward iterators but can also be iterated through backwards;
 * - Random-access iterators implement all the functionality of bidirectional iterators, and also have the
 *   ability to access ranges non-sequentially: distant elements can be accessed directly by applying an
 *   offset value to an iterator without iterating through all the elements in between. These iterators have
 *   a similar functionality to standard pointers (pointers are iterators of this category).
 *
 * Iterators are available in a "normal" version, without restriction on access to the object on which they
 * are placed, and in a "const" version, which does not allow changes to the object, implemented,
 * respectively, by the Table::Iterator and Table::ConstIterator class.
 *
 * @section records Accessing records
 * Access to records belonging to a Table object can be made by means of the Storage object that manages them
 * materially.
 *
 **/
class Table
{

public:

	/**
	 * @brief Iterator class
	 *
	 * @details The Table class implements a simple but efficient mechanism that governs access to columns.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the element on which the iterator is placed.
	 */
	class Iterator
	{
		friend class Table;
	public:
		Iterator() noexcept;
		Iterator operator++(int) noexcept;		//i++
		Iterator& operator++() noexcept;		//++i
		Iterator operator--(int) noexcept;		//i++
		Iterator& operator--() noexcept;		//++i
		bool operator == (const Iterator& __it) const noexcept;
		bool operator != (const Iterator& __it) const noexcept;
		bool brother (const Iterator& __it) const noexcept;
		Column& operator* () const throw ( AccessException& );
		Column* operator-> () const throw ( AccessException& );
	private:
		Table* parent;	/**< pointer to the Table object to which the iterator is associated */
		std::list<std::string>::iterator iter;
		Iterator& next () noexcept;
		Iterator& prev () noexcept;
	};

	/**
	 * @brief ConstIterator class
	 *
	 * @details The Table class implements a simple but efficient mechanism that governs access to columns.
	 * This mechanism is based on the concept of "iterator". You can imagine an iterator as a read/write head
	 * on a tape: it can be used to read and/or write the record on which the iterator is placed.
	 * This class of iterators belongs to the class of random access iterators and has read-only access
	 * restrictions to records.
	 */
	class ConstIterator
	{
		friend class Table;
	public:
		ConstIterator() noexcept;
		ConstIterator operator++(int) noexcept;		//i++
		ConstIterator& operator++() noexcept;		//++i
		ConstIterator operator--(int) noexcept;		//i++
		ConstIterator& operator--() noexcept;		//++i
		bool operator == (const ConstIterator& __it) const noexcept;
		bool operator != (const ConstIterator& __it) const noexcept;
		bool brother (const ConstIterator& __it) const noexcept;
		const Column& operator* () const throw ( AccessException& );
		const Column* operator-> () const throw ( AccessException& );
	private:
		Table* parent;	/**< pointer to the Table object to which the iterator is associated */
		std::list<std::string>::const_iterator iter;
		ConstIterator& next () noexcept;
		ConstIterator& prev () noexcept;
	};

	friend class Iterator;
	friend class ConstIterator;

	explicit Table (	const std::string& name,
						Schema* parent = 0 ) throw ( AccessException& );
	Table( const Table& table ) noexcept;
	const Table& operator= ( const Table& table ) noexcept;
	virtual ~Table() {};

	/**
	 * @brief Returns the table name;
	 *
	 * @return a std::string object, containing the table name;
	 */
	std::string getName() const noexcept {return name;}

	/**
	 * @brief Returns the number of columns belonging to the table;
	 *
	 * @return an unsigned number representing the number of column belonging to the table.
	 */
	unsigned columnsNum() const noexcept {return columnsOrder->size();}

	/**
	 * @brief Returns a reference to the Storage object that manages the record belonging to the table;
	 *
	 * @return a reference to the Storage object that manages the record belonging to the table.
	 */
	Storage& getStorage() noexcept {return *storage;}

	/**
	 * @brief Returns a reference to the Storage object that manages the record belonging to the table;
	 *
	 * @return a reference to the Storage object that manages the record belonging to the table.
	 */
	const Storage& getStorage() const noexcept {return (const Storage&) *storage;}

	std::pair<Iterator, bool> addColumn(	const std::string& name,
											const SqlType& type,
											bool key = false) throw (AccessException&);

	Iterator begin() noexcept;
	Iterator end() noexcept;
	Iterator at(const std::string& ColumnName) noexcept;
	ConstIterator cbegin() const noexcept;
	ConstIterator cend() const noexcept;
	ConstIterator cat(const std::string& ColumnName) const noexcept;
	std::string loadCommand ( bool addSchemaName = false ) const noexcept;
	void loadRecord( const Table& table ) throw ( BasicException& );

	void commit (	std::list<std::string>& commandList,
					bool appendCommand = true,
					bool addSchemaName = true ) const noexcept;

protected:
/**
 * @brief Table name.
 *
 * @details Tables in the same schema are identified through their name.
 */
	std::string name;

/**
 * @brief Pointer to the Schema object which the Table object belongs.
 */
	Schema* parent;

/**
 * @brief Structural view of a Table.
 *
 * @details The set of column objects that make up a table are contained in an unordered_map object, which
 * allows access to internal Column objects using their name.
 */
	std::shared_ptr<std::unordered_map<std::string, Column>> columnsMap;

/**
 * @brief List of column names
 *
 * @details
 * Column objects are identified from their own name (columns with the same name are not allowed in a table),
 * but the structure used in Table objects to hold Column object, a std::unordered_map, would alter the order
 * in which Column objects have been added to the Table object. For this reason, there is a std::list
 * object in which the Column objects names are progressively inserted in order.
 * The pointer to the list is stored in a shared_pointer object so that objects can be quickly copied easily.
 */
	std::shared_ptr<std::list <std::string>> columnsOrder;

/**
 * @brief Storage object that manages the records belonging to a Table object.
 *
 * @details The pointer to the object is stored in a shared_pointer object so that Table objects can be
 * quickly copied easily.
 */
	std::shared_ptr<Storage> storage;

	std::string sqlInsert(const Storage::ConstIterator& it, bool addSchemaName = true) const noexcept;
	std::string sqlUpdate(const Storage::ConstIterator& it, bool addSchemaName = true) const noexcept;
	std::string sqlDelete(const Storage::ConstIterator& it, bool addSchemaName = true) const noexcept;
	std::string sqlSelect(bool addSchemaName = true) const noexcept;

};

/**
 * @}
 * @}
 */


};

#endif
