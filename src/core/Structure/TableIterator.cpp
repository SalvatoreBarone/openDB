/**
 * @file TableIterator.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Table.hpp"
using namespace openDB;

/**
 * @brief Constructor
 *
 * @details You can build a Table::Iterator object, but you can not associate it with a Table
 * object. This is to further regulate how to access columns belonging to a table. You can get a
 * Table::Iterator object associated with a Table object only by using the Table::begin(),
 * Table::at() or Table::end() functions.
 */
Table::Iterator::Iterator() noexcept : parent(0) {}

/**
 * @brief Post-increase operator
 *
 * @details Moves the iterator one position forward, returning a new iterator to the previous
 * position.
 *
 * @return a new iterator placed on the previous position.
 */
Table::Iterator Table::Iterator::operator++(int) noexcept
{
	Table::Iterator it;
	it.parent = parent;
	it.iter = iter;
	next();
	return it;
}

/**
 * @brief Pre-increase operator
 *
 * @details Moves the iterator one position forward, returning a reference to the same iterator on
 * which the operator acted.
 *
 * @return a reference to the same iterator.
 */
Table::Iterator& Table::Iterator::operator++() noexcept
{
	next();
	return *this;
}

/**
 * @brief Post-decrease operator
 *
 * @details Moves the iterator one position backward, returning a new iterator to the previous
 * position.
 *
 * @return a new iterator placed on the previous position.
 */
Table::Iterator Table::Iterator::operator-- ( int ) noexcept
{
	Table::Iterator it;
	it.parent = parent;
	it.iter = iter;
	prev();
	return it;
}

/**
 * @brief Pre-decrease operator
 *
 * @details Moves the iterator one position backward, returning a reference to the same iterator on
 * which the operator acted.
 *
 * @return a reference to the same iterator.
 */
Table::Iterator& Table::Iterator::operator-- () noexcept
{
	prev();
	return *this;
}

/**
 * @brief Checks if two iterators are placed on the same element.
 *
 * @param [in] __it a Table::Iterator object;
 *
 * @retval true if two iterators are placed on the same element;
 * @retval false if two iterators aren't placed on the same element;
 */
bool Table::Iterator::operator == (const Table::Iterator& __it) const noexcept
{
	if (brother(__it) && iter == __it.iter)
		return true;
	return false;
}

/**
 * @brief Verify that two iterators are not placed on the same element.
 *
 * @param [in] __it a Table::Iterator object;
 *
 * @retval true if two iterators aren't placed on the same element;
 * @retval false if two iterators are placed on the same element;
 */
bool Table::Iterator::operator != (const Table::Iterator& __it) const noexcept
{
	return !operator==(__it);
}

/**
 * @brief Check if two iterators are associated with the same Table object.
 *
 * @param [in] __it a Table::Iterator object;
 *
 * @retval true if two iterators are associated with the same Table object;
 * @retval false  if two iterators aren't associated with the same Table object;
 */
bool Table::Iterator::brother(const Table::Iterator& __it) const noexcept
{
	if (parent->columnsMap.get() == __it.parent->columnsMap.get())
		return true;
	return false;
}

/**
 * @brief Dereference operator
 *
 * @return a reference to the Column object on which the iterator is placed. The reference remains
 * valid until the Table object to which the iterator belongs is deallocated.
 *
 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
 */
Column& Table::Iterator::operator*() const throw (AccessException&)
{
	if (!parent)
		throw AccessException (AccessException::IteratorNoParent, __PRETTY_FUNCTION__, "Error using Table::Iterator::operator*: no valid parent pointer;");
	if (iter == parent -> columnsOrder->end())
		throw AccessException (AccessException::IteratorAccessEnd, __PRETTY_FUNCTION__, "Error using Table::Iterator::operator*: can't access to end();");
	return parent->columnsMap->at(*iter);
}

/**
 * @brief Arrow operator
 *
 * @return a pointer to the Column object on which the iterator is placed. The pointer remains
 * valid until the Table object to which the iterator belongs is deallocated.
 *
 * @exception AccessException if the iterator is placed in an invalid position (eg end()).
 */
Column* Table::Iterator::operator->() const throw (AccessException&)
{
	if (!parent)
		throw AccessException (AccessException::IteratorNoParent, __PRETTY_FUNCTION__, "Error using Table::Iterator::operator->: no valid parent pointer;");
	if (iter == parent -> columnsOrder->end())
		throw AccessException (AccessException::IteratorAccessEnd, __PRETTY_FUNCTION__, "Error using Table::Iterator::operator->: can't access to end();");
	return &(parent->columnsMap->at(*iter));
}

/**
 * @brief Moves the iterator one position forward.
 *
 * @return a reference to the same object.
 */
Table::Iterator& Table::Iterator::next () noexcept
{
	if (parent && iter != parent->columnsOrder->end())
		iter++;
	return* this;
}

/**
 * @brief Moves the iterator one position backward.
 *
 * @return a reference to the same object.
 */
Table::Iterator& Table::Iterator::prev() noexcept
{
	if (parent && iter != parent->columnsOrder->begin())
		iter--;
	return* this;
}
