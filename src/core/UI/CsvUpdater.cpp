/**
 * @file CsvUpdater.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "CsvUpdater.hpp"
#include "SqlTypes.hpp"
#include "common.hpp"
#include "sha.hpp"

#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <iostream>

using namespace openDB;

const std::unordered_map<std::string, CsvUpdater::ParsedString::Operation>
								CsvUpdater::ParsedString::opcodeParsingMap = {
	{"", ParsedString::Operation::no_op},
	{"nop", ParsedString::Operation::no_op},
	{"no_operation", ParsedString::Operation::no_op},
	{"no operation", ParsedString::Operation::no_op},
	{"load", ParsedString::Operation::load_op},
	{"Load", ParsedString::Operation::load_op},
	{"insert", ParsedString::Operation::insert_op},
	{"Insert", ParsedString::Operation::insert_op},
	{"update", ParsedString::Operation::update_op},
	{"Update", ParsedString::Operation::update_op},
	{"partial", ParsedString::Operation::partialupdate_op},
	{"Partial", ParsedString::Operation::partialupdate_op},
	{"delete", ParsedString::Operation::delete_op},
	{"Delete", ParsedString::Operation::delete_op},
	{"erase", ParsedString::Operation::erase_op},
	{"Erase", ParsedString::Operation::erase_op}
};

const std::string CsvUpdater::defaultOperation = "update";

/**
 * @brief Returns the sequence of characters for opening a new line.
 *
 * @return Returns the character used for quoting, if enabled, an empty string otherwise.
 */
std::string CsvUpdater::Setting::startNewLine() const noexcept {
	if (quote)
		return std::string(1, quoteChar);
	return "";
}

/**
 * @brief Returns the sequence of characters for closing a line.
 *
 * @return Returns the character used for quoting, if enabled, an empty string otherwise.
 */
std::string CsvUpdater::Setting::endLine() const noexcept{
	if (quote)
		return std::string(1, quoteChar);
	return "";
}

/**
 * @brief Returns the sequence of characters for opening a new field.
 *
 * @return Returns a sequence of characters composed of two characters used for quoting, if enabled,
 * interspersed with the character used to separate the fields.
 */
std::string CsvUpdater::Setting::startNewField() const noexcept{
	if (quote)
		return std::string({quoteChar, fieldSeparator, quoteChar});
	return std::string(1, fieldSeparator);
}

/**
 * @brief Constructs a new CsvUpdater object, associating it with a Table object.
 *
 * @details Constructs a new CsvUpdater object, associating it with a Table object, so that it can act on the
 * Record objects added to the Storage object belonging to the specified Table object.
 *
 * @param [in] parent reference to a Table object.
 */
CsvUpdater::CsvUpdater(Table& parent) noexcept :
		parentTable(&parent) {
	parentTable = &parent;
	createTableIdentifier();
}

/**
 * @brief Exports the contents of the table object to which the CsvUpdater object is associated on a csv file.
 *
 * @param [in] filename name of the file.
 */
void CsvUpdater::simpleCsvExport(const std::string& filename) const throw (CsvUpdaterException&) {
	std::fstream stream(filename.c_str(), std::ios::out);
	if (!stream.is_open())
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Can't create " + filename);

	printColumn(stream, true);
	printRecord(stream, true);
	stream.close();
}

/**
 * @brief Create a csv template through which you can add, edit or delete records.
 *
 * @details
 * The created file is composed of:
 * - the first line is a control string which must not be changed; if you create multiple models using
 *   different CsvUpdater objects associated with the same Table object, they will contain the same first
 *   line, so you can consider this first line as a kind of identifier of the Table object on which you are
 *   acting;
 * - the second line is a further control string, but it will always be different, so it can be considered as
 *   a sort of identification for the modification session;
 * - the third row will contain the names of the Column objects that define the structure of the Table object
 *   to which the updater is associated.
 *
 * The remaining rows will contain the current value of the Record objects managed by the Storage object
 * associated with the Table object on which the present CsvUpdater object acts;
 *
 * Actually the first two columns of the rows from the third onwards are, in order:
 * - unique record identifier, this is automatically generated by the library for all the records that have
 *   been added to the Storage object belonging to a Table object; it must not be changed, pain not being able
 *   to identify the record on which to carry out modification operations;
 * - the operation to be performed on the record, a text string that can be:
 * 		- an empty string or "nop" for no operation;
 * 		- "update" to use the data present on the same line to modify the record whose identifier corresponds
 * 		  to the value of the first column; obviously the changes will become persistent on the remote
 * 		  database only at the time of committing;
 * 		- "delete" to mark the record whose identifier matches the value of the first column to be removed
 * 		  from the remote database at commit;
 * 		- "erase" to delete the record whose identifier corresponds to the value of the first column;
 * 		- "load" to use data on the same line to create a new record and mark it as if it had been loaded from
 * 		  the remote database;
 * 		- "inserting" to use data on the same line to create a new record and mark it to be removed from the
 * 		  remote database at commit;
 * 		.
 * 	In these last two cases, the "id" field (the first column) must necessarily be empty, so that the library
 * 	generates the unique identifier for the new record.
 *
 * @param [in] filename name of the file.
 */
void CsvUpdater::createUpdateModel(const std::string& filename) throw ( CsvUpdaterException& ) {
	std::fstream stream(filename.c_str(), std::ios::out);
	if (!stream.is_open())
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Can't create " + filename);

	controlString = generateRandomString(15);

	stream 	<< tableIdentifier << std::endl
			<< controlString << std::endl;
	printColumn(stream, false);
	printRecord(stream, false);
	stream.close();
}

/**
 * @brief Parse a csv template previously created with the CsvUpdater::createUpdateModel() function.
 *
 * @details
 * The model file is composed of:
 * - the first line is a control string which must not be changed; if you create multiple models using
 *   different CsvUpdater objects associated with the same Table object, they will contain the same first
 *   line, so you can consider this first line as a kind of identifier of the Table object on which you are
 *   acting;
 * - the second line is a further control string, but it will always be different, so it can be considered as
 *   a sort of identification for the modification session;
 * - the third row will contain the names of the Column objects that define the structure of the Table object
 *   to which the updater is associated.
 *
 * The remaining rows will contain the current value of the Record objects managed by the Storage object
 * associated with the Table object on which the present CsvUpdater object acts;
 *
 * The function throws an exception in the event that a serious error occurs that prevents file parsing from
 * completing. If, on the other hand, an exception occurs during the parsing of the rows associated with the
 * records, some information relating to the error is written to the log file. This is to allow you to reuse a
 * model after correcting errors. In the event that an error occurs during the parsing of the rows associated
 * with the records, no operation will be performed on them. The records will be modified according to the
 * model that was parsing if and only if no error occurred.
 *
 * @param [in] filename name of the file to be parsed.
 * @param [in] logfile name of the file used to log the operation.
 *
 * @exception CsvUpdaterException, if "filename" file can't be open;
 * @exception CsvUpdaterException, if "logfile" file can't be open;
 * @exception CsvUpdaterException, if the check on the first of the control strings fails.
 * @exception CsvUpdaterException, if the check on the second of the control strings fails.
 * @exception CsvUpdaterException, if the check on the name of the columns and their order fails.
 *
 * @retval true id no error occurred and all changes has been made.
 * @retval false if an error occurred and no changes has been made.
 *
 */
bool CsvUpdater::parseUpdateModel(const std::string& filename, const std::string& logfile) throw (BasicException&) {
	std::fstream recordstream(filename.c_str(), std::ios::in);
	if (!recordstream.is_open())
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Can't open " + filename);

	std::ofstream logstream(logfile.c_str(), std::ios::out);
	if (!logstream.is_open())
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"You must specify a valid stream for log purpose.");

	// the following three functions throw an exception corresponding to a serious error
	// they must cause the immediate termination of the parsing
	checkTableIdentifier(recordstream);
	checkCtrlString(recordstream);
	checkColumns(recordstream);

	std::string line;
	bool errorOccurred = false; // if an error occurs no changes will be made
	unsigned lineNumber = 4;	// the first record-line is line four
	while (!recordstream.eof()) {
		getline(recordstream, line);
		if (line.empty())
			logstream << lineNumber << ": line is empty" << std::endl;
		else {
			// the following block of code could throw an uncritical exception, which can be ignored
			// momentarily. All exceptions are reported in the log file.
			// In the event that exceptions occur, editing of record objects is not performed.
			try {
				ParsedString ps(parentTable, setting, line); // parsing of the line
				parsedStringList.push_back(ps);				 // if no error occurred, the object is inserted
															 // the list.
			} catch (BasicException& e) {
				// if an error occurred, the information is written in the log file
				logstream << lineNumber << ": " << e.what() << std::endl;
				// if an error occurs no changes will be made
				errorOccurred = true;
			}
		}
		lineNumber++;
	}


	if (!errorOccurred) {
		std::list<ParsedString>::iterator it = parsedStringList.begin(), end = parsedStringList.end();
		for (; it != end; it++)
			it->performChanges();
	}
	else // if an error occurs no changes will be made
		logstream << "No changes will be made due to errors found." << std::endl;

	recordstream.close();
	logstream.close();

	parsedStringList.clear(); // clearing the list of parsed string objects

	return !errorOccurred;
}

/**
 * @brief Creates an identifier used to recognize the Table object on which the CsvUpdater object will act.
 *
 * @details The identifier is used to verify that the file format conforms to what a CsvUpdater object would
 * expect. For example, if you pass any file to the CsvUpdater object, parsing the file will immediately be
 * stopped, because the format is recognized as altered. One of the first checks on the file format is done
 * by the checkTableIdentifier () function.
 */
void CsvUpdater::createTableIdentifier() noexcept {
	SHA256_sum_t hashsum;
	SHA256_hashsum((uint8_t*)parentTable->getName().c_str(), parentTable->getName().size(), &hashsum);
	std::stringstream str;
	for (int i = 0; i < 8; i++)
		str << std::hex << std::setfill('0') << std::setw(8) << hashsum.byte_array[i];
	tableIdentifier = str.str();
}

/**
 * Print the names of the Column objects that define the structure of the Table object to which the
 * CsvUpdater object is associated.
 *
 * @param [in,out]	stream std::fstream object associated with the file to print to
 * @param [in] 		simple if true it prints only the names of the Column objects, otherwise it adds the
 * 					column for the unique identifiers of the Record objects and the column to specify what
 * 					operation to perform on them.
 */
void CsvUpdater::printColumn(std::fstream& stream, bool simple) const noexcept {
	Table::ConstIterator it, begin = parentTable->cbegin();
	Table::ConstIterator end = parentTable->cend();
	stream << setting.startNewLine();
	if (!simple)
		stream << "internalID" << setting.startNewField() << "operation" << setting.startNewField();
	for (it = begin; it != end; it++) {
		if (it != begin)
			stream << setting.startNewField();
		stream << it->getName();
	}

	stream << setting.endLine() << std::endl;
}

/**
 * Print the current values of the Record object contained in the Storage object of the Table object to which
 * the CsvUpdater object is associated.
 *
 * @param [in,out]	stream std::fstream object associated with the file to print to
 * @param [in] 		simple if true it prints only the current values of the Record objects, otherwise it adds
 * 					the	column for the unique identifiers of the Record objects and the column to specify what
 * 					operation to perform on them.
 */
void CsvUpdater::printRecord(std::fstream& stream, bool simple) const noexcept {
	Table::ConstIterator 	col_it, col_begin = parentTable->cbegin(),
							col_end = parentTable->cend();
	Storage::OrderedConstIterator 	stor_it = parentTable->getStorage().obegin(),
									stor_end = parentTable->getStorage().oend();

	for (; stor_it != stor_end; stor_it++) {
		stream << setting.startNewLine();
		if (!simple)
			stream << stor_it->getKey() << setting.startNewField() << defaultOperation << setting.startNewField();
		for (col_it = col_begin; col_it != col_end; col_it++) {
			if (col_it != col_begin)
				stream << setting.startNewField();
			stream << stor_it->getCurrentValue(col_it->getName());
		}
		stream << setting.endLine() << std::endl;
	}
}

/**
 * @brief Check that the first of the control strings matches.
 *
 * @details
 * The model file is composed of:
 * - the first line is a control string which must not be changed; if you create multiple models using
 *   different CsvUpdater objects associated with the same Table object, they will contain the same first
 *   line, so you can consider this first line as a kind of identifier of the Table object on which you are
 *   acting;
 * - the second line is a further control string, but it will always be different, so it can be considered as
 *   a sort of identification for the modification session;
 * - the third row will contain the names of the Column objects that define the structure of the Table object
 *   to which the updater is associated.
 *
 * This function checks that the first of the control strings matches with the SHA-256 hashed name of the
 * Table object which the is associated with the current CsvUpdater object.
 *
 * @param stream std::fstream object associated with the file to parse
 *
 * @exception CsvUpdaterException, if the check on the first of the control strings fails.
 */
void CsvUpdater::checkTableIdentifier(std::fstream& stream) const throw (CsvUpdaterException&) {

	if (stream.eof()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Reached the end of the file without being able to check the first of the two control \
strings. The file is empty.");
	}

	std::string line;
	getline(stream, line);

	if (line.empty()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Failed to check the first of the two control strings. The first line must be the first \
control string. The file has been modified incorrectly.");
	}

	if (line != tableIdentifier) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Check on the first control string FAILED. The string contained in the file does not match.");
	}
}

/**
 * @brief Check that the second of the control strings matches.
 *
 * @details
 * The model file is composed of:
 * - the first line is a control string which must not be changed; if you create multiple models using
 *   different CsvUpdater objects associated with the same Table object, they will contain the same first
 *   line, so you can consider this first line as a kind of identifier of the Table object on which you are
 *   acting;
 * - the second line is a further control string, but it will always be different, so it can be considered as
 *   a sort of identification for the modification session;
 * - the third row will contain the names of the Column objects that define the structure of the Table object
 *   to which the updater is associated.
 *
 * This function checks that the second of the control strings matches.
 *
 * @param stream std::fstream object associated with the file to parse
 *
 * @exception CsvUpdaterException, if the check on the first of the control strings fails.
 */
void CsvUpdater::checkCtrlString(std::fstream& stream) const throw (CsvUpdaterException&) {
	if (stream.eof()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Reached the end of the file without being able to check the second of the two control \
strings. The file has been modified incorrectly.");
	}

	std::string line;
	getline(stream, line);

	if (line.empty()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Failed to check the second of the two control strings. The second line must be the second \
control string. The file has been modified incorrectly.");
	}

	if (line != controlString) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Check on the second control string FAILED. The string contained in the file does not match.");
	}
}

/**
 * @brief Check that the second of the control strings matches.
 *
 * @details
 * The model file is composed of:
 * - the first line is a control string which must not be changed; if you create multiple models using
 *   different CsvUpdater objects associated with the same Table object, they will contain the same first
 *   line, so you can consider this first line as a kind of identifier of the Table object on which you are
 *   acting;
 * - the second line is a further control string, but it will always be different, so it can be considered as
 *   a sort of identification for the modification session;
 * - the third row will contain the names of the Column objects that define the structure of the Table object
 *   to which the updater is associated.
 *
 * This function checks that the name of the columns and their order match.
 *
 * @param stream std::fstream object associated with the file to parse
 *
 * @exception CsvUpdaterException, if the check on the name of the columns and their order fails.
 */
void CsvUpdater::checkColumns(std::fstream& stream) const throw (CsvUpdaterException&) {
	if (stream.eof()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Reached the end of the file without being able to check the names of the columns. \
The file has been modified incorrectly.");
	}

	std::string line;
	getline(stream, line);

	if (line.empty()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
"Failed to check the names of the columns. \
The file has been modified incorrectly.");
	}

	// printing the name of the columns on a string
	// the following lines of code is the same of those in the printColumn() function
	std::stringstream ss;
	Table::ConstIterator it, begin = parentTable->cbegin();
	Table::ConstIterator end = parentTable->cend();
	ss << setting.startNewLine() << "internalID" << setting.startNewField() << "operation" << setting.startNewField();
	for (it = begin; it != end; it++) {
		if (it != begin)
			ss << setting.startNewField();
		ss << it->getName();
	}
	ss << setting.endLine();

	if (line != ss.str()) {
		stream.close();
		throw CsvUpdaterException(
				const_cast<CsvUpdater*>(this),
				CsvUpdaterException::streamError,
				__PRETTY_FUNCTION__,
				"Check on the name of the columns FAILED. The file has been modified incorrectly.");
	}


}

/**
 * @brief Constructs a ParsedString object by parsing a line of the file.
 *
 *
 * @param [in] parentTable
 * @param [in] line
 */
CsvUpdater::ParsedString::ParsedString(	Table* parentTable,
										const CsvUpdater::Setting& setting,
										const std::string& line) throw (BasicException&) :
	parentTable(parentTable)
{
	std::vector<std::string> tokenizedLine;
	csvTokenize(tokenizedLine, line, setting.getFieldSeparator(), setting.getQuoteChar());

	checkLineFormat(tokenizedLine);
	id = tokenizedLine[0];
	checkId();
	checkOpCode(tokenizedLine[1]);
	checkIdOpCodePair();
	makeColumnValuePair(tokenizedLine);
}

/**
 * @brief Perform the operation using data collected during parsing.
 */
void CsvUpdater::ParsedString::performChanges() noexcept {
	switch (op_code) {
		case insert_op: {
				Record record = parentTable->getStorage().newRecord();
				record.edit(recordValues);
				parentTable->getStorage().addRecord(record);
			}
			return;

		case load_op: {
				Record record = parentTable->getStorage().newRecord();
				record.edit(recordValues);
				record.setStatus(Record::loaded);
				parentTable->getStorage().addRecord(record);
			}
			return;

		case update_op:
		case partialupdate_op: {
			Storage::Iterator it = parentTable->getStorage().at(id);
			it->edit(recordValues);
			}
			return;

		case delete_op:
			parentTable->getStorage().at(id)->setStatus(Record::deleting);
			return;

		case erase_op: {
			Storage::Iterator it = parentTable->getStorage().at(id);
			parentTable->getStorage().removeRecord(it);
			}
			return;

		default:
			return;
	}

}

/**
 * @brief Checks that the line has the right number of "tokens"
 *
 * @param [in] tokenizedLine a vector containing the tokenized string, from ParsedString::ParsedString()
 *
 * @exception CsvUpdaterException if the number of tokens don't match the number of columns of the Table
 * object + 2.
 */
void CsvUpdater::ParsedString::checkLineFormat(const std::vector<std::string>& tokenizedLine) const throw (CsvUpdaterException&) {
	if (tokenizedLine.size() != parentTable->columnsNum() + 2)
		throw CsvUpdaterException(
				NULL,
				CsvUpdaterException::lineError,
				__PRETTY_FUNCTION__,
				"Invalid line format");
}

/**
 * @brief Verify that the id matches a record.
 *
 * @exception CsvUpdaterException if the Table object contains no records whose id is the one read from the
 * file
 */
void CsvUpdater::ParsedString::checkId() const throw (CsvUpdaterException&) {
	if (!id.empty() && parentTable->getStorage().at(id) == parentTable->getStorage().end())
		throw CsvUpdaterException(
				NULL,
				CsvUpdaterException::idError,
				__PRETTY_FUNCTION__,
				"Invalid record ID");
}

/**
 * @brief Parsing of the op-code.
 *
 * Checks that the specified operation matches to an opcode.
 * @param [in] opcode a string containing the operation to perform
 *
 * @exception CsvUpdaterException if you have not been able to recognize the operation you intend to carry out
 */
void CsvUpdater::ParsedString::checkOpCode(const std::string& opcode) throw (CsvUpdaterException&) {
	std::unordered_map<std::string, Operation>::const_iterator opcode_it = opcodeParsingMap.find(opcode);
	if (opcode_it == opcodeParsingMap.end())
		throw CsvUpdaterException(
				NULL,
				CsvUpdaterException::opCodeError,
				__PRETTY_FUNCTION__,
				"Invalid opcode");
	else
		op_code = opcode_it->second;

}

/**
 * @brief Verify that the id-opcode pair is valid
 *
 * @exception CsvUpdaterException if the opcode is "load" or "insert" and the ID field is is not empty
 * @exception CsvUpdaterException if the opcode is "update", "partial", "delete" or "erase" and the ID field
 * is empty
 */
void CsvUpdater::ParsedString::checkIdOpCodePair() const throw (CsvUpdaterException&) {

	if ((op_code == load_op || op_code == insert_op) && !id.empty())
			throw CsvUpdaterException(
				NULL,
				CsvUpdaterException::opCodeError,
				__PRETTY_FUNCTION__,
				"You can not perform this operation on this record.");

	if ((	op_code == update_op ||
			op_code == partialupdate_op ||
			op_code == delete_op ||
			op_code == erase_op) &&
			id.empty())
		throw CsvUpdaterException(
				NULL,
				CsvUpdaterException::opCodeError,
				__PRETTY_FUNCTION__,
				"You can not perform this operation without specifying the record id.");
}

/**
 * Builds the column-value map containing the values of a new record (or the new values with which to modify
 * an existing record)
 *
 * @param [in] tokenizedLine a vector containing the tokenized string, from ParsedString::ParsedString()
 *
 * @exception
 * @see Column::validate() function.
 */
void CsvUpdater::ParsedString::makeColumnValuePair(const std::vector<std::string> &tokenizedLine) throw (BasicException&) {
	Table::ConstIterator it = parentTable->cbegin(), end = parentTable->cend();
	int i = 2;
	for (; it != end; it++, i++)
		switch (op_code) {
			case load_op:
			case insert_op:
				// if a new record is to be added (marking it as "loaded" or "inserted") then all the columns
				// must be validated
				it->validate(tokenizedLine[i]);
				recordValues.emplace(std::pair<std::string, std::string>(it->getName(), tokenizedLine[i]));
				break;
			case update_op:
				// if an already inserted record needs to be modified then the "key" columns can be ignored
				if (!it->getIsKey()) {
					it->validate(tokenizedLine[i]);
					recordValues.emplace(std::pair<std::string, std::string>(it->getName(), tokenizedLine[i]));
				}
				break;
			case partialupdate_op:
				// if an already inserted record needs to be partially modified then the "key" columns and the
				// empty ones can be ignored
				if (!it->getIsKey() && !tokenizedLine[i].empty()) {
					it->validate(tokenizedLine[i]);
					recordValues.emplace(std::pair<std::string, std::string>(it->getName(), tokenizedLine[i]));
				}
				break;
			default: break;
		}
}
