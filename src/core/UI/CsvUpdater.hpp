/**
 * @file CsvUpdater.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef CSVUPDATEMANAGER_DBTABLE
#define CSVUPDATEMANAGER_DBTABLE

/**
 * @addtogroup Core
 * @{
 * @addtogroup UI
 * @{
 */

#include "../Exceptions/CsvUpdaterException.hpp"
#include "../Structure/Table.hpp"
#include <string>
#include <fstream>
#include <unordered_map>
#include <vector>

namespace openDB
{



class CsvUpdater
{
public:
	class Setting
	{
	public:
		Setting(char fs = ',', char qc = '"', bool q = true) :
			fieldSeparator(fs), quoteChar(qc), quote(q) {}

		char getFieldSeparator() const {return fieldSeparator;}

		void setFieldSeparator(char fs) {fieldSeparator = fs;}

		bool isQuote() const {return quote;}

		void setQuote(bool q) {quote = q;}

		char getQuoteChar() const {return quoteChar;}

		void setQuoteChar(char qc) {quoteChar = qc;}

		std::string startNewLine() const noexcept;

		std::string endLine() const noexcept;

		std::string startNewField() const noexcept;

	private:
		char fieldSeparator;
		char quoteChar;
		bool quote;
	};

	CsvUpdater(Table& parent) noexcept;

	void setUpdaterSetting(const Setting& s) {setting = s;}

	void simpleCsvExport(const std::string& filename) const throw (CsvUpdaterException&);

	void createUpdateModel(const std::string& filename) throw ( CsvUpdaterException& );

	bool parseUpdateModel(const std::string& filename, const std::string& logfile) throw ( BasicException& );

private:
	Table* parentTable; 			/**< Pointer to the Table object to which the CsvUpdater object is
										 associated*/

	Setting setting;				/**< Settings used in the csv export and in the creation of the model for
										 the modifications to be made on the Record objects */

	std::string tableIdentifier;	/**< Identifier of the table object to which the CsvUpdater object is
										 associated. The identifier is used to verify that the file format
										 conforms to what a CsvUpdater object would expect. If you create
										 multiple models using different CsvUpdater objects associated with
										 the same Table object, they will contain the same tableIdentifier */

	std::string controlString;		/**< a further control string, but it will always be different, it will be
										 generate every time the createUpdateModel() function is called, so it
										 can be considered as a sort of identification for the modification
										 session */

	static const std::string defaultOperation;	/**< default operation to be performed on records */

	/**
	 * This structure contains the result of parsing a line from the record-editing model created with the
	 * createUpdateModel() function.
	 */
	class ParsedString
	{
	public :
		enum Operation {
			no_op,				/**< no operation */
			load_op,			/**< The line will be used to create a new record, which will be placed in the
			 	 	 	 	 	 	 Record::loaded state */
			insert_op,			/**< The line will be used to create a new record, which will be placed in the
			 	 	 	 	 	 	 Record::inserting state */
			update_op,			/**< The row will be used to modify an existing record (whose id is
									 specified in the first column of the model). */
			partialupdate_op,	/**< Only non-null values in the row will be used to modify a new existing
									 record (whose id is specified in the first column of the model). */
			delete_op,			/**< The record will be marked for deletion from the database. */
			erase_op			/**< The record will be deleted from the Storage object. */
		};

		ParsedString(	Table* parentTable,
						const CsvUpdater::Setting& setting,
						const std::string& line) throw (BasicException&);

		void performChanges() noexcept;

	private:
		Table* parentTable;		/**< Pointer to the Table object to which the CsvUpdater object is
									 associated*/

		std::string id;			/**< Unique identifier of the record */

		Operation op_code;		/**< Operation to be performed on the record */

		/**
		 * column-value map containing the values of a new record or the new values with which to modify an
		 * existing record.
		 */
		std::unordered_map<std::string, std::string> recordValues;

		/**
		 * This map maintains the string-operation code pairs, used during the parsing of the file to
		 * recognize what operation is meant to be carried out with the data on that line. The recognized
		 * values, with the respective operation, are:
		 * - empty string: no_op;
		 * - "nop": no_op;
		 * - "load": load_op;
		 * - "Load": load_op;
		 * - "insert": insert_op;
		 * - "Insert": insert_op;
		 * - "update": update_op;
		 * - "Update": update_op;
		 * - "partial": partialupdate_op;
		 * - "Partial": partialupdate_op;
		 * - "delete": delete_op;
		 * - "Delete": delete_op;
		 * - "erase": erase_op;
		 * - "Erase": erase_op;
		 */
		static const std::unordered_map<std::string, Operation> opcodeParsingMap;

		void checkLineFormat(const std::vector<std::string> &tokenizedLine) const throw (CsvUpdaterException&);
		void checkId() const throw (CsvUpdaterException&);
		void checkOpCode(const std::string& opcode) throw (CsvUpdaterException&);
		void checkIdOpCodePair() const throw (CsvUpdaterException&);
		void makeColumnValuePair(const std::vector<std::string> &tokenizedLine) throw (BasicException&);

	};

	/**
	 * List of ParsedString objects, collects all the information about the lines of the model successfully
	 * parsed.
	 */
	std::list<ParsedString> parsedStringList;

	void createTableIdentifier() noexcept;
	void printColumn(std::fstream& stream, bool simple) const noexcept;
	void printRecord(std::fstream& stream, bool simple) const noexcept;
	void checkTableIdentifier(std::fstream& stream) const throw (CsvUpdaterException&);
	void checkCtrlString(std::fstream& stream) const throw (CsvUpdaterException&);
	void checkColumns(std::fstream& stream) const throw (CsvUpdaterException&);
};

/**
 * @}
 * @}
 */

};

#endif
