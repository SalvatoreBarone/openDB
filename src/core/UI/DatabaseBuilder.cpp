/**
 * @file DatabaseBuilder.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "DatabaseBuilder.hpp"
#include "SqlTypes.hpp"
#include "common.hpp"
#include <fstream>

using namespace openDB;

const std::string DatabaseBuilder::magicString = "DatabaseBuilderMagicString";
const std::string DatabaseBuilder::table_schema ="table_schema";
const std::string DatabaseBuilder::table_name ="table_name";
const std::string DatabaseBuilder::column_name ="column_name";
const std::string DatabaseBuilder::udt_name ="udt_name";
const std::string DatabaseBuilder::character_maximum_length ="character_maximum_length";
const std::string DatabaseBuilder::numeric_precision ="numeric_precision";
const std::string DatabaseBuilder::numeric_scale ="numeric_scale";
const std::string DatabaseBuilder::is_key ="is_key";

const std::string DatabaseBuilder::databaseSetting = "database";	// nome del setting che conterrà le informazioni riguardo la struttura del database
const std::string DatabaseBuilder::nameSetting = "name";		// nome del setting che conterrà i nomi di database, schemi, tabelle e colonne
const std::string DatabaseBuilder::schemasSetting = "schemas";		// nome del setting che conterrà le info riguardo gli schemi che compongono un database
const std::string DatabaseBuilder::tablesSetting = "tables";		// nome del setting che conterrà le info riguardo le tabelle che compongono uno schema
const std::string DatabaseBuilder::columnsSetting = "columns";		// nome del setting che conterrà le info riguardo le colonne che compongono una tabella
const std::string DatabaseBuilder::keySetting = "is_key";
const std::string DatabaseBuilder::sqltypesSetting = "type";		// nome del setting che conterrà le info riguardo il tipo sql di una colonna
const std::string DatabaseBuilder::sqllengthSetting = "length";		// nome del setting che conterrà le info riguardo la lunghezza di colonne di tipo varchar o character
const std::string DatabaseBuilder::sqlprecisionSetting = "precision";	// nome del setting che connterà le info riguardo la "precision" di una colonna numeric
const std::string DatabaseBuilder::sqlscaleSetting = "scale";		// nome del setting che connterà le info riguardo la "scale" di una colonna numeric

DatabaseBuilder::DatabaseBuilder ( const std::string& databaseName ) noexcept :
		__databaseName(databaseName),
		tmpTable(0),
		updateManager(0)
{
}

void DatabaseBuilder::createCSVModel(const std::string& fileName, struct CsvUpdater::Setting par) throw (BasicException&)
{
	defineTmpTable();
	updateManager = new CsvUpdater(*tmpTable);
	updateManager->model(fileName, par);
}

Database DatabaseBuilder::fromCSV (const std::string& fileName, std::list<std::string>& log, struct CsvUpdater::Setting par) throw (BasicException&)
{
	updateManager->parse(fileName, log, false, par);
	delete updateManager;
	return buildFromTmpTable();
}

Database DatabaseBuilder::fromTable (const Table& table) throw (BasicException&)
{
	defineTmpTable();
	tmpTable->loadRecord(table);

	return buildFromTmpTable();
}

Database DatabaseBuilder::fromConfigFile ( const std::string& configFile) throw (BasicException&)
{
	libconfig::Config cfg;
	Database database;
	try
	{
		cfg.readFile(configFile.c_str());
		libconfig::Setting& root = cfg.getRoot();

		libconfig::Setting& set_database = root[databaseSetting];
		Database tmp_database(set_database[nameSetting]);

		libconfig::Setting& schemas_list = set_database[schemasSetting];
		if (schemas_list.getType() != libconfig::Setting::TypeList)
			throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore: manca la lista di schemi!");

		addSchema(tmp_database, schemas_list); //provvederà ad aggiungere le colonne e il resto...

		database = tmp_database;
	}
	catch (libconfig::FileIOException& e) {
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Impossibile aprire il file " + configFile);
	}
	catch (libconfig::SettingNotFoundException& e) {
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Impossibile trovare la voce " + std::string(e.getPath()));
	}
	catch (libconfig::ParseException& e) {
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore durante il parsing alla linea " + std::to_string(e.getLine()) + ": " + std::string(e.getError()));
	}
	catch (libconfig::SettingTypeException& e) {
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore durante il parsina presso"+ std::string(e.getPath()) + ": " + std::string(e.what()));
	}
	catch (libconfig::SettingException& e)
	{
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore generico : " + std::string(e.what()));
	}
	catch (libconfig::ConfigException& e)
	{
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore generico : " + std::string(e.what()));
	}
	catch (BasicException& e)
	{
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore generico : " + std::string(e.what()));
	}
	catch (std::exception& e)
	{
		throw DatabaseBuilderException(__PRETTY_FUNCTION__, "Errore generico : " + std::string(e.what()));
	}

	return database;
}

void DatabaseBuilder::defineTmpTable () noexcept
{
	if (tmpTable)
		delete tmpTable;

	tmpTable = new Table("Temporary Table");
	tmpTable->addColumn(table_schema, Varchar());
	tmpTable->addColumn(table_name, Varchar());
	tmpTable->addColumn(column_name, Varchar());
	tmpTable->addColumn(udt_name, Varchar());
	tmpTable->addColumn(character_maximum_length, Integer());
	tmpTable->addColumn(numeric_precision, Integer());
	tmpTable->addColumn(numeric_scale, Integer());
	tmpTable->addColumn(is_key, Boolean());
}

Database DatabaseBuilder::buildFromTmpTable () const throw (BasicException&)
{
	Database database(__databaseName);

	Storage::ConstIterator stor_it = tmpTable->getStorage().cbegin(), stor_end = tmpTable->getStorage().cend();
	for (; stor_it != stor_end; stor_it++)
	{
		std::unordered_map<std::string, std::string> valuesMap;
		(*stor_it).getCurrentValues(valuesMap);

		Database::Iterator schema_it = database.addSchema(valuesMap[table_schema]);
		Schema::Iterator table_it = schema_it->addTable(valuesMap[table_name]);

		std::string columnName = valuesMap[column_name];
		SqlType* type = sqlType(valuesMap);
		std::string key = valuesMap[is_key];
		Table::Iterator col_it = table_it->addColumn(columnName, *type, Boolean::isTrue(key));

		delete type;
	}

	return database;
}

SqlType* DatabaseBuilder::sqlType(std::unordered_map<std::string, std::string>& valuesMap) const throw (BasicException&)
{
	std::string udtName = valuesMap[udt_name];

	if (udtName == Boolean::udtName || udtName == Boolean::typeName)
		return new Boolean;

	if (udtName == Smallint::udtName || udtName == Smallint::typeName)
		return new Smallint;

	if (udtName == Integer::udtName || udtName == Integer::typeName)
		return new Integer;

	if (udtName == Bigint::udtName || udtName == Bigint::typeName)
		return new Bigint;

	if (udtName == Real::udtName || udtName == Real::typeName)
		return new Real;

	if (udtName == DoublePrecision::udtName || udtName == DoublePrecision::typeName)
		return new DoublePrecision;

	if (udtName == Date::udtName || udtName == Date::typeName)
		return new Date;

	if (udtName == Time::udtName || udtName == Time::typeName)
		return new Time;

	if (udtName == Timestamp::udtName || udtName == Timestamp::typeName)
		return new Timestamp;

	if (udtName == Varchar::udtName || udtName == Varchar::typeName)
	{
		std::string lengh = valuesMap[character_maximum_length];
		int l = Varchar::defaultLength;
		try {l = std::stoi(lengh);}
		catch (std::exception& e) {}
		return new Varchar(l);
	}

	if (udtName == Character::udtName || udtName == Character::typeName)
	{
		std::string lengh = valuesMap[character_maximum_length];
		int l = Character::defaultLength;
		try {l = std::stoi(lengh);}
		catch (std::exception& e) {}
		return new Character(l);
	}

	if (udtName == Numeric::udtName || udtName == Numeric::typeName)
	{
		std::string precision = valuesMap [numeric_precision];
		std::string scale = valuesMap[numeric_scale];
		int prec = Numeric::defaultPrecision, sca = Numeric::defaultScale;
		try {prec = std::stoi(precision);}
		catch (std::exception& e) {}
		try {sca = std::stoi(scale);}
		catch (std::exception& e) {}
		SqlType* type = new Numeric (prec, sca);
		return type;
	}

	throw DatabaseBuilderException( __PRETTY_FUNCTION__, udtName + ": Invalid data type!");
}

void DatabaseBuilder::addSchema(Database& database, libconfig::Setting& schemas_list)
{
	for (int i = 0; i < schemas_list.getLength(); i++)
	{
		libconfig::Setting& set_schema = schemas_list[i];
		if (set_schema.getType() != libconfig::Setting::TypeGroup)
			throw DatabaseBuilderException(	__PRETTY_FUNCTION__,
							"Errore durante la lettura dello schema " + std::to_string(i) + ": non e' un gruppo!");

		std::string schemaName = set_schema[nameSetting];

		libconfig::Setting& tables_list = set_schema[tablesSetting];
		if (tables_list.getType() != libconfig::Setting::TypeList)
			throw DatabaseBuilderException(	__PRETTY_FUNCTION__,
							"Errore durante la lettura dello schema " + std::to_string(i) +
							": manca la lista delle tabelle!");

		Database::Iterator schema_it = database.addSchema(schemaName);
		addTable(*schema_it, tables_list);
	}
}

void DatabaseBuilder::addTable(Schema& schema, libconfig::Setting& tables_list)
{
	for (int i = 0; i < tables_list.getLength(); i++)
	{
		libconfig::Setting& set_table = tables_list[i];
		if (set_table.getType() != libconfig::Setting::TypeGroup)
			throw DatabaseBuilderException(	__PRETTY_FUNCTION__,
							"Errore durante la lettura della tabella " +
							std::to_string(i) + " dello schema " + schema.getName() +
							": non e' un gruppo!");

		std::string tableName = set_table[nameSetting];

		libconfig::Setting& columns_list = set_table[columnsSetting];
		if (columns_list.getType() != libconfig::Setting::TypeList)
			throw DatabaseBuilderException(	__PRETTY_FUNCTION__,
							"Errore durante la lettura della tabella " +
							std::to_string(i) + " dello schema " + schema.getName() +
							": manca la lista delle colonne!");

		Schema::Iterator table_it = schema.addTable(tableName);
		addColumn(*table_it, columns_list);
	}
}

void DatabaseBuilder::addColumn(Table& table, libconfig::Setting& columns_list)
{
	for (int i = 0; i < columns_list.getLength(); i++)
	{
		libconfig::Setting& set_column = columns_list[i];

		if (set_column.getType() != libconfig::Setting::TypeGroup)
			throw DatabaseBuilderException(	__PRETTY_FUNCTION__,
							"Errore durante la lettura della colonna " +
							std::to_string(i) + " della tabella " + table.getName() +
							": non e' un gruppo!");

		std::string columnName = set_column[nameSetting];
		bool isKey = false;
		if (set_column.exists(keySetting))
			isKey = set_column[keySetting];
		SqlType* type = sqlType(set_column);

		table.addColumn(columnName, *type, isKey);
	}

}

SqlType* DatabaseBuilder::sqlType(libconfig::Setting& setting) const throw (BasicException&)
{
	std::string udtName = setting[sqltypesSetting];

	if (udtName == Boolean::udtName || udtName == Boolean::typeName)
		return new Boolean;

	if (udtName == Smallint::udtName || udtName == Smallint::typeName)
		return new Smallint;

	if (udtName == Integer::udtName || udtName == Integer::typeName)
		return new Integer;

	if (udtName == Bigint::udtName || udtName == Bigint::typeName)
		return new Bigint;

	if (udtName == Real::udtName || udtName == Real::typeName)
		return new Real;

	if (udtName == DoublePrecision::udtName || udtName == DoublePrecision::typeName)
		return new DoublePrecision;

	if (udtName == Date::udtName || udtName == Date::typeName)
		return new Date;

	if (udtName == Time::udtName || udtName == Time::typeName)
		return new Time;

	if (udtName == Timestamp::udtName || udtName == Timestamp::typeName)
		return new Timestamp;

	if (udtName == Varchar::udtName || udtName == Varchar::typeName)
	{
		int l = (setting.exists(sqllengthSetting) ? setting[sqllengthSetting] : Varchar::defaultLength);
		return new Varchar(l);
	}

	if (udtName == Character::udtName || udtName == Character::typeName)
	{
		int l = (setting.exists(sqllengthSetting) ? setting[sqllengthSetting] : Character::defaultLength);
		return new Character(l);
	}

	if (udtName == Numeric::udtName || udtName == Numeric::typeName)
	{
		int precision = (setting.exists(sqlprecisionSetting) ? setting[sqlprecisionSetting] : Numeric::defaultPrecision);
		int scale = (setting.exists(sqlscaleSetting) ? setting[sqlscaleSetting] : Numeric::defaultScale);
		return new Numeric (precision, scale);
	}

	throw DatabaseBuilderException( __PRETTY_FUNCTION__, udtName + ": Invalid data type!");
}
