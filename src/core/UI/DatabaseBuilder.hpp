/**
 * @file DatabaseBuilder.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_DATABASEBUILDER
#define OPENDB2_DATABASEBUILDER

/**
 * @addtogroup UI
 * @{
 */

#include "Database.hpp"
#include "Table.hpp"
#include "CsvUpdateManager.hpp"
#include "SqlTypes.hpp"
#include <libconfig.h++>

namespace openDB
{

class DatabaseBuilder
{
public:
	explicit DatabaseBuilder ( const std::string& databaseName = "database" ) noexcept;

	void databaseName(const std::string& name) noexcept
		{__databaseName = name;}

	std::string databaseName() const noexcept
		{return __databaseName;}

	void createCSVModel(	const std::string& fileName,
				struct CsvUpdater::Setting par = CsvUpdater::Setting()) throw (BasicException&);

	Database fromCSV (	const std::string& fileName,
				std::list<std::string>& log,
				struct CsvUpdater::Setting par = CsvUpdater::Setting()) throw (BasicException&);

	Database fromTable ( const Table& table ) throw (BasicException&);
	/**
	 * Consente di definire la struttura di un database leggendola da file di configurazione.
	 * La struttura del file di configurazione deve essere la seguente:
	 *
	 * database :
	 * {
	 * 	name = "database_name";
	 * 	schemas =
	 * 		(
	 * 			{
	 * 			name = "schema_name";
	 * 			tables =
	 * 				(
	 * 					{
	 * 						name = "table_name";
	 * 						columns =
	 * 							(
	 * 								{
	 * 									name = "colum_name";
	 * 									type = sqlType;
	 * 									length = lenght; # solo per tipi Varchar e Character
	 * 									precision = precision; # solo per tipo numeric
	 * 									scale = scale; # solo per tipo numeric
	 * 								},
	 * 								{
	 *	 								name = "colum_name2";
	 * 									type = sqlType;
	 * 									length = lenght; # solo per tipi Varchar e Character
	 * 									precision = precision; # solo per tipo numeric
	 * 									scale = scale; # solo per tipo numeric
	 * 								}
	 *
	 * 							)
	 * 					},
	 * 					{
	 * 						name = "table_name";
	 * 						columns =
	 * 							(
	 * 								{
	 * 									name = "colum_name";
	 * 									type = sqlType;
	 * 									length = lenght; # solo per tipi Varchar e Character
	 * 									precision = precision; # solo per tipo numeric
	 * 									scale = scale; # solo per tipo numeric
	 * 								}
	 * 								{
	 * 									name = "colum_name2";
	 * 									type = sqlType;
	 * 									length = lenght; # solo per tipi Varchar e Character
	 * 									precision = precision; # solo per tipo numeric
	 * 									scale = scale; # solo per tipo numeric
	 * 								}
	 *
	 * 							)
	 * 					}
	 * 				)
	 * 			}
	 * 		)
	 * }
	 *
	 * @param configFile nome del file di configurazione
	 * @return database completamente strutturato secondo quanto letto da file.
	 */
	Database fromConfigFile ( const std::string& configFile ) throw (BasicException&);

private:
	static const std::string magicString;
	std::string __databaseName;

	Table* tmpTable;
	CsvUpdater* updateManager;

	/* nomi delle colonne del file csv e della tabella usata per costruire il nuovo database */
	static const std::string table_schema;
	static const std::string table_name;
	static const std::string column_name;
	static const std::string udt_name;
	static const std::string character_maximum_length;
	static const std::string numeric_precision;
	static const std::string numeric_scale;
	static const std::string is_key;

	void defineTmpTable () noexcept;
	Database buildFromTmpTable () const throw (BasicException&);
	SqlType* sqlType(std::unordered_map<std::string, std::string>& valuesMap) const throw (BasicException&);



	/* membri usati dalla funzione fromConfigFile() */
	static const std::string databaseSetting;	// nome del setting che conterrà le informazioni riguardo la struttura del database
	static const std::string nameSetting;		// nome del setting che conterrà i nomi di database, schemi, tabelle e colonne
	static const std::string schemasSetting;	// nome del setting che conterrà le info riguardo gli schemi che compongono un database
	static const std::string tablesSetting;		// nome del setting che conterrà le info riguardo le tabelle che compongono uno schema
	static const std::string columnsSetting;	// nome del setting che conterrà le info riguardo le colonne che compongono una tabella
	static const std::string keySetting;		// nome del setting che conterrà le true se la colonna è chiave
	static const std::string sqltypesSetting;	// nome del setting che conterrà le info riguardo il tipo sql di una colonna
	static const std::string sqllengthSetting;	// nome del setting che conterrà le info riguardo la lunghezza di colonne di tipo varchar o character
	static const std::string sqlprecisionSetting;	// nome del setting che connterà le info riguardo la "precision" di una colonna numeric
	static const std::string sqlscaleSetting;	// nome del setting che connterà le info riguardo la "scale" di una colonna numeric

	void addSchema(Database& database, libconfig::Setting& schemas_list);
	void addTable(Schema& schema, libconfig::Setting& tables_list);
	void addColumn(Table& table, libconfig::Setting& columns_list);
	SqlType* sqlType(libconfig::Setting& setting) const throw (BasicException&);


};

/**
 * @}
 */

};

#endif
