/**
 * @file common.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "common.hpp"

/**
 * @brief Divide a string into substrings, using a character as separator.
 *
 * @details The substrings are placed in a std::list<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token		std::list<std::string> object containing the substrings;
 * @param [in]	_string		string to be divided into substrings;
 * @param [in]	sep			separator character;
 **/
void openDB::tokenize ( std::list <std::string>& token, std::string _string, char sep ) noexcept
{
	std::string tmp;
	bool finished = false;
	std::size_t tabPos = 0;

	token.clear();

	do
	{
		tabPos = _string.find_first_of ( sep, 0 );

		switch ( tabPos )
		{
			case std::string::npos:
				tmp = _string;
				finished = true;
				break;

			case 0 :
				tmp = "";
				_string.erase ( 0, 1 );
				break;

			default
					:
				tmp = _string.substr ( 0, tabPos );
				_string.erase ( 0, tabPos + 1 );
		}

		token.push_back ( tmp );
	}
	while ( !finished );
}

/**
 * @brief Divide a string into substrings, using a character as separator.
 *
 * @details The substrings are placed in a std::vector<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token		std::vector<std::string> object containing the substrings;
 * @param [in]	_string		string to be divided into substrings;
 * @param [in]	sep			separator character;
 **/
void openDB::tokenize ( std::vector <std::string>& token, std::string _string, char sep ) noexcept
{
	std::string tmp;
	bool finished = false;
	std::size_t tabPos = 0;

	token.clear();

	int i = 0;
	do {
		tabPos = _string.find_first_of ( sep, 0 );
		switch ( tabPos ) {

			case std::string::npos:
				tmp = _string;
				finished = true;
				break;

			case 0 :
				tmp = "";
				_string.erase ( 0, 1 );
				break;

			default :
				tmp = _string.substr ( 0, tabPos );
				_string.erase ( 0, tabPos + 1 );
		}
		token[i++] = tmp;
		//token.push_back ( tmp );
	}
	while ( !finished );
}

/**
 * @brief Divide a string into substrings, using one or more characters as separator.
 *
 * @details The substrings are placed in a std::list<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token		std::list<std::string> object containing the substrings;
 * @param [in]	_string		string to be divided into substrings;
 * @param [in]	sep			string containing separator characters;
 **/
void openDB::tokenize ( std::list <std::string>& token, std::string _string, std::string sep ) noexcept
{
	std::string tmp;
	bool finished = false;
	std::size_t tabPos = 0;

	token.clear();

	do
	{
		tabPos = _string.find_first_of ( sep, 0 );

		switch ( tabPos )
		{
			case std::string::npos:
				tmp = _string;
				finished = true;
				break;

			case 0 :
				tmp = "";
				_string.erase ( 0, 1 );
				break;

			default
					:
				tmp = _string.substr ( 0, tabPos );
				_string.erase ( 0, tabPos + 1 );
		}

		token.push_back ( tmp );
	}
	while ( !finished );
}

/**
 * @brief Divide a string into substrings, using one or more characters as separator.
 *
 * @details The substrings are placed in a std::vector<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token		std::vector<std::string> object containing the substrings;
 * @param [in]	_string		string to be divided into substrings;
 * @param [in]	sep			string containing separator characters;
 **/
void openDB::tokenize ( std::vector <std::string>& token, std::string _string, std::string sep ) noexcept
{
	std::string tmp;
	bool finished = false;
	std::size_t tabPos = 0;

	token.clear();

	do
	{
		tabPos = _string.find_first_of ( sep, 0 );

		switch ( tabPos )
		{
			case std::string::npos:
				tmp = _string;
				finished = true;
				break;

			case 0 :
				tmp = "";
				_string.erase ( 0, 1 );
				break;

			default
					:
				tmp = _string.substr ( 0, tabPos );
				_string.erase ( 0, tabPos + 1 );
		}

		token.push_back ( tmp );
	}
	while ( !finished );
}

/**
 * @brief Divide a CSV string recognizing the use of double quotes.
 *
 * @details The substrings are placed in a std::list<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token	std::list<std::string> object containing the substrings;
 * @param [in]	str		string to be divided into substrings;
 * @param [in]	sep		separator character;
 * @param [in]	quote	quote character;
 **/
void openDB::csvTokenize ( std::list <std::string>& token, std::string str, char separator, char quote ) noexcept
{
	token.clear();

	unsigned substr_start, substr_end;
	unsigned it = 0;
	bool lastEmpty = (str.back() == separator ? true :  false);

	while ( it < str.size() )
	{

		if ( str[it] == quote )
		{
			substr_start = ++it;

			while ( it + 1 < str.size() && ! ( str[it] == quote  && str[it + 1] == separator ) )
				it++;

			substr_end = it;
			it += 2;
		}
		else
		{
			substr_start = it;

			while ( it < str.size() && str[it] != separator )
				it++;

			substr_end = it;
			it += 1;
		}

		token.push_back ( str.substr ( substr_start, substr_end - substr_start ) );

	}

	if (lastEmpty)
		token.push_back("");
}

/**
 * @brief Divide a CSV string recognizing the use of double quotes.
 *
 * @details The substrings are placed in a std::vector<std::string> object, keeping the order with which you
 * can locate them in the starting string.
 *
 * @param [out]	token			std::vector<std::string> object containing the substrings;
 * @param [in]	str				string to be divided into substrings;
 * @param [in]	separator		separator character;
 * @param [in]	quote			quote character;
 **/
void openDB::csvTokenize ( std::vector <std::string>& token, std::string str, char separator, char quote ) noexcept
{
	token.clear();

	unsigned substr_start, substr_end;
	unsigned it = 0;
	bool lastEmpty = (str.back() == separator ? true :  false);

	while ( it < str.size() )
	{

		if ( str[it] == quote )
		{
			substr_start = ++it;

			while ( it + 1 < str.size() && ! ( str[it] == quote && str[it + 1] == separator ) )
				it++;

			substr_end = it;
			it += 2;
		}
		else
		{
			substr_start = it;

			while ( it < str.size() && str[it] != separator )
				it++;

			substr_end = it;
			it += 1;
		}

		token.push_back ( str.substr ( substr_start, substr_end - substr_start ) );

	}

	if (lastEmpty)
		token.push_back("");
}

/**
 * @brief Writes a std::string object to a std::fstream stream object using the binary format.
 *
 * @param [in,out]	stream		std::fstream stream object;
 * @param [in] 		_string		std::string object;
 **/
void openDB::write ( std::fstream& stream, const std::string& _string ) noexcept
{
	unsigned length;
	length = _string.size();
	stream.write ( reinterpret_cast <const char*> ( &length ), sizeof ( unsigned ) );
	stream.write ( _string.c_str(), length );
}

/**
 * @brief Reads a std::string object from a std::fstream stream object using the binary format.
 *
 * @param [in,out]	stream		std::fstream stream object;
 * @param [out] 	_string		std::string object;
 **/
void openDB::read ( std::fstream& stream, std::string& _string ) noexcept
{
	unsigned length;
	char* tmp;
	stream.read ( reinterpret_cast <char*> ( &length ), sizeof ( unsigned ) );
	tmp = new char [length + 1];
	stream.read ( tmp, length );
	_string = std::string ( tmp, length );
	delete [] tmp;
}

/**
 * @brief Writes a list of std::string objects to a std::fstream stream object using the binary format.
 *
 * @param [in,out]	stream	std::fstream stream object;
 * @param [in] 		_list	std::list<std::string> object;
 **/
void openDB::write ( std::fstream& stream, const std::list <std::string>& _list ) noexcept
{
	unsigned elementNumber = _list.size();
	stream.write ( reinterpret_cast<const char*> ( &elementNumber ), sizeof ( unsigned ) );

	for ( std::list <std::string>::const_iterator it = _list.begin(); it != _list.end(); it++ )
		write ( stream, *it );
}

/**
 * @brief Reads a list of std::string objects from a std::fstream stream object using the binary format.
 *
 * @param [in,out]	stream	std::fstream stream object;
 * @param [out]		_list	std::list<std::string> object;
 **/
void openDB::read ( std::fstream& stream, std::list <std::string>& _list ) noexcept
{
	unsigned elementNumber;
	stream.read ( reinterpret_cast <char*> ( &elementNumber ), sizeof ( unsigned ) );
	std::string _string;

	for ( unsigned i = 0; i < elementNumber; i++ )
	{
		read ( stream, _string );
		_list.push_back ( _string );
	}
}

/**
 * @brief Allows to parameterize strings.
 *
 * @details  If the string contains parameters specified by the "$" character, those parameters will be
 * replaced with the value associated with them using the std::map<std::string, std::string>. For example,
 * suppose that you have a string like "insert into table (surname, name) values ($par1, $par2)". Using the
 * prepare() function with the {(par1, Barone), (par2, Salvatore)} map you can easly generate an insert sql
 * statement.
 *
 * @param [in, out] str			parametric string. The parameters will be replaced with the value associated
 * 								with them using the std::map<std::string, std::string> object.
 * @param [in]		parameter 	std::map<std::string, std::string> object containing the value-parameter
 * 								match;
 **/
void openDB::prepare ( std::string& str, const std::map<std::string, std::string>& parameter ) noexcept
{
	for ( std::map<std::string, std::string>::const_iterator it = parameter.begin(); it != parameter.end(); it++ )
	{
		std::string::size_type pos;

		do {
			pos = str.find ( std::string ( "$" + it->first ));
			if (pos != std::string::npos)
			{
				str.erase ( pos, it -> first.size() + 1 );
				str.insert ( pos, it -> second );
			}
		}
		while (pos != std::string::npos );

	}
}

/**
 * @brief Allows to parameterize strings.
 *
 * @details  If the string contains parameters specified by the "$" character, those parameters will be
 * replaced with the value associated with them using the std::unordered_map<std::string, std::string>.
 * For example, suppose that you have a string like "insert into table (surname, name) values ($par1, $par2)".
 * Using the prepare() function with the {(par1, Barone), (par2, Salvatore)} map you can easly generate an
 * insert sql statement.
 *
 * @param [in, out] str			parametric string. The parameters will be replaced with the value associated
 * 								with them using the std::unordered_map<std::string, std::string> object.
 * @param [in]		parameter 	std::unordered_map<std::string, std::string> object containing the
 * 								value-parameter match;
 **/
void openDB::prepare ( std::string& str, const std::unordered_map<std::string, std::string>& parameter ) noexcept
{
	for ( std::unordered_map<std::string, std::string>::const_iterator it = parameter.begin(); it != parameter.end(); it++ )
	{
		std::string::size_type pos;

		do {
			pos = str.find ( std::string ( "$" + it->first ));
			if (pos != std::string::npos)
			{
				str.erase ( pos, it -> first.size() + 1 );
				str.insert ( pos, it -> second );
			}
		}
		while (pos != std::string::npos );

	}
}

/**
 * @brief Generates a random string.
 *
 * @param [in] length length of the string
 *
 * @return :string, stringa casuale
 **/
std::string openDB::generateRandomString ( unsigned lenght ) noexcept
{
	static const char lookup[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	std::string random_str;

	for ( unsigned i = 0; i < lenght; i++ )
		random_str.push_back ( lookup[rand() % ( sizeof ( lookup ) - 1 )] );

	return random_str;
}

/**
 * @brief Reads a template from a file and writes it in a string.
 *
 * @param [out]	str			destination string
 * @param [in]	file_name	name of the file to be read
 */
void openDB::readTemplate(std::string& str, const std::string& file_name)
{
	std::string tmp;
	std::fstream stream;
	str.clear();
	stream.open(file_name.c_str(), std::ios::in);
	while (!stream.eof())
	{
		std::getline(stream, tmp);
		if (tmp != "")
			str += tmp + "\n";
	}
	stream.close();
}
