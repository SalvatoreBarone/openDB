/**
 * @file common.hpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef OPENDB2_COMMON
#define OPENDB2_COMMON

/**
 * @addtogroup Core
 * @{
 * @addtogroup common
 * @{
 */

#include <string>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include <unordered_map>

namespace openDB
{
void tokenize ( std::list <std::string>& token, std::string str, char sep ) noexcept;
void tokenize ( std::vector <std::string>& token, std::string str, char sep ) noexcept;
void tokenize ( std::list <std::string>& token, std::string str, std::string sep ) noexcept;
void tokenize ( std::vector <std::string>& token, std::string str, std::string sep ) noexcept;
void csvTokenize (	std::list <std::string>& token,
					std::string str,
					char sep = ';',
					char quote = '"' ) noexcept;
void csvTokenize (	std::vector <std::string>& token,
					std::string str,
					char sep = ';',
					char quote = '"' ) noexcept;
void write ( std::fstream& stream, const std::string& str ) noexcept;
void read ( std::fstream& stream, std::string& str ) noexcept;
void write ( std::fstream& stream, const std::list <std::string>& _list ) noexcept;
void read ( std::fstream& stream, std::list <std::string>& _list ) noexcept;
void prepare ( std::string& str, const std::map<std::string, std::string>& parameter ) noexcept;
void prepare ( std::string& str, const std::unordered_map<std::string, std::string>& parameter ) noexcept;
std::string generateRandomString ( unsigned length = 10 ) noexcept;
void readTemplate(std::string& str, const std::string& file_name);
/**
 * @}
 * @}
 */
};
#endif
