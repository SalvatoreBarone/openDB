/**
 * @file sha.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SHA_HEADER_H__
#define __SHA_HEADER_H__

/**
 * @addtogroup Hashing
 * @{
 *
 * @brief Implementazione C di alcune funzioni di Hashing.
 *
 * @addtogroup SHA
 * @{
 *
 * @brief Implementazione C delle funzioni hash SHA-256 e SHA-512
 */

#include <inttypes.h>

/**
 * @brief Struttura che astrae l'hashsum prodotto da SHA-512
 */
typedef struct {
	uint64_t byte_array[8]; /**< array di 64 byte, organizzati in otto word da otto byte, contenenti l'hashsum prodotto da SHA-512 */
} SHA512_sum_t;

/**
 * @brief Struttura che astrae l'hashsum prodotto da SHA-256
 */
typedef struct {
	uint32_t byte_array[8]; /**< array di 32 byte, organizzati in otto word da quattro byte, contenenti l'hashsum prodotto da SHA-256 */
} SHA256_sum_t;


/**
 * @brief Calcola l'hashsum SHA-512
 * @param[in]	inputData		vettore di byte di cui si desidera calcolare l'hashsum
 * @param[in]	size			dimensione, in byte, del vettore inputData
 * @param[out]	hashValue		puntatore a struttura SHA512_sum_t, essa conterra' l'hashsum calcolato dall'algoritmo
 *
 * @code
 * 	SHA512_sum_t hash;
 * 	uint8_t i, equals = 1;
 * 	SHA512_hashsum(message1, 3, &hash);
 * 	for (i = 0; i < 8; i++)
 * 		if (hash.byte_array[i] != expected1[i])
 * 			equals = 0;
 * 	if (equals)
 * 		printf("Test 1: Il valore hash prodotto da sha512Sum e' corretto\n");
 * 	else
 * 		printf("Test 1: Il valore hash prodotto da sha512Sum NON e' corretto\n");
 * @endcode
 *
 */
void SHA512_hashsum(uint8_t* inputData, uint64_t size, SHA512_sum_t *hashValue);

/**
 * @brief Calcola l'hashsum SHA-256
 *
 * @param[in]	inputData		vettore di byte di cui si desidera calcolare l'hashsum
 * @param[in]	size			dimensione, in byte, del vettore inputData
 * @param[out]	hashValue		puntatore a struttura SHA256_sum_t, essa conterra' l'hashsum calcolato dall'algoritmo
 *
 * @code
 * 	// test 1
 * uint8_t message1[] = {'a', 'b', 'c'};
 * SHA256_sum_t hash;
 * uint32_t expected1[8] = {0xba7816bf, 0x8f01cfea, 0x414140de, 0x5dae2223, 0xb00361a3, 0x96177a9c, 0xb410ff61, 0xf20015ad};
 * SHA256_hashsum(message1, 3, &hash);
 * uint8_t i, equals = 1;
 * for (i = 0; i < 8; i++)
 * 	if (hash.byte_array[i] != expected1[i])
 * 		equals = 0;
 * if (equals)
 * 	printf("Test 1: Il valore hash prodotto da sha256Sum e' corretto\n");
 * else
 * 	printf("Test 1: Il valore hash prodotto da sha256Sum NON e' corretto\n");
 * @endcode
 *
 */
void SHA256_hashsum(uint8_t* inputData, uint64_t size, SHA256_sum_t *hashValue);

/**
 * @}
 * @}
 */

#endif
