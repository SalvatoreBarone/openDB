/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "CsvExportSetting.hpp"
using namespace openDB;

#include <QtGui>
#include <QMainWindow>

CsvExportSettingDialog::CsvExportSettingDialog(struct CsvUpdateManager::Setting* par, QMainWindow* parent) :
		QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint),
		parameter(par),
		parentWindow(parent),
		labelSeparatorCharacter( new QLabel("Separatore", this)),
		editSeparatorCharacter(new QLineEdit(this)),
		checkboxQuote(new QCheckBox("Quota", this)),
		editQuoteCharacter(new QLineEdit(this)),
		checkboxStringQuote(new QCheckBox("Solo le stringhe", this)),
		buttonOk(new QPushButton("Ok", this)),
		buttonCancel(new QPushButton("Annulla", this)),
		layoutButton(new QHBoxLayout()),
		mainLayout(new QGridLayout(this))
{
	setLayout(mainLayout);
	QSize size;
	size.setWidth(300);
	size.setHeight(200);
	setFixedSize(size);
	setWindowTitle("Impostazioni Csv");

	editSeparatorCharacter->setMaxLength(1);
	editQuoteCharacter->setMaxLength(1);

	mainLayout->addWidget(labelSeparatorCharacter, 0, 0);
	mainLayout->addWidget(editSeparatorCharacter, 0, 1, 1, 2);
	mainLayout->addWidget(checkboxQuote, 1, 0);
	mainLayout->addWidget(checkboxStringQuote, 1, 1);
	mainLayout->addWidget(editQuoteCharacter, 1, 2);
	layoutButton->addWidget(buttonOk);
	layoutButton->addWidget(buttonCancel);
	mainLayout->addLayout(layoutButton, 3, 0, 1, 3, Qt::AlignCenter | Qt::AlignBottom);

	connect(buttonOk, SIGNAL(clicked()), this, SLOT(storeConfig()));
	connect(buttonCancel, SIGNAL(clicked()), this, SLOT(close()));
	loadConfig();
}

CsvExportSettingDialog::~CsvExportSettingDialog()
{
	mainLayout->removeWidget(labelSeparatorCharacter);
	mainLayout->removeWidget(editSeparatorCharacter);
	mainLayout->removeWidget(checkboxQuote);
	mainLayout->removeWidget(labelSeparatorCharacter);
	mainLayout->removeWidget(editSeparatorCharacter);
	mainLayout->removeWidget(checkboxQuote);
	layoutButton->removeWidget(buttonOk);
	layoutButton->removeWidget(buttonCancel);
	mainLayout->removeItem(layoutButton);

	delete labelSeparatorCharacter;
	delete editSeparatorCharacter;
	delete checkboxQuote;
	delete editQuoteCharacter;
	delete checkboxStringQuote;
	delete layoutButton;
	delete mainLayout;
}

void CsvExportSettingDialog::loadConfig() noexcept
{
	checkboxQuote->setChecked(parameter->quote);
	checkboxStringQuote->setChecked(parameter->quote_only_strings);
	std::string sep(1, parameter->separator_character), qte(1, parameter->quote_character);

	editSeparatorCharacter->setText(QString::fromStdString(sep));
	editQuoteCharacter->setText(QString::fromStdString(qte));
}

void CsvExportSettingDialog::storeConfig() noexcept
{
	parameter->quote = checkboxQuote->isChecked();
	parameter->quote_only_strings = checkboxStringQuote->isChecked();
	std::string sep = editSeparatorCharacter->text().toStdString();
	std::string qte = editQuoteCharacter->text().toStdString();
	parameter->separator_character = sep[0];
	parameter->quote_character = qte[0];
	close();
}
