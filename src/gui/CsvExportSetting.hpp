/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __CSVEXPORTSETTINGDIALOG__
#define __CSVEXPORTSETTINGDIALOG__

#include <QDialog>
#include <QWidget>
#include "core/Table.hpp"
#include "core/CsvUpdateManager.hpp"

class QMainWindow;
class QLabel;
class QLineEdit;
class QCheckBox;
class QPushButton;
class QGridLayout;
class QHBoxLayout;

namespace openDB
{

class CsvExportSettingDialog : public QDialog
{
	Q_OBJECT
public:
	CsvExportSettingDialog(struct CsvUpdateManager::Setting* par, QMainWindow* parent);

	virtual ~CsvExportSettingDialog();

public slots:
	void loadConfig() noexcept;
	void storeConfig() noexcept;

private:
	struct CsvUpdateManager::Setting* parameter;
	QMainWindow* parentWindow;

	QLabel* labelSeparatorCharacter;
	QLineEdit* editSeparatorCharacter;
	QCheckBox* checkboxQuote;
	QLineEdit* editQuoteCharacter;
	QCheckBox* checkboxStringQuote;
	QPushButton* buttonOk;
	QPushButton* buttonCancel;
	QHBoxLayout* layoutButton;
	QGridLayout* mainLayout;



};

};

#endif
