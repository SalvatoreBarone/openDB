/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "DatabaseMenu.hpp"
#include "SchemaMenu.hpp"
#include "core/Schema.hpp"
#include "core/Database.hpp"
#include "MainWindow.hpp"
using namespace openDB;



DatabaseMenu::DatabaseMenu(	Database* database,
				MainWindow* window ) noexcept :
								QMenu(window),
								parentDatabase(database),
								parentWindow(window)
{
	setTitle(QString::fromStdString(parentDatabase->name()));
	Database::Iterator it = parentDatabase->begin(), end = parentDatabase->end();
	for(; it != end; it++)
	{
		SchemaMenu* menu = new SchemaMenu(&(*it), parentWindow);
		menuList.push_back(menu);
		addMenu(menu);
	}
}
		
DatabaseMenu::~DatabaseMenu()
{
	std::list<SchemaMenu*>::iterator it = menuList.begin(), end = menuList.end();
	for (; it != end; it++)
		delete *it;
}
