/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "EditRecord.hpp"
#include "EditValue.hpp"
#include "MainWindow.hpp"
#include "TableMenu.hpp"
#include "TableView.hpp"

#include <QtGui>
#include <QMessageBox>

#include <iostream>

using namespace openDB;


EditRecord::EditRecord(	Table* table,
			Storage::Iterator sit,
			MainWindow* __window,
			TableMenu* __menu,
			QWidget* parent) :
		QWidget(parent),
		parentTable(table),
		window(__window),
		menu(__menu),
		iterator(sit),

		viewport(new QWidget(this)),
		scrollArea(new QScrollArea(this)),
		mainLayout(new QHBoxLayout(this)),

		gridLayout(new QGridLayout()),
		buttonLayout(new QHBoxLayout()),
		buttonOk(new QPushButton("Ok", this)),
		buttonPrec(new QPushButton("Rec. Prec.", this)),
		buttonNext(new QPushButton("Rec. Succ.", this)),
		buttonView(new QPushButton("Vai alla Tabella", this))
{
	Table::Iterator it = parentTable->begin(), end = parentTable->end();
	int i = 0;
	for (; it != end; it++, i++)
	{
		EditValue* widget = new EditValue(it, gridLayout, i, this);
		widgetList.push_back(widget);
	}

	buttonLayout->addWidget(buttonOk);
	buttonLayout->addWidget(buttonPrec);
	buttonLayout->addWidget(buttonNext);
	buttonLayout->addWidget(buttonView);
	gridLayout->addLayout(buttonLayout, i, 0, 1, 2, Qt::AlignCenter | Qt::AlignBottom);

	viewport->setLayout(gridLayout);
	scrollArea->setWidget(viewport);
	scrollArea->setWidgetResizable(true);
	mainLayout->addWidget(scrollArea);
	setLayout(mainLayout);

	connect(buttonOk, SIGNAL(clicked()), this, SLOT(slotOk()));
	connect(buttonPrec, SIGNAL(clicked()), this, SLOT(slotRecPrec()));
	connect(buttonNext, SIGNAL(clicked()), this, SLOT(slotRecNext()));
	connect(buttonView, SIGNAL(clicked()), this, SLOT(slotView()));

	load();
}

EditRecord::~EditRecord()
{
	buttonLayout->removeWidget(buttonOk);
	buttonLayout->removeWidget(buttonPrec);
	buttonLayout->removeWidget(buttonNext);
	gridLayout->removeItem(buttonLayout);
	delete buttonPrec;
	delete buttonNext;
	delete buttonOk;
	delete buttonView;
	delete buttonLayout;

	std::list<EditValue*>::iterator it = widgetList.begin(), end = widgetList.end();
	for (; it != end; it++)
		delete *it;
}

void EditRecord::load()
{
	if (iterator != parentTable->storage().end())
	{
		record = *iterator;
		std::list<EditValue*>::iterator it = widgetList.begin(), end = widgetList.end();
		for (; it != end; it++)
			(*it)->load(&record);
	}
}


void EditRecord::store()
{
	std::list<EditValue*>::iterator it = widgetList.begin(), end = widgetList.end();
	for (; it != end; it++)
		(*it)->store(&record);
}


void EditRecord::slotOk()
{
	try
	{
		store();
		Record updatedRecord = record;
		parentTable->storage().updateRecord(iterator, updatedRecord);
	}
	catch (BasicException& e)
	{
		QMessageBox::warning(this, "Errore!", e.what(), QMessageBox::Ok);
	}
}

void EditRecord::slotRecPrec()
{
	if (iterator != iterator.getParent()->begin())
	{
		iterator--;
		load();
	}
}

void EditRecord::slotRecNext()
{
	if (iterator != iterator.getParent()->end())
	{
		iterator++;
		load();
	}
}

void EditRecord::slotView()
{
	TableView* tableView = new TableView(parentTable, true, window, menu, window);
	window->setSessionWidget(QString::fromStdString(parentTable->name()), tableView);
}
