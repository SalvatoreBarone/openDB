/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __OPENDB_RECORDEDIT__
#define __OPENDB_RECORDEDIT__

#include <QWidget>

#include "core/Table.hpp"
#include "core/Storage.hpp"
#include "core/Record.hpp"

class QLabel;
class QCheckBox;
class QComboBox;
class QLineEdit;
class QTextEdit;
class QPushButton;
class QGridLayout;
class QHBoxLayout;
class QScrollArea;

namespace openDB
{

class EditValue;
class TableMenu;
class MainWindow;

class EditRecord : public QWidget
{
	Q_OBJECT
public:
	EditRecord(	Table* table,
			Storage::Iterator rec_iterator,
			MainWindow* __window,
			TableMenu* __menu,
			QWidget* parent);

	virtual ~EditRecord();

private:
	Table* parentTable;
	MainWindow* window;
	TableMenu* menu;
	Storage::Iterator iterator;
	Record record;

	std::list<EditValue*> widgetList;

	QWidget* viewport;
	QScrollArea* scrollArea;
	QHBoxLayout* mainLayout;
	QGridLayout* gridLayout;
	QHBoxLayout* buttonLayout;
	QPushButton* buttonOk;
	QPushButton* buttonPrec;
	QPushButton* buttonNext;
	QPushButton* buttonView;

	void load();
	void store();

private slots:
	void slotOk();
	void slotRecPrec();
	void slotRecNext();
	void slotView();

};

};

#endif
