/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "EditValue.hpp"

#include "LineEdit.hpp"
#include "TextEdit.hpp"
#include "core/Record.hpp"
#include "core/Storage.hpp"
#include "core/Column.hpp"
#include "core/SqlTypes.hpp"

#include <QtGui>
#include <QMessageBox>
#include <iostream>
using namespace openDB;

const QString EditValue::qtDateFormat = "dd/MM/yyyy";
const QString EditValue::qtTimeFormat = "hh:mm:ss";
const QString EditValue::pgDateFormat = "yyyy-mm-dd";
const QString EditValue::pgTimeFormat = "hh:mm:ss";

EditValue::EditValue(Table::Iterator cit, QGridLayout* layout, int row, QWidget* parent)  :
		QWidget(parent),
		citerator(cit),
		columnName(new QLabel(QString::fromStdString(citerator->name()))),
		columnWidget(0),
		parentLayout(layout)
{
	createWidget(parent);
	parentLayout->addWidget(columnName, row, 0);
	parentLayout->addWidget(columnWidget, row, 1);
}

EditValue::~EditValue()
{
	parentLayout->removeWidget(columnName);
	parentLayout->removeWidget(columnWidget);
	delete columnName;
	delete columnWidget;
}

void EditValue::load(Record* rec)
{
	std::string value = rec->current(citerator->name());
	setValue(value);
}


void EditValue::store(Record* rec)
{
	std::string value = getValue();
	rec->edit(citerator->name(), value);
}

void EditValue::createWidget(QWidget* parent)
{
	SqlType::TypeInfo column_info = citerator->typeInfo();
	switch(column_info.internalID)
	{
		case Boolean::internalID :
			columnWidget = new QCheckBox(parent);

			break;

		case Date::internalID :
		case Time::internalID :
		case Timestamp::internalID :
		case Smallint::internalID :
		case Integer::internalID :
		case Bigint::internalID :
		case Real::internalID :
		case DoublePrecision::internalID :
		case Numeric::internalID :
			{
				columnWidget = new LineEdit(parent);
				connect(columnWidget, SIGNAL(editFinished()), this, SLOT(validate()));
				connect(columnWidget, SIGNAL(editingFinished ()), this, SLOT(validate())); //connect to validate on edit termination
			}
			break;

		case Character::internalID :
		case Varchar::internalID :
			{
				if (column_info.vcharLength > vcharTextLenght)
				{
					columnWidget = new TextEdit(parent);
					connect(columnWidget, SIGNAL(editFinished()), this, SLOT(validate()));
				}
				else
				{
					columnWidget = new LineEdit(parent);
					static_cast<LineEdit*>(columnWidget)->setMaxLength(column_info.vcharLength);
					connect(columnWidget, SIGNAL(editFinished()), this, SLOT(validate()));
				}
			}
			break;


	}
}




void EditValue::setValue(const std::string& str)
{
	std::string value = str;
	SqlType::TypeInfo column_info = citerator->typeInfo();
	switch(column_info.internalID)
	{
		case Boolean::internalID :
			if (SqlType::isNull(str))
				value = Boolean::falseDefault;
			static_cast<QCheckBox*>(columnWidget)->setChecked(Boolean::isTrue(value));
			break;

		case Date::internalID :
		case Time::internalID :
		case Timestamp::internalID :
		case Smallint::internalID :
		case Integer::internalID :
		case Bigint::internalID :
		case Real::internalID :
		case DoublePrecision::internalID :
		case Numeric::internalID :
			if (!SqlType::isNull(value))
				static_cast<LineEdit*>(columnWidget)->setText(QString::fromStdString(value));
			else
				static_cast<LineEdit*>(columnWidget)->setText(QString::fromStdString(""));
			break;

		case Character::internalID :
		case Varchar::internalID :
			if (SqlType::isNull(value))
				value = "";
			if (column_info.vcharLength > vcharTextLenght)
				static_cast<TextEdit*>(columnWidget)->setPlainText(QString::fromStdString(value));
			else
				static_cast<LineEdit*>(columnWidget)->setText(QString::fromStdString(value));
			break;


	}
}


std::string EditValue::getValue() const
{
	SqlType::TypeInfo column_info = citerator->typeInfo();
	std::string value;
	switch(column_info.internalID)
	{
		case Boolean::internalID :
			if (static_cast<QCheckBox*>(columnWidget)->isChecked())
				value = Boolean::trueDefault;
			else
				value = Boolean::falseDefault;
			break;

		case Date::internalID :
		case Time::internalID :
		case Timestamp::internalID :
		case Smallint::internalID :
		case Integer::internalID :
		case Bigint::internalID :
		case Real::internalID :
		case DoublePrecision::internalID :
		case Numeric::internalID :
				value = static_cast<LineEdit*>(columnWidget)->text().toStdString();
			break;

		case Character::internalID :
		case Varchar::internalID :
			if (column_info.vcharLength > vcharTextLenght)
				value = static_cast<TextEdit*>(columnWidget)->toPlainText().toStdString();
			else
				value = static_cast<LineEdit*>(columnWidget)->text().toStdString();
			break;


	}
	return value;
}


void EditValue::validate()
{
	std::string value = getValue();
	try
	{
		citerator->validate(value);
	}
	catch (BasicException& e)
	{
		QMessageBox::warning(this, "Errore!", e.what(), QMessageBox::Ok);
	}
}

