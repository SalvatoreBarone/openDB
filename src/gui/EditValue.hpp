/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __EDIT_VALUE_H
#define __EDIT_VALUE_H


#include <string>
#include <QWidget>

#include "core/Table.hpp"

class QLabel;
class QGridLayout;

namespace openDB
{

class Record;

class EditValue : public QWidget
{
	Q_OBJECT
public:
	EditValue(Table::Iterator cit, QGridLayout* layout, int row, QWidget* parent);

	virtual ~EditValue();

public slots:
	void load(Record* rec);
	void store(Record* rec);

private:
	Table::Iterator citerator;

	QLabel* columnName;
	QWidget* columnWidget;
	QGridLayout* parentLayout;

	void createWidget(QWidget* parent);
	void setValue(const std::string& str);
	std::string getValue() const;

	static const QString qtDateFormat;
	static const QString qtTimeFormat;
	static const QString pgDateFormat;
	static const QString pgTimeFormat;
	static const unsigned vcharTextLenght = 300;

private slots:
	void validate();

};



};



#endif
