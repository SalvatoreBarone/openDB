/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "InsertRecord.hpp"
#include "EditValue.hpp"
#include "MainWindow.hpp"
#include "TableMenu.hpp"
#include "TableView.hpp"
#include <QtGui>
#include <QString>
using namespace openDB;


InsertRecord::InsertRecord(Table* table,
				MainWindow* __window,
				TableMenu* __menu,
				QWidget* parent) noexcept :
		QWidget(parent),
		parentTable(table),
		window(__window),
		menu(__menu),
		viewport(new QWidget(this)),
		scrollArea(new QScrollArea(this)),
		mainLayout(new QHBoxLayout(this)),
		gridLayout(new QGridLayout()),
		buttonLayout(new QHBoxLayout()),
		buttonOk(new QPushButton("Ok", this)),
		buttonView(new QPushButton("Vai alla Tabella", this))
{
	rec = parentTable->storage().newRecord();
	Table::Iterator it = parentTable->begin(), end = parentTable->end();

	int i = 0;
	for (; it != end; it++, i++)
	{
		EditValue* widget = new EditValue(it, gridLayout, i, this);
		widgetList.push_back(widget);
	}

	buttonLayout->addWidget(buttonOk);
	buttonLayout->addWidget(buttonView);
	gridLayout->addLayout(buttonLayout, i, 0, 1, 2, Qt::AlignCenter | Qt::AlignBottom);

	viewport->setLayout(gridLayout);
	scrollArea->setWidget(viewport);
	scrollArea->setWidgetResizable(true);
	mainLayout->addWidget(scrollArea);
	setLayout(mainLayout);

	connect(buttonOk, SIGNAL(clicked()), this, SLOT(slotOk()));
	connect(buttonView, SIGNAL(clicked()), this, SLOT(slotView()));

}

InsertRecord::~InsertRecord()
{
	buttonLayout->removeWidget(buttonOk);
	gridLayout->removeItem(buttonLayout);
	delete buttonOk;
	delete buttonView;
	delete buttonLayout;

	std::list<EditValue*>::iterator it = widgetList.begin(), end = widgetList.end();
	for (; it != end; it++)
		delete *it;
}

void InsertRecord::slotOk()
{
	try
	{
		std::list<EditValue*>::iterator it = widgetList.begin(), end = widgetList.end();
		for (; it != end; it++)
			(*it)->store(&rec);

		rec.state(Record::inserting);
		parentTable->storage().insertRecord(rec);
	}
	catch (BasicException& e)
	{
		QMessageBox::warning(this, "Errore!", e.what(), QMessageBox::Ok);
	}
}

void InsertRecord::slotView()
{
	TableView* tableView = new TableView(parentTable, true, window, menu, window);
	window->setSessionWidget(QString::fromStdString(parentTable->name()), tableView);
}
