/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __OPENDB_INSERTRECORD__
#define __OPENDB_INSERTRECORD__

#include <QWidget>

#include <list>
#include <string>

#include "core/Table.hpp"
#include "core/Storage.hpp"
#include "core/Record.hpp"

class QPushButton;
class QGridLayout;
class QHBoxLayout;
class QScrollArea;

namespace openDB
{

class Table;
class EditValue;
class MainWindow;
class TableMenu;

class InsertRecord : public QWidget
{
	Q_OBJECT
public:
	InsertRecord(	Table* table,
			MainWindow* __window,
			TableMenu* __menu,
			QWidget* parent) noexcept;

	virtual ~InsertRecord();

private:
	Table* parentTable;
	MainWindow* window;
	TableMenu* menu;
	Record rec;

	QWidget* viewport;
	QScrollArea* scrollArea;
	QHBoxLayout* mainLayout;

	std::list<EditValue*> widgetList;

	QGridLayout* gridLayout;
	QHBoxLayout* buttonLayout;
	QPushButton* buttonOk;
	QPushButton* buttonView;

private slots:
	void slotOk();
	void slotView();


};

};

#endif
