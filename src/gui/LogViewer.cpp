/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "LogViewer.hpp"
using namespace openDB;

#include "core/BackendConnector.hpp"

#include <QTextBrowser>
#include <QPushButton>
#include <QVBoxLayout>

#include <list>
#include <string>

LogViewer::LogViewer(BackendConnector* connector, QWidget* parent) :
		QWidget(parent),
		backendConnector(connector),
		textBrowser(new QTextBrowser(this)),
		buttonRefresh(new QPushButton("Aggiorna", this)),
		layout(new QVBoxLayout(this))
{
	setLayout(layout);
	layout->addWidget(textBrowser);
	layout->addWidget(buttonRefresh, Qt::AlignCenter | Qt::AlignBottom);
	buttonRefresh->setFixedSize(buttonRefresh->sizeHint());
	connect(buttonRefresh, SIGNAL(clicked()), this, SLOT(slotRefresh()));

	slotRefresh();
}

LogViewer::~LogViewer()
{
	layout->removeWidget(textBrowser);
	layout->removeWidget(buttonRefresh);
	delete textBrowser;
	delete buttonRefresh;
	delete layout;
}

void LogViewer::slotRefresh()
{
	textBrowser->clear();
	BackendConnector::Iterator it = backendConnector->begin(), end = backendConnector->end();
	for (; it != end; it++)
	{
		Result::Status queryStatus = it.result().status();
		if (queryStatus == Result::commandOk || queryStatus == Result::tupleOk || queryStatus == Result::error)
		{
			std::list<std::string> msg;
			it.transaction()->commands(msg);
			QString logMsg;
			std::list<std::string>::const_iterator msg_it = msg.begin(), msg_end = msg.end();
			for (;msg_it != msg_end; msg_it++)
				logMsg += QString::fromStdString(*msg_it) + "<br>";
			logMsg += "<b><i>" + QString::fromStdString(it.result().info()) + "</b></i><br><br>";
			textBrowser->insertHtml(logMsg);
		}
	}
}
