/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __LOG_VIEWER_H
#define __LOG_VIEWER_H

#include <QWidget>
#include <QString>

class QTextBrowser;
class QPushButton;
class QVBoxLayout;

namespace openDB
{

class BackendConnector;

class LogViewer : public QWidget
{
	Q_OBJECT
public:
	explicit LogViewer(BackendConnector* connector, QWidget* parent);

	virtual ~LogViewer();

private:
	BackendConnector* backendConnector;
	std::list<std::string> log_messages;
	QTextBrowser* textBrowser;
	QPushButton* buttonRefresh;
	QVBoxLayout* layout;

private slots:
	void slotRefresh();
};

};

#endif
