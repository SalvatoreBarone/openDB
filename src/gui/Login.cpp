/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "Login.hpp"
using namespace openDB;

#include "MainWindow.hpp"
#include "LoginWidget.hpp"
#include "PgLogin.hpp"
#include "Sqlite3Login.hpp"
#include "core/BackendConnector.hpp"
#include "core/common.hpp"

#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>

#include <iostream>
#include <fstream>

const QString Login::sqlite3CBoxEntry = "Sqlite3";
const QString Login::postgresqlCBoxEntry = "Postgresql";

Login::Login(	Database*& __database,
		BackendConnector*& __connector,
		MainWindow* __window,
		QWidget* __parent) :
		QWidget(__parent),
		parentWindow(__window),
		database(__database),
		connector(__connector),
		dbmsType(new QComboBox(this)),
		loginWidget(0),
		buttonConnetti(new QPushButton("Connetti", this)),
		buttonAnnulla(new QPushButton("Annulla", this)),
		buttonLayout(new QHBoxLayout()),
		mainLayout(new QGridLayout(this))
{
	dbmsType -> addItem(sqlite3CBoxEntry);
	dbmsType -> addItem(postgresqlCBoxEntry);
	loginWidget = new Sqlite3Login(database, connector, parentWindow, this);

	mainLayout->addWidget(dbmsType, 0, 0, Qt::AlignCenter);
	mainLayout->addWidget(loginWidget, 1, 0, Qt::AlignCenter);
	buttonLayout->addWidget(buttonConnetti);
	buttonLayout->addWidget(buttonAnnulla);
	mainLayout->addLayout(buttonLayout, 2, 0, Qt::AlignHCenter | Qt::AlignBottom);

	connect (dbmsType, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLoginWidget(int)));
	connect(loginWidget, SIGNAL(done()), this, SIGNAL(login()));
	connect(buttonConnetti, SIGNAL(clicked()), loginWidget, SLOT(login()));
	connect(buttonAnnulla, SIGNAL(clicked()), this, SIGNAL(cancel()));
}

Login::~Login()
{
	delete dbmsType;
	delete loginWidget;
	delete buttonConnetti;
	delete buttonAnnulla;
	delete buttonLayout;
	delete mainLayout;
}

void Login::changeLoginWidget(int index)
{
	delete loginWidget;
	switch (index)
	{
		case sqlite3Index:
			loginWidget = new Sqlite3Login(database, connector, parentWindow, this);
			break;

		case postgresqlIndex:
			loginWidget = new PgLogin(database, connector, parentWindow, this);
			break;
	}
	connect(buttonConnetti, SIGNAL(clicked()), loginWidget, SLOT(login()));
	connect(loginWidget, SIGNAL(done()), this, SIGNAL(login()));
	mainLayout->addWidget(loginWidget, 1, 0, Qt::AlignCenter);
}
