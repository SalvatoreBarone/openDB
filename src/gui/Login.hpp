/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __LOGIN_WIDGET_H
#define __LOGIN_WIDGET_H

#include <QWidget>
#include <string>


class QComboBox;
class QPushButton;
class QHBoxLayout;
class QGridLayout;

namespace openDB
{

class MainWindow;
class LoginWidget;
class Database;
class BackendConnector;

class Login : public QWidget
{
	Q_OBJECT
public:
	explicit Login(		Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent);

	virtual ~Login();

signals:
	void login(); // emesso se il login va a buon fine
	void cancel();	// emesso se il login viene annullato

private:
	MainWindow* parentWindow;
	Database*& database;
	BackendConnector*& connector;

	QComboBox* dbmsType;
	LoginWidget* loginWidget;
	QPushButton* buttonConnetti;
	QPushButton* buttonAnnulla;
	QHBoxLayout* buttonLayout;
	QGridLayout* mainLayout;

	static const QString sqlite3CBoxEntry;
	static const int sqlite3Index = 0;
	static const QString postgresqlCBoxEntry;
	static const int postgresqlIndex = 1;

private slots:
	void changeLoginWidget(int index);

};

};

#endif
