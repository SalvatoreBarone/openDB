/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __BASIC_LOGIN_WIDGET_H
#define __BASIC_LOGIN_WIDGET_H

#include <QWidget>

namespace openDB
{

class Database;
class BackendConnector;
class MainWindow;

class LoginWidget : public QWidget
{
	Q_OBJECT
public:
	explicit LoginWidget(	Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent) :
		QWidget(__parent),
		database(__database),
		connector(__connector),
		window(__window)
	{}

signals:
	void done(); //emesso quando il login è completato con successo e le strutture correttamente inizializzate

public slots:
	virtual void login() noexcept = 0; // interfaccia allo slot che effettua tutte le operazioni necessarie alla connessione
					   // deve emettere il segnale done al completamento del download e gestire internamente
					   // tutte le eccezioni
protected:
	Database*& database;
	BackendConnector*& connector;
	MainWindow* window;
	static const std::string loginConfig; // nome del gruppo di configurazioni per il login
};

}

#endif
