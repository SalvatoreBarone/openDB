
/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "MainWindow.hpp"

using namespace openDB;

#include "core/Table.hpp"
#include "core/Database.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"

#include <string>
#include <iostream>

#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QMessageBox>
#include <QTabWidget>
#include <QString>
#include <QProgressDialog>
#include <QFileDialog>
#include <QFileInfo>

#include "Login.hpp"
#include "DatabaseMenu.hpp"
#include "LogViewer.hpp"
#include "ResultMenu.hpp"
#include "SessionManager.hpp"




MainWindow::MainWindow (QWidget *parent, Qt::WindowFlags flags) :
		QMainWindow(parent, flags),
		connector(0),
		database(0),
		loginWidget(0),
		sessionWidget(0),
		openDBMenu(0),
		toolMenu(0),
		databaseMenu(0),
		databaseAction(0),
		resultMenu(0),
		menuAbout(0)
{
	showMaximized();

	// creazione della working directory
	workspaceDirectory = QDir::homePath() + "/.openDB2/";
	configFile = workspaceDirectory + "setting.cfg";
	lastDir = QDir::homePath() + "/";
	QDir dir(QDir::homePath());
	dir.mkdir(QString::fromStdString(".openDB2"));

	createDBMenu();
}

MainWindow::~MainWindow()
{
	deleteDBMenu();
	deleteToolMenu();
	deleteDatabaseMenu();
	deleteResultMenu();
	deleteAboutMenu();
	if (loginWidget) delete loginWidget;
	if (sessionWidget) delete sessionWidget;
}

void MainWindow::slotLogin()
{
	loginWidget = new Login(database, connector, this, this);
	setCentralWidget(loginWidget);
	connect(loginWidget, SIGNAL(login()), this, SLOT(slotConnesso()));
	connect(loginWidget, SIGNAL(cancel()), this, SLOT(close()));
}

void MainWindow::setLastDir(const QString& fileName) noexcept
{
	if (!fileName.isEmpty())
		lastDir = QFileInfo(fileName).absolutePath() + "/";
}

void MainWindow::addToResultMenu(BackendConnector::Iterator  it)
{
	ResultMenu* newResult = new ResultMenu(QString::fromStdString(it.result().resultTable().name()), it, this, this);
	resultMenu->addMenu(newResult);
}
void MainWindow::createDBMenu()
{
	openDBMenu = menuBar()->addMenu("openDB");
	actionConnetti = new QAction("Nuova Connessione", this);
	actionTestConnessione = new QAction("Test Connessione", this);
	actionResetConnessione = new QAction("Reset Connessione", this);
	actionNuovaSessione = new QAction("Nuova Sessione", this);
	actionChiudiSessione = new QAction("Chiudi Sessione", this);
	actionEsci = new QAction("Esci", this);

	openDBMenu->addAction(actionConnetti);
	openDBMenu->addAction(actionTestConnessione);
	openDBMenu->addAction(actionResetConnessione);
	openDBMenu->addAction(actionNuovaSessione);
	openDBMenu->addAction(actionChiudiSessione);
	openDBMenu->addAction(actionEsci);

	actionTestConnessione->setEnabled(false);
	actionResetConnessione->setEnabled(false);
	actionNuovaSessione->setEnabled(false);
	actionChiudiSessione->setEnabled(false);

	connect(actionConnetti, SIGNAL(triggered()), this, SLOT(slotLogin()));
	connect(actionTestConnessione, SIGNAL(triggered()), this, SLOT(slotTestConnessione()));
	connect(actionResetConnessione, SIGNAL(triggered()), this, SLOT(slotResetConnessione()));
	connect(actionNuovaSessione, SIGNAL(triggered()), this, SLOT(slotNuovaSessione()));
	connect(actionChiudiSessione, SIGNAL(triggered()), this, SLOT(slotChiudiSessione()));
	connect(actionEsci, SIGNAL(triggered()), this, SLOT(slotEsci()));
}

void MainWindow::deleteDBMenu()
{
	if (openDBMenu)
	{
		delete openDBMenu;
		delete actionConnetti;
		delete actionTestConnessione;
		delete actionResetConnessione;
		delete actionNuovaSessione;
		delete actionChiudiSessione;
		delete actionEsci;
	}
}

void MainWindow::createToolMenu()
{
	toolMenu = menuBar()->addMenu("Strumenti");
	actionSendQuery = new QAction("Invia Query in Sql", this);
	actionViewLog = new QAction("Visualizza log", this);

	toolMenu->addAction(actionSendQuery);
	toolMenu->addAction(actionViewLog);

	connect(actionSendQuery, SIGNAL(triggered()), this, SLOT(slotSendQuery()));
	connect(actionViewLog, SIGNAL(triggered()), this, SLOT(slotViewLog()));
}

void MainWindow::deleteToolMenu()
{
	if (toolMenu)
	{
		delete toolMenu;
		delete actionSendQuery;
		delete actionViewLog;
	}
}

void MainWindow::createDatabaseMenu()
{
	databaseMenu = menuBar()->addMenu("Database");
	databaseAction = new DatabaseMenu(database, this);
	databaseMenu->addMenu(databaseAction);
}

void MainWindow::deleteDatabaseMenu()
{
	if (databaseMenu)
	{
		delete databaseMenu;
		delete databaseAction;
	}
}

void MainWindow::createResultMenu()
{
	resultMenu = menuBar()->addMenu("Result");
	actionCleanResult = new QAction ("Pulisci", resultMenu);
	resultMenu->addAction(actionCleanResult);
	connect(actionCleanResult, SIGNAL(triggered()), this, SLOT(slotCleanResult()));
}

void MainWindow::deleteResultMenu()
{
	if (resultMenu)
	{
		std::list<ResultMenu*>::iterator it = resultsMenuList.begin(), end = resultsMenuList.end();
		for (; it != end; it++)
		{
			QAction* menuAction = (*it)->menuAction();
			resultMenu->removeAction(menuAction);
			delete (*it);
		}
		resultMenu->removeAction(actionCleanResult);
		delete actionCleanResult;
		delete resultMenu;
	}

}

void MainWindow::createAboutMenu()
{
	menuAbout = menuBar()->addMenu("About");
	actionLicense = new QAction("Licenza d'uso", this);
	actionAboutQt = new QAction("About Qt", this);

	menuAbout->addAction(actionAboutQt);
	menuAbout->addAction(actionLicense);

	connect (actionAboutQt, SIGNAL(triggered()), this, SLOT(slotAboutQt()));
	connect (actionLicense, SIGNAL(triggered()), this, SLOT(slotLicense()));
}

void MainWindow::deleteAboutMenu()
{
	if (menuAbout)
	{
		delete menuAbout;
		delete actionLicense;
		delete actionAboutQt;
	}
}

void MainWindow::slotConnesso()
{
	//createDBMenu();
	createToolMenu();
	createResultMenu();
	createDatabaseMenu();
	createAboutMenu();

	actionConnetti->setEnabled(false);
	actionTestConnessione->setEnabled(true);
	actionResetConnessione->setEnabled(true);
	actionNuovaSessione->setEnabled(true);
	actionChiudiSessione->setEnabled(true);

	sessionWidget = new SessionManager(this, this);
	setCentralWidget(sessionWidget);
	loginWidget = 0;
}

void MainWindow::slotTestConnessione()
{
	try {connector->test();}
	catch (BackendException& e)
	{
		if
		(
			QMessageBox::warning(	this,
						"Errore di connessione",
						QString(QString(e.what()) + QString("\nRitentare il login?")),
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		)
			slotLogin();
		else
			close();

	}
}

void MainWindow::slotResetConnessione()
{
	connector->reset();
}


void MainWindow::slotEsci()
{
	if
	(
		QMessageBox::question (
				this,
				"",
				"Sicuro di voler uscire?",
				QMessageBox::Yes | QMessageBox::No) == QMessageBox::No
	)
		return;

	connector->disconnect();
	close();
}

void MainWindow::slotSendQuery()
{
	sessionWidget->setSession("Invio Query Sql", new SendQuery(this, sessionWidget));
}



void MainWindow::slotCleanResult()
{

	if(	QMessageBox::warning(	this,
					"Attenzione!",
					"Assicurati che non ci siano operazioni pendenti che usano i result caricati.\nContinuare?",
					QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)

	{
		std::list<ResultMenu*>::iterator menu_it = resultsMenuList.begin(), menu_end = resultsMenuList.end();

		for (; menu_it != menu_end; menu_it++)
		{
			QAction* menuAction = (*menu_it)->menuAction();
			resultMenu->removeAction(menuAction);
			connector->deleteTransaction((*menu_it)->getIterator());
			delete (*menu_it);
		}


#ifdef __CLEAN_ALL_RESULT
		openDB::BackendConnector::Iterator back_it = connector.begin(), back_end = connector.end();
		for (; back_it != back_end; back_it++)
			connector.deleteTransaction(back_it);
#endif
	}

}

void MainWindow::slotViewLog()
{
	setSessionWidget("Log di esecuzione query", new LogViewer(connector, sessionWidget));
}

void MainWindow::slotLicense()
{
	QString licese = "\
Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>\n\
\n\
This program is free software; you can redistribute it and/or modify it under the terms of the \
GNU General Public License as published by the Free Software Foundation; either version 3 of the \
License, or any later version.\n\
\n\
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without \
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU \
General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License along with this program; if \
not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA \
02110-1301, USA.\
";
	QMessageBox::information(this, "Licenza d'uso", licese, QMessageBox::Ok);
}

void MainWindow::slotAboutQt()
{
	QMessageBox::aboutQt(this, "About Qt");
}
