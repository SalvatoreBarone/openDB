/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __MAIN_WINDOW_H
#define __MAIN_WINDOW_H

#include <QMainWindow>
#include <QString>

#include "Login.hpp"
#include "SendQuery.hpp"
#include "SessionManager.hpp"
#include "core/BackendConnector.hpp"

class QMenu;
class QAction;
class QTabWidget;

namespace openDB
{
class Database;
class BackendConnector;
class Login;
class DatabaseMenu;
class ResultMenu;

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:

	explicit MainWindow (QWidget *parent = 0, Qt::WindowFlags flags = 0);

	virtual ~MainWindow();

	/**
	 * Restituisce il percorso della directory contenente file di configurazione ed history
	 * @return
	 */
	const QString& getWorkspaceDirectory() const noexcept
		{return workspaceDirectory;}

	const QString& getConfigFile() const noexcept
		{return configFile;}

	const QString& getLastDir() const noexcept
		{return lastDir;}

	void setLastDir(const QString& fileName) noexcept;

	BackendConnector& backendConnector() noexcept
		{return *connector;}

	Database& managedDatabase() noexcept
		{return *database;}

	void setSessionWidget(const QString& sessionName, QWidget* widget) noexcept
		{ if (sessionWidget) sessionWidget->setSession(sessionName, widget); }

	void addToResultMenu(BackendConnector::Iterator it);

protected:
	QString workspaceDirectory;
	QString configFile;
	QString lastDir;

	BackendConnector* connector;
	Database* database;

	Login* loginWidget;
	SessionManager* sessionWidget;

	QMenu* openDBMenu;
	QAction* actionConnetti;
	QAction* actionTestConnessione;
	QAction* actionResetConnessione;
	QAction* actionNuovaSessione;
	QAction* actionChiudiSessione;
	QAction* actionEsci;
	virtual void createDBMenu();
	virtual void deleteDBMenu();

	QMenu* toolMenu;
	QAction* actionSendQuery;
	QAction* actionViewLog;
	virtual void createToolMenu();
	virtual void deleteToolMenu();

	QMenu* databaseMenu;
	DatabaseMenu* databaseAction;
	virtual void createDatabaseMenu();
	virtual void deleteDatabaseMenu();

	QMenu* resultMenu;
	QAction* actionCleanResult;
	std::list<ResultMenu*> resultsMenuList;
	virtual void createResultMenu();
	virtual void deleteResultMenu();

	QMenu* menuAbout;
	QAction* actionLicense;
	QAction* actionAboutQt;
	virtual void createAboutMenu();
	virtual void deleteAboutMenu();

protected slots:
	/* Ridefinendo questa funzione si può personalizzare la procedura di login ad un database.
	 * La seguente fa uso della classe standard "Login" che permette di scegliere, innanzitutto, il dbms che gestisce il
	 * database al quale si desidera connettersi e i parametri di connessione.
	 */
	virtual void slotLogin();

	/* Ridefinendo questo slot può personalizzare le operazioni eseguite dopo il login
	 * La funzione ridefinita dovrà necessariamente chiamare questa funzione.
	 */
	virtual void slotConnesso();

private slots:
	void slotTestConnessione();
	void slotResetConnessione();

	void slotNuovaSessione()
		{ if (sessionWidget) sessionWidget->newSession(); }
	void slotChiudiSessione()
		{ if (sessionWidget) sessionWidget->closeCurrentSession(); }

	void slotEsci();

	void slotSendQuery();
	void slotViewLog();

	void slotCleanResult();

	void slotLicense();
	void slotAboutQt();

};

};

#endif
