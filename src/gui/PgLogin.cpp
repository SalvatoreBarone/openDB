/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "PgLogin.hpp"
#include "MainWindow.hpp"
#include "core/DatabaseBuilder.hpp"
#include "core/PgConnector.hpp"
#include "core/PgConnection.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"

#include <QtGui>
#include <QMessageBox>

using namespace openDB;

const std::string PgLogin::postgresqlSetting = "postgresql";
const std::string PgLogin::hostSetting = "host";
const std::string PgLogin::portSetting = "port";
const std::string PgLogin::dbnameSetting = "dbname";
const std::string PgLogin::userSetting = "username";
const std::string PgLogin::passwordSetting = "password";
const std::string PgLogin::timeoutSetting = "timeout";

openDB::PgLogin::PgLogin(	Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent) :
		LoginWidget(__database, __connector, __window, __parent),
		labelHost(new QLabel("Indirizzo", this)),
		editHost( new QLineEdit(this)),
		labelPort(new QLabel("Porta TCP", this)),
		editPort( new QLineEdit(this)),
		labelDBname(new QLabel("Database", this)),
		editDBname( new QLineEdit(this)),
		labelUsername(new QLabel("Username", this)),
		editUsername( new QLineEdit(this)),
		labelPassword(new QLabel("Password", this)),
		editPassword(new QLineEdit(this)),
		labelTimeout(new QLabel("Timeout", this)),
		editTimeout(new QSpinBox(this)),
		gridLayout(new QGridLayout(this))
{
	setLayout(gridLayout);
	gridLayout->addWidget(labelHost, 0, 0);
	gridLayout->addWidget(editHost, 0, 1);
	gridLayout->addWidget(labelPort, 1, 0);
	gridLayout->addWidget(editPort, 1, 1);
	gridLayout->addWidget(labelDBname, 2, 0);
	gridLayout->addWidget(editDBname, 2, 1);
	gridLayout->addWidget(labelUsername, 3, 0);
	gridLayout->addWidget(editUsername, 3, 1);
	gridLayout->addWidget(labelPassword, 4, 0);
	gridLayout->addWidget(editPassword, 4, 1);
	gridLayout->addWidget(labelTimeout, 5, 0);
	gridLayout->addWidget(editTimeout, 5, 1);
	editPassword->setEchoMode(QLineEdit::Password);
	loadConfig();
}

openDB::PgLogin::~PgLogin()
{
	delete labelHost;
	delete editHost;
	delete labelPort;
	delete editPort;
	delete labelDBname;
	delete editDBname;
	delete labelUsername;
	delete editUsername;
	delete labelPassword;
	delete editPassword;
	delete labelTimeout;
	delete editTimeout;
	delete gridLayout;
}

void openDB::PgLogin::loadConfig() noexcept
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(window->getConfigFile().toStdString().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(LoginWidget::loginConfig) &&
			root[LoginWidget::loginConfig].isGroup() &&
			root[LoginWidget::loginConfig].exists(postgresqlSetting) &&
			root[LoginWidget::loginConfig][postgresqlSetting].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		) {
			libconfig::Setting& postgres = root[LoginWidget::loginConfig][postgresqlSetting];
			editHost->setText(QString::fromStdString(postgres[hostSetting]));
			editPort->setText(QString::fromStdString(postgres[portSetting]));
			editDBname->setText(QString::fromStdString(postgres[dbnameSetting]));
			editUsername->setText(QString::fromStdString(postgres[userSetting]));
			editPassword->setText(QString::fromStdString(postgres[passwordSetting]));
			int timeout = postgres[timeoutSetting];
			editTimeout->setValue(timeout);
		}

	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e) {
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e) {
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void openDB::PgLogin::storeConfig() noexcept
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(window->getConfigFile().toStdString().c_str());

		libconfig::Setting &root = cfg.getRoot();
		if (!root.exists(LoginWidget::loginConfig))
			addConfig(cfg);
		else {
			libconfig::Setting& login = root[LoginWidget::loginConfig];
			if (!login.isGroup()) {
				root.remove(LoginWidget::loginConfig);
				addConfig(cfg);
			}
			else {
				if (!login.exists(postgresqlSetting))
					addConfig(cfg);
				else {
					libconfig::Setting& postgresql = login[postgresqlSetting];
					if (!postgresql.isGroup()) {
						login.remove(postgresqlSetting);
						addConfig(cfg);
					}
					else {
						postgresql[hostSetting] = editHost->text().toStdString();
						postgresql[portSetting] = editPort->text().toStdString();
						postgresql[dbnameSetting] = editDBname->text().toStdString();
						postgresql[userSetting] = editUsername->text().toStdString();
						postgresql[passwordSetting] = editPassword->text().toStdString();
						postgresql[timeoutSetting] = editTimeout->value();
					}
				}
			}
		}
		cfg.writeFile(window->getConfigFile().toStdString().c_str());
	}
	catch (libconfig::FileIOException& e)
	{
		addConfig(cfg);
		cfg.writeFile(window->getConfigFile().toStdString().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void openDB::PgLogin::login() noexcept
{
	PgConnector* _connector = new PgConnector;
	_connector->hostaddr(editHost->text().toStdString());
	_connector->port(editPort->text().toStdString());
	_connector->dbname(editDBname->text().toStdString());
	_connector->username(editUsername->text().toStdString());
	_connector->password(editPassword->text().toStdString());
	_connector->timeout(editTimeout->value());

	try {
		_connector->connect();

		std::string querycmd = PgConnection::pgDbStructureQuery;
		NonTransactional txn;
		txn.appendCommand(querycmd);
		BackendConnector::Iterator bit = _connector->execTransaction(txn);
		Table table = bit.result().resultTable();

		DatabaseBuilder dbuilder(_connector->dbname());
		Database* _database = new Database;
		*_database = dbuilder.fromTable(table);
		_connector->deleteTransaction(bit);

		if (connector)
			delete connector;
		connector = _connector;

		if (database)
			delete database;
		database = _database;

		storeConfig();
		emit done();
	}
	catch (BackendException& e) {
		QMessageBox::warning(this, "Errore durante la Connessione", e.what(), QMessageBox::Ok);
		delete _connector;
	}
	catch (DatabaseBuilderException& e){
		QMessageBox::warning(this, "Errore durante la Connessione", e.what(), QMessageBox::Ok);
		delete _connector;
	}
}

void openDB::PgLogin::addConfig(libconfig::Config& cfg) const noexcept
{
	libconfig::Setting &root = cfg.getRoot();
	libconfig::Setting& login = (!root.exists(LoginWidget::loginConfig) ? root.add(LoginWidget::loginConfig, libconfig::Setting::TypeGroup) : root[LoginWidget::loginConfig]);
	libconfig::Setting& postgresql = (!login.exists(postgresqlSetting) ? login.add(postgresqlSetting, libconfig::Setting::TypeGroup) : login[postgresqlSetting]);

	postgresql.add(hostSetting, libconfig::Setting::TypeString) = editHost->text().toStdString();
	postgresql.add(portSetting, libconfig::Setting::TypeString) = editPort->text().toStdString();
	postgresql.add(dbnameSetting, libconfig::Setting::TypeString) = editDBname->text().toStdString();
	postgresql.add(userSetting, libconfig::Setting::TypeString) = editUsername->text().toStdString();
	postgresql.add(passwordSetting, libconfig::Setting::TypeString) = editPassword->text().toStdString();
	int value = editTimeout->value();
	postgresql.add(timeoutSetting, libconfig::Setting::TypeInt) = value;
}
