/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __POSTGRESQL_LOGIN_WIDGET_H
#define __POSTGRESQL_LOGIN_WIDGET_H

#include "LoginWidget.hpp"
#include <libconfig.h++>
#include <string>
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class QGridLayout;

namespace openDB
{

class PgLogin : public LoginWidget
{
	Q_OBJECT
public:
	explicit PgLogin (	Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent);

	virtual ~PgLogin();

public slots:
	virtual void login() noexcept; // slot di connessione per postgres

private:
	QLabel* labelHost;
	QLineEdit* editHost;
	QLabel* labelPort;
	QLineEdit* editPort;
	QLabel* labelDBname;
	QLineEdit* editDBname;
	QLabel* labelUsername;
	QLineEdit* editUsername;
	QLabel* labelPassword;
	QLineEdit* editPassword;
	QLabel* labelTimeout;
	QSpinBox* editTimeout;
	QGridLayout* gridLayout;

	static const std::string postgresqlSetting;
	static const std::string hostSetting;
	static const std::string portSetting;
	static const std::string dbnameSetting;
	static const std::string userSetting;
	static const std::string passwordSetting;
	static const std::string timeoutSetting;
	void loadConfig() noexcept;
	void storeConfig() noexcept;
	void addConfig(libconfig::Config& cfg) const noexcept;
};

};

#endif
