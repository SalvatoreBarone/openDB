/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "QuerySetting.hpp"

#include "core/Column.hpp"
#include "core/BackendConnector.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"

#include "MainWindow.hpp"
#include "TableView.hpp"

#include <QtGui>
#include <QMainWindow>
#include <QString>

using namespace openDB;

QuerySettingWidget::QuerySettingWidget (Table* table, MainWindow* window, TableMenu* menu) noexcept :
		QWidget(window),
		parentTable(table),
		parentWindow(window),
		parentMenu(menu),
		viewport(new QWidget(this)),
		scrollArea(new QScrollArea(this)),
		mainLayout(new QHBoxLayout(this)),

		columnName(new QLabel("Colonna", this)),
		project(new QLabel("Proietta", this)),
		select(new QLabel("Seleziona", this)),
		compareOperator(new QLabel("Op. Compar.", this)),
		selectValue(new QLabel("Valore Selezione", this)),
		orderBy(new QLabel("Ordina", this)),
		orderMode(new QLabel("Modo Ord.", this)),
		buttonSave(new QPushButton("Salva", this)),
		buttonSaveAndLoad(new QPushButton("Salva e Carica", this)),
		buttonLayout(new QHBoxLayout()),
		gridLayout(new QGridLayout())
{
	gridLayout->addWidget(columnName, 0, ColumnWidget::columnNameColIndex);
	gridLayout->addWidget(project, 0, ColumnWidget::projectColIndex);
	gridLayout->addWidget(select, 0, ColumnWidget::selectColIndex);
	gridLayout->addWidget(compareOperator, 0, ColumnWidget::compareOperatorColIndex);
	gridLayout->addWidget(selectValue, 0, ColumnWidget::selectValueColIndex);
	gridLayout->addWidget(orderBy, 0, ColumnWidget::orderByColIndex);
	gridLayout->addWidget(orderMode, 0, ColumnWidget::orderModeColIndex);

	int i = 1;
	Table::Iterator it = parentTable->begin(), end = parentTable->end();
	for (; it != end; it++, i++)
		widgetsList.push_back(new ColumnWidget(it, gridLayout, i, this));

	buttonLayout->addWidget(buttonSave);
	buttonLayout->addWidget(buttonSaveAndLoad);
	gridLayout->addLayout(buttonLayout, i, 0, 1, ColumnWidget::orderModeColIndex+1, Qt::AlignCenter | Qt::AlignBottom);

	viewport->setLayout(gridLayout);
	scrollArea->setWidget(viewport);
	scrollArea->setWidgetResizable(true);
	mainLayout->addWidget(scrollArea);
	setLayout(mainLayout);

	connect(buttonSave, SIGNAL(clicked()), this, SLOT(storeConfig()));
	connect(buttonSaveAndLoad, SIGNAL(clicked()), this, SLOT(storeAndLoad()));
	loadConfig();
}

QuerySettingWidget::~QuerySettingWidget()
{
	gridLayout->removeWidget(columnName);
	gridLayout->removeWidget(project);
	gridLayout->removeWidget(select);
	gridLayout->removeWidget(compareOperator);
	gridLayout->removeWidget(selectValue);
	gridLayout->removeWidget(orderMode);
	gridLayout->removeWidget(orderBy);
	gridLayout->removeItem(buttonLayout);
	buttonLayout->removeWidget(buttonSave);

	std::list<ColumnWidget*>::iterator it = widgetsList.begin(), end = widgetsList.end();
	for (; it != end; it++)
		delete *it;

	delete columnName;
	delete project;
	delete select;
	delete compareOperator;
	delete selectValue;
	delete orderBy;
	delete orderMode;
	delete buttonLayout;
	delete gridLayout;
}

void QuerySettingWidget::loadConfig() noexcept
{
	std::list<ColumnWidget*>::iterator it = widgetsList.begin(), end = widgetsList.end();
	for (; it != end; it++)
		(*it)->loadConfig();
}

void QuerySettingWidget::storeConfig() noexcept
{
	std::list<ColumnWidget*>::iterator it = widgetsList.begin(), end = widgetsList.end();
	for (; it != end; it++)
		(*it)->storeConfig();
}

QuerySettingWidget::ColumnWidget::ColumnWidget(Table::Iterator it, QGridLayout* l, int row, QWidget* parent) noexcept:
		iterator(it),
		layout(l),
		columnName(new QLabel(QString::fromStdString(it->name()), parent)),
		project(new QCheckBox(parent)),
		select(new QCheckBox(parent)),
		compareOperator(createCompareOperatorComboBox(parent)),
		selectValue(new QLineEdit(parent)),
		orderBy(new QCheckBox(parent)),
		orderMode(createOrderModeComboBox(parent))
{
	layout->addWidget(columnName, row, columnNameColIndex);
	layout->addWidget(project, row, projectColIndex);
	layout->addWidget(select, row, selectColIndex);
	layout->addWidget(compareOperator, row, compareOperatorColIndex);
	layout->addWidget(selectValue, row, selectValueColIndex);
	layout->addWidget(orderBy, row, orderByColIndex);
	layout->addWidget(orderMode, row, orderModeColIndex);
}

QuerySettingWidget::ColumnWidget::~ColumnWidget()
{
	layout->removeWidget(columnName);
	layout->removeWidget(project);
	layout->removeWidget(select);
	layout->removeWidget(compareOperator);
	layout->removeWidget(selectValue);
	layout->removeWidget(orderBy);
	layout->removeWidget(orderMode);

	delete columnName;
	delete project;
	delete select;
	delete selectValue;
	delete compareOperator;
	delete orderBy;
	delete orderMode;
}

void QuerySettingWidget::ColumnWidget::loadConfig() noexcept
{
	QuerySetting setting = iterator->querySetting();

	if (setting.project()) project->setChecked(true);
	if (setting.select()) select->setChecked(true);
	compareOperator->setCurrentIndex(compareOperator->findData(QVariant(setting.compareOperatorEnum())));
	selectValue->setText(QString::fromStdString(setting.selectValue()));
	if (setting.orderBy()) orderBy->setChecked(true);
	orderMode->setCurrentIndex(orderMode->findData(QVariant(setting.orderModeEnum())));
}

void QuerySettingWidget::ColumnWidget::storeConfig() noexcept
{
	QuerySetting setting;
	if (project->isChecked()) setting.project(true);
	if (select->isChecked()) setting.select(true);
	setting.compareOperator((QuerySetting::sqlCompareOperator)compareOperator->currentIndex());
	setting.selectValue(selectValue->text().toStdString());
	if (orderBy->isChecked()) setting.orderBy(true);
	setting.orderMode((QuerySetting::sqlOrder)orderMode->currentIndex());
	iterator->setQuerySetting(setting);
}

QComboBox* QuerySettingWidget::ColumnWidget::createCompareOperatorComboBox(QWidget* parent) noexcept
{
	QVariant	more(QuerySetting::more),
			moreEqual(QuerySetting::moreEqual),
			equal(QuerySetting::equal),
			disequal(QuerySetting::disequal),
			like(QuerySetting::like),
			notLike(QuerySetting::notLike),
			lessEqual(QuerySetting::lessEqual),
			less(QuerySetting::less),
			in(QuerySetting::in),
			notIn(QuerySetting::notIn);

	QComboBox* cbox = new QComboBox(parent);
	cbox -> addItem(">", more);
	cbox -> addItem(">=", moreEqual);
	cbox -> addItem("=", equal);
	cbox -> addItem("<>", disequal);
	cbox -> addItem("like", like);
	cbox -> addItem("not like", notLike);
	cbox -> addItem("<=", lessEqual);
	cbox -> addItem("<", less);
	cbox -> addItem("in", in);
	cbox -> addItem("not in", notIn);
	return cbox;
}

QComboBox* QuerySettingWidget::ColumnWidget::createOrderModeComboBox(QWidget* parent) noexcept
{
	QVariant asc(QuerySetting::asc), desc(QuerySetting::desc);
	QComboBox* cbox = new QComboBox(parent);
	cbox -> addItem("crescente", asc);
	cbox -> addItem("decrescente", desc);
	return cbox;
}

void QuerySettingWidget::loadTuples()
{
	std::string command = parentTable->loadCommand(true);
	NonTransactional txn;
	txn.appendCommand(command);
	BackendConnector::Iterator it = parentWindow->backendConnector().execTransaction(txn);
	Table result = it.result().resultTable();
	parentTable->loadRecord(result);
	parentWindow->backendConnector().deleteTransaction(it);
}

void QuerySettingWidget::storeAndLoad()
{
	storeConfig();
	loadTuples();
	parentWindow->setSessionWidget(	QString::fromStdString(parentTable->name()),
					new TableView(parentTable, true, parentWindow, parentMenu, parentWindow));
}
