/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __QUERY_SETTING_H
#define __QUERY_SETTING_H

#include <QWidget>
#include <list>
#include "core/Table.hpp"

class QLabel;
class QCheckBox;
class QComboBox;
class QLineEdit;
class QPushButton;
class QScrollArea;
class QGridLayout;
class QHBoxLayout;
class QMainWindow;

namespace openDB
{

class MainWindow;
class TableMenu;

class QuerySettingWidget : public QWidget
{
	Q_OBJECT
public:
	explicit QuerySettingWidget (Table* table, MainWindow* window, TableMenu* menu) noexcept;

	virtual ~QuerySettingWidget();

private:
	Table* parentTable;
	MainWindow* parentWindow;
	TableMenu* parentMenu;

	class ColumnWidget
	{
	public:
		ColumnWidget(Table::Iterator it, QGridLayout* layout, int row, QWidget* parent) noexcept;

		virtual ~ColumnWidget();

		void loadConfig() noexcept;
		void storeConfig() noexcept;

		static const int columnNameColIndex = 0;
		static const int projectColIndex = 1;
		static const int selectColIndex = 2;
		static const int compareOperatorColIndex = 3;
		static const int selectValueColIndex = 4;
		static const int orderByColIndex = 5;
		static const int orderModeColIndex = 6;

	private:
		Table::Iterator iterator;
		QGridLayout* layout;

		QLabel* columnName;
		QCheckBox* project;
		QCheckBox* select;
		QComboBox* compareOperator;
		QLineEdit* selectValue;
		QCheckBox* orderBy;
		QComboBox* orderMode;

		QComboBox* createCompareOperatorComboBox(QWidget* parent) noexcept;
		QComboBox* createOrderModeComboBox(QWidget* parent) noexcept;
	};

	QWidget* viewport;
	QScrollArea* scrollArea;
	QHBoxLayout* mainLayout;

	std::list<ColumnWidget*> widgetsList;

	QLabel* columnName;
	QLabel* project;
	QLabel* select;
	QLabel* compareOperator;
	QLabel* selectValue;
	QLabel* orderBy;
	QLabel* orderMode;

	QPushButton* buttonSave;
	QPushButton* buttonSaveAndLoad;
	QHBoxLayout* buttonLayout;

	QGridLayout* gridLayout;

private slots:
	void loadTuples();
	void loadConfig() noexcept;
	void storeConfig() noexcept;
	void storeAndLoad();

};

};


#endif
