/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "ResultMenu.hpp"
using namespace openDB;
#include "openFileMacro.hpp"
#include "MainWindow.hpp"
#include "TableView.hpp"
#include "CsvExportSetting.hpp"

#include <QAction>
#include <QMessageBox>
#include <QFileDialog>

#include "core/Table.hpp"
#include "core/CsvUpdateManager.hpp"

ResultMenu::ResultMenu (const QString& menuName, BackendConnector::Iterator  it, MainWindow* window, QWidget* parent) :
		QMenu(menuName, parent),
		iterator(it),
		parentWindow(window),
		actionInfo(new QAction("Stato di esecuzione", this)),
		actionView(new QAction("Visualizza result", this)),
		actionExport(new QAction("Esporta result su csv", this))
{
	addAction(actionInfo);
	addAction(actionView);
	addAction(actionExport);

	connect (actionInfo, SIGNAL(triggered()), this, SLOT(slotInfo()));
	connect (actionView, SIGNAL(triggered()), this, SLOT(slotView()));
	connect (actionExport, SIGNAL(triggered()), this, SLOT(slotExport()));
}

ResultMenu::~ResultMenu()
{
	delete actionInfo;
	delete actionView;
	delete actionExport;
}

void ResultMenu::slotInfo()
{
	QMessageBox::information(	parentWindow,
					"Info",
					QString::fromStdString(iterator.result().info()),
					QMessageBox::Ok);
}

void ResultMenu::slotView()
{
	parentWindow->setSessionWidget(	QString::fromStdString(iterator.result().resultTable().name()),
					new TableView(
						&(iterator.result().resultTable()),
						false,
						parentWindow,
						0,
						parentWindow));
}

void ResultMenu::slotExport()
{
	QString fileName =  QFileDialog::getSaveFileName (	this,
								"Export in formato csv",
								parentWindow->getLastDir() + QString::fromStdString(iterator.result().resultTable().name() + ".csv"),
								"csv file (*.csv)" );
	parentWindow->setLastDir(fileName);
	if ( !fileName.isEmpty() )
	{
		CsvUpdateManager::Setting par;
		CsvExportSettingDialog dialog(&par, parentWindow);
		dialog.exec();
		CsvUpdateManager manager(iterator.result().resultTable());
		manager.csvExport(fileName.toStdString(), par);


	}
}
