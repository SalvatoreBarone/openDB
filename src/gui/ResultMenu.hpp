/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __RESULT_MENU_H
#define __RESULT_MENU_H

#include <QMenu>
#include <QWidget>

#include "core/BackendConnector.hpp"

class QAction;

namespace openDB {

class MainWindow;

class ResultMenu : public QMenu
{
	Q_OBJECT
public:
	explicit ResultMenu (const QString& menuName, BackendConnector::Iterator it, MainWindow* window, QWidget* parent);

	virtual ~ResultMenu();

	BackendConnector::Iterator& getIterator()
		{return iterator;}
private:
	BackendConnector::Iterator iterator;
	MainWindow* parentWindow;

	QAction* actionInfo;
	QAction* actionView;
	QAction* actionExport;

private slots:
	void slotInfo();
	void slotView();
	void slotExport();

};

};

#endif
