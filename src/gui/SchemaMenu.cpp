/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "SchemaMenu.hpp"

#include "MainWindow.hpp"
#include "TableMenu.hpp"
#include "core/Table.hpp"
#include "core/Schema.hpp"

using namespace openDB;

SchemaMenu::SchemaMenu(	Schema* schema,
			MainWindow* window ) noexcept :
							QMenu((QMainWindow*)window),
							parentSchema(schema),
							parentWindow(window)
{
	setTitle(QString::fromStdString(parentSchema->name()));
	Schema::Iterator it = parentSchema->begin(), end = parentSchema->end();
	for(; it != end; it++)
	{
		TableMenu* table_menu = new TableMenu(&(*it), parentWindow);
		menuList.push_back(table_menu);
		addMenu(table_menu);
	}
}
									
			
SchemaMenu::~SchemaMenu()
{
	std::list<TableMenu*>::iterator it = menuList.begin(), end = menuList.end();
	for (; it != end; it++)
		delete *it;
}
