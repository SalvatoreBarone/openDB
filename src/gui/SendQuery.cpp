/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "SendQuery.hpp"
#include "core/BackendConnector.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"
#include "MainWindow.hpp"

#include <QtGui>
#include <QMainWindow>
#include <QMessageBox>

#include <string>
#include <typeinfo>
using namespace openDB;

SendQuery::SendQuery(MainWindow* window, QWidget* parent) :
		QWidget(parent),
		parentWindow(window),
		layout(new QGridLayout(this)),
		editCommand(new QTextEdit(this)),
		checkboxTransaction(new QCheckBox("Singola Transazione", this)),
		buttonSend(new QPushButton("Invia", this)),
		buttonLayout(new QHBoxLayout())
{
	setLayout(layout);

	layout->addWidget(editCommand, 0, 0);
	buttonLayout->addWidget(checkboxTransaction);
	buttonLayout->addWidget(buttonSend);
	layout->addLayout(buttonLayout, 1, 0, Qt::AlignHCenter | Qt::AlignBottom);

	connect(buttonSend, SIGNAL(clicked()), this, SLOT(slotOkClicked()));
}

SendQuery::~SendQuery()
{

	layout->removeWidget(editCommand);
	buttonLayout->removeWidget(checkboxTransaction);
	buttonLayout->removeWidget(buttonSend);
	layout->removeItem(buttonLayout);

	delete editCommand;
	delete buttonSend;
	delete checkboxTransaction;
	delete buttonLayout;
	delete layout;
}

void SendQuery::slotOkClicked()
{
	std::string command = editCommand->toPlainText().toStdString();
	if (checkboxTransaction->isChecked())
	{
		Transaction txn(command);
		parentWindow->addToResultMenu(parentWindow->backendConnector().execTransaction(txn));
	}
	else
	{
		NonTransactional txn(command);
		parentWindow->addToResultMenu(parentWindow->backendConnector().execTransaction(txn));
	}
}

