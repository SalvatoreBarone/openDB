/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "SessionManager.hpp"
using namespace openDB;

#include "MainWindow.hpp"

SessionManager::SessionManager (MainWindow* window, QWidget* parent) :
	QTabWidget(parent),
	parentWindow(window)
{
	addTab(new QWidget(this), "Sessione 1");
}

SessionManager::~SessionManager()
{

}

void SessionManager::newSession()
{
	if (count() < maxSessions)
	{
		std::string sessionName = "Sessione " + std::to_string(count() + 1);
		int current = addTab(new QWidget(this), QString::fromStdString(sessionName));
		setCurrentIndex(current);
	}
}

void SessionManager::closeCurrentSession()
{

	if (count() > 1)
	{
		int current = currentIndex();
		QWidget* oldwidget = widget(current);

		removeTab(current);
		delete oldwidget;
	}
}

void SessionManager::setSession(const QString& sessionName, QWidget* newwidget)
{
	int current = currentIndex();
	QWidget* oldwidget = widget(current);

	removeTab(current);
	delete oldwidget;

	insertTab(current, newwidget, sessionName);
	setCurrentIndex(current);


}
