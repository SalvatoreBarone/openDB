/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __SESSION_MANAGER_H
#define __SESSION_MANAGER_H

#include <QTabWidget>
#include <QThread>

#include <vector>

namespace openDB
{

class MainWindow;

class SessionManager : public QTabWidget
{
	Q_OBJECT
public:
	explicit SessionManager (MainWindow* window, QWidget* parent);

	virtual ~SessionManager();

	void newSession();

	void closeCurrentSession();

	void setSession(const QString& sessionName, QWidget* widget);

	static const unsigned short maxSessions = 15;

private:
	MainWindow* parentWindow;
};

};

#endif
