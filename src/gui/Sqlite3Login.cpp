/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "Sqlite3Login.hpp"
#include "MainWindow.hpp"
#include "core/Database.hpp"
#include "core/DatabaseBuilder.hpp"
#include "core/Sqlite3Connector.hpp"
#include "openFileMacro.hpp"
#include <QtGui>
#include <QFileDialog>
#include <QString>
using namespace openDB;

const std::string Sqlite3Login::sqlite3Setting = "sqlite3";
const std::string Sqlite3Login::dbFileSetting = "dbfile";
const std::string Sqlite3Login::configFileSetting = "configfile";

Sqlite3Login::Sqlite3Login(	Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent) :
	LoginWidget(__database, __connector, __window, __parent),
	labelDbFile(new QLabel("Database", this)),
	editDbFile(new QLineEdit(this)),
	buttonDbFile(new QPushButton("Apri", this)),
	labelConfigFile(new QLabel("File di configurazione", this)),
	editConfigFile(new QLineEdit(this)),
	buttonConfigFile(new QPushButton("Apri", this)),
	layout(new QGridLayout(this))
{
	setLayout(layout);
	layout->addWidget(labelDbFile, 0, 0);
	layout->addWidget(editDbFile, 0, 1);
	layout->addWidget(buttonDbFile, 0, 2);
	layout->addWidget(labelConfigFile, 1, 0);
	layout->addWidget(editConfigFile, 1, 1);
	layout->addWidget(buttonConfigFile, 1, 2);

	connect(buttonDbFile, SIGNAL(clicked()), this, SLOT(openDbFile()));
	connect(buttonConfigFile, SIGNAL(clicked()), this, SLOT(openConfigFile()));
	loadConfig();
}

Sqlite3Login::~Sqlite3Login()
{
	delete labelDbFile;
	delete editDbFile;
	delete buttonDbFile;
	delete labelConfigFile;
	delete editConfigFile;
	delete buttonConfigFile;
	delete layout;
}

void Sqlite3Login::loadConfig() noexcept
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(window->getConfigFile().toStdString().c_str());
		libconfig::Setting& root = cfg.getRoot();
		if
		(
			root.exists(LoginWidget::loginConfig) &&
			root[LoginWidget::loginConfig].isGroup() &&
			root[LoginWidget::loginConfig].exists(sqlite3Setting) &&
			root[LoginWidget::loginConfig][sqlite3Setting].isGroup() &&
			QMessageBox::question(	this,
						"",
						"Caricare gli ultimi setting usati?",
						QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes
		) {
			libconfig::Setting& _sqlite3 = root[LoginWidget::loginConfig][sqlite3Setting];
			editDbFile->setText(QString::fromStdString(_sqlite3[dbFileSetting]));
			editConfigFile->setText(QString::fromStdString(_sqlite3[configFileSetting]));
		}

	}
	catch (libconfig::FileIOException& e) {}
	catch (libconfig::ParseException& e) {
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e) {
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void Sqlite3Login::storeConfig() noexcept
{
	libconfig::Config cfg;
	try
	{
		cfg.readFile(window->getConfigFile().toStdString().c_str());

		libconfig::Setting &root = cfg.getRoot();
		if (!root.exists(LoginWidget::loginConfig))
			addConfig(cfg);
		else {
			libconfig::Setting& login = root[LoginWidget::loginConfig];
			if (!login.isGroup()) {
				root.remove(LoginWidget::loginConfig);
				addConfig(cfg);
			}
			else {
				if (!login.exists(sqlite3Setting))
					addConfig(cfg);
				else {
					libconfig::Setting& _sqlite3 = login[sqlite3Setting];
					if (!_sqlite3.isGroup()) {
						login.remove(sqlite3Setting);
						addConfig(cfg);
					}
					else {
						_sqlite3[dbFileSetting] = editDbFile->text().toStdString();
						_sqlite3[configFileSetting] = editConfigFile->text().toStdString();
					}
				}
			}
		}
		cfg.writeFile(window->getConfigFile().toStdString().c_str());
	}
	catch (libconfig::FileIOException& e)
	{
		addConfig(cfg);
		cfg.writeFile(window->getConfigFile().toStdString().c_str());
	}
	catch (libconfig::ParseException& e)
	{
		std::string message = "Parse error alla linea " + std::to_string(e.getLine()) + ":\n" + e.getError();
		QMessageBox::warning(this,"Parse error", QString::fromStdString(message), QMessageBox::Ok);
	}
	catch (libconfig::SettingNotFoundException& e)
	{
		std::string path = e.getPath();
		std::string what = e.what();
		std::string message = 	"Errore nel file di configurazione:\n" + path +
					" non e' stato trovato. (" +
					what + ")";
		QMessageBox::warning(this,"Setting error", QString::fromStdString(message), QMessageBox::Ok);
	}
}

void Sqlite3Login::login() noexcept
{
	Sqlite3Connector* _connector = new Sqlite3Connector;
	_connector->open(editDbFile->text().toStdString());
	try {
		_connector->connect();

		DatabaseBuilder dbuilder;
		Database* _database = new Database;
		*_database = dbuilder.fromConfigFile(editConfigFile->text().toStdString());

		if (connector)
			delete connector;
		connector = _connector;

		if (database)
			delete database;
		database = _database;

		storeConfig();
		emit done();
	}
	catch (BackendException& e){
		QMessageBox::warning(this, "Errore durante la Connessione", e.what(), QMessageBox::Ok);
		delete _connector;
	}
	catch (DatabaseBuilderException& e){
		QMessageBox::warning(this, "Errore durante la Connessione", e.what(), QMessageBox::Ok);
		delete _connector;
	}

}

void Sqlite3Login::openDbFile() noexcept
{
	chooseOpenFile(window, editDbFile, "Database File", "*.*")
}

void Sqlite3Login::openConfigFile() noexcept
{
	chooseOpenFile(window, editConfigFile, "Configuration File", "*.*")
}

void Sqlite3Login::addConfig(libconfig::Config& cfg) const noexcept
{
	libconfig::Setting &root = cfg.getRoot();
	libconfig::Setting& login = (!root.exists(LoginWidget::loginConfig) ? root.add(LoginWidget::loginConfig, libconfig::Setting::TypeGroup) : root[LoginWidget::loginConfig]);
	libconfig::Setting& _sqlite3 = (!login.exists(sqlite3Setting) ? login.add(sqlite3Setting, libconfig::Setting::TypeGroup) : login[sqlite3Setting]);
	_sqlite3.add(dbFileSetting, libconfig::Setting::TypeString) = editDbFile->text().toStdString();
	_sqlite3.add(configFileSetting, libconfig::Setting::TypeString) = editConfigFile->text().toStdString();
}
