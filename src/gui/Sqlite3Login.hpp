/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __SQLITE3_LOGIN_WIDGET_H
#define __SQLITE3_LOGIN_WIDGET_H

#include "LoginWidget.hpp"
#include <libconfig.h++>

class QLabel;
class QLineEdit;
class QPushButton;
class QGridLayout;

namespace openDB
{

class Sqlite3Login : public LoginWidget
{
	Q_OBJECT
public:
	explicit Sqlite3Login (	Database*& __database,
				BackendConnector*& __connector,
				MainWindow* __window,
				QWidget* __parent);

	virtual ~Sqlite3Login();

public slots:
	virtual void login() noexcept; // slot di connessione a Sqlite3

private slots:
	void openDbFile() noexcept;
	void openConfigFile() noexcept;

private:
	QLabel* labelDbFile;
	QLineEdit* editDbFile;
	QPushButton* buttonDbFile;
	QLabel* labelConfigFile;
	QLineEdit* editConfigFile;
	QPushButton* buttonConfigFile;
	QGridLayout* layout;

	static const std::string sqlite3Setting;
	static const std::string dbFileSetting;	// nome della configurazione per il file contenente il database
	static const std::string configFileSetting; // nome della configurazione contenente che riferisce il file contenente la struttura del db
	void loadConfig() noexcept;
	void storeConfig() noexcept;
	void addConfig(libconfig::Config& cfg) const noexcept;
};

};

#endif
