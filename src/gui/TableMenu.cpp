/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "TableMenu.hpp"

#include <list>
#include <string>
#include <fstream>
#include <iostream>

#include <QAction>
#include <QMessageBox>
#include <QFileDialog>

#include "core/Table.hpp"
#include "core/CsvUpdateManager.hpp"
#include "core/BackendConnector.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"
#include "openFileMacro.hpp"
#include "MainWindow.hpp"
#include "TableView.hpp"
#include "QuerySetting.hpp"
#include "InsertRecord.hpp"
#include "CsvExportSetting.hpp"

using namespace openDB;

TableMenu::TableMenu(	Table* table,
			MainWindow* window )  :
		QMenu(window),
		parentTable(table),
		parentWindow(window),

		actionView( new QAction("Guarda Tabella", this) ),
		actionQuerySettings( new QAction("Impostazioni Interrogazione", this) ),
		actionClean(new QAction("Pulisci", this)),
		actionInsertRecord(new QAction("Inserisci Record", this)),
		actionCsvImportModel(new QAction("Crea modello importazione/modifica CSV", this)),
		actionCsvImport(new QAction("Importa da modello CSV", this)),
		actionExportAsCsv(new QAction("Esporta su file CSV", this)),
		actionSave(new QAction("Salva modifiche", this)),
		upmanager(0)
{
	setTitle(QString::fromStdString(parentTable->name()));

	addAction(actionView);
	addAction(actionQuerySettings);
	addAction(actionSave);
	addAction(actionClean);
	addAction(actionCsvImportModel);
	addAction(actionCsvImport);
	addAction(actionExportAsCsv);
	addAction(actionInsertRecord);

	connect(actionView, SIGNAL(triggered()), this, SLOT(slotView()));
	connect(actionQuerySettings, SIGNAL(triggered()), this, SLOT(slotQuerySettings()));
	connect(actionClean, SIGNAL(triggered()), this, SLOT(slotClean()));
	connect(actionInsertRecord, SIGNAL(triggered()), this, SLOT(slotInsertRecord()));
	connect(actionCsvImportModel, SIGNAL(triggered()), this, SLOT(slotCsvImportModel()));
	connect(actionCsvImport, SIGNAL(triggered()), this, SLOT(slotCsvImport()));
	connect(actionExportAsCsv, SIGNAL(triggered()), this, SLOT(slotExportAsCsv()));
	connect(actionSave, SIGNAL(triggered()), this, SLOT(slotSaveAll()));
}
		
TableMenu::~TableMenu()
{
	delete actionView;
	delete actionQuerySettings;
	delete actionInsertRecord;
	delete actionExportAsCsv;
	delete actionCsvImportModel;
	delete actionCsvImport;
	delete actionClean;
	delete actionSave;
}


void TableMenu::slotView()
{
	parentWindow->setSessionWidget(QString::fromStdString(parentTable->name()), new TableView(parentTable, true, parentWindow, this, parentWindow));
}

void TableMenu::slotQuerySettings()
{
	parentWindow->setSessionWidget(QString::fromStdString(parentTable->name()) + " query setting", new QuerySettingWidget(parentTable, parentWindow, this));
}

void TableMenu::slotLoadRecord()
{
	std::string command = parentTable->loadCommand(true);
	NonTransactional txn(command);
	BackendConnector::Iterator it = parentWindow->backendConnector().execTransaction(txn);
	Table result = it.result().resultTable();
	parentTable->loadRecord(result);
	parentWindow->backendConnector().deleteTransaction(it);
}

void TableMenu::slotInsertRecord()
{
	parentWindow->setSessionWidget("Inserimento in " + QString::fromStdString(parentTable->name()), new InsertRecord(parentTable, parentWindow, this, parentWindow));
}

void TableMenu::slotExportAsCsv()
{
	QString fileName =  QFileDialog::getSaveFileName (	this,
								"Export in formato csv",
								parentWindow->getLastDir() + QString::fromStdString(parentTable->name() + ".csv"),
								"csv file (*.csv)" );
	parentWindow->setLastDir(fileName);


	if ( !fileName.isEmpty() )
	{
		CsvUpdateManager::Setting par;
		CsvExportSettingDialog dialog(&par, parentWindow);
		dialog.exec();
		CsvUpdateManager manager(*parentTable);
		manager.csvExport(fileName.toStdString(), par);


	}
}

void TableMenu::slotCsvImportModel()
{
	QString fileName =  QFileDialog::getSaveFileName (	parentWindow,
								"Crea file csv  per caricamento",
								parentWindow->getLastDir() + QString::fromStdString(parentTable->name() + ".csv"),
								"csv file (*.csv)" );
	parentWindow->setLastDir(fileName);

	if ( !fileName.isEmpty() )
	{
		CsvUpdateManager::Setting par;
		CsvExportSettingDialog dialog(&par, parentWindow);
		dialog.exec();
		upmanager = new CsvUpdateManager(*parentTable);
		upmanager->model(fileName.toStdString(), par);
	}
}

void TableMenu::slotCsvImport()
{
	if (!upmanager)
	{
		QMessageBox::warning(this, "Erroe", "Non è possibile effettuare l'importazione.\nNon hai impostato un Update Manager.", QMessageBox::Ok);
		return;
	}

	QString fileName =  QFileDialog::getOpenFileName (	parentWindow,
								"Apri file csv  per caricamento",
								parentWindow->getLastDir() + QString::fromStdString(parentTable->name() + ".csv"),
								"csv file (*.csv)" );
	parentWindow->setLastDir(fileName);

	if ( !fileName.isEmpty() )
	{
		std::list<std::string> log;
		try {
			CsvUpdateManager::Setting par;
			CsvExportSettingDialog dialog(&par, parentWindow);
			dialog.exec();
			upmanager->parse(fileName.toStdString(), log, true, par);
			if (log.size() != 0 && QMessageBox::warning(	parentWindow,
									"Errore durante l'import",
									"Si sono verificati degli errori durante l'import.\nVuoi salvare il log su file?",
									QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
			{
				QString logFileName =  QFileDialog::getSaveFileName (	parentWindow,
											"Salva Log",
											parentWindow->getLastDir() + "log.txt",
											"*.txt" );
				if ( !logFileName.isEmpty() )
				{
					std::fstream logstream(logFileName.toStdString().c_str(), std::ios::out);
					std::list<std::string>::const_iterator it = log.begin(), end = log.end();
					for (; it != end; it++)
						logstream << *it <<std::endl;
					logstream.close();
				}
			}
			delete upmanager;
			upmanager = 0;
		}
		catch (BasicException& e)
		{
			QMessageBox::warning(	parentWindow,
						"Errore durante l'import",
						e.what(),
						QMessageBox::Ok);
		}
	}
}

void TableMenu::slotClean()
{
	if(	QMessageBox::warning(
			parentWindow,
			"Attenzione!",
			"Eseguendo la pulizia verranno perse eventuali modifiche non salvate.\nContinuare?",
			QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
		parentTable->storage().clear();
}

void TableMenu::slotSaveAll()
{
	std::list<std::string> commandList;
	parentTable->commit(commandList, true, true);

	switch
	(
		QMessageBox::question(
			parentWindow,
			"Attenzione!",
			"Eseguire le operazioni come unica transazione?",
			QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel)
	)
	{
		case QMessageBox::Yes:
		{
			Transaction txn(commandList);
			parentWindow->backendConnector().addTransaction(txn);
		}
		break;

		case QMessageBox::No:
		{
			NonTransactional txn(commandList);
			parentWindow->backendConnector().addTransaction(txn);
		}
		break;
		default: break;
	}
}
