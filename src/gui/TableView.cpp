/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include "TableView.hpp"

#include "core/Table.hpp"
#include "core/Storage.hpp"
#include "core/Record.hpp"
#include "core/CsvUpdateManager.hpp"
#include "core/BackendConnector.hpp"
#include "core/Transaction.hpp"
#include "core/NonTransactional.hpp"

#include "openFileMacro.hpp"
#include "InsertRecord.hpp"
#include "EditRecord.hpp"
#include "CsvExportSetting.hpp"
#include "MainWindow.hpp"
#include "TableMenu.hpp"

#include <QtGui>
#include <iostream>
#include <algorithm>

using namespace openDB;

TableView::TableView(Table* table, bool editable, MainWindow* window, TableMenu* menu, QWidget* parent) :
		QTableWidget(parent),
		parentTable(table),
		isEditable(editable),
		parentWindow(window),
		parentMenu(menu),
		lastRow(0),

		contextMenu(new QMenu(this)),
		actionRefresh(new QAction("Refresh", contextMenu)),
		actionInsertRecord(new QAction("Inserisci Record", contextMenu)),
		actionEditRecord(new QAction("Modifica Record", contextMenu)),
		actionDeleteRecord(new QAction("Cancella Record", contextMenu)),
		actionEraseRecord(new QAction("Elimina Record", contextMenu)),
		actionCsvImportModel(new QAction("Genera modello importazione/modifica", contextMenu)),
		actionCsvImport(new QAction("Importa da modello", contextMenu)),
		actionExportAsCsv(new QAction("Esporta su Csv", contextMenu)),
		actionClean(new QAction("Pulisci", contextMenu)),
		actionSave(new QAction("Salva modifiche", contextMenu)),
		actionQuerySettings(new QAction("Impostazioni interrogazione", contextMenu))
{
	setColumnCount(parentTable->columns());
	setRowCount (parentTable->storage().records());
	setSelectionMode(QAbstractItemView::NoSelection);
	setEditTriggers(QAbstractItemView::NoEditTriggers);

	/* creo l'header */
	int col = 0;
	Table::Iterator it = parentTable->begin(), end = parentTable->end();
	for (; it != end; it++, col++)
	{
		QTableWidgetItem* item = new QTableWidgetItem;
		QString name = QString::fromStdString(it->name());
		item->setText(name);
		setHorizontalHeaderItem(col, item);
		headerWidgets.push_back(item);
	}

	rowsVector.resize(parentTable->storage().records());
	Storage::Iterator stor_it = parentTable->storage().begin(), stor_end = parentTable->storage().end();
	for (; stor_it != stor_end; stor_it++, lastRow++)
		rowsVector[lastRow] = new RowWidget(parentTable, stor_it, this, lastRow);

	setContextMenuPolicy(Qt::CustomContextMenu);


	contextMenu->addAction(actionSave);
	contextMenu->addAction(actionQuerySettings);
	contextMenu->addAction(actionClean);
	contextMenu->addAction(actionInsertRecord);
	contextMenu->addAction(actionEditRecord);
	contextMenu->addAction(actionDeleteRecord);
	contextMenu->addAction(actionEraseRecord);
	contextMenu->addAction(actionCsvImportModel);
	contextMenu->addAction(actionCsvImport);
	contextMenu->addAction(actionExportAsCsv);
	contextMenu->addAction(actionRefresh);

	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(slotContextMenu(const QPoint&)));
	connect(actionRefresh, SIGNAL(triggered()), this, SLOT(slotRefresh()));
	connect(actionInsertRecord, SIGNAL(triggered()), this, SLOT(slotInsertRecord()));
	connect(actionEditRecord, SIGNAL(triggered()), this, SLOT(slotEditRecord()));
	connect(actionDeleteRecord, SIGNAL(triggered()), this, SLOT(slotDeleteRecord()));
	connect(actionEraseRecord, SIGNAL(triggered()), this, SLOT(slotEraseRecord()));
	connect(actionClean, SIGNAL(triggered()), this, SLOT(slotClean()));

	if (parentMenu != 0)
	{
		connect(actionCsvImportModel, SIGNAL(triggered()), parentMenu, SLOT(slotCsvImportModel()));
		connect(actionCsvImport, SIGNAL(triggered()), this, SLOT(slotCsvImport()));
		connect(actionExportAsCsv, SIGNAL(triggered()), parentMenu, SLOT(slotExportAsCsv()));
		connect(actionSave, SIGNAL(triggered()), parentMenu, SLOT(slotSaveAll()));
		connect(actionQuerySettings, SIGNAL(triggered()), parentMenu, SLOT(slotQuerySettings()));
	}
	else
		connect(actionExportAsCsv, SIGNAL(triggered()), this, SLOT(slotCsvExport()));

	if (!isEditable)
	{
		actionRefresh->setEnabled(true);
		actionInsertRecord->setEnabled(false);
		actionEditRecord->setEnabled(false);
		actionDeleteRecord->setEnabled(false);
		actionCsvImportModel->setEnabled(false);
		actionCsvImport->setEnabled(false);
		actionClean->setEnabled(false);
		actionSave->setEnabled(false);
		actionQuerySettings->setEnabled(false);
	}
}


TableView::~TableView()
{
	std::vector<RowWidget*>::iterator row_it = rowsVector.begin(), row_end = rowsVector.end();
	for (; row_it != row_end; row_it++)
		delete *row_it;

	std::list<QTableWidgetItem*>::iterator it = headerWidgets.begin(), end = headerWidgets.end();
	for (; it != end; it++)
		delete *it;

	delete actionRefresh;
	delete actionInsertRecord;
	delete actionEditRecord;
	delete actionDeleteRecord;
	delete actionEraseRecord;
	delete actionExportAsCsv;
	delete actionSave;
	delete actionClean;
	delete actionQuerySettings;
	delete contextMenu;
}

void TableView::slotContextMenu(const QPoint& pt)
{
	rightClickPoint = pt;
	//contextMenu->exec(mapToGlobal(pt)); // funziona
	contextMenu->exec(viewport()->mapToGlobal(pt)); // funziona ugualmente ma fa una chiamata in più

}

void TableView::slotInsertRecord()
{
	InsertRecord* insertRecord = new InsertRecord(parentTable, parentWindow, parentMenu, parentWindow);
	parentWindow->setSessionWidget("Inserimento in " + QString::fromStdString(parentTable->name()), insertRecord);
}

void TableView::slotEditRecord()
{
	QModelIndex index = indexAt(rightClickPoint);
	if (index.isValid())
	{
		RowWidget* rowWidget = rowsVector[index.row()];
		Storage::Iterator iterator = rowWidget->iterator();
		EditRecord* editRecord = new EditRecord(parentTable, iterator, parentWindow, parentMenu, parentWindow);
		parentWindow->setSessionWidget("Modifica in " + QString::fromStdString(parentTable->name()),editRecord);
	}
}

void TableView::slotDeleteRecord()
{
	QModelIndex index = indexAt(rightClickPoint);
	if (index.isValid())
	{
		RowWidget* rowWidget = rowsVector[index.row()];
		Storage::Iterator iterator = rowWidget->iterator();
		parentTable->storage().deleteRecord(iterator);
		rowWidget->refresh();
	}
}

void TableView::slotEraseRecord()
{
	QModelIndex index = indexAt(rightClickPoint);
	if (index.isValid())
	{
		RowWidget* rowWidget = rowsVector[index.row()];
		Storage::Iterator iterator = rowWidget->iterator();
		parentTable->storage().eraseRecord(iterator);
		rowsVector.erase(rowsVector.begin() + index.row());
		removeRow(index.row()); //dealloca automaticamente la memoria occupata dai widget
	}
}

void TableView::slotCsvImport()
{
	parentMenu->slotCsvImport();
	slotRefresh();
}

void TableView::slotClean()
{
	if(	QMessageBox::warning(
			parentWindow,
			"Attenzione!",
			"Eseguendo la pulizia verranno perse eventuali modifiche non salvate.\nContinuare?",
			QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
	{
		parentTable->storage().clear();

		std::vector<RowWidget*>::iterator row_it = rowsVector.begin(), row_end = rowsVector.end();
		for (; row_it != row_end; row_it++)
			delete *row_it;

		rowsVector.clear();
		lastRow = 0;
		rowsVector.resize(parentTable->storage().records());

		setColumnCount(parentTable->columns());
		setRowCount (parentTable->storage().records());
	}
}

void TableView::slotCsvExport()
{

	QString fileName =  QFileDialog::getSaveFileName (	this,
								"Export in formato csv",
								parentWindow->getLastDir() + QString::fromStdString(parentTable->name() + ".csv"),
								"csv file (*.csv)" );
	parentWindow->setLastDir(fileName);

	if ( !fileName.isEmpty() )
	{
		CsvUpdateManager::Setting par;
		CsvExportSettingDialog dialog(&par, parentWindow);
		dialog.exec();
		CsvUpdateManager manager(*parentTable);
		manager.csvExport(fileName.toStdString(), par);


	}
}

void openDB::TableView::slotRefresh()
{
	std::vector<RowWidget*>::iterator row_it = rowsVector.begin(), row_end = rowsVector.end();
	for (; row_it != row_end; row_it++)
		delete *row_it;

	rowsVector.clear();
	lastRow = 0;
	rowsVector.resize(parentTable->storage().records());

	setColumnCount(parentTable->columns());
	setRowCount (parentTable->storage().records());

	Storage::Iterator stor_it = parentTable->storage().begin(), stor_end = parentTable->storage().end();
	for (; stor_it != stor_end; stor_it++, lastRow++)
		rowsVector[lastRow] = new RowWidget(parentTable, stor_it, this, lastRow);
}

const QColor TableView::RowWidget::loadedColor(255, 255, 255, 255);
const QColor TableView::RowWidget::insertedColor(0, 255, 0, 180);
const QColor TableView::RowWidget::updatedColor(0, 255, 255, 180);
const QColor TableView::RowWidget::deletedColor(255, 0, 0, 180);

TableView::RowWidget::RowWidget(Table* table, Storage::Iterator it, TableView* tableWidget, int row) :
		parentTable(table),
		storageIterator(it),
		parentTableWidget(tableWidget)
{
	std::unordered_map<std::string, std::string> values;
	(*storageIterator).current(values);

	Table::Iterator col_it = parentTable->begin(), col_end = parentTable->end();
	int col = 0;
	for (; col_it != col_end; col_it++)
	{
		QTableWidgetItem* widget = new QTableWidgetItem;

		setCellColour(widget);
		QString text = QString::fromStdString(values.at(col_it->name()));
		widget->setText(text);
		parentTableWidget-> setItem(row, col++, widget);
		widgetList.push_back(widget);
	}
}

TableView::RowWidget::~RowWidget()
{
	std::list<QTableWidgetItem*>::iterator it = widgetList.begin(), end = widgetList.end();
	for (; it != end; it++)
		delete *it;
}

void TableView::RowWidget::refresh()
{
	std::unordered_map<std::string, std::string> values;
	(*storageIterator).current(values);

	Table::Iterator col_it = parentTable->begin(), col_end = parentTable->end();
	std::list<QTableWidgetItem*>::iterator wit = widgetList.begin(), wend = widgetList.end();
	for (; col_it != col_end; col_it++, wit++)
	{
		std::string value = values.at(col_it->name());
		(*wit)->setText( QString::fromStdString(value));
		setCellColour((*wit));
	}
}

void TableView::RowWidget::setCellColour(QTableWidgetItem* widgetItem)
{
	switch ((*storageIterator).state())
	{
		case Record::inserting:
			widgetItem->setBackgroundColor(insertedColor);
			break;

		case Record::updating:
			widgetItem->setBackgroundColor(updatedColor);
			break;

		case Record::deleting:
			widgetItem->setBackgroundColor(deletedColor);
			break;

		default:
			widgetItem->setBackgroundColor(loadedColor);
			break;
	}
}
