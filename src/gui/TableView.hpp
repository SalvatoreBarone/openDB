/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __OPENDB_TABLE_VIEW__
#define __OPENDB_TABLE_VIEW__

#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QPoint>
#include <QColor>

#include "core/Storage.hpp"

#include <vector>

class QMenu;
class QAction;
class QMainWindow;

namespace openDB
{

class Table;
class RowWidget;
class MainWindow;
class TableMenu;
class CsvUpdateManager;

class TableView : public QTableWidget
{
	Q_OBJECT

public:
	TableView(	Table* table,
			bool editable,
			MainWindow* window,
			TableMenu* menu,
			QWidget* parent = 0);

	virtual ~TableView();

private:
	Table* parentTable;
	bool isEditable;
	MainWindow* parentWindow;
	TableMenu* parentMenu;

	std::list<QTableWidgetItem*> headerWidgets;
	int lastRow;

	class RowWidget
	{
	public:
		RowWidget(Table* table, Storage::Iterator it, TableView* tableWidget, int row);

		virtual ~RowWidget();

		void refresh();

		Storage::Iterator iterator() const
			{return storageIterator;}

	private:
		Table* parentTable;
		Storage::Iterator storageIterator;
		TableView* parentTableWidget;
		int rowIndex;

		std::list<QTableWidgetItem*> widgetList;

		static const QColor loadedColor;
		static const QColor insertedColor;
		static const QColor updatedColor;
		static const QColor deletedColor;
		void setCellColour(QTableWidgetItem* widgetItem);
	};

	std::vector<RowWidget*> rowsVector;

	QPoint rightClickPoint;
	QMenu* contextMenu;

	QAction* actionRefresh;
	QAction* actionInsertRecord;
	QAction* actionEditRecord;
	QAction* actionDeleteRecord;
	QAction* actionEraseRecord;
	QAction* actionCsvImportModel;
	QAction* actionCsvImport;
	QAction* actionExportAsCsv;
	QAction* actionClean;
	QAction* actionSave;
	QAction* actionQuerySettings;

private slots:
	void slotContextMenu(const QPoint& pt);

	void slotRefresh();
	void slotInsertRecord();
	void slotEditRecord();
	void slotDeleteRecord();
	void slotEraseRecord();
	void slotCsvImport();
	void slotCsvExport();
	void slotClean();
};




};


#endif

