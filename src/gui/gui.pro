TEMPLATE = lib
CONFIG += dll plugin
TARGET = ../openDB2gui
DEPENDPATH +=
INCLUDEPATH += .. /usr/include/postgresql
#LIBS += -L.. -L. -lpthread -lopenDB2core -lopenDB2backend -lpq
#QMAKE_LFLAGS += -Wl,-rpath=../,-rpath=./
QMAKE_CXXFLAGS += -std=c++11 -Wextra -ggdb3

HEADERS += \
openFileMacro.hpp \
LineEdit.hpp \
TextEdit.hpp \
SchemaMenu.hpp \
DatabaseMenu.hpp \
TableMenu.hpp \
ResultMenu.hpp \
EditValue.hpp \
EditRecord.hpp \
InsertRecord.hpp \
QuerySetting.hpp \
CsvExportSetting.hpp \
TableView.hpp \
SendQuery.hpp \
LogViewer.hpp \
LoginWidget.hpp \
PgLogin.hpp \
Sqlite3Login.hpp \
Login.hpp \
SessionManager.hpp \
MainWindow.hpp \
          
SOURCES += \
TableMenu.cpp \
SchemaMenu.cpp \
DatabaseMenu.cpp \
ResultMenu.cpp \
EditValue.cpp \
EditRecord.cpp \
InsertRecord.cpp \
QuerySetting.cpp \
CsvExportSetting.cpp \
TableView.cpp \
SendQuery.cpp \
LogViewer.cpp \
LoginWidget.cpp \
PgLogin.cpp \
Sqlite3Login.cpp \
Login.cpp \
SessionManager.cpp \
MainWindow.cpp \


