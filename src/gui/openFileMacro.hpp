/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __OPEN_FILE_MACRO_H
#define __OPEN_FILE_MACRO_H

#include <QFileDialog>
#include <QString>


/**
 * chooseOpenFile permette di implementare uno slot di apertura file.
 *
 * @param component QLineEdit dove sara' memorizzato il percorso del file
 * @param message intestazione della finestra di dialogo
 * @param path percorso directory di partenza
 * @param estensione del file, es. "*.html"
 */
#define chooseOpenFile(parentWindow, component, message, filter) \
QString path = parentWindow->getLastDir();\
QString fileName = QFileDialog::getOpenFileName(this, message, path, filter);\
if (!fileName.isEmpty())\
	component->setText(fileName);\
parentWindow->setLastDir(fileName);\

/**
 * chooseFile permette di implementare uno slot di salvataggio file.
 *
 * @param component QLineEdit dove sara' memorizzato il percorso del file
 * @param message intestazione della finestra di dialogo
 * @param path percorso directory di partenza
 * @param estensione del file, es. "*.html"
 */
#define chooseSaveFile(parentWindow, component, message, filter) \
QString path = parentWindow->getLastDir();\
QString fileName = QFileDialog::getSaveFileName(this, message, path, filter);\
if (!fileName.isEmpty())\
	component->setText(fileName);\
parentWindow->setLastDir(fileName);\

#endif
