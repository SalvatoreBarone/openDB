#!/bin/bash

if [ $# -ne 4 ]
then
    echo "Numero di parametri errato";
    echo "createNewTest.sh <filename> <Letter> <first> <last>";
    exit 0;
fi

extension=${1##*.};
executable=${1/.$extension};
source=$executable.$extension;

read -p "Sicuro di voler creare il file? (s/n) " risposta
if [  "$risposta" = "s" ] || [ "$risposta" = "S" ]; then

	printf "/**
 * @file %s
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include \"openDBcore.hpp\"
#include \"test.hpp\"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
" "$1" > $source;

	for ((i=$3; i<=$4; i++)); do
		printf "/**
 * @test Test %s%04i
 *
 * 
 */
void test_%s_%04i() {

}

" "$2" "$i" "$2" "$i" >> $source;
	done;

	printf "
int main() {
	signal(SIGABRT, abortHandler);
	test_function function[%i] = {
		" "$(($4-$3+1))" >> $source;

	for ((i=$3; i<=$4; i++)); do
		printf "test_%s_%04i" "$2" "$i" >> $source;
		if ((i<$4)); then
			printf ", " >> $source;
		fi;
	done;

printf "
	};

	for (int i = 0; i < %i; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}\n" "$(($4-$3+1))" >> $source;

	read -p "Aggiungi i file al CMakeLists.txt? (s/n) " risposta
	if [  "$risposta" = "s" ] || [ "$risposta" = "S" ]; then
		printf "add_executable(%s %s)\n" "$executable" "$source" >> CMakeLists.txt;
	fi;
fi;
