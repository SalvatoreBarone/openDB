/**
 * @file test-C-006.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-006 - Table::end(), Table::Iterator::operator++, Table::Iterator::operator==. Loops between
 * the Column objects added to a Table object, verifying that the insertion order is respected and that the
 * name, type and "is key" attribute match.
 * @example test-C-006.cpp
 * Test C-006 - Table::end(), Table::Iterator::operator++, Table::Iterator::operator==. Loops between
 * the Column objects added to a Table object, verifying that the insertion order is respected and that the
 * name, type and "is key" attribute match.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	typedef struct  {
		std::string name;
		int internalID;
		bool isKey;
	} columnsInfo;

	columnsInfo vector[] = {
			{"Boolean", Boolean::internalID, false},
			{"Date", Date::internalID, false},
			{"Time", Time::internalID, false},
			{"Timestamp", Timestamp::internalID, false},
			{"Small", Smallint::internalID, false},
			{"Int", Integer::internalID, false},
			{"Bigint", Bigint::internalID, false},
			{"Real", Real::internalID, false},
			{"Double", DoublePrecision::internalID, false},
			{"Numeric", Numeric::internalID, false},
			{"Char", Character::internalID, false},
			{"VChar", Varchar::internalID, true}
	};


	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table::Iterator it = table.begin(), end = table.end();
		int i = 0;
		for (; it != end; it++, i++) {
//			cout 	<< it->getName() <<"\t" << it->getSqlType()->typeInfo().internalID <<"\t" << it->getIsKey() << endl
//					<< vector[i].name <<"\t" << vector[i].internalID <<"\t" << vector[i].isKey << endl
//					<< endl;
			assert(it->getName() == vector[i].name);
			assert(it->getSqlType()->typeInfo().internalID == vector[i].internalID);
			assert(it->getIsKey() == vector[i].isKey);
		}

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
