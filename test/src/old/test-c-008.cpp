/**
 * @file test-C-008.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-008 - Table::cend(), Table::ConstIterator::operator++, Table::ConstIterator::operator==.
 * Loops between the Column objects added to a Table object, verifying that the insertion order is respected
 * and that the name, type and "is key" attribute match.
 * @example test-C-008.cpp
 * Test C-008 - Table::cend(), Table::ConstIterator::operator++, Table::ConstIterator::operator==.
 * Loops between the Column objects added to a Table object, verifying that the insertion order is respected
 * and that the name, type and "is key" attribute match.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	typedef struct  {
		std::string name;
		int internalID;
		bool isKey;
	} columnsInfo;

	columnsInfo vector[] = {
			{"Boolean", Boolean::internalID, false},
			{"Date", Date::internalID, false},
			{"Time", Time::internalID, false},
			{"Timestamp", Timestamp::internalID, false},
			{"Small", Smallint::internalID, false},
			{"Int", Integer::internalID, false},
			{"Bigint", Bigint::internalID, false},
			{"Real", Real::internalID, false},
			{"Double", DoublePrecision::internalID, false},
			{"Numeric", Numeric::internalID, false},
			{"Char", Character::internalID, false},
			{"VChar", Varchar::internalID, true}
	};


	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table::ConstIterator cit = table.cbegin(), cend = table.cend();
		int i = 0;
		for (; cit != cend; cit++, i++) {
//			cout 	<< cit->getName() <<"\t" << cit->getSqlType()->typeInfo().internalID <<"\t" << cit->getIsKey() << endl
//					<< vector[i].name <<"\t" << vector[i].internalID <<"\t" << vector[i].isKey << endl
//					<< endl;
			assert(cit->getName() == vector[i].name);
			assert(cit->getSqlType()->typeInfo().internalID == vector[i].internalID);
			assert(cit->getIsKey() == vector[i].isKey);
		}

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
