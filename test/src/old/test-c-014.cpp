/**
 * @file test-C-014.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-014 - Storage::addRecord(), Storage::begin(). Adds a Record object to the Storage it belongs
 * to, verifying that Storage :: begin () returns an iterator placed on the first inserted record. Through the
 * iterator the status of the record and the value of its fields are checked.
 * @example test-C-014.cpp
 * Test C-014 - Storage::addRecord(), Storage::begin(). Adds a Record object to the Storage it belongs
 * to, verifying that the function returns an iterator matching Storage::begin(), placed on the inserted
 * record. Through the iterator the status of the record and the value of its fields are checked.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Record record = table.getStorage().newRecord();
		assert(record.getStatus() == Record::empty);

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		};
		record.edit(valueMap1);

		assert(table.getStorage().begin() == table.getStorage().end());
		Storage::Iterator sit = table.getStorage().addRecord(record);
		assert(table.getStorage().begin() != table.getStorage().end());
		assert(sit == table.getStorage().begin());
		assert(sit->getStatus() == Record::inserting);
		unordered_map<string, string> values;
		sit->getCurrentValues(values);
		Table::ConstIterator cit = table.cbegin(), cend = table.cend();
		for (; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap1.at(cit->getName()));

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
