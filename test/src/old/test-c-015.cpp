/**
 * @file test-C-015.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-015 - Storage::addRecord(), Storage::end(). Attempts to add a record object, eighty from a
 * Storage A object, to a Storage B object. Verify that no insertion is made and that the Storage::addRecord()
 * function returns an iterator Storage::end().
 * @example test-C-015.cpp
 * Test C-015 - Storage::addRecord(), Storage::end(). Attempts to add a record object, eighty from a
 * Storage A object, to a Storage B object. Verify that no insertion is made and that the Storage::addRecord()
 * function returns an iterator Storage::end().
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table anotherTable("anotherTable");
		anotherTable.addColumn("Boolean", Boolean());
		anotherTable.addColumn("Date", Date());
		anotherTable.addColumn("Time", Time());
		anotherTable.addColumn("Timestamp", Timestamp());
		anotherTable.addColumn("Small", Smallint());
		anotherTable.addColumn("Int", Integer());
		anotherTable.addColumn("Bigint", Bigint());
		anotherTable.addColumn("Real", Real());
		anotherTable.addColumn("Double", DoublePrecision());
		anotherTable.addColumn("Numeric", Numeric(10,5));
		anotherTable.addColumn("Char", Character(100));
		anotherTable.addColumn("VChar", Varchar(100), true);

		Record record = anotherTable.getStorage().newRecord();

		assert(table.getStorage().addRecord(record) == table.getStorage().end());


	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
