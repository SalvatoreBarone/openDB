/**
 * @file test-C-018.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-018 - Record::edit() on Record object in the Record::loaded state. Verify that the changes
 * are made, respecting the fields with the attribute "is key" and that the state of the object becomes
 * Record::updating. The correspondence of the old record values is also checked.
 * @example test-C-018.cpp
 * Test C-018 - Record::edit() on Record object in the Record::loaded state. Verify that the changes
 * are made, respecting the fields with the attribute "is key" and that the state of the object becomes
 * Record::updating. The correspondence of the old record values is also checked.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Record record = table.getStorage().newRecord();
		assert(record.getStatus() == Record::empty);
		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		};
		record.edit(valueMap1);
		assert(record.getStatus() == Record::inserting);
		unordered_map<string, string> values, oldValues;
		record.getCurrentValues(values);
		Table::ConstIterator cit = table.cbegin(), cend = table.cend();
		for (; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap1.at(cit->getName()));

		record.setStatus(Record::loaded);
		assert(record.getStatus() == Record::loaded);
		unordered_map<string, string> valueMap2 =
		{
			{"Boolean", "false"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"},
			{"VChar", "valueMap1"}
		};
		record.edit(valueMap2);
		assert(record.getStatus() == Record::updating);

		record.getCurrentValues(values);
		record.getOldValues(oldValues);
		cit = table.cbegin();
		for (; cit != cend; cit++) {
			if (cit->getIsKey())
				assert(values.at(cit->getName()) == valueMap1.at(cit->getName()));
			else {
				assert(values.at(cit->getName()) == valueMap2.at(cit->getName()));
				assert(oldValues.at(cit->getName()) == valueMap1.at(cit->getName()));
			}

		}


	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}



	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
