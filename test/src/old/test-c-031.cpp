/**
 * @file test-C-031.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-031 - Copy operator of the Table class. Verify that the copy is, in reality, the same
 * object.
 * @example test-C-031.cpp
 * Test C-031 - Copy operator of the Table class. Verify that the copy is, in reality, the same
 * object.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	typedef struct  {
		std::string name;
		int internalID;
		bool isKey;
	} columnsInfo;

	columnsInfo vector[] = {
			{"Boolean", Boolean::internalID, false},
			{"Date", Date::internalID, false},
			{"Time", Time::internalID, false},
			{"Timestamp", Timestamp::internalID, false},
			{"Small", Smallint::internalID, false},
			{"Int", Integer::internalID, false},
			{"Bigint", Bigint::internalID, false},
			{"Real", Real::internalID, false},
			{"Double", DoublePrecision::internalID, false},
			{"Numeric", Numeric::internalID, false},
			{"Char", Character::internalID, false},
			{"VChar", Varchar::internalID, true}
	};


	try {
		Table table("table");

		Table anotherTable("anotherTable");
		anotherTable = table;

		assert(anotherTable.begin() == table.begin());
		assert(anotherTable.cbegin() == table.cbegin());
		assert(anotherTable.end() == table.end());
		assert(anotherTable.cend() == table.cend());
		assert(anotherTable.getStorage().begin() == table.getStorage().begin());
		assert(anotherTable.getStorage().cbegin() == table.getStorage().cbegin());
		assert(anotherTable.getStorage().end() == table.getStorage().end());
		assert(anotherTable.getStorage().cend() == table.getStorage().cend());

		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table::Iterator it = anotherTable.begin(), end = anotherTable.end();
		int i = 0;
		for (; it != end; it++, i++) {
//			cout 	<< it->getName() <<"\t" << it->getSqlType()->typeInfo().internalID <<"\t" << it->getIsKey() << endl
//					<< vector[i].name <<"\t" << vector[i].internalID <<"\t" << vector[i].isKey << endl
//					<< endl;
			assert(it->getName() == vector[i].name);
			assert(it->getSqlType()->typeInfo().internalID == vector[i].internalID);
			assert(it->getIsKey() == vector[i].isKey);
		}

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		},
		valueMap2 =
		{
			{"Boolean", "false"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"},
			{"VChar", "valueMap2"}
		},
		valueMap3 =
		{
			{"Boolean", "false"},
			{"Small", "31"},
			{"Int", "431"},
			{"Bigint", "8654"},
			{"Real", "43.1"},
			{"Double", "754.321"},
			{"Numeric", "134.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:40"},
			{"Timestamp", "23/12/2017 12:40"},
			{"Char", "a second different string"},
			{"VChar", "valueMap3"}
		};

		assert(table.getStorage().recordsNum() == 0);
		assert(anotherTable.getStorage().recordsNum() == 0);

		Record record1 = anotherTable.getStorage().newRecord();
		record1.edit(valueMap1);
		assert(anotherTable.getStorage().addRecord(record1) != table.getStorage().end());
		assert(table.getStorage().recordsNum() == 1);
		assert(anotherTable.getStorage().recordsNum() == 1);

		Record record2 = table.getStorage().newRecord();
		record2.edit(valueMap2);
		assert(anotherTable.getStorage().addRecord(record2) != anotherTable.getStorage().end());
		assert(table.getStorage().recordsNum() == 2);
		assert(anotherTable.getStorage().recordsNum() == 2);

		Record record3 = anotherTable.getStorage().newRecord();
		record3.edit(valueMap3);
		assert(table.getStorage().addRecord(record3) != anotherTable.getStorage().end());
		assert(table.getStorage().recordsNum() == 3);
		assert(anotherTable.getStorage().recordsNum() == 3);

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
