/**
 * @file test-C-032.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-032 - Tests the Table::loadRecord() function. Attempts to load the record from a source tabl
 * with a different structure. Verify that an exception is thrown and no records are loaded to the
 * destination table.
 * @example test-C-032.cpp
 * Test C-032 - Tests the Table::loadRecord() function. Attempts to load the record from a source tabl
 * with a different structure. Verify that an exception is thrown and no records are loaded to the
 * destination table.
 */
#include "test.hpp"
#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Table table1("table1"), table2 ("table2");

	table1.addColumn("Boolean", Boolean());
	table1.addColumn("Date", Date());
	table1.addColumn("Time", Time());
	table1.addColumn("Small", Smallint());
	table1.addColumn("Int", Integer());
	table1.addColumn("Bigint", Bigint());
	table1.addColumn("Real", Real());
	table1.addColumn("Double", DoublePrecision());
	table1.addColumn("Numeric", Numeric(10,5));
	table1.addColumn("Char", Character(100));
	table1.addColumn("VChar", Varchar(100));

	table2.addColumn("boolean", Boolean());
	table2.addColumn("date", Date());
	table2.addColumn("time", Time());
	table2.addColumn("small", Smallint());
	table2.addColumn("int", Integer());
	table2.addColumn("bigint", Bigint());
	table2.addColumn("real", Real());
	table2.addColumn("double", DoublePrecision());
	table2.addColumn("numeric", Numeric(10,5));
	table2.addColumn("char", Character(100));
	table2.addColumn("vchar", Varchar(100));

	try
	{
		// Creating, editing and inserting four new record for table1
		Record rec1 = table1.getStorage().newRecord();
		Record rec2 = table1.getStorage().newRecord();
		Record rec3 = table1.getStorage().newRecord();
		Record rec4 = table2.getStorage().newRecord();
		rec1.edit(valueMap1);
		rec2.edit(valueMap2);
		rec3.edit(valueMap3);
		rec4.edit(valueMap4);
		table1.getStorage().addRecord(rec1);
		table1.getStorage().addRecord(rec2);
		table1.getStorage().addRecord(rec3);
		table1.getStorage().addRecord(rec4);

		table2.loadRecord(table1);
		assert(false);
	}
	catch (BasicException& e)
	{
		cout << e.what() <<endl;
		assert(table2.getStorage().recordsNum() == 0);
		cout << __FILE__ << TEST_PASSED << endl;
	}
	return 0;
}
