/**
 * @file test-C-033.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-033 - Tests the function Table::loadRecord(). Attempts to load a record from a source table
 * with a compatible structure (Coincident column names, but different type). Verify that uploading takes
 * place only if the content of the source table is compatible with the SQL types of the columns in the target
 * table.
 * @example test-C-033.cpp
 * Test C-033 - Tests the function Table::loadRecord(). Attempts to load a record from a source table
 * with a compatible structure (Coincident column names, but different type). Verify that uploading takes
 * place only if the content of the source table is compatible with the SQL types of the columns in the target
 * table.
 */
#include "test.hpp"
#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Table table1("table1"), table2 ("table2");

	table1.addColumn("Boolean", Boolean());
	table1.addColumn("Date", Date());
	table1.addColumn("Time", Time());
	table1.addColumn("Small", Smallint());
	table1.addColumn("Int", Integer());
	table1.addColumn("Bigint", Bigint());
	table1.addColumn("Real", Real());
	table1.addColumn("Double", DoublePrecision());
	table1.addColumn("Numeric", Numeric(10,5));
	table1.addColumn("Char", Character(100));
	table1.addColumn("VChar", Varchar(100));

	table2.addColumn("Boolean", Varchar(100));
	table2.addColumn("Date", Varchar(100));
	table2.addColumn("Time", Varchar(100));
	table2.addColumn("Small", Varchar(100));
	table2.addColumn("Int", Varchar(100));
	table2.addColumn("Bigint", Varchar(100));
	table2.addColumn("Real", Varchar(100));
	table2.addColumn("Double", Varchar(100));
	table2.addColumn("Numeric", Varchar(100));
	table2.addColumn("Char", Varchar(100));
	table2.addColumn("VChar", Varchar(100));

	try
	{
		// Creating, editing and inserting four new record for table1
		Record rec1 = table1.getStorage().newRecord();
		Record rec2 = table1.getStorage().newRecord();
		Record rec3 = table1.getStorage().newRecord();
		Record rec4 = table1.getStorage().newRecord();
		rec1.edit(valueMap1);
		rec2.edit(valueMap2);
		rec3.edit(valueMap3);
		rec4.edit(valueMap4);
		table1.getStorage().addRecord(rec1);
		table1.getStorage().addRecord(rec2);
		table1.getStorage().addRecord(rec3);
		table1.getStorage().addRecord(rec4);

		assert(table2.getStorage().recordsNum() == 0);
		assert(table1.getStorage().recordsNum() == 4);
		table1.loadRecord(table2);

		table2.loadRecord(table1);

		// table1 and table 2 must have the same records
		assert(table2.getStorage().recordsNum() == table1.getStorage().recordsNum());

		Storage::ConstIterator sit1 = table1.getStorage().cbegin(), send1 = table1.getStorage().cend();
		Storage::ConstIterator sit2 = table2.getStorage().cbegin(), send2 = table2.getStorage().cend();
		std::unordered_map<std::string, std::string> current1, current2, old1, old2;
		unordered_map<string, string>::const_iterator vit, vend;
		while (sit1 != send1 && sit2 != send2) {
			sit1->getCurrentValues(current1);
			sit1->getOldValues(old1);
			sit2->getCurrentValues(current2);
			sit2->getOldValues(old2);
			// check if current values match
			vit = current1.begin();
			vend = current1.end();
			for (; vit != vend; vit++)
				assert(vit->second == current2.at(vit->first));
			// check if old values match
			vit = old1.begin();
			vend = old1.end();
			for (; vit != vend; vit++)
				assert(vit->second == old2.at(vit->first));

			sit1++;
			sit2++;
		}

		// table1 and table2 must be indipendent!
		Record rec5 = table2.getStorage().newRecord();
		rec5.edit(valueMap4);
		table2.getStorage().addRecord(rec5);
		assert (table1.getStorage().recordsNum() != table2.getStorage().recordsNum());

		cout << __FILE__ << TEST_PASSED << endl;

	}
	catch (BasicException& e)
	{
		cout << e.what() <<endl;
		assert(false);
	}

	return 0;
}
