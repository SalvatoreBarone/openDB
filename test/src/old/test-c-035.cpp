/**
 * @file test-C-035.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-035 - Tests the Record::edit() function with an incomplete column-value map. Verify that only
 * the fields in the map are changed.
 * @example test-C-035.cpp
 * Test C-035 - Tests the Record::edit() function with an incomplete column-value map. Verify that only
 * the fields in the map are changed.
 */
#include "test.hpp"
#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Table table1("table1");
	table1.addColumn("Boolean", Boolean());
	table1.addColumn("Date", Date());
	table1.addColumn("Time", Time());
	table1.addColumn("Small", Smallint());
	table1.addColumn("Int", Integer());
	table1.addColumn("Bigint", Bigint());
	table1.addColumn("Real", Real());
	table1.addColumn("Double", DoublePrecision());
	table1.addColumn("Numeric", Numeric(10,5));
	table1.addColumn("Char", Character(100));
	table1.addColumn("VChar", Varchar(100));

	assert(table1.columnsNum() == 11);

	Record rec1 = table1.getStorage().newRecord();
	rec1.edit(valueMap1);
	rec1.setStatus(Record::loaded);
	assert(rec1.getStatus() == Record::loaded);

	// We will edit only a few columns
	unordered_map<string, string> partialMap =
	{
		{"Int", "99999"},
		{"Real", "99999.99"},
		{"Date", "2017-11-16"},
		{"Time", "17:34:00"},
		{"VChar", "EDITED!"}
	};
	rec1.edit(partialMap);

	// We have a record marked as "updating" now
	assert(rec1.getStatus() == Record::updating);

	// only the previously specified columns must be affected by the edit() function
	unordered_map<string, string> current, old;
	rec1.getCurrentValues(current);
	rec1.getOldValues(old);
	unordered_map<string, string>::iterator cit = current.begin(), cend = current.end();
	for (; cit != cend; cit++) {
		unordered_map<string, string>::iterator edited = partialMap.find(cit->first);
		if (edited == partialMap.end()) // if the field hasn't been edited
		{
			// the current value must be unchanged
			unordered_map<string, string>::iterator current_value = valueMap1.find(cit->first);
			assert(cit->second == current_value->second);
			// the old value must be an empty string
			unordered_map<string, string>::iterator old_value = old.find(cit->first);
			assert (old_value->second == "");
		}
		else // if the field has been changed
		{
			// the current value must be changed
			unordered_map<string, string>::iterator current_value = partialMap.find(cit->first);
			assert(cit->second == current_value->second);
			// the old value must be the old one
			unordered_map<string, string>::iterator old_value = valueMap1.find(cit->first);
			unordered_map<string, string>::iterator old_it = old.find(cit->first);
			assert (old_it->second == old_value->second);
		}
	}

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
