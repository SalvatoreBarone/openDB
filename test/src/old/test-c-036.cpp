/**
 * @file test-C-036.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-036 - Tests the Record::edit() function with a column-value map with column names not
 * compatible with those of the Table to which the record object belongs. Verify that no changes are made.
 * @example test-C-036.cpp
 * Test C-036 - Tests the Record::edit() function with a column-value map with column names not
 * compatible with those of the Table to which the record object belongs. Verify that no changes are made.
 */
#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Table table("table1"), table2("table2");

	table.addColumn("Boolean", Boolean());
	table.addColumn("Date", Date());
	table.addColumn("Time", Time());
	table.addColumn("Timestamp", Timestamp());
	table.addColumn("Small", Smallint());
	table.addColumn("Int", Integer());
	table.addColumn("Bigint", Bigint());
	table.addColumn("Real", Real());
	table.addColumn("Double", DoublePrecision());
	table.addColumn("Numeric", Numeric(10,5));
	table.addColumn("Char", Character(100));
	table.addColumn("VChar", Varchar(100));

	assert(table.columnsNum() == 12);

	Record rec = table.getStorage().newRecord();
	unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		};
	rec.edit(valueMap1);
	unordered_map<string, string> values;
	rec.getCurrentValues(values);
	Table::ConstIterator cit = table.cbegin(), cend = table.cend();
	for (; cit != cend; cit++)
		assert(values.at(cit->getName()) == valueMap1.at(cit->getName()));

	unordered_map<string, string> valueMap5 = {
		{"boolean", "true"}, {"Small", "12"}
	};
	try	{
		rec.edit(valueMap5);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap6 =	{
		{"Boolean", "true"}, {"small", "12"}, {"Int", "1234"}
	};
	try	{
		rec.edit(valueMap6);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap7 =	{
		{"int", "1234"}, {"Bigint", "12345678"}
	};
	try	{
		rec.edit(valueMap7);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap8 =	{
		{"bigint", "12345678"}, {"Real", "12.34"}
	};
	try	{
		rec.edit(valueMap8);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap9 = {
		 {"real", "12.34"}, {"Double", "12.34567"}, {"Numeric", "1234.5678"}
	};
	try	{
		rec.edit(valueMap9);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap10 = {
		{"double", "12.34567"}, {"Numeric", "1234.5678"}
	};
	try	{
		rec.edit(valueMap10);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap11 =	{
		{"numeric", "1234.5678"}
	};
	try	{
		rec.edit(valueMap11);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap12 =
	{
		{"date", "2012-11-14"}, {"Time", "12:11:00"}, {"Char", "scemo"}
	};
	try	{
		rec.edit(valueMap12);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap13 =
	{
		{"time", "12:11:00"}, {"Char", "scemo"},
		{"VChar", "valueMap1"}
	};
	try	{
		rec.edit(valueMap13);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap14 =
	{
		{"char", "scemo"},
		{"VChar", "valueMap1"}
	};
	try	{
		rec.edit(valueMap14);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	unordered_map<string, string> valueMap15 =
	{
		{"Boolean", "true"},
		{"vChar", "valueMap1"}
	};
	try	{
		rec.edit(valueMap15);
		assert(false);
	} catch (BasicException& e){
		cout << e.what() <<endl;
	}

	rec.getCurrentValues(values);
	cit = table.cbegin();
	for (; cit != cend; cit++)
		assert(values.at(cit->getName()) == valueMap1.at(cit->getName()));

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
