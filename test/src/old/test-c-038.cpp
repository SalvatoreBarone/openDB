/**
 * @file test-C-038.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test C-038 - Tests the Record::getKey() function with a Table object having just one column making
 * the key.
 * @example test-C-038.cpp
 * Test C-038 - Tests the Record::getKey() function with a Table object having just one column making
 * the key.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {

		Table table("table1");

		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(20));
		table.addColumn("VChar", Varchar(20), true);

		assert(table.columnsNum() == 12);
		assert(table.at("VChar")->getIsKey() == true);

		SHA256_sum_t hashsum;
		SHA256_hashsum((uint8_t*)"valueMap1", 9, &hashsum);
		std::stringstream str;
		for (int i = 0; i < 8; i++)
			str << std::hex << std::setfill('0') << std::setw(8) << hashsum.byte_array[i];

		Record rec = table.getStorage().newRecord();
		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		};
		rec.edit(valueMap1);
		cout 	<< str.str() << endl
				<< rec.getKey() << endl;
		assert(rec.getKey() == str.str());

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}



	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
