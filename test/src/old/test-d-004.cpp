/**
 * @file test-D-004.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-004 - Tests the CsvUpdater::createUpdateModel() function. Checks that the first line doesn't
 * change if you use the same CsvUpdater object multiple times.
 * @example test-D-004.cpp
 * Test D-004 - Tests the CsvUpdater::createUpdateModel() function. Checks that the first line doesn't
 * change if you use the same CsvUpdater object multiple times.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		CsvUpdater updater(table);
		string filename = "test-D-004.csv";

		updater.createUpdateModel(filename);
		std::fstream stream(filename.c_str(), std::ios::in);
		if (!stream.is_open())
			throw CsvUpdaterException(
					&updater,
					CsvUpdaterException::streamError,
					__PRETTY_FUNCTION__,
					"Can't open " + filename);
		string ctrlLine1;
		getline(stream, ctrlLine1);
		assert(!stream.eof());
		stream.close();


		updater.createUpdateModel(filename);
		stream.open(filename.c_str(), std::ios::in);
		if (!stream.is_open())
			throw CsvUpdaterException(
					&updater,
					CsvUpdaterException::streamError,
					__PRETTY_FUNCTION__,
					"Can't open " + filename);
		string ctrlLine2;
		getline(stream, ctrlLine2);
		assert(!stream.eof());
		stream.close();

		assert (ctrlLine1 == ctrlLine2);

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
