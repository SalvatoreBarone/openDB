/**
 * @file test-D-012.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-012 - Tests the CsvUpdater::createUpdateModel() function. Checks that the Columns name are
 * printed in the right order. Without quoting and with a different field separator.
 * @example test-D-012.cpp
 * Test D-012 - Tests the CsvUpdater::createUpdateModel() function. Checks that the Columns name are
 * printed in the right order. Without quoting and with a different field separator.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		CsvUpdater updater(table);
		CsvUpdater::Setting setting('|', '"', false);
		updater.setUpdaterSetting(setting);
		string filename = "test-D-012.csv";
		string columnRow = "internalID|operation|Boolean|Date|Time|Timestamp|Small|Int|Bigint|Real|Double|Numeric|Char|VChar";

		updater.createUpdateModel(filename);
		std::fstream stream(filename.c_str(), std::ios::in);
		if (!stream.is_open())
			throw CsvUpdaterException(
					&updater,
					CsvUpdaterException::streamError,
					__PRETTY_FUNCTION__,
					"Can't open " + filename);

		string line;
		getline(stream, line); // first line ignored, it's the table name control string
		assert(!stream.eof());
		getline(stream, line); // second line ignored, it's the session control string
		assert(!stream.eof());
		getline(stream, line); // third line it's the column name row!
		assert(!stream.eof());
		cout << columnRow << endl;
		cout << line << endl;
		assert(line == columnRow);

		getline(stream, line);
		assert(line.empty());
		assert(stream.eof());

		stream.close();

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
