/**
 * @file test-D-013.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-013 - Tests the CsvUpdater::createUpdateModel() function. Checks that the Columns values are
 * printed in the right order. Without quoting and with a different field separator.
 * @example test-D-013.cpp
 * Test D-013 - Tests the CsvUpdater::createUpdateModel() function. Checks that the Columns values are
 * printed in the right order. Without quoting and with a different field separator.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		},
		valueMap2 =
		{
			{"Boolean", "false"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Char", "a different string"},
			{"VChar", "valueMap2"}
		},
		valueMap3 =
		{
			{"Boolean", "false"},
			{"Date", "23/12/2017"},
			{"Time", "12:40"},
			{"Timestamp", "23/12/2017 12:40"},
			{"Small", "31"},
			{"Int", "431"},
			{"Bigint", "8654"},
			{"Real", "43.1"},
			{"Double", "754.321"},
			{"Numeric", "134.5678"},
			{"Char", "a second different string"},
			{"VChar", "valueMap3"}
		};

		Record record1 = table.getStorage().newRecord();
		record1.edit(valueMap1);
		assert(table.getStorage().addRecord(record1) != table.getStorage().end());
		Record record2 = table.getStorage().newRecord();
		record2.edit(valueMap2);
		assert(table.getStorage().addRecord(record2) != table.getStorage().end());
		Record record3 = table.getStorage().newRecord();
		record3.edit(valueMap3);
		assert(table.getStorage().addRecord(record3) != table.getStorage().end());

		CsvUpdater updater(table);
		CsvUpdater::Setting setting('|', '"', false);
		updater.setUpdaterSetting(setting);
		string filename = "test-D-013.csv";
		string columnRow = "internalID|operation|Boolean|Date|Time|Timestamp|Small|Int|Bigint|Real|Double|Numeric|Char|VChar";
		string row1 = record1.getKey() + "|update|true|2012-11-14|12:11:00|23/12/2017 11:38|12|1234|12345678|12.34|12.34567|1234.5678|a string|valueMap1";
		string row2 = record2.getKey() + "|update|false|23/12/2017|12:12|23/12/2017 12:12|21|4321|87654|43.21|7654.321|1234.5678|a different string|valueMap2";
		string row3 = record3.getKey() + "|update|false|23/12/2017|12:40|23/12/2017 12:40|31|431|8654|43.1|754.321|134.5678|a second different string|valueMap3";

		updater.createUpdateModel(filename);
		std::fstream stream(filename.c_str(), std::ios::in);
		if (!stream.is_open())
			throw CsvUpdaterException(
					&updater,
					CsvUpdaterException::streamError,
					__PRETTY_FUNCTION__,
					"Can't open " + filename);

		string line;
		getline(stream, line); // first line ignored, it's the table name control string
		assert(!stream.eof());
		getline(stream, line); // second line ignored, it's the session control string
		assert(!stream.eof());

		getline(stream, line);
		assert(!stream.eof());
		cout << line << endl;
		cout << columnRow << endl;
		assert(line == columnRow);

		getline(stream, line);
		assert(!stream.eof());
		cout << line << endl;
		cout << row1 << endl;
		assert(line == row1);

		getline(stream, line);
		assert(!stream.eof());
		cout << line << endl;
		cout << row2 << endl;
		assert(line == row2);

		getline(stream, line);
		assert(!stream.eof());
		cout << line << endl;
		cout << row3 << endl;
		assert(line == row3);

		getline(stream, line);
		assert(line.empty());
		assert(stream.eof());

		stream.close();

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
