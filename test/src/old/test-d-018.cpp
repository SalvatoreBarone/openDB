/**
 * @file test-D-018.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-018 - test of the CsvUpdater::parseUpdateModel() function. Verifies that an exception is
 * thrown if, for the open file, the check on the first of the two control strings is passed, but the check
 * on the second control string fails, because the second line of the file is empty.
 * @example test-D-018.cpp
 * Test D-018 - test of the CsvUpdater::parseUpdateModel() function. Verifies that an exception is
 * thrown if, for the open file, the check on the first of the two control strings is passed, but the check
 * on the second control string fails, because the second line of the file is empty.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		SHA256_sum_t hashsum;
		SHA256_hashsum((uint8_t*)table.getName().c_str(), table.getName().size(), &hashsum);
		std::stringstream ss;
		for (int i = 0; i < 8; i++)
			ss << std::hex << std::setfill('0') << std::setw(8) << hashsum.byte_array[i];

		fstream stream("test-D-018.csv", ios::out);
		assert(stream.is_open());
		stream << ss.str() << endl;
		stream.close();

		CsvUpdater updater(table);
		updater.parseUpdateModel("test-D-018.csv", "log.txt");
		assert(false && "Unthrown exception");

	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
