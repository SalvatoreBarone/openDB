/**
 * @file test-D-020.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-020 - test of the CsvUpdater::parseUpdateModel() function. Verify that an exception is thrown
 * if, for the open file, the control on the first and second control string is exceeded, but the third row,
 * that relating to column names, is missing or empty.
 * @example test-D-020.cpp
 * Test D-020 - test of the CsvUpdater::parseUpdateModel() function. Verify that an exception is thrown
 * if, for the open file, the control on the first and second control string is exceeded, but the third row,
 * that relating to column names, is missing or empty.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-020.csv";
		Table table("table");

		CsvUpdater updater(table);
		updater.createUpdateModel(filename); // the file created doesn't contain the third line

		// adding columns to the Table object
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		updater.parseUpdateModel(filename, "log.txt");
		assert(false && "Unthrown exception");

	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
