/**
 * @file test-D-022.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-022 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the first and the
 * second control string are correct, column names are correct and in the right order, no exception is thrown.
 * The test is performed with default settings for the CsvUpdater object, ie ',' as a field separator and '"'
 * as a quoting character.
 * @example test-D-022.cpp
 * Test D-022 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the first and the
 * second control string are correct, column names are correct and in the right order, no exception is thrown.
 * The test is performed with default settings for the CsvUpdater object, ie ',' as a field separator and '"'
 * as a quoting character.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-022.csv";
		Table table("table");
		// adding columns to the Table object
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		CsvUpdater updater(table);
		updater.createUpdateModel(filename);

		updater.parseUpdateModel(filename, "log.txt");

	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
		assert(false && "No exception must be thrown");
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
