/**
 * @file test-D-023.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-023 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the template for the
 * update is empty (without record-line), no changes are made. The test is performed with default settings for
 * the CsvUpdater object, ie ',' as a field separator and '"' as a quoting character.
 * @example test-D-023.cpp
 * Test D-023 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the template for the
 * update is empty (without record-line), no changes are made. The test is performed with default settings for
 * the CsvUpdater object, ie ',' as a field separator and '"' as a quoting character.
 */
#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-023.csv";
		Table table("table");
		// adding columns to the Table object
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		CsvUpdater updater(table);
		updater.createUpdateModel(filename);

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		},
		valueMap2 =
		{
			{"Boolean", "false"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"},
			{"VChar", "valueMap2"}
		},
		valueMap3 =
		{
			{"Boolean", "false"},
			{"Small", "31"},
			{"Int", "431"},
			{"Bigint", "8654"},
			{"Real", "43.1"},
			{"Double", "754.321"},
			{"Numeric", "134.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:40"},
			{"Timestamp", "23/12/2017 12:40"},
			{"Char", "a second different string"},
			{"VChar", "valueMap3"}
		};

		// Creating, editing and inserting four new record for table1
		Record rec1 = table.getStorage().newRecord();
		Record rec2 = table.getStorage().newRecord();
		Record rec3 = table.getStorage().newRecord();
		rec1.edit(valueMap1);
		rec2.edit(valueMap2);
		rec3.edit(valueMap3);
		table.getStorage().addRecord(rec1);
		table.getStorage().addRecord(rec2);
		table.getStorage().addRecord(rec3);

		updater.parseUpdateModel(filename, "log.txt");

		unordered_map<string, string> values;
		Table::ConstIterator cit, cbegin = table.cbegin(), cend = table.cend();
		Storage::OrderedConstIterator oit = table.getStorage().obegin();
		assert(oit != table.getStorage().oend() && "The Storage object must not be empty");
		assert(oit->getKey() == rec1.getKey() && "The first Record must be the first one added to the Storage object");
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap1.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		assert(oit->getKey() == rec2.getKey());
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap2.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		assert(oit->getKey() == rec3.getKey());
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap3.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		assert(oit == table.getStorage().oend() && "The Storage object must contain exactly 4 Record objects");

	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
		assert(false && "No exception must be thrown");
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
