/**
 * @file test-D-024.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-024 - test of the CsvUpdater::parseUpdateModel()function. Verify that if a line contains an
 * "insert" opcode, no value on the column for the identifiers of the records, and a set of valid values
 * (with respect to the SQL type of the column to which they belong), the insertion is successful. The test is
 * run with default settings for the UpdateManager object, that is, using ',' as a field separator and '"' as
 * a quoting character.
 * @example test-D-024.cpp
 * Test D-024 - test of the CsvUpdater::parseUpdateModel()function. Verify that if a line contains an
 * "insert" opcode, no value on the column for the identifiers of the records, and a set of valid values
 * (with respect to the SQL type of the column to which they belong), the insertion is successful. The test is
 * run with default settings for the UpdateManager object, that is, using ',' as a field separator and '"' as
 * a quoting character.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-024.csv";
		Table table("table");
		// adding columns to the Table object
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		CsvUpdater updater(table);
		updater.createUpdateModel(filename);

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		},
		valueMap2 =
		{
			{"Boolean", "false"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"},
			{"VChar", "valueMap2"}
		},
		valueMap3 =
		{
			{"Boolean", "false"},
			{"Small", "31"},
			{"Int", "431"},
			{"Bigint", "8654"},
			{"Real", "43.1"},
			{"Double", "754.321"},
			{"Numeric", "134.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:40"},
			{"Timestamp", "23/12/2017 12:40"},
			{"Char", "a second different string"},
			{"VChar", "valueMap3"}
		};

		Table::ConstIterator cit, cbegin = table.cbegin(), cend = table.cend();
		stringstream line1, line2, line3;
		line1 << "\"\",\"insert";
		line2 << "\"\",\"insert";
		line3 << "\"\",\"insert";
		for (cit = cbegin; cit != cend; cit++) {
			line1 << "\",\"" << valueMap1.at(cit->getName());
			line2 << "\",\"" << valueMap2.at(cit->getName());
			line3 << "\",\"" << valueMap3.at(cit->getName());
		}
		line1 << "\"";
		line2 << "\"";
		line3 << "\"";

		cout 	<< line1.str() << endl
				<< line2.str() << endl
				<< line3.str() << endl;

		fstream stream(filename.c_str(), ios::out | ios::app);
		stream 	<< line1.str() << endl
				<< line2.str() << endl
				<< line3.str() << endl;
		stream.close();

		updater.parseUpdateModel(filename, "log.txt");

		assert(table.getStorage().recordsNum() == 3 && "The Storage object must contain exactly 3 Record objects");

		unordered_map<string, string> values;
		Storage::OrderedConstIterator oit = table.getStorage().obegin();
		assert(oit != table.getStorage().oend() && "Storage::obegin() and Storage::oend() must differs.");
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap1.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap2.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		oit->getCurrentValues(values);
		for (cit = cbegin; cit != cend; cit++)
			assert(values.at(cit->getName()) == valueMap3.at(cit->getName()) && "Record's value must be not changed");

		oit++;
		assert(oit == table.getStorage().oend() && "The Storage object must contain exactly 3 Record objects");
	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
		assert(false && "No exception must be thrown");
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
