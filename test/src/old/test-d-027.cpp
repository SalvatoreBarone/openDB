/**
 * @file test-D-027.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-027 - test of the CsvUpdater::parseUpdateModel() function. Verify that if a row contains id
 * corresponding to one of the records and "erase" as opcode, the record is actually deleted. The test is run
 * with default settings for the UpdateManager object, that is, using ',' as a field separator and '"' as a
 * quoting character.
 * @example test-D-027.cpp
 * Test D-027 - test of the CsvUpdater::parseUpdateModel() function. Verify that if a row contains id
 * corresponding to one of the records and "erase" as opcode, the record is actually deleted. The test is run
 * with default settings for the UpdateManager object, that is, using ',' as a field separator and '"' as a
 * quoting character.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-046.csv";
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		CsvUpdater updater(table);
		updater.createUpdateModel(filename);

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		};

		// Adding the Record to be edited
		Record record = table.getStorage().newRecord();
		record.edit(valueMap1);
		table.getStorage().addRecord(record);
		assert(table.getStorage().recordsNum() == 1 && "The Storage object must contain exactly 1 Record objects");
		assert(table.getStorage().begin()->getStatus() == Record::inserting && "The status of the Record object must be Record::inserting");

		// Inserting the line to edit the record into the update-model file
		Table::ConstIterator cit, cbegin = table.cbegin(), cend = table.cend();
		stringstream line;
		line << "\"" << record.getKey() << "\",\"erase";
		for (cit = cbegin; cit != cend; cit++)
			line << "\",\"" << "";
		line << "\"";
		cout 	<< line.str() << endl;
		fstream stream(filename.c_str(), ios::out | ios::app);
		stream 	<< line.str() << endl;
		stream.close();

		// parsing the update-model
		updater.parseUpdateModel(filename, "log.txt");

		// checking the record number
		assert(table.getStorage().recordsNum() == 0 && "The Storage object must contain zero Record objects");


	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
		assert(false && "No exception must be thrown");
	}
	catch (std::exception& e) {
		cout <<e.what() << endl;
		assert(false && "No exception must be thrown");
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
