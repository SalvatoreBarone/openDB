/**
 * @file test-D-041.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-041 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the first and
 * second check strings are correct, but the column names are wrong or in the wrong order, an exception is
 * thrown. The test is performed with custom settings for the CsvUpdater object, ie '|' as a field separator
 * and no quoting character.
 * @example test-D-041.cpp
 * Test D-041 - test of the CsvUpdater::parseUpdateModel() function. Verify that if the first and
 * second check strings are correct, but the column names are wrong or in the wrong order, an exception is
 * thrown. The test is performed with custom settings for the CsvUpdater object, ie '|' as a field separator
 * and no quoting character.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

		try {
		string filename = "test-D-041.csv";
		Table table("table");

		CsvUpdater updater(table);
		CsvUpdater::Setting setting('|', '"', false);
		updater.setUpdaterSetting(setting);
		updater.createUpdateModel(filename); // the file created doesn't contain the third line

		// adding columns to the Table object
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100));

		// inserting insert column names in the file without respecting names and order

		fstream stream(filename.c_str(), ios::out | ios::app);
		assert(stream.is_open());
		stream << "id|operation|field1|field2|field3|field4|Boolean" << endl;
		stream.close();

		updater.parseUpdateModel(filename, "log.txt");
		assert(false && "Unthrown exception");

	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
