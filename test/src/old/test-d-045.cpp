/**
 * @file test-D-045.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test D-045 - test of the CsvUpdater::parseUpdateModel() function. Check that if a row contains id
 * corresponding to one of the records and "update" (or "partial") as opcode, the record is actually updated
 * using the values of the specified fields (ignoring key and empty fields). The test is run with custom
 * settings for the UpdateManager object, that is, using '!' as a field separator and no quoting
 * character.
 * @example test-D-045.cpp
 * Test D-045 - test of the CsvUpdater::parseUpdateModel() function. Check that if a row contains id
 * corresponding to one of the records and "update" (or "partial") as opcode, the record is actually updated
 * using the values of the specified fields (ignoring key and empty fields). The test is run with custom
 * settings for the UpdateManager object, that is, using '!' as a field separator and no quoting
 * character.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	try {
		string filename = "test-D-045.csv";
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		CsvUpdater updater(table);
		updater.setUpdaterSetting(CsvUpdater::Setting('|', '"', false));
		updater.createUpdateModel(filename);

		unordered_map<string, string> valueMap1 =
		{
			{"Boolean", "true"},
			{"Small", "12"},
			{"Int", "1234"},
			{"Bigint", "12345678"},
			{"Real", "12.34"},
			{"Double", "12.34567"},
			{"Numeric", "1234.5678"},
			{"Date", "2012-11-14"},
			{"Time", "12:11:00"},
			{"Timestamp", "23/12/2017 11:38"},
			{"Char", "a string"},
			{"VChar", "valueMap1"}
		},
		valueMap2 =
		{
			{"Boolean", "false"},
			{"Small", "21"},
			{"Int", "4321"},
			{"Bigint", "87654"},
			{"Real", "43.21"},
			{"Double", "7654.321"},
			{"Numeric", "1234.5678"},
			{"Date", "23/12/2017"},
			{"Time", "12:12"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"}
		};

		// Adding the Record to be edited
		Record record = table.getStorage().newRecord();
		record.edit(valueMap1);
		table.getStorage().addRecord(record);
		assert(table.getStorage().recordsNum() == 1 && "The Storage object must contain exactly 1 Record objects");
		assert(table.getStorage().begin()->getStatus() == Record::inserting && "The status of the Record object must be Record::inserting");

		// Inserting the line to edit the record into the update-model file
		Table::ConstIterator cit, cbegin = table.cbegin(), cend = table.cend();
		stringstream line;
		line << record.getKey() << "|update";
		for (cit = cbegin; cit != cend; cit++) {
			if (!cit->getIsKey())
				line << "|" << valueMap2.at(cit->getName());
			else
				line << "|" << "";
		}
		cout 	<< line.str() << endl;
		fstream stream(filename.c_str(), ios::out | ios::app);
		stream 	<< line.str() << endl;
		stream.close();

		// parsing the update-model
		updater.parseUpdateModel(filename, "log.txt");

		// checking the record number
		assert(table.getStorage().recordsNum() == 1 && "The Storage object must contain exactly 1 Record objects");
		// cheching the record values and status
		unordered_map<string, string> values;
		Storage::ConstIterator oit = table.getStorage().cbegin();
		assert(oit != table.getStorage().cend() && "Storage::cbegin() and Storage::cend() must differs.");
		oit->getCurrentValues(values);

		for (cit = cbegin; cit != cend; cit++) {
			assert(values.find(cit->getName()) != values.end() && "Column not found in the values map");
			if (cit->getIsKey()) {
				assert(valueMap1.find(cit->getName()) != valueMap1.end() && "ValueMap1 doesn't contain this field");
				assert(values.at(cit->getName()) == valueMap1.at(cit->getName()) && "Record's key value must be not changed");
			}
			else {
				assert(valueMap2.find(cit->getName()) != valueMap2.end() && "ValueMap2 doesn't contain this field");
				assert(values.at(cit->getName()) == valueMap2.at(cit->getName()) && "Record's value must be changed");
			}
		}



		unordered_map<string, string> valueMap3 =
		{
			{"Int", "4321"},
			{"Real", "43.21"},
			{"Date", "23/12/2017"},
			{"Timestamp", "23/12/2017 12:12"},
			{"Char", "a different string"}
		};

		table.getStorage().clean();
		updater.createUpdateModel(filename);

		record = table.getStorage().newRecord();
		record.edit(valueMap1);
		table.getStorage().addRecord(record);
		assert(table.getStorage().recordsNum() == 1 && "The Storage object must contain exactly 1 Record objects");
		assert(table.getStorage().begin()->getStatus() == Record::inserting && "The status of the Record object must be Record::inserting");

		// Inserting the line to edit the record into the update-model file
		line.str(string());
		line << record.getKey() << "|partial";
		unordered_map<string, string>::const_iterator vit;
		for (cit = cbegin; cit != cend; cit++) {
			vit = valueMap3.find(cit->getName());
			if (vit != valueMap3.end())
				line << "|" << vit->second;
			else
				line << "|" << "";
		}
		cout 	<< line.str() << endl;
		stream.open(filename.c_str(), ios::out | ios::app);
		stream 	<< line.str() << endl;
		stream.close();

		// parsing the update-model
		updater.parseUpdateModel(filename, "log.txt");

		// checking the record number
		assert(table.getStorage().recordsNum() == 1 && "The Storage object must contain exactly 1 Record objects");
		oit = table.getStorage().cbegin();
		assert(oit != table.getStorage().cend() && "Storage::cbegin() and Storage::cend() must differs.");
		oit->getCurrentValues(values);

		for (cit = cbegin; cit != cend; cit++) {
			vit = valueMap3.find(cit->getName());
			cout << cit->getName() << "\t" << values.at(cit->getName()) << endl;
			if (vit != valueMap3.end())
				assert(values.at(cit->getName()) == valueMap3.at(cit->getName()) && "Record's value must be changed");
			else
				assert(values.at(cit->getName()) == valueMap1.at(cit->getName()) && "Record's value must be not changed");
		}


	} catch (CsvUpdaterException& e) {
		cout << e.what() << endl;
		assert(false && "No exception must be thrown");
	}
	catch (std::exception& e) {
		cout <<e.what() << endl;
		assert(false && "No exception must be thrown");
	}
	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
