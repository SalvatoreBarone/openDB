/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* TEST CsvUpdateManager
 * 45 - update() con string check non attivo, controlString coerente, struttura non conforme, record conforme
 */

#include "test.hpp"

int main ()
{
	try
	{
		Table table1("table1"), table2 ("table2");

		table1.addColumn("Boolean", Boolean());
		table1.addColumn("Date", Date());
		table1.addColumn("Time", Time());
		table1.addColumn("Small", Smallint());
		table1.addColumn("Int", Integer());
		table1.addColumn("Bigint", Bigint());
		table1.addColumn("Real", Real());
		table1.addColumn("Double", DoublePrecision());
		table1.addColumn("Numeric", Numeric(10,5));
		table1.addColumn("Char", Character(100));
		table1.addColumn("VChar", Varchar(100));

		Record rec1 = table1.storage().newRecord(Record::loaded);
		Record rec2 = table1.storage().newRecord(Record::loaded);
		Record rec3 = table1.storage().newRecord(Record::loaded);
		Record rec4 = table1.storage().newRecord(Record::loaded);
		Record rec5 = table1.storage().newRecord(Record::loaded);

		rec1.edit(valueMap1);
		rec2.edit(valueMap2);
		rec3.edit(valueMap3);
		rec4.edit(valueMap4);
		rec5.edit(valueMap1);

		table1.storage().insertRecord(rec1);
		table1.storage().insertRecord(rec2);
		table1.storage().insertRecord(rec3);
		table1.storage().insertRecord(rec4);
		table1.storage().insertRecord(rec5);

		CsvUpdateManager upmanager(table1);
		fstream stream("csv/test_45.csv", ios::out);
		upmanager.model(stream);
		stream.close();

		cout <<"Premi invio quando hai finito" <<endl;
		cin.get();

		list<string> log;
		stream.open("csv/test_45.csv", ios::in);
		upmanager.parse(stream, log, false);
		stream.close();

		for (list<string>::const_iterator it = log.begin(); it != log.end(); it++)
			cout <<*it <<endl;

		cout <<endl;

		for (ColumnConstIterator cit = table1.cbegin(); cit != table1.cend(); cit++)
			cout << cit->name() <<";";
		cout <<endl;
		for (StorageConstIterator it = table1.storage().cbegin(); it != table1.storage().cend(); it++)
		{
			unordered_map<string, string> values;
			(*it).current(values);
			for (ColumnConstIterator cit = table1.cbegin(); cit != table1.cend(); cit++)
				cout <<values[cit->name()] <<";";
			cout <<endl;
		}

	}
	catch (BasicException& e)
	{
		cout << e.what() <<endl;
	}

}
