/**
 * @file test-F-001.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test F-001 - Testing Schema::addTable(), trying to add a table whose name match a table belonging to the schema
 * @example test-F-001.cpp
 * Test F-001 - Schema::addTable(), trying to add a table whose name is already assigned to a table belonging
 * to the schema<br>
 * The purpose of this test is to check that the table you are trying to add is not actually added, that the
 * function returns an iterator pointing the past-the-end object, and that an exception is fired when you try
 * to access that object using this iterator.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include "test.hpp"

#include <cassert>
#include <string>
#include <list>
#include <unordered_map>
#include <iostream>
using namespace std;
using namespace openDB;

#include <csignal>

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main ()
{
	signal(SIGABRT, abortHandler);
	
	try
	{
		// Define a new Schema object
		Schema schema("schema");
		// Add a Table object to the schema previously created
		Schema::Iterator tab_it1 = schema.addTable("table1");
		// Define the structure of the Table object previously created
		tab_it1->addColumn("Boolean", Boolean());
		tab_it1->addColumn("Date", Date());
		tab_it1->addColumn("Time", Time());
		tab_it1->addColumn("Small", Smallint());
		tab_it1->addColumn("Int", Integer());
		tab_it1->addColumn("Bigint", Bigint());
		tab_it1->addColumn("Real", Real());
		tab_it1->addColumn("Double", DoublePrecision());
		tab_it1->addColumn("Numeric", Numeric(10,5));
		tab_it1->addColumn("Char", Character(100));
		tab_it1->addColumn("VChar", Varchar(100));
		// Add a second Table object to the schema previously created
		Schema::Iterator tab_it2 = schema.addTable("table2");
		tab_it2->addColumn("Boolean", Boolean());
		tab_it2->addColumn("Date", Date());
		tab_it2->addColumn("Time", Time());
		tab_it2->addColumn("Small", Smallint());
		tab_it2->addColumn("Int", Integer());
		tab_it2->addColumn("Bigint", Bigint());
		tab_it2->addColumn("Real", Real());
		tab_it2->addColumn("Double", DoublePrecision());
		tab_it2->addColumn("Numeric", Numeric(10,5));
		tab_it2->addColumn("Char", Character(100));
		tab_it2->addColumn("VChar", Varchar(100));

		// Trying to add a Table object having the same name of the one previously added
		Schema::Iterator tab_it3 = schema.addTable("table2");
		// The function must return end()
		assert(tab_it3 == schema.end());

		// An exception will be fired!
		// Trying to define the structure of the Table object added...
		tab_it3->addColumn("Boolean", Boolean());
		tab_it3->addColumn("Date", Date());
		tab_it3->addColumn("Time", Time());
		tab_it3->addColumn("Small", Smallint());
		tab_it3->addColumn("Int", Integer());
		tab_it3->addColumn("Bigint", Bigint());
		tab_it3->addColumn("Real", Real());
		tab_it3->addColumn("Double", DoublePrecision());
		tab_it3->addColumn("Numeric", Numeric(10,5));
		tab_it3->addColumn("Char", Character(100));
		tab_it3->addColumn("VChar", Varchar(100));
		
		cout << __FILE__ << TEST_FAILED <<endl;

	}
	catch (BasicException& e)
	{
		cout << __FILE__ << TEST_PASSED << e.what() << endl;
	}
	return 0;
}
