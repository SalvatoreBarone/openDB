/**
 * @file test-F-003.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test F-003 - Test the copy-constructor for the Schema class.
 * @example test-F-003.cpp
 * Test F-003 - Test the copy-constructor for the Schema class.<br>
 * The purpose of the test is to verify that the function returns a Schema object identical to the original one
 * and that copy maintain references to the original object both in terms of structure and content.
 * The test, after adding new Tables object and Record objects to the copy, verifies that the original one has
 * changed.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include "test.hpp"

#include <cassert>
#include <string>
#include <list>
#include <unordered_map>
#include <iostream>
using namespace std;
using namespace openDB;


#include <csignal>

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main ()
{
	signal(SIGABRT, abortHandler);
	Schema schema1("schema");
	// Adding a Table object to the previously created Schema Object
	Schema::Iterator tab_it1 = schema1.addTable("table1");
	// Defining the structure of the table
	tab_it1->addColumn("Boolean", Boolean());
	tab_it1->addColumn("Date", Date());
	tab_it1->addColumn("Time", Time());
	tab_it1->addColumn("Small", Smallint());
	tab_it1->addColumn("Int", Integer());
	tab_it1->addColumn("Bigint", Bigint());
	tab_it1->addColumn("Real", Real());
	tab_it1->addColumn("Double", DoublePrecision());
	tab_it1->addColumn("Numeric", Numeric(10,5));
	tab_it1->addColumn("Char", Character(100));
	tab_it1->addColumn("VChar", Varchar(100));
	// Adding some records to the table
	Record rec1 = tab_it1->getStorage().newRecord();
	Record rec2 = tab_it1->getStorage().newRecord();
	rec1.edit(valueMap1);
	rec2.edit(valueMap2);
	tab_it1->getStorage().addRecord(rec1);
	tab_it1->getStorage().addRecord(rec2);

	try
	{
		Schema schema2 = schema1;

		// The two schema object must have the same name
		assert(schema1.getName() == schema2.getName());

		// The two Schema object will have the same structure and content
		// Adding a new table to schema1
		Schema::Iterator tab_it2 = schema1.addTable("table2");
		tab_it2->addColumn("Boolean", Boolean());
		tab_it2->addColumn("Date", Date());
		tab_it2->addColumn("Time", Time());
		tab_it2->addColumn("Small", Smallint());
		tab_it2->addColumn("Int", Integer());
		tab_it2->addColumn("Bigint", Bigint());
		tab_it2->addColumn("Real", Real());
		tab_it2->addColumn("Double", DoublePrecision());
		tab_it2->addColumn("Numeric", Numeric(10,5));
		tab_it2->addColumn("Char", Character(100));
		tab_it2->addColumn("VChar", Varchar(100));
		// Adding some records to the table
		Record rec3 = tab_it2->getStorage().newRecord();
		Record rec4 = tab_it2->getStorage().newRecord();
		rec3.edit(valueMap1);
		rec4.edit(valueMap2);
		tab_it2->getStorage().addRecord(rec3);
		tab_it2->getStorage().addRecord(rec4);


		// Also schema2 will be affected
		assert(schema1.tablesNum() == schema2.tablesNum());

		// The two schema object must have the same structure
		Schema::ConstIterator tit1 = schema1.cbegin(), tend1 = schema1.cend();
		for (; tit1 != tend1; tit1++) {
			Schema::ConstIterator tit2 = schema2.cat(tit1->getName());
			assert (tit2 != schema2.cend()); // The table must exist

			// table1 and table 2 must have the same structure
			assert(tit1->columnsNum() == tit2->columnsNum());
			Table::ConstIterator cit = tit1->cbegin(), cend = tit1->cend();
			for (; cit != cend; cit++) {
				assert(tit2->cat(cit->getName()) != tit2->cend()); // check if columns name match
				Table::ConstIterator cit2 = tit2->cat(cit->getName());
				int type_id1 = cit->getSqlType()->typeInfo().internalID;
				int type_id2 = cit2->getSqlType()->typeInfo().internalID;
				assert(type_id1 == type_id2); // check if type match
				bool key2 = cit2->getIsKey();
				bool key1 = cit->getIsKey();
				assert(key1 == key2);
			}

			// table1 and table 2 must have the same records
			assert(tit2->getStorage().recordsNum() == tit1->getStorage().recordsNum());
			Storage::ConstIterator sit1 = tit1->getStorage().cbegin(), send1 = tit1->getStorage().cend();
			Storage::ConstIterator sit2 = tit2->getStorage().cbegin(), send2 = tit2->getStorage().cend();
			std::unordered_map<std::string, std::string> current1, current2, old1, old2;
			unordered_map<string, string>::const_iterator vit, vend;
			while (sit1 != send1 && sit2 != send2) {
				sit1->getCurrentValues(current1);
				sit1->getOldValues(old1);
				sit2->getCurrentValues(current2);
				sit2->getOldValues(old2);
				// check if current values match
				vit = current1.begin();
				vend = current1.end();
				for (; vit != vend; vit++)
					assert(vit->second == current2.at(vit->first));
				// check if old values match
				vit = old1.begin();
				vend = old1.end();
				for (; vit != vend; vit++)
					assert(vit->second == old2.at(vit->first));

				sit1++;
				sit2++;
			}
		}

		cout << __FILE__ << TEST_PASSED << endl;
	}
	catch (BasicException& e)
	{
		cout << __FILE__ << TEST_FAILED << e.what() <<endl;
	}
}

