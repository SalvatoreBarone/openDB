/**
 * @file test-G-001.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test G-001 - Testing Database::addSchema(), trying to add a schema whose name match a schma belonging to the database.
 * @example test-G-001.cpp
 * Test G-001 - Testing Database::addSchema(), trying to add a schema whose name match a schma belonging to the
 * database.<br>
 * The purpose of this test is to check that the schema you are trying to add is not actually added, that the
 * function returns an iterator pointing the past-the-end object, and that an exception is fired when you try
 * to access that object using that iterator.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include "test.hpp"

#include <cassert>
#include <string>
#include <list>
#include <unordered_map>
#include <iostream>
using namespace std;
using namespace openDB;


#include <csignal>

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main ()
{
	signal(SIGABRT, abortHandler);
	
try
	{
		// Define a new Schema object
		Database database1;
		// Add a Schema object to the Database object previously created
		database1.addSchema("schema1");

		// Trying to add a Schema object having the same name of the one previously added
		Database::Iterator schema2 = database1.addSchema("schema1");

		// the function must return end()
		assert(schema2 == database1.end());

		// an exception must be fired!
		Schema::Iterator tab_it1 = schema2->addTable("table1");

		tab_it1->addColumn("acolumn", Varchar(100));
		
		cout << __FILE__ << TEST_FAILED <<endl;

	}
	catch (BasicException& e)
	{
		cout << __FILE__ << TEST_PASSED << e.what() << endl;
	}
	return 0;
}
