/**
 * @file test-G-002.cpp
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test G-002 -
 * @example test-G-002.cpp
 * Test G-002 -
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include "test.hpp"

#include <cassert>
#include <string>
#include <list>
#include <unordered_map>
#include <iostream>
using namespace std;
using namespace openDB;


#include <csignal>

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main ()
{
	signal(SIGABRT, abortHandler);


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
