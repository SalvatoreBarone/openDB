/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* TEST Classe Database
 * 74 - importazione struttura database da tabella con struttura non compatibile
 */

#include "test.hpp"
#include "core/DatabaseBuilder.hpp"

int main ()
{
	try
	{
		Table table("table");
		table.addColumn("Table_schema", Varchar());
		table.addColumn("Table_name", Varchar());
		table.addColumn("Column_name", Varchar());
		table.addColumn("Udt_name", Varchar());
		table.addColumn("Character_maximum_length", Integer());
		table.addColumn("Numeric_precision", Integer());
		table.addColumn("Numeric_scale", Integer());
		table.addColumn("Is_key", Boolean());

		CsvUpdateManager upmanager(table);
		fstream stream("csv/test_74.csv", ios::out);
		upmanager.model(stream);
		stream.close();

		cout <<"Premi invio quando sei pronto" <<endl;
		cin.get();

		stream.open("csv/test_74.csv", ios::in);
		list<string> log;
		upmanager.parse(stream, log);
		stream.close();

		for (list<string>::const_iterator it = log.begin(); it != log.end(); it++)
			cout <<*it <<endl;

		DatabaseBuilder dbuilder("database");
		Database database = dbuilder.fromTable(table);

		cout <<endl <<database.name() <<endl;
		for (SchemaConstIterator schema_it = database.cbegin(); schema_it != database.cend(); schema_it++)
		{
			cout <<"\t" << schema_it->name() <<endl;
			for (TableConstIterator tab_it = schema_it->cbegin(); tab_it != schema_it->cend(); tab_it++)
			{
				cout <<"\t\t" << tab_it->name() <<endl;
				for (ColumnConstIterator col_it = tab_it->cbegin(); col_it != tab_it->cend(); col_it++)
					cout <<"\t\t\t" <<col_it->name() <<" " <<col_it->typeInfo().udtName <<";" <<endl;
			}
		}

	}
	catch (BasicException& e)
	{
		cout << e.what() <<endl;
	}
	return 0;
}
