/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* TEST Classe Database
 * 75 - importazione struttura database da tabella creata dal modulo backend (postgres)
 */

#include "test.hpp"
#include "core/DatabaseBuilder.hpp"
#include "backend/Transaction.hpp"
#include "backend/PgConnection.hpp"
#include "backend/NonTransactional.hpp"
#include "backend/BackendConnector.hpp"
#include "backend/BackendIterator.hpp"

int main ()
{
	try
	{
		BackendConnector remoteDb;
		remoteDb.hostaddr("milky");
		remoteDb.dbname("decoration");
		remoteDb.username("ssaa");
		remoteDb.password("9aF7lYL6");
		remoteDb.timeout(5);
		remoteDb.connect();

		string querycmd = PgConnection::pgDbStructureQuery;
		NonTransactional txn;
		txn.appendCommand(querycmd);
		BackendIterator bit = remoteDb.execTransaction(txn);

		Table table = bit.result().resultTable();

		DatabaseBuilder dbuilder("database");
		Database database = dbuilder.fromTable(table);

		cout <<endl <<database.name() <<endl;
		for (SchemaConstIterator schema_it = database.cbegin(); schema_it != database.cend(); schema_it++)
		{
			cout <<"\t" << schema_it->name() <<endl;
			for (TableConstIterator tab_it = schema_it->cbegin(); tab_it != schema_it->cend(); tab_it++)
			{
				cout <<"\t\t" << tab_it->name() <<endl;
				for (ColumnConstIterator col_it = tab_it->cbegin(); col_it != tab_it->cend(); col_it++)
					cout <<"\t\t\t" <<col_it->name() <<" " <<col_it->typeInfo().udtName <<(col_it->isKey() ? " Key" : "") <<";" <<endl;
			}
		}


	}
	catch (BasicException& e)
	{
		cout << e.what() <<endl;
	}
	return 0;
}
