/* Copyright 2013-2015 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* TEST Classe Database
 * 76 - 724 - importazione struttura database da file di configurazione
 */

#include <string>
#include <iostream>
#include <fstream>
#include "../openDB2core.hpp"
using namespace std;
using namespace openDB;
int main (int argc, char** argv)
{
	if (argc != 2)
		cout << "Uso: test_76_723 fileName" <<endl;

	string fileName = argv[1];

	try {
		DatabaseBuilder dbuilder;
		Database database = dbuilder.fromConfigFile(fileName);

		cout << database.name() << endl;
		Database::Iterator db_it = database.begin(), db_end = database.end();
		for (; db_it != db_end; db_it++)
		{
			cout << "\t" << (*db_it).name() << endl;
			Schema::Iterator sc_it = (*db_it).begin(), sc_end = (*db_it).end();
			for (; sc_it != sc_end; sc_it++)
			{
				cout << "\t\t" << (*sc_it).name() << endl;
				Table::Iterator tb_it = (*sc_it).begin(), tb_end = (*sc_it).end();
				for (; tb_it != tb_end; tb_it++)
					cout << "\t\t\t" << (*tb_it).name() <<"\t" <<((*tb_it).isKey() ? "key" : "") <<"\t" <<(*tb_it).typeInfo().udtName <<"\t" <<(*tb_it).typeInfo().vcharLength <<"\t" <<(*tb_it).typeInfo().numericPrecision <<"\t" <<(*tb_it).typeInfo().numericScale << endl;

			}
		}
	}
	catch (DatabaseBuilderException& e) {
		cout << e.what() <<endl;
	}


}
