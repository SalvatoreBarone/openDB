
/**
 * @file test-I-000.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-000 - Tests the Sqlite3Connection::createAndConnect() function. Checks that a new database is
 * created on the first call to the function and verifies that no exception is thrown when you use the same
 * function or the connect() function on the newly created database.
 * @example test-I-000.cpp
 * Test I-000 - Tests the Sqlite3Connection::createAndConnect() function. Checks that a new database is
 * created on the first call to the function and verifies that no exception is thrown when you use the same
 * function or the connect() function on the newly created database.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sqlite3Connection connection;
	connection.setFileName("test-I-0xx.sql");
	try {
		connection.createAndConnect();
		connection.disconnect();
	} catch (BackendException& e) {
		cout << e.what() << endl;
		assert(false);
	}

	Sqlite3Connection connection2;
	connection2.setFileName("test-I-0xx.sql");
	try {
		connection2.createAndConnect();
		connection2.disconnect();
	} catch (BackendException& e) {
		cout << e.what() << endl;
		assert(false);
	}

	Sqlite3Connection connection3;
	connection3.setFileName("test-I-0xx.sql");
	try {
		connection3.connect();
		connection3.disconnect();
	} catch (BackendException& e) {
		cout << e.what() << endl;
		assert(false);
	}

	cout << __FILE__ << TEST_PASSED << endl;

	return 0;
}
