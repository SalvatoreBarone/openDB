
/**
 * @file test-I-007.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-007 - Test the Sqlite3Connection::exec() and Table::loadRecord() functions, performing the
 * load of records from a database.
 * @example test-I-007.cpp
 * Test I-007 - Test the Sqlite3Connection::exec() and Table::loadRecord() functions, performing the
 * load of records from a database.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sqlite3Connection connection;
	connection.setFileName("test-I-007.sql");

	try {
		// create a new empty database
		connection.createAndConnect();

		// create a new table in the database
		Result result1;
		connection.exec("begin;\
						create table atable (field1 varchar(10), field2 varchar(10));\
						insert into atable(field1, field2) values('value1', 'value2');\
						end;", result1);
		assert(result1.getStatus() == Result::commandOk);

		// create a Table object
		Table table("atable");
		table.addColumn("field1", Varchar(10));
		table.addColumn("field2", Varchar(10));

		// queryng the database
		Result result3;
		connection.exec(table.loadCommand(), result3);

		// performing some checks
		assert(result3.getStatus() == Result::tupleOk);
		assert(result3.getStorage().recordsNum() == 1);
		assert(result3.getStorage().begin()->getCurrentValue("field1") == "value1");
		assert(result3.getStorage().begin()->getCurrentValue("field2") == "value2");

		// load records from database
		table.loadRecord(result3);
		assert(table.getStorage().recordsNum() == 1);
		assert(table.getStorage().begin()->getStatus() == Record::loaded);
		assert(table.getStorage().begin()->getCurrentValue("field1") == "value1");
		assert(table.getStorage().begin()->getCurrentValue("field2") == "value2");

		connection.disconnect();

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
