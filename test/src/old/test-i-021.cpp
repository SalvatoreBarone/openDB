
/**
 * @file test-I-021.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-021 - Tests the Sqlite3Connector::connect() function. Checks that a BackendException is
 * thrown if you try to use the function on a non-existent database.
 * @example test-I-021.cpp
 * Test I-021 - Tests the Sqlite3Connector::connect() function. Checks that a BackendException is
 * thrown if you try to use the function on a non-existent database.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sqlite3Connector connection;

	connection.setFileName("nonExistentDatabase.sql");
	try {
		connection.connect();
		assert(false);
	} catch (BackendException& e) {
		cout << e.what() << endl;
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
