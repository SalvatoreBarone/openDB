
/**
 * @file test-I-028.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-028 - Tests the Sqlite3Connector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used to execute a transaction mode. A second attempt is made, attempting to insert
 * other records. The command list is added with a command that will fail, in order to verify that no
 * further insertion takes place.
 * @example test-I-028.cpp
 * Test I-028 - Tests the Sqlite3Connector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used to execute a transaction mode. A second attempt is made, attempting to insert
 * other records. The command list is added with a command that will fail, in order to verify that no
 * further insertion takes place.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sqlite3Connector connection;
	connection.setFileName("test-I-028.sql");

	try {
		connection.createAndConnect();

		// create a new table in the database
		BackendConnector::Iterator result = connection.exec(
				"create table atable (field1 varchar(10), field2 varchar(10));",
				"result1",
				false,
				false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// create a Table object
		Table table("atable");
		table.addColumn("field1", Varchar(10));
		table.addColumn("field2", Varchar(10));

		// inserting some records in the table
		const int N = 100;
		for (unsigned i = 0; i < N; i++) {
			unordered_map<string, string> vmap1;
			vmap1.insert(pair<string, string>("field1", generateRandomString()));
			Record rec1 = table.getStorage().newRecord();
			rec1.edit(vmap1);
			table.getStorage().addRecord(rec1);
		}
		assert(table.getStorage().recordsNum() == N);

		// committing changes on the database
		list<string> commandList;
		table.commit(commandList);
		result = connection.exec(commandList, "insert", true, false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// querying the database
		result = connection.exec(table.loadCommand(), "result3", false, false);
		assert(result->getResult().getStatus() == Result::tupleOk);
		table.loadRecord(result->getResult());
		// performing some checks
		assert(table.getStorage().recordsNum() == N);


		for (unsigned i = 0; i < N; i++) {
			unordered_map<string, string> vmap1;
			vmap1.insert(pair<string, string>("field1", generateRandomString()));
			Record rec1 = table.getStorage().newRecord();
			rec1.edit(vmap1);
			table.getStorage().addRecord(rec1);
		}
		assert(table.getStorage().recordsNum() == 2*N);

		// committing changes on the database
		table.commit(commandList);
		// fault injection
		commandList.push_back("afaultystring;");
		result = connection.exec(commandList, "insert", true, false);
		assert(result->getResult().getStatus() == Result::error);

		connection.exec("rollback;", "rollback", false, false);

		result = connection.exec(table.loadCommand(), "result3", false, false);
		assert(result->getResult().getStatus() == Result::tupleOk);
		table.loadRecord(result->getResult());
		assert(table.getStorage().recordsNum() == N);

		connection.disconnect();

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
