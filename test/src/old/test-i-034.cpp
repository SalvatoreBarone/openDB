
/**
 * @file test-I-034.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-034 - Tests the PgConnector::reset() function. Checks that no BackendException is
 * thrown if you try to use the function on a regular connection.
 * @example test-I-034.cpp
 * Test I-034 - Tests the PgConnector::reset() function. Checks that no BackendException is
 * thrown if you try to use the function on a regular connection.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	PgConnector connection;
	connection.setHostaddr("localhost");
	connection.setPort("5432");
	connection.setDbName("opendb_test");
	connection.setUsername("postgres");
	connection.setPassword("postgres");

	try {
		connection.connect();
		connection.reset();
		connection.disconnect();
	} catch (BackendException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
