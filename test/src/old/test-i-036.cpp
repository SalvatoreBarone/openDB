
/**
 * @file test-I-036.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-036 -  Tests the PgConnector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used in blocking mode.
 * @example test-I-036.cpp
 * Test I-036 - Tests the PgConnector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used in blocking mode.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	PgConnector connection;
	connection.setHostaddr("localhost");
	connection.setPort("5432");
	connection.setDbName("opendb_test");
	connection.setUsername("postgres");
	connection.setPassword("postgres");

	try {
		// create a new empty database
		connection.connect();

		// create a new table in the database
		BackendConnector::Iterator result = connection.exec(
				"drop table atable; create table if not exists atable (field1 varchar(10), field2 varchar(10));",
				"result1",
				false,
				false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// create a Table object
		Table table("atable");
		table.addColumn("field1", Varchar(10));
		table.addColumn("field2", Varchar(10));

		// Add a record to the object
		unordered_map<string, string> vmap1 = {
				{"field1", "value1"},
				{"field2", "value2"}
		};
		Record record = table.getStorage().newRecord();
		record.edit(vmap1);
		table.getStorage().addRecord(record);

		// committing changes on the database
		list<string> commandList;
		table.commit(commandList);
		list<string>::iterator it = commandList.begin();
		result = connection.exec(*it, "result2", false, false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// querying the database
		result = connection.exec(table.loadCommand(), "result3", false, false);
		assert(result->getResult().getStatus() == Result::tupleOk);
		table.loadRecord(result->getResult());
		// performing some checks
		assert(table.getStorage().recordsNum() == 1);
		assert(table.getStorage().begin()->getStatus() == Record::loaded);
		assert(table.getStorage().begin()->getCurrentValue("field1") == "value1");
		assert(table.getStorage().begin()->getCurrentValue("field2") == "value2");

		// updating a record

		vmap1 = {
				{"field1", "mod_value1"},
				{"field2", "mod_value2"}
		};
		Storage::Iterator sit = table.getStorage().begin();
		assert(sit->getStatus() == Record::loaded);
		sit->edit(vmap1);
		assert(table.getStorage().recordsNum() == 1);
		assert(sit->getStatus() == Record::updating);

		// committing changes on the remote database
		list<string> commandsList;
		table.commit(commandsList);
		assert(commandsList.size() == 1);
		result = connection.exec(*commandsList.begin(), "result4", false, false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// querying the database
		result = connection.exec(table.loadCommand(), "result5", false, false);
		assert(result->getResult().getStatus() == Result::tupleOk);
		table.loadRecord(result->getResult());
		// performing some checks
		assert(table.getStorage().recordsNum() == 1);
		assert(table.getStorage().begin()->getStatus() == Record::loaded);
		assert(table.getStorage().begin()->getCurrentValue("field1") == "mod_value1");
		assert(table.getStorage().begin()->getCurrentValue("field2") == "mod_value2");

		connection.disconnect();

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}


	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
