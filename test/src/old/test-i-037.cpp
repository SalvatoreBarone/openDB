
/**
 * @file test-I-037.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 *
 * @test Test I-037 -  Tests the PgConnector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used in non-blocking mode.
 * @example test-I-037.cpp
 * Test I-037 - Tests the PgConnector::exec() function, for commands and queries. This program
 * creates a new database, inserts some records in it and checks that the insertion is correctly performed
 * when the function is used in non-blocking mode.
 */

#include "openDBcore.hpp"
#include "color.hpp"
#include <cassert>
#include <csignal>
#include <string>
#include <iostream>
using namespace std;
using namespace openDB;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	PgConnector connection;
	connection.setHostaddr("localhost");
	connection.setPort("5432");
	connection.setDbName("opendb_test");
	connection.setUsername("postgres");
	connection.setPassword("postgres");

	try {
		// create a new empty database
		connection.connect();

		// create a new table in the database
		BackendConnector::Iterator result = connection.exec(
				"drop table table1; drop table table2; create table table1 (field1 varchar(10), timestamp timestamp DEFAULT CURRENT_TIMESTAMP);\
				create table table2 (field1 varchar(10), timestamp timestamp DEFAULT CURRENT_TIMESTAMP);",
				"result1",
				true,
				false);
		assert(result->getResult().getStatus() == Result::commandOk);

		// create two Table objects
		Table table1("table1");
		table1.addColumn("field1", Varchar(10));
		Table table2("table2");
		table2.addColumn("field1", Varchar(10));

		// inserting some records in the two tables
		const int N = 10000;
		for (unsigned i = 0; i < N; i++) {
			unordered_map<string, string> vmap1, vmap2;
			vmap1.insert(pair<string, string>("field1", generateRandomString()));
			vmap2.insert(pair<string, string>("field1", generateRandomString()));
			Record rec1 = table1.getStorage().newRecord();
			rec1.edit(vmap1);
			table1.getStorage().addRecord(rec1);
			Record rec2 = table2.getStorage().newRecord();
			rec2.edit(vmap2);
			table2.getStorage().addRecord(rec2);
		}
		assert(table1.getStorage().recordsNum() == N);
		assert(table2.getStorage().recordsNum() == N);

		list<string> cList1, cList2;
		table1.commit(cList1);
		table2.commit(cList2);
		assert(cList1.size() == N);
		assert(cList2.size() == N);

		BackendConnector::Iterator res1 = connection.exec(cList1, "query1", false, true);
		BackendConnector::Iterator res2 = connection.exec(cList2, "query2", false, true);
		assert(res1->getResult().getStatus() != Result::error);
		assert(res2->getResult().getStatus() != Result::error);

		// wait until the execution completes
		while(res1->getResult().getStatus() == Result::running || res2->getResult().getStatus() == Result::running);
		assert(res1->getResult().getStatus() == Result::commandOk);
		assert(res2->getResult().getStatus() == Result::commandOk);

		// checks the number of record inserted
		res1 = connection.exec(table1.loadCommand(), "table1", false, false);
		assert(res1->getResult().getStatus() == Result::tupleOk);
		assert(res1->getResult().getStorage().recordsNum() == N);
		res2 = connection.exec(table2.loadCommand(), "table2", false, false);
		assert(res2->getResult().getStatus() == Result::tupleOk);
		assert(res2->getResult().getStorage().recordsNum() == N);

	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false);
	}



	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
