/**
 * @file color.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __TEST_HEADER_FILE__
#define __TEST_HEADER_FILE__

#include <cassert>
#include <csignal>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <vector>
#include <list>
#include <unordered_map>

#define TERM_COLOR_DEFAULT	"\e[0m"
#define TERM_COLOR_RED		"\e[1;31m"
#define TERM_COLOR_GREEN	"\e[1;32m"
#define TERM_COLOR_BLUE		"\e[1;34m"
#define TERM_COLOR_YELLOW	"\e[1;33m"
#define TERM_COLOR_MAGENTA	"\e[1;35m"
#define TERM_COLOR_CYAN		"\e[1;36m"

#define TEST_PASSED "\e[1;32m [PASSED] \e[0m"
#define TEST_FAILED "\e[1;31m [FAILED] \e[0m"

#endif
