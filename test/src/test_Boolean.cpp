/**
 * @file test_Boolean.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0002
 * Tests the Boolean::validate() function.
 * The function is stimulated with the strings of the Boolean::trueValues collection, verifying that no
 * exception is thrown
 *
 */
void test_A_0002() {
	Boolean object;
	try {
		for (list<string>::const_iterator it = Boolean::trueValues.begin(); it != Boolean::trueValues.end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0002: A value in the Boolean::trueValues collection isn't evaluated as valid");
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0003
 * Tests the Boolean::validate() function.
 * The function is stimulated with the strings of the Boolean::falseValues collection, verifying that no
 * exception is thrown
 *
 */
void test_A_0003() {
	Boolean object;
	try {
		for (list<string>::const_iterator it = Boolean::falseValues.begin(); it != Boolean::falseValues.end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0003: A value in the Boolean::falseValues collection isn't evaluated as valid");
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0004
 * Tests the Boolean::validate() function.
 * The function is stimulated with the of the Boolean::trueDefault string, verifying that no exception is
 * thrown
 *
 */
void test_A_0004() {
	Boolean object;
	try {
		object.validate(Boolean::trueDefault);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0004: Boolean::trueDefault isn't evaluated as valid");
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0005
 * Tests the Boolean::validate() function.
 * The function is stimulated with the of the Boolean::falseDefault string, verifying that no exception is
 * thrown
 *
 */
void test_A_0005() {
	Boolean object;
	try {
		object.validate(Boolean::falseDefault);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0005: Boolean::falseDefault isn't evaluated as valid");
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0006
 * Tests the Boolean::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown
 *
 */
void test_A_0006() {
	Boolean object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0006: A value in the SqlType::nullValues collection isn't evaluated as valid");
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0007
 * Tests the Boolean::validate() function.
 * The function is stimulated with a string not belonging to the SqlType::nullValues, Boolean::trueValues nor
 * Boolean::falseValues collections, verifying that exception is thrown
 *
 */
void test_A_0007() {
	Boolean object;
	try {
		object.validate("a string");
		assert(false && "Test A0007: an invalid string, not belonging to the SqlType::nullValues, Boolean::trueValues nor Boolean::falseValues collections, isn't evaluated as invalid");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (std::exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test A0008
 * Test the Boolean::isTrue() function.
 * The function is stimulated with the strings of the Boolean::trueValues collection, verifying that 'true' is
 * returned.
 *
 */
void test_A_0008() {
	Boolean object;
	for (list<string>::const_iterator it = Boolean::trueValues.begin(); it != Boolean::trueValues.end(); it++)
		assert(Boolean::isTrue(*it) == true && "Test A0009: A value in the Boolean::trueValues collection isn't recognized as true");
}

/**
 * @test Test A0009
 * Test the Boolean::isTrue() function.
 * The function is stimulated with the strings of the Boolean::falseValues collection, verifying that 'false'
 * is returned.
 *
 */
void test_A_0009() {
	Boolean object;
	for (list<string>::const_iterator it = Boolean::falseValues.begin(); it != Boolean::falseValues.end(); it++)
		assert(Boolean::isTrue(*it) == false && "Test A0009: A value in the Boolean::falseValuess collection isn't recognized as false");
}

/**
 * @test Test A0010
 * Test the Boolean::isTrue() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that 'false'
 * is returned.
 *
 */
void test_A_0010() {
	Boolean object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
		assert(Boolean::isTrue(*it) == false && "Test A0010: A value in the SqlType::nullValues collection isn't recognized as false");
}

/**
 * @test Test A0011
 * Tests the Boolean::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying
 * that SqlType::null is returned.
 *
 */
void test_A_0011() {
	Boolean object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
		assert(object.prepare(*it) == SqlType::null && "Test A0011: A value in the SqlType::nullValues collection isn't prepared as SqlType::null");
 }

/**
 * @test Test A0012
 * Tests the Boolean::prepare() function.
 * The function is stimulated with the strings of the Boolean::trueValues collection, verifying that
 * Boolean::trueDefault is returned.
 *
 */
void test_A_0012() {
	Boolean object;
	for (list<string>::const_iterator it = Boolean::trueValues.begin(); it != Boolean::trueValues.end(); it++)
		assert(object.prepare(*it) == '\'' + Boolean::trueDefault + '\'' && "Test A0012: A value in the Boolean::trueValues collection isn't prepared as Boolean::trueDefault");
}

/**
 * @test Test A0013
 * Tests the Boolean::prepare() function.
 * The function is stimulated with the strings of the Boolean::falseValues collection, verifying that
 * Boolean::falseDefault is returned.
 *
 */
void test_A_0013() {
	Boolean object;
	for (list<string>::const_iterator it = Boolean::falseValues.begin(); it != Boolean::falseValues.end(); it++)
		assert(object.prepare(*it) == '\'' + Boolean::falseDefault + '\'' && "Test A0013: A value in the Boolean::falseValues collection isn't prepared as Boolean::falseDefault");
}

/**
 * @test Test A0014
 * Tests the Boolean::typeInfo() function.
 * Verifies that the function returns correct information about the Boolean type.
 */
void test_A_0014() {
	Boolean object;
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Boolean::internalID && "Test A0014: the returned internalID doesnt' match Boolean::internalID");
	assert(info.typeName == Boolean::typeName && "Test A0014: the returned typeName doesnt' match Boolean::typeName");
	assert(info.udtName == Boolean::udtName && "Test A0014: the returned udtName doesnt' match Boolean::udtName");
}


int main() {
	signal(SIGABRT, abortHandler);

	test_function function[13] = {
		test_A_0002, test_A_0003, test_A_0004, test_A_0005, test_A_0006, test_A_0007, test_A_0008,
		test_A_0009, test_A_0010, test_A_0011, test_A_0012, test_A_0013, test_A_0014
	};

	for (int i = 0; i < 13; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
