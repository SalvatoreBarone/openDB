/**
 * @file test_Character.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0131
 * Tests the Character::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown (Ex. SqlType::nullValues). The function must not throw exceptions.
 *
 */
void test_A_0131() {
	Character object(10);
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0131: The function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0132
 * Tests the Character::validate() function.
 * The function is stimulated using a string  whose length exceeds that allowed (10 characters) (Ex. “a very
 * very long string”). The function must trow a ValueTooLong exception.
 */
void test_A_0132() {
	Character object(10);
	try {
		object.validate("a very very long string");
		assert(false && "Test A0132: the funtion must throw a ValueTooLong exception");
	} catch (ValueTooLong& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0133
 * Tests the Character::validate() function.
 * The function is stimulated using a string whose length does not exceed that allowed (10 characters)
 * (Ex. “a string”). The function must not throw any exception
 */
void test_A_0133() {
	Character object(10);
	try {
		object.validate("a string");

	} catch (ValueTooLong& e) {
		cout << e.what() << endl;
		assert(false && "Test A0133: the funtion must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0134
 * Tests the Character::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying
 * that SqlType::null is returned.
 */
void test_A_0134() {
	Character object(10);
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert(object.prepare(*it) == SqlType::null && "Test A0134: valuea in the SqlType::nullValues aren't prepared as SqlType::null");
}
/**
 * @test Test A0135
 * Tests the Character::prepare() function.
 * The function is stimulated a string without single quote characters.(Ex. “a string”).
 */
void test_A_0135() {
	Character object(10);
	assert(object.prepare("a string") == "'a string'" && "Test A0135: values without single quote aren't correctly prepared");
}
/**
 * @test Test A0136
 * Tests the Character::prepare() function.
 * The function is stimulated with a string containing some single quote characters. If the input is
 * “good mornin’” then  the output must be “good mornin’’’”
 */
void test_A_0136() {
	Character object(10);
	assert(object.prepare("good mornin'") == "'good mornin'''" && "Test A0135: values with single quote aren't correctly prepared");
}
/**
 * @test Test A0137
 * Tests the Character::typeInfo() function.
 * Verifies that the function returns information about the Character type correctly. The output must be
 * (Character::internalID, Character::typeName,Character::udtName)
 */
void test_A_0137() {
	Character object(10);
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Character::internalID && "Test A0137: the returned internalID doesnt' match Character::internalID");
	assert(info.typeName == Character::typeName && "Test A0137: the returned typeName doesnt' match Character::typeName");
	assert(info.udtName == Character::udtName && "Test A0137: the returned udtName doesnt' match Character::udtName");
}


int main() {
	signal(SIGABRT, abortHandler);
	test_function function[7] = {
		test_A_0131, test_A_0132, test_A_0133, test_A_0134, test_A_0135, test_A_0136, test_A_0137
	};

	for (int i = 0; i < 7; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
