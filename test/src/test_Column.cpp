/**
 * @file test_Column.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
/**
 * @test Test B0000
 * Tests the Column class constructor. The test checks that an exception is thrown when no name is specified for a
 * new Column object. If the input is columnName=“” then the function must throw an AccessException exception
 */
void test_B_0000() {
	try {
		Column acolumn("", Varchar());
		assert(false && "the function must throw an AccessException exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0001
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Boolean then the function 
 * ust not throw any exception. The typeInfo function returns correct information for Boolean data type.
 */
void test_B_0001() {
	try {
		Column acolumn("acolumn", Boolean());
		assert(acolumn.getSqlType()->typeInfo().internalID == Boolean::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0002
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Bigint then the function
 * must not throw any exception. The typeInfo function returns correct information for Bigint data type.
 */
void test_B_0002() {
	try {
		Column acolumn("acolumn", Bigint());
		assert(acolumn.getSqlType()->typeInfo().internalID == Bigint::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0003
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Integer then the function
 * must not throw any exception. The typeInfo function returns correct information for Integer data type.
 */
void test_B_0003() {
	try {
		Column acolumn("acolumn", Integer());
		assert(acolumn.getSqlType()->typeInfo().internalID == Integer::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0004
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Smallint then the function
 * must not throw any exception. The typeInfo function returns correct information for Smallint data type.
 */
void test_B_0004() {
	try {
		Column acolumn("acolumn", Smallint());
		assert(acolumn.getSqlType()->typeInfo().internalID == Smallint::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0005
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Real then the function must 
 * not throw any exception. The typeInfo function returns correct information for Real data type.
 */
void test_B_0005() {
	try {
		Column acolumn("acolumn", Real());
		assert(acolumn.getSqlType()->typeInfo().internalID == Real::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0006
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=DoublePrecision then the
 * function must not throw any exception. The typeInfo function returns correct information for DoublePrecision data
 * type.
 */
void test_B_0006() {
	try {
		Column acolumn("acolumn", DoublePrecision());
		assert(acolumn.getSqlType()->typeInfo().internalID == DoublePrecision::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0007
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Numeric then the function
 * must not throw any exception. The typeInfo function returns correct information for Numeric data type.
 */
void test_B_0007() {
	try {
		Column acolumn("acolumn", Numeric());
		assert(acolumn.getSqlType()->typeInfo().internalID == Numeric::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0008
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Date then the function must
 * not throw any exception. The typeInfo function returns correct information for Date data type.
 *
 * 
 */
void test_B_0008() {
	try {
		Column acolumn("acolumn", Date());
		assert(acolumn.getSqlType()->typeInfo().internalID == Date::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0009
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Time then the function must
 * not throw any exception. The typeInfo function returns correct information for Time data type.
 */
void test_B_0009() {
	try {
		Column acolumn("acolumn", Time());
		assert(acolumn.getSqlType()->typeInfo().internalID == Time::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0010
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Timestamp then the function
 * must not throw any exception. The typeInfo function returns correct information for Timestamp data type.
 */
void test_B_0010() {
	try {
		Column acolumn("acolumn", Timestamp());
		assert(acolumn.getSqlType()->typeInfo().internalID == Timestamp::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0011
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Varchar then the function
 * must not throw any exception. The typeInfo function returns correct information for Varchar data type.
 */
void test_B_0011() {
	try {
		Column acolumn("acolumn", Varchar());
		assert(acolumn.getSqlType()->typeInfo().internalID == Varchar::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0012
 * Tests the Column class constructor. The test checks that the type of the new Column object is set respecting the
 * value of the columnType paramether. If the input is columnName=”column” and columnType=Character then the function
 * must not throw any exception. The typeInfo function returns correct information for Character data type.
 */
void test_B_0012() {
	try {
		Column acolumn("acolumn", Character());
		assert(acolumn.getSqlType()->typeInfo().internalID == Character::internalID && "type mismatch");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0013
 * Tests the Column class constructor. The test checks that the isKey property of the new Column object is set
 * respecting the value of the isKey parameter. If the input is isKey=true then the function must not throw any
 * exception. The getIsKey function will return true.
 */
void test_B_0013() {
	try {
		Column acolumn("acolumn", Varchar(), true);
		assert(acolumn.getIsKey() == true && "getKey() must return true");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0014
 * Tests the Column::validate() function. The test checks that an exception is thrown if the isKey property of the
 * Column object is set and you are trying to validate an empty or null value. If the input is value is a string
 * belonging to the SqlType::nullValues collection then The function must throw an EmptyKey exception
 */
void test_B_0014() {
	Column acolumn("acolumn", Varchar(), true);
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
		try {
			acolumn.validate(*it);
			assert(false && "the function must throw an EmptyKey exception");
		} catch (EmptyKey& e) {
			cout << e.what() << endl;
		} catch (exception& e) {
			cout << e.what() << endl;
			assert (false && "Uncaught exception");
		}
}

/**
 * @test Test B0015
 * Tests the Column::validate() function. The test checks that no exception is thrown if the isKey property of the
 * Column object is unset and you are trying to validate an empty or null value. If the input is value is a string
 * belonging to the SqlType::nullValues collection then the function must not throw any exception.
 */
void test_B_0015() {
	Column acolumn("acolumn", Varchar(), false);
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
		try {
			acolumn.validate(*it);
		} catch (BasicException& e) {
			cout << e.what() << endl;
			assert(false && "the function must not throw any exception");
		} catch (exception& e) {
			cout << e.what() << endl;
			assert (false && "Uncaught exception");
		}
}

/**
 * @test Test B0016
 * Tests the Column class constructor. The test checks that the name of the nee Column object contains only
 * alphanumerical characters. This means that you must use only {A..Z}, {a..z} and {0..9} chatacters. If the input is
 * columnName=”a column” then the function must throw an AccessException exception
 */
void test_B_0016() {
	try {
		Column acolumn("a column", Varchar());
		assert(false && "the function must throw an AccessException exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

/**
 * @test Test B0017
 * Tests the Column class constructor. The test checks that the name of the nee Column object contains only
 * alphanumerical characters. This means that you must use only {A..Z}, {a..z} and {0..9} chatacters. If the input is
 * columnName=“a,column” then the function must throw an AccessException exception
 */
void test_B_0017() {
	try {
		Column acolumn("a,column", Varchar());
		assert(false && "the function must throw an AccessException exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert (false && "Uncaught exception");
	}
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[18] = {
		test_B_0000, test_B_0001, test_B_0002, test_B_0003, test_B_0004, test_B_0005, test_B_0006, test_B_0007,
		test_B_0008, test_B_0009, test_B_0010, test_B_0011, test_B_0012, test_B_0013, test_B_0014, test_B_0015,
		test_B_0016, test_B_0017
	};

	for (int i = 0; i < 18; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
