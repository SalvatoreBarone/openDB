/**
 * @file test_Date.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0066
 * Tests the Date::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown.
 *
 */
void test_A_0066() {
	Date object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0066: the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0067
 * Tests the Date::validate() function.
 * The function is stimulated with a string with no istance of the separator characters (‘-’ or ‘/’)
 * (Ex. "06012017”). The function must throw an InvalidArgument exception
 *
 */
void test_A_0067() {
	Date object;
	try {
		object.validate("06012017");
		assert(false && "Test A0067: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0068
 * Tests the Date::validate() function.
 * The function is stimulated with a string with more than 2 istance of the separator characters (‘-’ or ‘/’)
 * (Ex. "06/01/20/17”). The function must throw an InvalidArgument exception
 *
 */
void test_A_0068() {
	Date object;
	try {
		object.validate("06/01/20/17");
		assert(false && "Test A0068: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0069
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first field isn’t numeric; (Ex. "aa/01/2017”). The function must throw an InvalidArgument exception
 *
 */
void test_A_0069() {
	Date object;
	try {
		object.validate("aa/01/2017");
		assert(false && "Test A0069: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0070
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first field is a negative number (Ex. "-5/01/2017”). The function must throw an InvalidArgument
 * exception
 *
 */
void test_A_0070() {
	Date object;
	try {
		object.validate("-5/01/2017");
		assert(false && "Test A0070: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0071
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first field is positive number, but the second isn’ a numeric string; (Ex. "06/aa/2017”). The function
 * must throw an InvalidArgument exception
 *
 */
void test_A_0071() {
	Date object;
	try {
		object.validate("06/aa/2017");
		assert(false && "Test A0071: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0072
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first field is positive number, but the second is a negative number; (Ex. "06/-1/2017”). The function
 * must throw an InvalidArgument exception
 *
 */
void test_A_0072() {
	Date object;
	try {
		object.validate("06/-1/2017");
		assert(false && "Test A0072: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0073
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first and the second fields is positive number, but the third isn’t a  (Ex. "06/01/abcd”). The function
 * must throw an InvalidArgument exception
 *
 */
void test_A_0073() {
	Date object;
	try {
		object.validate("06/01/abcd");
		assert(false && "Test A0073: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0074
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * The first and the second fields is positive number, but the third is a negative number; (Ex. "06/01/-2017”).
 * The function must throw an InvalidArgument exception
 *
 */
void test_A_0074() {
	Date object;
	try {
		object.validate("06/01/-2017");
		assert(false && "Test A0074: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0075
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator character (‘-’ or ‘/’).
 * Each fiels is is a positive number. The length of the first field is 4, so the format is yyyy/MM/dd;
 * (Ex. "2017/01/06”). The function must not throw exceptions
 *
 */
void test_A_0075() {
	Date object;
	try {
		object.validate("2017/01/06");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0075: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0076
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each fiels is is a positive number. The length of the first field is 2, the length of the third field is 4,
 * so the format is dd/MM/yyyy; (Ex. "06/01/2017”). "The function must not throw exceptions
 *
 */
void test_A_0076() {
	Date object;
	try {
		object.validate("06/01/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0076: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0077
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each fiels is is a positive number. The length of the first field is 2, the length of the third field is 2,
 * so the format isn’t dd/MM/yyyy; (Ex. "06/01/17”). The function must throw an AmbiguousDate exception
 *
 */
void test_A_0077() {
	Date object;
	try {
		object.validate("06/01/17");
		assert(false && "Test A0077: The function must throw an AmbiguousDate exception");
	} catch (AmbiguousDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0078
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (1,3,5,7,8,10,12), the day is 0; (Ex. "0/01/2017”). The function must
 * throw an InvalidDate exception
 *
 */
void test_A_0078() {
	Date object;
	try {
		object.validate("0/01/2017");
		assert(false && "Test A0078: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0079
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (1,3,5,7,8,10,12), the day is greater than 31; (Ex. "32/01/2017”). The
 * function must throw an InvalidDate exception
 *
 */
void test_A_0079() {
	Date object;
	try {
		object.validate("32/01/2017");
		assert(false && "Test A0079: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0080
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (1,3,5,7,8,10,12), the day is less or equal to 31; (Ex. "31/01/2017”).
 * The function must not throw any exception
 *
 */
void test_A_0080() {
	Date object;
	try {
		object.validate("31/01/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0080: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0081
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (4,6,9,11), the day is 0; (Ex. "0/04/2017”). The function must throw
 * an InvalidDate exception
 *
 */
void test_A_0081() {
	Date object;
	try {
		object.validate("0/04/2017");
		assert(false && "Test A0081: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0082
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (4,6,9,11), the day is greater than 30; (Ex. "31/04/2017”). The
 * function must throw an InvalidDate exception
 *
 */
void test_A_0082() {
	Date object;
	try {
		object.validate("31/04/2017");
		assert(false && "Test A0082: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0083
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is (4,6,9,11), the day is less or equal to 30; (Ex. "30/04/2017”). The
 * function must not throw any exception
 *
 */
void test_A_0083() {
	Date object;
	try {
		object.validate("30/04/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0083: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0084
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is 2, the day is 0; (Ex. "0/2/2017”). The function must throw an
 * InvalidDate exception
 *
 */
void test_A_0084() {
	Date object;
	try {
		object.validate("0/2/2017");
		assert(false && "Test A0084: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0085
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is 2, the day is between 1 and 28; (Ex. "12/02/2017”). The function must
 * not throw any exception
 *
 */
void test_A_0085() {
	Date object;
	try {
		object.validate("12/02/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0085: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0086
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is 2, the day is between 29, the year is a leap year; (Ex. "29/2/2016”).
 * The function must not throw any exception
 *
 */
void test_A_0086() {
	Date object;
	try {
		object.validate("29/2/2016");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0086: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0087
 * Tests the Date::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator characters (‘-’ or ‘/’).
 * Each field is positive. The month is 2, the day is between 29, the year is not a leap year;
 * (Ex. "29/2/2017”). The function must throw an InvalidDate exception
 *
 */
void test_A_0087() {
	Date object;
	try {
		object.validate("29/2/2017");
		assert(false && "Test A0087: The function must throw an InvalidArgument exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0088
 * Tests the Date::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned.
 *
 */
void test_A_0088() {
	Date object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert(object.prepare(*it) == SqlType::null && "Test A0088: the function doesn't return SqlType::null");
}
/**
 * @test Test A0089
 * Tests the Date::prepare() function.
 * The function is stimulated with a valid date, checking that it is prepared correctly.
 *
 */
void test_A_0089() {
	Date object;
	assert(object.prepare("07/01/2018") == "'2018-01-07'");
}
/**
 * @test Test A0090
 * Tests the Date::typeInfo() function.
 * Verifies that the function returns information about the Date type correctly.
 *
 */
void test_A_0090() {
	Date object;
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Date::internalID && "Test A0090: the returned internalID doesnt' match Date::internalID");
	assert(info.typeName == Date::typeName && "Test A0090: the returned typeName doesnt' match Date::typeName");
	assert(info.udtName == Date::udtName && "Test A0090: the returned udtName doesnt' match Date::udtName");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[25] = {
		test_A_0066, test_A_0067, test_A_0068, test_A_0069, test_A_0070, test_A_0071, test_A_0072, test_A_0073,
		test_A_0074, test_A_0075, test_A_0076, test_A_0077, test_A_0078, test_A_0079, test_A_0080, test_A_0081,
		test_A_0082, test_A_0083, test_A_0084, test_A_0085, test_A_0086, test_A_0087, test_A_0088, test_A_0089,
		test_A_0090
	};

	for (int i = 0; i < 25; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
