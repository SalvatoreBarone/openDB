/**
 * @file test_DoublePrecision.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
/**
 * @test Test A0173
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown. If the input is SqlType::nullValues then The function must not throw exceptions
 */
void test_A_0173() {
	DoublePrecision object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0174
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “INF” or "INFINITY" then The function must not throw exceptions
 *
 */
void test_A_0174() {
	DoublePrecision object;
	try {
		object.validate("INF");
		object.validate("INFINITY");
		object.validate("inf");
		object.validate("infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0175
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “+INF” then The function must not throw exceptions
 *
 */
void test_A_0175() {
	DoublePrecision object;
	try {
		object.validate("+INF");
		object.validate("+inf");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0176
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “-INF” then The function must not throw exceptions
 *
 */
void test_A_0176() {
	DoublePrecision object;
	try {
		object.validate("-INF");
		object.validate("-inf");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0177
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “+INFINITY” then The function must not throw exceptions
 *
 */
void test_A_0177() {
	DoublePrecision object;
	try {
		object.validate("+INFINITY");
		object.validate("+infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0178
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “-INFINITY” then The function must not throw exceptions
 *
 */
void test_A_0178() {
	DoublePrecision object;
	try {
		object.validate("-INFINITY");
		object.validate("-infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0179
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation If
 * the input is “NAN” then The function must not throw exceptions
 *
 */
void test_A_0179() {
	Real object;
	try {
		object.validate("NAN");
		object.validate("nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0180
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation If
 * the input is "+NAN" then The function must not throw exceptions
 *
 */
void test_A_0180() {
	Real object;
	try {
		object.validate("+NAN");
		object.validate("+nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0181
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation If
 * the input is "-NAN" then The function must not throw exceptions
 *
 */
void test_A_0181() {
	Real object;
	try {
		object.validate("-NAN");
		object.validate("-nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0182
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string containing an illegal character If the input is “12,34f” then The
 * function must throw an InvalidArgoument exception
 *
 */
void test_A_0182() {
	DoublePrecision object;
	try {
		object.validate("12,34f");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0183
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string containing an illegal character If the input is “12.34f” then The
 * function must throw an InvalidArgoument exception
 *
 */
void test_A_0183() {
	DoublePrecision object;
	try {
		object.validate("12.34F");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0184
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string containing multiple characters that are not allowed If the input
 * is “acbd” then The function must throw an InvalidArgoument exception
 *
 */
void test_A_0184() {
	DoublePrecision object;
	try {
		object.validate("abcd");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0185
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Comma is used as a separator for decimals. If the input is “1234,56” then The
 * function must not throw exceptions
 *
 */
void test_A_0185() {
	DoublePrecision object;
	try {
		object.validate("1234,56");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0186
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Point is used as a separator for decimals. If the input is "-123,456" then The
 * function must not throw exceptions
 *
 */
void test_A_0186() {
	DoublePrecision object;
	try {
		object.validate("-1234.56");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0187
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values expressed in exponential notation. Comma is used as a separator for decimals. If the
 * input is “1234,56E+2” then The function must not throw exceptions
 *
 */
void test_A_0187() {
	DoublePrecision object;
	try {
		object.validate("1234.56E+2");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0188
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that more than one “e” or “E” character. If the input is
 * "-123,4e56e-3" then The function must throw an InvalidArgument exception
 *
 */
void test_A_0188() {
	DoublePrecision object;
	try {
		object.validate("-123.e456e-3");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0189
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the upper end of the set of representable values.
 * If the input is std::numeric_limits<double>::max() then The function must not throw exceptions
 *
 */
void test_A_0189() {
	DoublePrecision object;
	try {
		object.validate(to_string(std::numeric_limits<double>::max()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0190
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the smallest element of the set of representable
 * values. If the input is std::numeric_limits<double>::lowest() then The function must not throw exceptions
 *
 */
void test_A_0190() {
	DoublePrecision object;
	try {
		object.validate(to_string(std::numeric_limits<double>::lowest()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0191
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the largest negative number of the set of
 * representable values. If the input is -std::numeric_limits<float>::lowest() then The function must not
 * throw exceptions
 *
 */
void test_A_0191() {
	DoublePrecision object;
	try {
		object.validate(to_string(-std::numeric_limits<double>::lowest()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0192
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents the largest small negative of the set of
 * representable values. If the input is -std::numeric_limits<float>::max() then The function must not
 * throw exceptions
 *
 */
void test_A_0192() {
	DoublePrecision object;
	try {
		object.validate(to_string(-std::numeric_limits<double>::max()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0193
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a positive number that falls outside the set of
 * representable values, beyond the upper limit. If the input is "1.9e+308" then The function must throw an
 * OutOfBound exception
 *
 */
void test_A_0193() {
	DoublePrecision object;
	try {
		object.validate("1.9e+308");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0194
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a positive number that falls outside the set of
 * representable values, beyond the lower limit. If the input is “2e-308” then The function must throw an
 * OutOfBound exception
 *
 */
void test_A_0194() {
	DoublePrecision object;
	try {
		object.validate("2e-308");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0195
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a negative number that falls outside the set of
 * representable values, beyond the upper limit. If the input is "-2.e-308" then The function must throw an
 * OutOfBound exception
 *
 */
void test_A_0195() {
	DoublePrecision object;
	try {
		object.validate("-2.e-308");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0196
 * Tests the DoublePrecision::validate() function.
 * The function is stimulated with a string that represents a negative number that falls outside the set of
 * representable values, beyond the lower limit. If the input is "-1.9e+308" then The function must throw an
 * OutOfBound exception
 *
 */
void test_A_0196() {
	DoublePrecision object;
	try {
		object.validate("-1.9e+308");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0197
 * Tests the DoublePrecision::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned. If the input is SqlType::nullValues  then  the output must be SqlType::null
 *
 */
void test_A_0197() {
	DoublePrecision object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert (object.prepare(*it) == SqlType::null);
}

/**
 * @test Test A0198
 * Tests the DoublePrecision::prepare() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Comma is used as a separator for decimals. If the input is “123,456” then
 *
 */
void test_A_0198() {
	DoublePrecision object;
	int precision = 16;
	string value = "123,456"; // this value will be aproximated to the nearest fp-number...
	std::stringstream ss;
    ss << fixed << std::setprecision(precision) << 123.456;
	assert (object.prepare(value) == ss.str());
}

/**
 * @test Test A0199
 * Tests the DoublePrecision::prepare() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Point is used as a separator for decimals. If the input is “123.456” then
 *
 */
void test_A_0199() {
	DoublePrecision object;
	int precision = 16;
	string value = "123.456"; // this value will be aproximated to the nearest fp-number...
	std::stringstream ss;
    ss << fixed << std::setprecision(precision) << 123.456;
	assert (object.prepare(value) == ss.str());
}

/**
 * @test Test A0200
 * Tests the DoublePrecision::typeInfo() function.
 * Verifies that the function returns information about the Real type correctly. the output must be
 * (DoublePrecision::internalID, DoublePrecision::typeName,DoublePrecision::udtName)
 */
void test_A_0200() {
		DoublePrecision object;
		SqlType::TypeInfo info = object.typeInfo();
		assert(info.internalID == DoublePrecision::internalID && "the returned internalID doesnt' match Integer::internalID");
		assert(info.typeName == DoublePrecision::typeName && "the returned typeName doesnt' match DoublePrecision::typeName");
		assert(info.udtName == DoublePrecision::udtName && "the returned udtName doesnt' match DoublePrecision::udtName");
}


int main() {
	signal(SIGABRT, abortHandler);
	test_function function[28] = {
		test_A_0173, test_A_0174, test_A_0175, test_A_0176, test_A_0177, test_A_0178, test_A_0179, test_A_0180,
		test_A_0181, test_A_0182, test_A_0183, test_A_0184, test_A_0185, test_A_0186, test_A_0187, test_A_0188,
		test_A_0189, test_A_0190, test_A_0191, test_A_0192, test_A_0193, test_A_0194, test_A_0195, test_A_0196,
		test_A_0197, test_A_0198, test_A_0199, test_A_0200
	};

	for (int i = 0; i < 28; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
