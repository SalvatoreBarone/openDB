/**
 * @file test_Integer.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}


/**
 * @test Test A0026
 * Tests the Integer::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no exception is thrown
 *
 */
void test_A_0026() {
	Integer object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0026: the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0027
 * Tests the Integer::validate() function.
 * The function is stimulated using a string that represents a numerical value that can be correctly
 * translated as a positive number and falls within the set of allowed values. It is verified that the
 * function does not throw exceptions.
 *
 */
void test_A_0027() {
	Integer object;
	try {
		object.validate("1234567890");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
		assert(false && "Test A0027: the function throws an exception with a positive number and falls within the set of allowed values");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0028
 * Tests the Integer::validate() function.
 * The function is stimulated using a string that represents a numerical value that can be correctly
 * translated as a negative number and falls within the set of allowed values. It is verified that the
 * function does not throw exceptions.
 *
 */
void test_A_0028() {
	Integer object;
	try {
		object.validate("-1234567890");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
		assert(false && "Test A0028: the function throws an exception with a negative number and falls within the set of allowed values");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0029
 * Tests the Integer::validate() function.
 * The function is stimulated using a string that represents a numerical value that can be correctly
 * translated as a positive number and that corresponds to the upper limit of the set of allowed values.
 * It is verified that the function does not throw exceptions.
 *
 */
void test_A_0029() {
	Integer object;
	try {
		object.validate(to_string(std::numeric_limits<int>::max()));
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
		assert(false && "Test A0029: the function throws an exception with the upper limit of the set of allowed values.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0030
 * Tests the Integer::validate() function.
 * The function is stimulated using a string that represents a numerical value that can be correctly
 * translated as a positive number and that corresponds to the lower limit of the set of allowed values. It
 * is verified that the function does not throw exceptions.
 *
 */
void test_A_0030() {
	Integer object;
 	try {
		object.validate(to_string(std::numeric_limits<int>::min()));
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
		assert(false && "Test A0030: the function throws an exception with the lower limit of the set of allowed values.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0031
 * Tests the Integer::validate() function.
 * The function is stimulated using a string, which represents a numerical value that can be correctly
 * translated as a positive number, but which is greater than the upper limit of the set of allowed values,
 * and then falls outside of the latter. The function is checked to throw an OutOfBound exception.
 *
 */
void test_A_0031() {
	Integer object;
	try {
		object.validate("12345678901234567890");
		assert(false && "Test A0031: the function doesn't throw an exception with a value greater than the upper limit.");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0032
 * Tests the Integer::validate() function.
 * The function is stimulated using a string, which represents a numerical value that can be correctly
 * translated as a negative number, but which is lower than the lower limit   of the set of allowed values,
 * then fall outside the latter. The function is checked to throw an OutOfBound exception.
 *
 */
void test_A_0032() {
	Integer object;
	try {
		object.validate("-12345678901234567890");
		assert(false && "Test A0032: the function doesn't throw an exception with a value lower than the lower limit.");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0033
 * Tests the Integer::validate() function.
 * The function is stimulated using a string that can not be translated as a number. The function is checked
 * to throw an InvalidArgoument exception.
 *
 */
void test_A_0033() {
	Integer object;
	try {
		object.validate("abc");
		assert(false && "Test A0033: the function doesn't throw an InvalidArgoument exception if the input is a string that can not be translated as a number");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}

}

/**
 * @test Test A0034
 * Tests the Integer::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned.
 *
 */
void test_A_0034() {
	Integer object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			assert (object.prepare(*it) == SqlType::null);
}

/**
 * @test Test A0035
 * Tests the Integer::prepare() function.
 * The function is stimulated using valid strings (translatable as a number). It is verified that the function
 * returns the input string as it is.
 *
 */
void test_A_0035() {
	Integer object;
	assert(object.prepare("12345") == "12345" && "the function doesn't returns the input string as it is");
	assert(object.prepare("23") == "23" && "the function doesn't returns the input string as it is");
	assert(object.prepare("25") == "25" && "the function doesn't returns the input string as it is");
	assert(object.prepare("79") == "79" && "the function doesn't returns the input string as it is");
	assert(object.prepare("51") == "51" && "the function doesn't returns the input string as it is");
	assert(object.prepare("64") == "64" && "the function doesn't returns the input string as it is");
}

/**
 * @test Test A0036
 * Tests the Integer::typeInfo() function.
 * Verifies that the function returns information about the Integer type correctly.
 *
  */
void test_A_0036() {
	Integer object;
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Integer::internalID && "Test A0036: the returned internalID doesnt' match Integer::internalID");
	assert(info.typeName == Integer::typeName && "Test A0036: the returned typeName doesnt' match Integer::typeName");
	assert(info.udtName == Integer::udtName && "Test A0036: the returned udtName doesnt' match Integer::udtName");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[11] = {
		test_A_0026, test_A_0027, test_A_0028, test_A_0029, test_A_0030, test_A_0031, test_A_0032, test_A_0033,
		test_A_0034, test_A_0035, test_A_0036
	};

	for (int i = 0; i < 11; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
