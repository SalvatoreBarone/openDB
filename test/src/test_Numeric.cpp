/**
 * @file test_Numeric.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
/**
 * @test Test A0201
 * Tests the Numeric::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown. If the input is SqlType::nullValues then The function must not throw exceptions
 */
void test_A_0201() {
	Numeric object(10, 5);
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0202
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation.
 * If the input is “NAN” then The function must not throw exceptions
 */
void test_A_0202() {
	Numeric object(10, 5);
	try {
		object.validate("NAN");
		object.validate("nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0203
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing multiple characters that are not allowed If the input
 * is “abc” then The function must throw an InvalidArgument exception
 *
 *
 */
void test_A_0203() {
	Numeric object(10, 5);
	try {
		object.validate("abc");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0204
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one sign If the input is "+3+" then The
 * function must throw an InvalidArgument exception
 *
 *
 */
void test_A_0204() {
	Numeric object(10, 5);
	try {
		object.validate("+3+");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0205
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one sign If the input is “+3-” then The
 * function must throw an InvalidArgument exception
 *
 *
 */
void test_A_0205() {
	Numeric object(10, 5);
	try {
		object.validate("+3-");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0206
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one sign If the input is “-3-” then The
 * function must throw an InvalidArgument exception
 *
 *
 */
void test_A_0206() {
	Numeric object(10, 5);
	try {
		object.validate("-3-");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0207
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one decimal separator If the input is “3,,5”
 * then The function must throw an InvalidArgument exception
 *
 *
 */
void test_A_0207() {
	Numeric object(10, 5);
	try {
		object.validate("3,,5");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0208
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one decimal separator If the input is “3.,5”
 * then The function must throw an InvalidArgument exception
 */
void test_A_0208() {
	Numeric object(10, 5);
	try {
		object.validate("3.,5");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0209
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing more than one decimal separator If the input is “3..5”
 * then The function must throw an InvalidArgument exception
 */
void test_A_0209() {
	Numeric object(10, 5);
	try {
		object.validate("3..5");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (BasicException& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0210
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string representing an integer number If the input is “123456” then The
 * function must not throw exceptions
 */
void test_A_0210() {
	Numeric object(10, 5);
	try {
		object.validate("123456");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0211
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string representing a fixed-point real number If the input is “12345,6”
 * then The function must not throw exceptions
 */
void test_A_0211() {
	Numeric object(10, 5);
	try {
		object.validate("12345,6");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0212
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string representing a fixed-point real number If the input is “0,3” then
 * The function must not throw exceptions
 */
void test_A_0212() {
	Numeric object(10, 5);
	try {
		object.validate("0.3");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0213
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string representing a fixed-point real number, starting with the decimal
 * separator character If the input is “.3” then The function must not throw exceptions
 */
void test_A_0213() {
	Numeric object(10, 5);
	try {
		object.validate(".3");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0214
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing a number whose precision is exactly the maximum allowed
 * If the input is 1234567890 then The function must not throw exceptions
 */
void test_A_0214() {
	Numeric object(10, 5);
	try {
		object.validate("1234567890");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0215
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing a number whose precision excedes the maximum allowed
 * If the input is “12345678901” then The function must throw an OutOfBound exception
 */
void test_A_0215() {
	Numeric object(10, 5);
	try {
		object.validate("12345678901");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0216
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing a number whose scale is exactly the maximum allowed
 * If the input is “123,45678” then The function must not throw exceptions
 */
void test_A_0216() {
	Numeric object(10, 5);
	try {
		object.validate("123,45678");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "The function must not throw exceptions");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0217
 * Tests the Numeric::validate() function.
 * The function is stimulated with a string containing a number whose scale excedes the maximum allowed If
 * the input is “123,456789” then The function must throw an OutOfBound exception
 */
void test_A_0217() {
	Numeric object(10, 5);
	try {
		object.validate("123,456789");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0218
 * Tests the Numeric::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned. If the input is SqlType::nullValues  then  the output must be SqlType::null
 */
void test_A_0218() {
	Numeric object(10, 5);
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert (object.prepare(*it) == SqlType::null && "the function doen't return SqlType::null");
}

/**
 * @test Test A0219
 * Tests the Numeric::prepare() function.
 * The function is stimulated with a number with a comma as decimal separator If the input is “123,456” then
 * the output must be “123.456”
 */
void test_A_0219() {
	Numeric object(10, 5);
	assert(object.prepare("123,456") == "123.456" && "the value has not been prepared corectly");
}

/**
 * @test Test A0220
 * Tests the Numeric::prepare() function.
 * The function is stimulated with a number with a point ad decimal separator If the input is “123.456” then
 * the output must be “123.456”
 */
void test_A_0220() {
	Numeric object(10, 5);
	assert(object.prepare("123.456") == "123.456" && "the value has not been prepared corectly");
}

/**
 * @test Test A0221
 * Tests the Numeric::typeInfo() function.
 * Verifies that the function returns information about the Numeric type correctly. the output must be
 * (Numeric::internalID, Numeric::typeName,Numeric::udtName)
 */
void test_A_0221() {
	Numeric object(10,5);
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Numeric::internalID && "the returned internalID doesnt' match Numeric::internalID");
	assert(info.typeName == Numeric::typeName && "the returned typeName doesnt' match Numeric::typeName");
	assert(info.udtName == Numeric::udtName && "the returned udtName doesnt' match Numeric::udtName");
}


int main() {
	signal(SIGABRT, abortHandler);
	test_function function[21] = {
		test_A_0201, test_A_0202, test_A_0203, test_A_0204, test_A_0205, test_A_0206, test_A_0207, test_A_0208,
		test_A_0209, test_A_0210, test_A_0211, test_A_0212, test_A_0213, test_A_0214, test_A_0215, test_A_0216,
		test_A_0217, test_A_0218, test_A_0219, test_A_0220, test_A_0221
	};

	for (int i = 0; i < 21; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
