/**
 * @file test_Real.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
/**
 * @test Test A0145
 * Tests the Real::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown. If the input is SqlType::nullValues then The function must not throw exceptions
 */
void test_A_0145() {
	Real object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0146
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “INF” or "INFINITY" then The function must not throw exceptions
 */
void test_A_0146() {
	Real object;
	try {
		object.validate("INF");
		object.validate("INFINITY");
		object.validate("inf");
		object.validate("infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0147
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “+INF” then The function must not throw exceptions
 */
void test_A_0147() {
	Real object;
	try {
		object.validate("+INF");
		object.validate("+inf");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0148
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “-INF” then The function must not throw exceptions
 */
void test_A_0148() {
	Real object;
	try {
		object.validate("-INF");
		object.validate("-inf");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0149
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “+INFINITY” then The function must not throw exceptions
 */
void test_A_0149() {
	Real object;
	try {
		object.validate("+INFINITY");
		object.validate("+infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0150
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the infinite number expressed in floating-point
 * notation If the input is “-INFINITY” then The function must not throw exceptions
 */
void test_A_0150() {
	Real object;
	try {
		object.validate("-INFINITY");
		object.validate("-infinity");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0151
 * Tests the Real::validate() function.
 * the The function is stimulated with a string representing non-a-number expressed in floating-point notation
 * If the input is “NAN” then The function must not throw exceptions
 */
void test_A_0151() {
	Real object;
	try {
		object.validate("NAN");
		object.validate("nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0152
 * Tests the Real::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation
 * If the input is "+NAN" then The function must not throw exceptions
 */
void test_A_0152() {
	Real object;
	try {
		object.validate("+NAN");
		object.validate("+nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0153
 * Tests the Real::validate() function.
 * The function is stimulated with a string representing non-a-number expressed in floating-point notation
 * If the input is "-NAN" then The function must not throw exceptions
 */
void test_A_0153() {
	Real object;
	try {
		object.validate("-NAN");
		object.validate("-nan");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0154
 * Tests the Real::validate() function.
 * The function is stimulated with a string containing an illegal character If the input is “12,34f” then
 * The function must throw an InvalidArgoument exception
 */
void test_A_0154() {
	Real object;
	try {
		object.validate("12,34f");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0155
 * Tests the Real::validate() function.
 * The function is stimulated with a string containing an illegal character If the input is “12.34f” then
 * The function must throw an InvalidArgoument exception
 */
void test_A_0155() {
	Real object;
	try {
		object.validate("12.34F");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0156
 * Tests the Real::validate() function.
 * The function is stimulated with a string containing multiple characters that are not allowed If the input
 * is “acbd” then The function must throw an InvalidArgoument exception
 */
void test_A_0156() {
	Real object;
	try {
		object.validate("abcd");
		assert(false && "the function must throw an InvalidArgoument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0157
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Comma is used as a separator for decimals. If the input is “1234,56” then
 * The function must not throw exceptions
 */
void test_A_0157() {
	Real object;
	try {
		object.validate("1234,56");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0158
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Point is used as a separator for decimals. If the input is "-123.456" then The
 * function must not throw exceptions
 */
void test_A_0158() {
	Real object;
	try {
		object.validate("-1234.56");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0159
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values expressed in exponential notation. Comma is used as a separator for decimals. If the
 * input is “1234,56E+2” then The function must not throw exceptions
 */
void test_A_0159() {
	Real object;
	try {
		object.validate("1234.56E+2");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0160
 * Tests the Real::validate() function.
 * The function is stimulated with a string that more than one “e” or “E” character. If the input is
 * "-123,4e56e-3" then The function must throw an InvalidArgument exception
 */
void test_A_0160() {
	Real object;
	try {
		object.validate("-123.e456e-3");
		assert(false && "the function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0161
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the upper end of the set of representable values.
 * If the input is std::numeric_limits<float>::max() then The function must not throw exceptions
 */
void test_A_0161() {
	Real object;
	try {
		object.validate(to_string(std::numeric_limits<float>::max()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0162
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the smallest element of the set of representable
 * values. If the input is std::numeric_limits<float>::lowest() then The function must not throw exceptions
 */
void test_A_0162() {
	Real object;
	try {
		object.validate(to_string(std::numeric_limits<float>::lowest()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0163
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the largest negative number of the set of
 * representable values. If the input is -std::numeric_limits<float>::lowest() then The function must not
 * throw exceptions
 */
void test_A_0163() {
	Real object;
	try {
		object.validate(to_string(-std::numeric_limits<float>::lowest()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0164
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents the largest small negative of the set of
 * representable values. If the input is -std::numeric_limits<float>::max() then The function must not
 * throw exceptions
 */
void test_A_0164() {
	Real object;
	try {
		object.validate(to_string(-std::numeric_limits<float>::max()));
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "the function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0165
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a positive number that falls outside the set of
 * representable values, beyond the upper limit. If the input is "3.402823e+39" then The function must throw
 * an OutOfBound exception
 */
void test_A_0165() {
	Real object;
	try {
		object.validate("3.402823e+39");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0166
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a positive number that falls outside the set of
 * representable values, beyond the lower limit. If the input is "1.175494e-39" then The function must throw
 * an OutOfBound exception
 */
void test_A_0166() {
	Real object;
	try {
		object.validate("1.175494e-39");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0167
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a negative number that falls outside the set of
 * representable values, beyond the upper limit. If the input is "-1.175494e-39" then The function must throw
 * an OutOfBound exception
 */
void test_A_0167() {
	Real object;
	try {
		object.validate("-1.175494e-39");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0168
 * Tests the Real::validate() function.
 * The function is stimulated with a string that represents a negative number that falls outside the set of
 * representable values, beyond the lower limit. If the input is "-3.402823e+39" then The function must throw
 * an OutOfBound exception
 */
void test_A_0168() {
	Real object;
	try {
		object.validate("-3.402823e+39");
		assert(false && "the function must throw an OutOfBound exception");
	} catch (OutOfBound& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}

/**
 * @test Test A0169
 * Tests the Real::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying
 * that SqlType::null is returned. If the input is SqlType::nullValues  then  the output must be SqlType::null
 */
void test_A_0169() {
	Real object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert (object.prepare(*it) == SqlType::null);
}

/**
 * @test Test A0170
 * Tests the Real::prepare() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Comma is used as a separator for decimals. If the input is “123,456” then the output
 * must be the same returned by the to_string(stof(“123.456”).
 */
void test_A_0170() {
	Real object;
	assert(object.prepare("123,456") == to_string((float)123.456));
}

/**
 * @test Test A0171
 * Tests the Real::prepare() function.
 * The function is stimulated with a string that represents a floating point number within the set of
 * representable values. Point is used as a separator for decimals. If the input is “123.456” then the output
 * must be the same returned by the to_string(stof(“123.456”)).
 */
void test_A_0171() {
	Real object;
	assert(object.prepare("123.456") == to_string((float)123.456));
}

/**
 * @test Test A0172
 * Tests the Real::typeInfo() function.
 * Verifies that the function returns information about the Real type correctly. the output must be
 * (Real::internalID, Real::typeName,Real::udtName)
 */
void test_A_0172() {
		Real object;
		SqlType::TypeInfo info = object.typeInfo();
		assert(info.internalID == Real::internalID && "the returned internalID doesnt' match Real::internalID");
		assert(info.typeName == Real::typeName && "the returned typeName doesnt' match Real::typeName");
		assert(info.udtName == Real::udtName && "the returned udtName doesnt' match Real::udtName");
}


int main() {
	signal(SIGABRT, abortHandler);
	test_function function[28] = {
		test_A_0145, test_A_0146, test_A_0147, test_A_0148, test_A_0149, test_A_0150, test_A_0151, test_A_0152,
		test_A_0153, test_A_0154, test_A_0155, test_A_0156, test_A_0157, test_A_0158, test_A_0159, test_A_0160,
		test_A_0161, test_A_0162, test_A_0163, test_A_0164, test_A_0165, test_A_0166, test_A_0167, test_A_0168,
		test_A_0169, test_A_0170, test_A_0171, test_A_0172
	};

	for (int i = 0; i < 28; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
