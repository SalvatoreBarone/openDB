/**
 * @file test_SqlType.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0000
 * Tests the SqlType::isNull() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that 'true' is
 * returned.
 *
 */
void test_A_0000() {
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues.end(); it++)
		assert(SqlType::isNull(*it) == true && "Test A0000: A value in the SqlType::nullValues collection isn't recognized as null");
}
/**
 * @test Test A0001
 * Tests the SqlType::isNull() function.
 * The function is stimulated with the strings not belonging to the SqlType::nullValues collection, verifying
 * that 'false' is returned.
 *
 */
void test_A_0001() {
	assert(SqlType::isNull("a string") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
	assert(SqlType::isNull("a different string") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
	assert(SqlType::isNull("1234") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
	assert(SqlType::isNull("1234,5678") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
	assert(SqlType::isNull("04/01/2017") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
	assert(SqlType::isNull("23:14") == false && "Test A0001: A value not belonging to the SqlType::nullValues collection is recognized as null");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[2] = {
		test_A_0000, test_A_0001
	};

	for (int i = 0; i < 2; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
