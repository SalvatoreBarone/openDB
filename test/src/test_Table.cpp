/**
 * @file test_Table.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}
/**
 * @test Test C0000
 * Tests the constructor of the Table class. If the name for the new Table object is an empty string then an
 * AccessException exception must be thrown. If the input is _name= then The function must throw an
 * AccessException exception
 */
void test_C_0000() {
	try {
		Table table("");
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0001
 * Tests the constructor of the Table class. If the name for the new Table object contains non-alphanumerical
 * character then an AccessException exception must be thrown. If the input is _name=a table then The function
 * must throw an AccessException exception
 */
void test_C_0001() {
	try {
		Table table("a table");
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0002
 * Tests the constructor of the Table class. If the name for the new Table object contains non-alphanumerical
 * character then an AccessException exception must be thrown. If the input is _name=a-table then The function
 * must throw an AccessException exception
 */
void test_C_0002() {
	try {
		Table table("a-table");
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0003
 * Tests the Table::addColumn() function. The test checks that an exception is thrown when no name is specified
 * for a new Column object. If the input is _name= then The function must throw an AccessException exception
 */
void test_C_0003() {
	try {
		Table table("table");
		table.addColumn("", Boolean());
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0004
 * Tests the Table::addColumn() function. The test checks that the name of the new Column object contains only
 * alphanumerical characters. This means that you must use only {A..Z}, {a..z} and {0..9} chatacters. If the input
 * is _name=a column then The function must throw an AccessException exception
 */
void test_C_0004() {
	try {
		Table table("table");
		table.addColumn("a column", Boolean());
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0005
 * Tests the Table::addColumn() function. The test checks that the name of the new Column object contains only
 * alphanumerical characters. This means that you must use only {A..Z}, {a..z} and {0..9} chatacters. If the input
 * is name=a-column then The function must throw an AccessException exception
 */
void test_C_0005() {
	try {
		Table table("table");
		table.addColumn("a-column", Boolean());
		assert(false && "The function must throw an AccessException exception");
	} catch (AccessException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0006
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=boolean and columnType=Boolean then  the
 * output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0006() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("boolean", Boolean());
		assert(p.first == table.at("boolean") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Boolean::internalID && "type mismatch");
		assert(p.first->getName() == "boolean" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0007
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=bigint and columnType=Bigint then  the
 * output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0007() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("bigint", Bigint());
		assert(p.first == table.at("bigint") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Bigint::internalID && "type mismatch");
		assert(p.first->getName() == "bigint" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0008
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=integer and columnType=Integer then  the
 * output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column,
 * the second field is true.
 */
void test_C_0008() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("integer", Integer());
		assert(p.first == table.at("integer") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Integer::internalID && "type mismatch");
		assert(p.first->getName() == "integer" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0009
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=smallint and columnType=Smallint then  the
 * output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0009() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("smallint", Smallint());
		assert(p.first == table.at("smallint") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Smallint::internalID && "type mismatch");
		assert(p.first->getName() == "smallint" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0010
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=real and columnType=Real then  the output
 * must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the second
 * field is true.
 */
void test_C_0010() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("real", Real());
		assert(p.first == table.at("real") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Real::internalID && "type mismatch");
		assert(p.first->getName() == "real" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0011
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=doubleprecision and columnType=DoublePrecision
 * then  the output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the
 * column, the second field is true.
 */
void test_C_0011() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("doubleprecision", DoublePrecision());
		assert(p.first == table.at("doubleprecision") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == DoublePrecision::internalID && "type mismatch");
		assert(p.first->getName() == "doubleprecision" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0012
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=numeric and columnType=Numeric then the
 * output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0012() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("numeric", Numeric(10, 5));
		assert(p.first == table.at("numeric") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Numeric::internalID && "type mismatch");
		assert(p.first->getSqlType()->typeInfo().numericPrecision == 10 && "precision mismatch");
		assert(p.first->getSqlType()->typeInfo().numericScale == 5 && "scale mismatch");
		assert(p.first->getName() == "numeric" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0013
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=date and columnType=Date then  the output
 * must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0013() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("date", Date());
		assert(p.first == table.at("date") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Date::internalID && "type mismatch");
		assert(p.first->getName() == "date" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0014
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=time and columnType=Time then  the output
 * must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column, the
 * second field is true.
 */
void test_C_0014() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("time", Time());
		assert(p.first == table.at("time") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Time::internalID && "type mismatch");
		assert(p.first->getName() == "time" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0015
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=timestamp and columnType=Timestamp then
 * the output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column,
 * the second field is true.
 */
void test_C_0015() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("timestamp", Timestamp());
		assert(p.first == table.at("timestamp") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Timestamp::internalID && "type mismatch");
		assert(p.first->getName() == "timestamp" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0016
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=varchar; columnType=Varchar; isKey=true then
 * the output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column,
 * the second field is true.
 */
void test_C_0016() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("varchar", Varchar(47), true);
		assert(p.first == table.at("varchar") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Varchar::internalID && "type mismatch");
		assert(p.first->getSqlType()->typeInfo().vcharLength == 47 && "type mismatch");
		assert(p.first->getName() == "varchar" && "name mismatch");
		assert(p.first->getIsKey() == true && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0017
 * Tests the Table::addColumn() function. The test checks that the type of the new Column object is set respecting
 * the value of the columnType paramether. If the input is columnName=character and columnType=Character then
 * the output must be The first field of the std::pair<Table::Iterator, bool> is an Iterator placend on the column,
 * the second field is true.
 */
void test_C_0017() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("character", Character(53));
		assert(p.first == table.at("character") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Character::internalID && "type mismatch");
		assert(p.first->getSqlType()->typeInfo().vcharLength == 53 && "type mismatch");
		assert(p.first->getName() == "character" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0018
 * Tests the Table::addColumn() function. The test checks that if the name for the new Column object is the same
 * name of a Column object that already belongs to the Table object, no column object is added.  If the input is
 * columnName=character and columnType=Boolean then  the output must be The first field of the
 * std::pair<Table::Iterator, bool> is an Iterator placend on the column, the second field is false.
 */
void test_C_0018() {
	try {
		Table table("table");
		std::pair<Table::Iterator, bool> p = table.addColumn("character", Character(53));
		assert(p.first == table.at("character") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Character::internalID && "type mismatch");
		assert(p.first->getSqlType()->typeInfo().vcharLength == 53 && "type mismatch");
		assert(p.first->getName() == "character" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == true && "The second field of the std::pair<> returned by the function must be \"true\"");

		p = table.addColumn("character", Boolean());
		assert(p.first == table.at("character") && "The iterator returned by the function must be placed on the right Column object");
		assert(p.first->getSqlType()->typeInfo().internalID == Character::internalID && "type mismatch");
		assert(p.first->getSqlType()->typeInfo().vcharLength == 53 && "type mismatch");
		assert(p.first->getName() == "character" && "name mismatch");
		assert(p.first->getIsKey() == false && "key mismatch");
		assert(p.second == false && "The second field of the std::pair<> returned by the function must be \"false\"");
	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0019
 * Tests the Table::begin(), Table::end(), Table::Iterator::operator++(int), Table::Iterator::operator==(),
 * Table::Iterator::operator!=() functions. Loops between the Column objects added to a Table object, verifying
 * that the insertion order is respected and that the  name, type and "isKey" attribute match.
 */
void test_C_0019() {

	typedef struct  {
		std::string name;
		int internalID;
		bool isKey;
	} columnsInfo;

	columnsInfo vector[] = {
			{"Boolean", Boolean::internalID, false},
			{"Date", Date::internalID, false},
			{"Time", Time::internalID, false},
			{"Timestamp", Timestamp::internalID, false},
			{"Small", Smallint::internalID, false},
			{"Int", Integer::internalID, false},
			{"Bigint", Bigint::internalID, false},
			{"Real", Real::internalID, false},
			{"Double", DoublePrecision::internalID, false},
			{"Numeric", Numeric::internalID, false},
			{"Char", Character::internalID, false},
			{"VChar", Varchar::internalID, true}
	};

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table::Iterator it = table.begin(), end = table.end();
		int i = 0;
		for (; it != end; it++, i++) {
			cout 	<< it->getName() <<"\t" << it->getSqlType()->typeInfo().internalID <<"\t" << it->getIsKey() << endl;
			assert(it->getName() == vector[i].name && "name mismatch");
			assert(it->getSqlType()->typeInfo().internalID == vector[i].internalID && "type mismatch");
			assert(it->getIsKey() == vector[i].isKey && "key mismatch");
		}

	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0020
 * Tests the Table::at() function. Verify that if no Column object exists with the specified name, the function
 * returns the iterator end(), which points to the "past-the-end" element. If the input is columnName=column
 * then  the output must be Table::end()
 */
void test_C_0020() {
	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		assert(table.at("column") == table.end() && "the Iterato must points the \"past-the-end\" element");

	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}	
}

/**
 * @test Test C0021
 * Tests the Table::begin(), Table::end(), Table::Iterator::operator++(int), Table::Iterator::operator==(),
 * Table::Iterator::operator!=() functions. Loops between the Column objects added to a Table object, verifying
 * that the insertion order is respected and that the  name, type and "isKey" attribute match.
 */
void test_C_0021() {

	typedef struct  {
		std::string name;
		int internalID;
		bool isKey;
	} columnsInfo;

	columnsInfo vector[] = {
			{"Boolean", Boolean::internalID, false},
			{"Date", Date::internalID, false},
			{"Time", Time::internalID, false},
			{"Timestamp", Timestamp::internalID, false},
			{"Small", Smallint::internalID, false},
			{"Int", Integer::internalID, false},
			{"Bigint", Bigint::internalID, false},
			{"Real", Real::internalID, false},
			{"Double", DoublePrecision::internalID, false},
			{"Numeric", Numeric::internalID, false},
			{"Char", Character::internalID, false},
			{"VChar", Varchar::internalID, true}
	};

	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		Table::ConstIterator it = table.cbegin(), end = table.cend();
		int i = 0;
		for (; it != end; it++, i++) {
			cout 	<< it->getName() <<"\t" << it->getSqlType()->typeInfo().internalID <<"\t" << it->getIsKey() << endl;
			assert(it->getName() == vector[i].name && "name mismatch");
			assert(it->getSqlType()->typeInfo().internalID == vector[i].internalID && "type mismatch");
			assert(it->getIsKey() == vector[i].isKey && "key mismatch");
		}

	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}
}

/**
 * @test Test C0022
 * Tests the Table::cat() function. Verify that if no Column object exists with the specified name, the function
 * returns the iterator cend(), which points to the "past-the-end" element. If the input is columnName=column
 * then  the output must be Table::cend()
 */
void test_C_0022() {
	try {
		Table table("table");
		table.addColumn("Boolean", Boolean());
		table.addColumn("Date", Date());
		table.addColumn("Time", Time());
		table.addColumn("Timestamp", Timestamp());
		table.addColumn("Small", Smallint());
		table.addColumn("Int", Integer());
		table.addColumn("Bigint", Bigint());
		table.addColumn("Real", Real());
		table.addColumn("Double", DoublePrecision());
		table.addColumn("Numeric", Numeric(10,5));
		table.addColumn("Char", Character(100));
		table.addColumn("VChar", Varchar(100), true);

		assert(table.cat("column") == table.cend() && "the Iterato must points the \"past-the-end\" element");

	} catch (BasicException& e) {
		cout << e.getFunction() << ":\t" << e.what() << endl;
		assert(false && "No exception must be thrown");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception");
	}	
}


int main() {
	signal(SIGABRT, abortHandler);
	test_function function[23] = {
		test_C_0000, test_C_0001, test_C_0002, test_C_0003, test_C_0004, test_C_0005, test_C_0006, test_C_0007,
		test_C_0008, test_C_0009, test_C_0010, test_C_0011, test_C_0012, test_C_0013, test_C_0014, test_C_0015,
		test_C_0016, test_C_0017, test_C_0018, test_C_0019, test_C_0020, test_C_0021, test_C_0022
	};

	for (int i = 0; i < 23; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
