/**
 * @file test_Time.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0048
 * Tests the Time::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown.
  */
void test_A_0048() {
	Time object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0048: the function throws an exception with strings of the SqlType::nullValues collection.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0048: uncaught exception.");
	}

}
/**
 * @test Test A0049
 * Tests the Time::validate() function.
 * The function is stimulated with a string with no istance of the separator character (‘:’). An
 * InvalidArgument exception must be thrown.
 */
void test_A_0049() {
	Time object;
	try {
		object.validate("123456");
		assert(false && "Test A0049: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0049: uncaught exception.");
	}
}
/**
 * @test Test A0050
 * Tests the Time::validate() function.
 * The function is stimulated with a string with more than 2 istance of the separator character (‘:’). An
 * InvalidArgument exception must be thrown.
 */
void test_A_0050() {
    Time object;
	try {
		object.validate("1:2:3:4");
		assert(false && "Test A0050: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0050: uncaught exception.");
	}
}
/**
 * @test Test A0051
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * isn’t a numeric string. An InvalidArgument exception must be thrown.
 */
void test_A_0051() {
    Time object;
	try {
		object.validate("abc:12");
		assert(false && "Test A0051: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0051: uncaught exception.");
	}
}
/**
 * @test Test A0052
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a negative number (in a string). An InvalidArgument exception must be thrown.
 */
void test_A_0052() {
    Time object;
	try {
		object.validate("-12:34");
		assert(false && "Test A0052: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0052: uncaught exception.");
	}
}
/**
 * @test Test A0053
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a positive number greather than 23. An InvalidTime exception must be thrown.
 */
void test_A_0053() {
    Time object;
	try {
		object.validate("25:55");
		assert(false && "Test A0053: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0053: uncaught exception.");
	}
}
/**
 * @test Test A0054
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a positive number less or equal to 23. The “minute” isn’t a numeric string. . An InvalidArgument
 * exception must be thrown.
 */
void test_A_0054() {
    Time object;
	try {
		object.validate("20:ab");
		assert(false && "Test A0054: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0054: uncaught exception.");
	}
}
/**
 * @test Test A0055
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a positive number less or equal to 23. The “minute”is a negative number. An InvalidTime exception
 * must be thrown.
 */
void test_A_0055() {
    Time object;
	try {
		object.validate("20:-2");
		assert(false && "Test A0055: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0055: uncaught exception.");
	}
}
/**
 * @test Test A0056
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a positive number less or equal to 23. The “minute”is a positive number greather than 59. An
 * InvalidTime exception must be thrown.
 */
void test_A_0056() {
    Time object;
	try {
		object.validate("19:60");
		assert(false && "Test A0056: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0056: uncaught exception.");
	}
}
/**
 * @test Test A0057
 * Tests the Time::validate() function.
 * The function is stimulated with a string with only one istance of the separator character (‘:’). The “hour”
 * is a positive number less or equal to 23. The “minute”is a positive number less or equal to 59. No exception
 * must be thrown.
 */
void test_A_0057() {
    Time object;
	try {
		object.validate("23:59");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0057: the function must not throws any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0057: uncaught exception.");
	}
}
/**
 * @test Test A0058
 * Tests the Time::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator character (‘:’). The
 * “second” isn’t a numeric string. An InvalidArgument exception must be thrown.
 */
void test_A_0058() {
    Time object;
	try {
		object.validate("23:59:ab");
		assert(false && "Test A0058: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0058: uncaught exception.");
	}
}
/**
 * @test Test A0059
 * Tests the Time::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator character (‘:’) . The
 * “second”is a negative numeric string. An InvalidTime exception must be thrown.
 */
void test_A_0059() {
    Time object;
	try {
		object.validate("23:59:-5");
		assert(false && "Test A0059: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0059: uncaught exception.");
	}
}
/**
 * @test Test A0060
 * Tests the Time::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator character (‘:’) . The
 * “second”is a positive numeber greater than 59. An InvalidTime exception must be thrown.
 */
void test_A_0060() {
    Time object;
	try {
		object.validate("23:59:75");
		assert(false && "Test A0060: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0060: uncaught exception.");
	}
}
/**
 * @test Test A0061
 * Tests the Time::validate() function.
 * The function is stimulated with a string with exactly 2 istance of the separator character (‘:’) . The
 * “second”is a positive numeber less than 59. No exception must be thrown.
 */
void test_A_0061() {
    Time object;
	try {
		object.validate("23:59:59");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0061: the function must not throws any exception.");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Test A0061: uncaught exception.");
	}
}
/**
 * @test Test A0062
 * Tests the Time::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned.
 *
 */
void test_A_0062() {
    Time object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert (object.prepare(*it) == SqlType::null && "Test A0062: the function doen't return SqlType::null");
}
/**
 * @test Test A0063
 * Tests the Time::prepare() function.
 * The function is stimulated with a string having exactly 1 istance of the separator character (‘:’). It is
 * verified that the string returned has the “second” field set as zero. The whole string must be enclosed in single quote.
 */
void test_A_0063() {
    Time object;
	assert(object.prepare("23:59") == "'23:59:00'" && "Test A0063: the output of the function comes in a wrong format");

}
/**
 * @test Test A0064
 * Tests the Time::prepare() function.
 * The function is stimulated with a string having exactly 2 istance of the separator character (‘:’). The
 * whole string must be enclosed in single quote.
 *
 */
void test_A_0064() {
    Time object;
	assert(object.prepare("23:59:12") == "'23:59:12'" && "Test A0064: the output of the function comes in a wrong format");
}
/**
 * @test Test A0065
 * Tests the Time::typeInfo() function.
 */
void test_A_0065() {
    Time object;
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Time::internalID && "Test A0025: the returned internalID doesnt' match Time::internalID");
	assert(info.typeName == Time::typeName && "Test A0025: the returned typeName doesnt' match Time::typeName");
	assert(info.udtName == Time::udtName && "Test A0025: the returned udtName doesnt' match Time::udtName");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[18] = {
		test_A_0048, test_A_0049, test_A_0050, test_A_0051, test_A_0052, test_A_0053, test_A_0054, test_A_0055,
		test_A_0056, test_A_0057, test_A_0058, test_A_0059, test_A_0060, test_A_0061, test_A_0062, test_A_0063,
		test_A_0064, test_A_0065
	};

	for (int i = 0; i < 18; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
