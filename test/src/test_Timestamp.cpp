/**
 * @file test_Timestamp.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0091
 * Tests the Timestamp::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown.
 *
 */
void test_A_0091() {
    Timestamp object;
	try {
		for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
			object.validate(*it);
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0091: the function throws an exception with strings of the SqlType::nullValues collection.");
	}
}
/**
 * @test Test A0092
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with no istance of the Date separator characters (‘-’ or ‘/’) (Ex. "06012017”). The function
 * must throw an InvalidArgoument exception
 *
 */
void test_A_0092() {
   Timestamp object;
	try {
		object.validate("06012017");
		assert(false && "Test A0092: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0093
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with more than 2 istance of the Date separator characters (‘-’ or ‘/’) (Ex. "06/01/20/17”).
 * The function must throw an InvalidArgoument exception
 *
 */
void test_A_0093() {
   Timestamp object;
	try {
		object.validate("06/01/20/17");
		assert(false && "Test A0093: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0094
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first field isn’t
 * numeric; (Ex. "aa/01/2017”). The function must throw an InvalidArgoument exception
 *
 */
void test_A_0094() {
   Timestamp object;
	try {
		object.validate("aa/01/2017");
		assert(false && "Test A0094: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0095
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first field is a
 * negative number (Ex. "-5/01/2017”). The function must throw an InvalidArgoument exception
 *
 */
void test_A_0095() {
   Timestamp object;
	try {
		object.validate("-5/01/2017");
		assert(false && "Test A0095: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0096
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character
 * (a whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first field is
 * positive number, but the second isn’ a numeric string; (Ex. "06/aa/2017”). The function must throw an
 * InvalidArgoument exception
 *
 */
void test_A_0096() {
   Timestamp object;
	try {
		object.validate("06/aa/2017");
		assert(false && "Test A0096: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0097
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first field is
 * positive number, but the second is a negative number; (Ex. "06/-1/2017”). The function must throw an
 * InvalidArgoument exception
 *
 */
void test_A_0097() {
   Timestamp object;
	try {
		object.validate("06/-1/2017");
		assert(false && "Test A0097: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0098
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first and the second
 * fields is positive number, but the third isn’t a  (Ex. "06/01/abcd”). The function must throw an
 * InvalidArgoument exception
 *
 */
void test_A_0098() {
   Timestamp object;
	try {
		object.validate("06/01/abcd");
		assert(false && "Test A0098: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0099
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). The first and the second
 * fields is positive number, but the third is a negative number; (Ex. "06/01/-2017”). The function must throw
 * an InvalidArgoument exception
 *
 */
void test_A_0099() {
   Timestamp object;
	try {
		object.validate("06/01/-2017");
		assert(false && "Test A0099: The function must throw an InvalidArgument exception");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0100
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator character (‘-’ or ‘/’). Each fiels is is a
 * positive number. The length of the first field is 4, so the format is yyyy/MM/dd; (Ex. "2017/01/06”). The
 * function must not throw exceptions
 *
 */
void test_A_0100() {
   Timestamp object;
	try {
		object.validate("2017/01/06");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0100: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0101
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each fiels is is a
 * positive number. The length of the first field is 2, the length of the third field is 4, so the format is
 * dd/MM/yyyy; (Ex. "06/01/2017”). "The function must not throw exceptions
 *
 */
void test_A_0101() {
   Timestamp object;
	try {
		object.validate("06/01/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0101: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0102
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each fiels is is a
 * positive number. The length of the first field is 2, the length of the third field is 2, so the format
 * isn’t dd/MM/yyyy; (Ex. "06/01/17”). The function must throw an AmbiguousDate exception
 *
 */
void test_A_0102() {
   Timestamp object;
	try {
		object.validate("06/01/17");
		assert(false && "Test A0102: The function must throw an AmbiguousDate exception");
	} catch (AmbiguousDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0103
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (1,3,5,7,8,10,12), the day is 0; (Ex. "0/01/2017”). The function must throw an
 * InvalidDate exception
 *
 */
void test_A_0103() {
   Timestamp object;
	try {
		object.validate("0/01/2017");
		assert(false && "Test A0103: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0104
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (1,3,5,7,8,10,12), the day is greater than 31; (Ex. "32/01/2017”). The function must throw an
 * InvalidDate exception
 *
 */
void test_A_0104() {
   Timestamp object;
	try {
		object.validate("32/01/2017");
		assert(false && "Test A0104: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0105
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (1,3,5,7,8,10,12), the day is less or equal to 31; (Ex. "31/01/2017”). The function must not
 * throw any exception
 *
 */
void test_A_0105() {
   Timestamp object;
	try {
		object.validate("31/01/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0105: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0106
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (4,6,9,11), the day is 0; (Ex. "0/04/2017”). The function must throw an InvalidDate
 * exception
 *
 */
void test_A_0106() {
   Timestamp object;
	try {
		object.validate("0/04/2017");
		assert(false && "Test A0106: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0107
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (4,6,9,11), the day is greater than 30; (Ex. "31/04/2017”). The function must throw an
 * InvalidDate exception
 *
 */
void test_A_0107() {
   Timestamp object;
	try {
		object.validate("31/04/2017");
		assert(false && "Test A0107: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0108
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is (4,6,9,11), the day is less or equal to 30; (Ex. "30/04/2017”). The function must not throw
 * any exception
 *
 */
void test_A_0108() {
   Timestamp object;
	try {
		object.validate("30/04/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0108: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0109
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is 2, the day is 0; (Ex. "0/2/2017”). The function must throw an InvalidDate exception
 *
 */
void test_A_0109() {
   Timestamp object;
	try {
		object.validate("0/2/2017");
		assert(false && "Test A0109: The function must throw an InvalidDate exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0110
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is 2, the day is between 1 and 28; (Ex. "12/02/2017”). The function must not throw any exception
 *
 */
void test_A_0110() {
   Timestamp object;
	try {
		object.validate("12/02/2017");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0110: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0111
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is 2, the day is between 29, the year is a leap year; (Ex. "29/2/2016”). The function must not
 * throw any exception
 *
 */
void test_A_0111() {
   Timestamp object;
	try {
		object.validate("29/2/2016");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0111: The function must not throw any exception");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0112
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having no instance of the Timestamp separator character (a
 * whitespace), with exactly 2 istance of the Date separator characters (‘-’ or ‘/’). Each field is positive.
 * The month is 2, the day is between 29, the year is not a leap year; (Ex. "29/2/2017”). The function must
 * throw an InvalidDate exception
 *
 */
void test_A_0112() {
   Timestamp object;
	try {
		object.validate("29/2/2017");
		assert(false && "Test A0112: The function must throw an InvalidArgument exception");
	} catch (InvalidDate& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0113
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), no istance of the Time separator character (‘:’)
 *
 */
void test_A_0113() {
   Timestamp object;
	try {
		object.validate("07/01/2018 123456");
		assert(false && "Test A0113: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0114
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), more than 2 istance of the Time separator character
 * (‘:’)
 *
 */
void test_A_0114() {
   Timestamp object;
	try {
		object.validate("07/01/2018 1:2:3:4");
		assert(false && "Test A0114: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0115
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” isn’t a numeric string
 *
 */
void test_A_0115() {
   Timestamp object;
	try {
		object.validate("07/01/2018 abc:12");
		assert(false && "Test A0115: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0116
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a negative number (in a string)
 *
 */
void test_A_0116() {
   Timestamp object;
	try {
		object.validate("07/01/2018 -12:34");
		assert(false && "Test A0116: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0117
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a positive number greather than 23
 *
 */
void test_A_0117() {
   Timestamp object;
	try {
		object.validate("07/01/2018 25:55");
		assert(false && "Test A0117: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0118
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a positive number less or equal to 23. The “minute” isn’t a numeric string.
 *
 */
void test_A_0118() {
   Timestamp object;
	try {
		object.validate("07/01/2018 20:ab");
		assert(false && "Test A0119: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0119
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a positive number less or equal to 23. The “minute”is a negative number.
 *
 */
void test_A_0119() {
   Timestamp object;
	try {
		object.validate("07/01/2018 20:-2");
		assert(false && "Test A0119: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0120
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a positive number less or equal to 23. The “minute”is a positive number greather than
 * 59.
 *
 */
void test_A_0120() {
   Timestamp object;
	try {
		object.validate("07/01/2018 19:60");
		assert(false && "Test A0120: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0121
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), only one istance of the Time separator character
 * (‘:’). The “hour” is a positive number less or equal to 23. The “minute”is a positive number less or equal
 * to 59.
 *
 */
void test_A_0121() {
   Timestamp object;
	try {
		object.validate("07/01/2018 23:59");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0121: the function must not throws any exception.");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0122
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace),
 * a valid "date field" (in respect with the Date datatype), exactly 2 istance of the Time separator character
 * (‘:’). The “second” isn’t a numeric string
 *
 */
void test_A_0122() {
   Timestamp object;
	try {
		object.validate("07/01/2018 23:59:ab");
		assert(false && "Test A0122: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0123
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace),
 * a valid "date field" (in respect with the Date datatype), exactly 2 istance of the Time separator character
 * (‘:’) . The “second”is a negative numeric string
 *
 */
void test_A_0123() {
   Timestamp object;
	try {
		object.validate("07/01/2018 23:59:-5");
		assert(false && "Test A0123: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0124
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), exactly 2 istance of the Time separator character
 * (‘:’) . The “second”is a positive numeber greater than 59.
 *
 */
void test_A_0124() {
   Timestamp object;
	try {
		object.validate("07/01/2018 23:59:75");
		assert(false && "Test A0124: the function must throws an InvalidTime exception.");
	} catch (InvalidTime& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0125
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string having exactly one Timestamp separator character (a whitespace), a
 * valid "date field" (in respect with the Date datatype), exactly 2 istance of the Time separator character
 * (‘:’) . The “second”is a positive numeber less than 59.
 *
 */
void test_A_0125() {
   Timestamp object;
	try {
		object.validate("07/01/2018 23:59:59");
	} catch (BasicException& e) {
		cout << e.what() << endl;
		assert(false && "Test A0125: the function must not throws any exception.");
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0126
 * Tests the Timestamp::validate() function.
 * The function is stimulated with a string with more than one Timestamp separator character (a whitespace),
 * checking that an InvalidArgoument exception is thrown.
 *
 */
void test_A_0126() {
   Timestamp object;
	try {
		object.validate("a random string");
		assert(false && "Test A0126: the function must throws an InvalidArgument exception.");
	} catch (InvalidArgument& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << "UNCAUGHT EXCEPTION! " << e.what() << endl;
		assert (false && "UNCAUGHT EXCEPTION! ");
	}
}
/**
 * @test Test A0127
 * Tests the Timestamp::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned.
 *
 */
void test_A_0127() {
   Timestamp object;
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert(object.prepare(*it) == SqlType::null && "Test A0127: the function doesn't return SqlType::null");
}
/**
 * @test Test A0128
 * Tests the Timestamp::prepare() function.
 * The function is stimulated using a string containing no Timestamp separator character (a whitespace),
 * checking that the value has been prepared as expected.
 *
 */
void test_A_0128() {
   Timestamp object;
	assert(object.prepare("07/01/2018") == "'2018-01-07'" && "Test a0128: a string containing no Timestamp separator character isn't prepared correctly.");
}
/**
 * @test Test A0129
 * Tests the Timestamp::prepare() function.
 * The function is stimulated using a string containing exactly one Timestamp separator character (a whitespace),
 * checking that the value has been prepared as expected.
 *
 */
void test_A_0129() {
   Timestamp object;
	assert(object.prepare("07/01/2018 22:48") == "'2018-01-07 22:48:00'" && "Test a0128: a string containing exactly one Timestamp separator character isn't prepared correctly.");
	assert(object.prepare("07/01/2018 22:48:32") == "'2018-01-07 22:48:32'" && "Test a0128: a string containing exactly one Timestamp separator character isn't prepared correctly.");
}
/**
 * @test Test A0130
 * Tests the Timestamp::typeInfo() function.
 * Verifies that the function returns information about the Timestamp type correctly.
 *
 */
void test_A_0130() {
   Timestamp object;
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Timestamp::internalID && "Test A0130: the returned internalID doesnt' match Timestamp::internalID");
	assert(info.typeName == Timestamp::typeName && "Test A0130: the returned typeName doesnt' match Timestamp::typeName");
	assert(info.udtName == Timestamp::udtName && "Test A0130: the returned udtName doesnt' match Timestamp::udtName");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[40] = {
		test_A_0091, test_A_0092, test_A_0093, test_A_0094, test_A_0095, test_A_0096, test_A_0097, test_A_0098,
		test_A_0099, test_A_0100, test_A_0101, test_A_0102, test_A_0103, test_A_0104, test_A_0105, test_A_0106,
		test_A_0107, test_A_0108, test_A_0109, test_A_0110, test_A_0111, test_A_0112, test_A_0113, test_A_0114,
		test_A_0115, test_A_0116, test_A_0117, test_A_0118, test_A_0119, test_A_0120, test_A_0121, test_A_0122,
		test_A_0123, test_A_0124, test_A_0125, test_A_0126, test_A_0127, test_A_0128, test_A_0129, test_A_0130
	};

	for (int i = 0; i < 40; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
