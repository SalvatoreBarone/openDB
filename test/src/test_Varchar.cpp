/**
 * @file test_Varchar
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2013-2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of openDB.
 *
 * openDB is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * openDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "openDBcore.hpp"
#include "test.hpp"
using namespace std;
using namespace openDB;

typedef void (*test_function)(void);

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

/**
 * @test Test A0138
 * Tests the Varchar::validate() function.
 * The function is stimulated with the strings of the SqlType::nullValues collection, verifying that no
 * exception is thrown. If the input is SqlType::nullValues then the function must not throw exceptions.
 */
void test_A_0138() {
	Varchar object(10);
	try {
	 for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		 object.validate(*it);
	} catch (BasicException& e) {
	 cout << e.what() << endl;
	 assert(false && "The function must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0139
 * Tests the Varchar::validate() function.
 * The function is stimulated using a string  whose length exceeds that allowed (10 characters) If the input
 * is “a very very long string” then the function must trow a ValueTooLong exception.
 */
void test_A_0139() {
	Varchar object(10);
	try {
		object.validate("a very very long string");
		assert(false && "Test A0132: the funtion must throw a ValueTooLong exception");
	} catch (ValueTooLong& e) {
		cout << e.what() << endl;
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0140
 * Tests the Varchar::validate() function.
 * The function is stimulated using a string whose length does not exceed that allowed (10 characters) If the
 * input is “a string” then the function must not throw any exception.
 */
void test_A_0140() {
	Varchar object(10);
	try {
		object.validate("a string");

	} catch (ValueTooLong& e) {
		cout << e.what() << endl;
		assert(false && "Test A0140: the funtion must not throw any exception");
	} catch (exception& e) {
		cout << e.what() << endl;
		assert(false && "Uncaught exception!");
	}
}
/**
 * @test Test A0141
 * Tests the Varchar::prepare() function.
 * The function is stimulated with the strings belonging to the SqlType::nullValues collection, verifying that
 * SqlType::null is returned. If the input is SqlType::nullValues then the output must be SqlType::null
 */
void test_A_0141() {
	Varchar object(10);
	for (list<string>::const_iterator it = SqlType::nullValues.begin(); it != SqlType::nullValues. end(); it++)
		assert(object.prepare(*it) == SqlType::null && "Test A0141: valuea in the SqlType::nullValues aren't prepared as SqlType::null");
}
/**
 * @test Test A0142
 * Tests the Varchar::prepare() function.
 * The function is stimulated a string without single quote characters. If the input is “a string” then  the
 * output must be “’a string’”
 */
void test_A_0142() {
	Varchar object(10);
	assert(object.prepare("a string") == "'a string'" && "Test A0142: values without single quote aren't correctly prepared");
}
/**
 * @test Test A0143
 * Tests the Varchar::prepare() function.
 * The function is stimulated with a string containing some single quote characters. If the input is “good
 * mornin’” then  the output must be “good mornin’’”
 */
void test_A_0143() {
	Varchar object(10);
	assert(object.prepare("good mornin'") == "'good mornin'''" && "Test A0143: values with single quote aren't correctly prepared");
}
/**
 * @test Test A0144
 * Tests the Varchar::typeInfo() function.
 * Verifies that the function returns information about the Character type correctly. the output must be
 * (Varchar::internalID, Varchar::typeName,Varchar::udtName)
 */
void test_A_0144() {
	Varchar object(10);
	SqlType::TypeInfo info = object.typeInfo();
	assert(info.internalID == Varchar::internalID && "Test A0144: the returned internalID doesnt' match Varchar::internalID");
	assert(info.typeName == Varchar::typeName && "Test A0144: the returned typeName doesnt' match Varchar::typeName");
	assert(info.udtName == Varchar::udtName && "Test A0144: the returned udtName doesnt' match Varchar::udtName");
}

int main() {
	signal(SIGABRT, abortHandler);
	test_function function[7] = {
		test_A_0138, test_A_0139, test_A_0140, test_A_0141, test_A_0142, test_A_0143, test_A_0144
	};

	for (int i = 0; i < 7; i++)
		function[i]();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
